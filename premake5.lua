workspace "Quadrion"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release"
	}

	startproject "qdriver"

	local LIB_DIR = "build\\"	
	local DEP_DIR = "dependencies\\"

	IncludeDir = {}
	IncludeDir["Assimp"] = DEP_DIR .. "Assimp\\include"
	IncludeDir["GL"] = DEP_DIR .. "GL"
	IncludeDir["FreeImage"] = DEP_DIR .. "FreeImage\\include"
	IncludeDir["spdlog"] = DEP_DIR .. "spdlog\\include"
	IncludeDir["qtl"] = DEP_DIR .. "qtl\\include"
	IncludeDir["tbb"] = DEP_DIR .. "qtl\\include"
	IncludeDir["imgui"] = DEP_DIR .. "imgui"
	IncludeDir["sml"] = DEP_DIR .. "sml\\include"

	LibDir = {}
	LibDir["Build"] = "build\\"
	LibDir["Assimp"] = DEP_DIR .. "Assimp\\lib64"
	LibDir["FreeImage"] = DEP_DIR .. "FreeImage\\bin"
    LibDir["tbb"] = DEP_DIR .. "qtl\\lib\\tbb_x64"
	LibDir["sml"] = DEP_DIR .. "sml"

	BinDir = {}

    local sln = solution()

project "qdriver"
	location "projects/qdriver"

	uuid "90574aa7-4a4d-4147-b4e8-2dba701a2068"
	kind "WindowedApp"
	language "C++"

	targetdir "build"
	objdir "bin-int/%{prj.name}"

	flags
	{
		"MultiProcessorCompile"
	}

	dependson
	{
		"qengine"
	}

	includedirs
	{
		"projects/qengine/include",
		"projects/qdriver/include",
		"%{IncludeDir.GL}",
		"%{IncludeDir.Assimp}",
		"%{IncludeDir.spdlog}",
		"%{IncludeDir.qtl}",
		"%{IncludeDir.imgui}",
		"%{IncludeDir.sml}"
	}

	libdirs
	{
		"%{LibDir.Build}",
		"%{LibDir.Assimp}",
		"%{LibDir.qtl}",
        "%{LibDir.tbb}",
		"%{LibDir.sml}"
	}

	files 
	{ 
		"projects/" .. project().name .. "/**.h", 
		"projects/" .. project().name .. "/**.c", 
		"projects/" .. project().name .. "/**.hpp", 
		"projects/" .. project().name .. "/**.cpp",
		"dependencies/imgui/**.h",
		"dependencies/imgui/**.cpp" 
	}

    filter "system:windows"
        postbuildcommands "(robocopy \"$(SolutionDir)%{LibDir.tbb}\" \"$(SolutionDir)build\" /E) ^& IF %ERRORLEVEL% LEQ 1 exit 0"
		cppdialect "C++17"
		staticruntime "Off"
		systemversion "latest"
		toolset "v142"
		debugdir "%{sln.location}"

		entrypoint "WinMainCRTStartup"

		disablewarnings
		{
			"4005"
		}

		linkoptions
    	{
    		"/ignore:4006",
    		"/ignore:4221",
    		"/ignore:4099",
    		"/ignore:4075"
    	}
    	
    	buildoptions
    	{
    	  "/std:c++17",
    	  "/permissive-",
	  	  "/W3"
    	}

    	links 
		{ 
			"Opengl32", 
			"qengine"
        }

        flags 
        {
            "DisableFastLink",
        }

        defines 
		{ 
			"_WINDOWS", 
			"WIN32", 
			"NOMINMAX", 
			"_CRT_SECURE_NO_WARNINGS",
			"FREEIMAGE_LIB",
			"_SILENCE_CXX17_RESULT_OF_DEPRECATION_WARNING"
		}


	filter "system:linux"
		
		staticruntime "Off"
		toolset "clang"
		postbuildcommands "chmod -R 777 ../../build/"
		debugdir "..\\"

		disablewarnings
		{
			"return-type-c-linkage"
		}

		libdirs
		{
			"/usr/lib",
			"/usr/local/lib",
--			"./build/tbb_x64"
		}

		defines
		{
			"FREEIMAGE_LIB",
			"_LINUX"
		}

		buildoptions 
		{
			"-std=c++17",
			"-mavx",
			"-masm=intel"
		}
		linkoptions
		{
			"-Wl,-rpath=./build",
			"-Wl,-rpath=./",
			"-Wl,-rpath=./tbb_x64/"
--			"-Ofast"
		}

		links 
		{ 
			"Xxf86vm", 
			"GL", 
			"X11",
			"assimp",
			"freeimage",
			"atomic",
			"qengine",
			"pthread"
		}

		includedirs
		{
			"/usr/include/x86_64-linux-gnu"
		}


	filter "configurations:Debug"
		buildoptions
		{
--			"-O0"
		}

		defines
		{
			"_DEBUG"
		}

		runtime "debug"
		symbols "On"

	filter "configurations:Release"
		buildoptions
		{
--			"-Ofast"
		}

		defines
		{
			"NDEBUG"
		}

		runtime "release"
--		optimize "On"

	filter { "configurations:Debug"}
        links
        {
            "tbb_debug",
            "tbbmalloc_debug",
            "tbbmalloc_proxy_debug"
        }

    filter { "configurations:Release"}
        links
        {
            "tbb",
            "tbbmalloc",
            "tbbmalloc_proxy"
        }

project "qengine"
	location "projects/qengine"

	uuid "cbbc1227-faa0-481f-9d37-02fcbade6ce1"
	kind "SharedLib"
	language "C++"

	targetdir "build"
	objdir "bin-int/%{prj.name}"

	flags
	{
		"MultiProcessorCompile"
	}

	defines
	{
		"QENGINE_EXPORTS", 
		"_ENABLE_ATOMIC_ALIGNMENT_FIX",
		"FREEIMAGE_LIB",
		"_SILENCE_CXX17_RESULT_OF_DEPRECATION_WARNING"
	}

	includedirs
	{
		"projects/qengine/include",
		"%{IncludeDir.FreeImage}",
		"%{IncludeDir.GL}",
		"%{IncludeDir.Assimp}",
		"%{IncludeDir.spdlog}",
		"%{IncludeDir.qtl}",
		"%{IncludeDir.tbb}",
		"%{IncludeDir.sml}"
	}

	libdirs
	{
		"%{LibDir.Build}",
		"%{LibDir.FreeImage}",
		"%{LibDir.Assimp}",
		"%{LibDir.qtl}",
		"%{LibDir.tbb}",
		"%{LibDir.sml}"
	}

	files 
	{ 
		"projects/" .. project().name .. "/**.h", 
		"projects/" .. project().name .. "/**.c", 
		"projects/" .. project().name .. "/**.hpp", 
		"projects/" .. project().name .. "/**.cpp"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "Off"
		systemversion "latest"
		toolset "v142"

		disablewarnings
		{
			"4005"
		}

		linkoptions
    	{
    		"/ignore:4006",
    		"/ignore:4221",
    		"/ignore:4099",
    		"/ignore:4075"
    	}

	flags 
        {
            "DisableFastLink",
        }
    	
    	buildoptions
    	{
    	  "/std:c++17",
    	  "/permissive-",
	  "/W3"
    	}

    	links 
	{ 
			"Opengl32"
        }

        defines 
		{ 
			"_WINDOWS", 
			"WIN32", 
			"NOMINMAX", 
			"_CRT_SECURE_NO_WARNINGS"
		}

		postbuildcommands "(robocopy \"$(SolutionDir)dependencies\\qtl\\bin\\tbb_x64\" \"$(SolutionDir)build\" & robocopy \"$(SolutionDir)dependencies\\qtl\\lib\" \"$(SolutionDir)build\" & \"$(SolutionDir)dependencies\\Freeimage\\bin\" \"$(SolutionDir)build\" & robocopy \"$(SolutionDir)dependencies\\Assimp\\bin64\" \"$(SolutionDir)build\"/E) ^& IF %ERRORLEVEL% LEQ 1 exit 0"

		filter {"system:windows", "configurations:Debug"}
			postbuildcommands "(robocopy \"$(SolutionDir)dependencies\\Freeimage\\bin\" \"$(SolutionDir)build\" & robocopy \"$(SolutionDir)dependencies\\Assimp\\bin64\" \"$(SolutionDir)build\" & IF %ERRORLEVEL% LEQ 1 exit 0"

			links
			{
				"FreeImage",
				"assimp",
				"irrXML",
				"zlibstaticd"
			}

		filter {"system:windows", "configurations:Release"}
			postbuildcommands { "(robocopy \"$(SolutionDir)dependencies\\Freeimage\\bin\" \"$(SolutionDir)build\" & robocopy \"$(SolutionDir)dependencies\\Assimp\\bin64\" \"$(SolutionDir)build\" & IF %ERRORLEVEL% LEQ 1 exit 0" }

			links
			{
				"FreeImagerel",
				"assimprel",
				"irrXMLrel",
				"zlibstatic"
			}

	filter "system:linux"
		buildoptions "-std=c++17"
		staticruntime "Off"
		toolset "clang"
		postbuildcommands "ln -sfn ../Media ../../build/Media"
		prebuildcommands "ln -sf libassimp.so.5.0.0 ../../dependencies/Assimp/lib64/libassimp.so.5"
		prebuildcommands "ln -sf libassimp.so.5 ../../dependencies/Assimp/lib64/libassimp.so" 

		disablewarnings
		{
			"return-type-c-linkage"
		}

		linkoptions
		{
			"-Wl,-rpath=./build",
			"-Wl,-rpath=./",
			"-Wl,-rpath=./tbb_x64/",
			"-Wl,-rpath-link=./build",
			"-v"
		}

		buildoptions
		{
--			"-fstandalone-debug",
			"-fvisibility=hidden",
			"-mavx"
		}

		libdirs
		{
--			"./build/tbb_x64"
		}

		includedirs 
		{
			"/usr/include/x86_64-linux-gnu"
		}

		links 
		{ 
			"assimp",
			"GL", 
			"freeimage",
		}

		filter {"system:linux", "configurations:Debug"}
			prebuildcommands "cp -r -u ../../dependencies/Assimp/lib64/. ../../build/ && cp -r -u ../../dependencies/FreeImage/bin/. ../../build/ && cp -r -u ../../dependencies/qtl/lib/. ../../build/"
--			prebuildcommands "cp -r -u ../../dependencies/qtl/bin/. ../../build/"


		filter {"system:linux", "configurations:Release"}
			prebuildcommands "cp -r -u ../../dependencies/Assimp/lib64/. ../../build/ && cp -r -u ../../dependencies/FreeImage/bin/. ../../build/ && cp -r -u ../../dependencies/qtl/lib/. ../../build/"


	filter "configurations:Debug"
		defines
		{
			"_DEBUG"
		}

		runtime "debug"
		symbols "On"

	filter "configurations:Release"
		defines
		{
			"NDEBUG"
		}

		runtime "release"
		optimize "On"

