QuadrionShader Fullscreen
{
	#version 410 core

	interface VertIn
	{
        vec2 position : 0;
        vec2 uv0 : 1;
        vec4 color0 : 2;
	};

	interface VertInNoBlend
	{
		vec3 position : 0;
		vec2 uv0 : 8;
	};

	interface FragIn
	{
		vec2 texCoord0;
        vec4 oColor0;
 	};

	interface FragOut
	{
        vec4 color : 0;
	};

    uniform sampler2D tex0;




	//////////////////////////////////////////////////////////////////////
	//
	//			SHADER FUNCS 
	//
	//////////////////////////////////////////////////////////////////////
	void vert(in VertIn, out FragIn)
	{
        float x = position.x / $SCREEN_WIDTH;
        float y = 1.0 - (position.y / $SCREEN_HEIGHT);
        vec2 oPos = vec2(x * 2.0 - 1.0, y * 2.0 - 1.0);

        texCoord0 = uv0;
        oColor0 = color0;

        gl_Position = vec4(oPos, 1.0, 1.0);
	}

	void vertNoBlend(in VertInNoBlend, out FragIn)
	{
//		float x = position.x / $SCREEN_WIDTH;
//        float y = 1.0 - (position.y / $SCREEN_HEIGHT);
//        vec2 oPos = vec2(x * 2.0 - 1.0, y * 2.0 - 1.0);

        texCoord0 = uv0;
        oColor0 = vec4(1.0);

        gl_Position = vec4(position.xy, 1.0, 1.0);	
	}

	void frag(in FragIn, out FragOut)
	{
        color =  oColor0 * texture(tex0, texCoord0);
	}

	void fragNoBlend(in FragIn, out FragOut)
	{
		color = vec4(texture(tex0, texCoord0).rgb, 1.0);
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	//				TECHNIQUES 
	//
	////////////////////////////////////////////////////////////////////////////////
	technique FullscreenTechnique
	{
		pass p0
		{
			DepthTest = False;
			DepthFunc = Lequal;
			DepthWrite = False;

			CullEnable = False;
			CullFace = Back;
			FrontFace = CCW;

            BlendEnabled = True;
            BlendFunc = SrcAlpha OneMinusSrcAlpha;
			
			VertexShader = vert;
			FragmentShader = frag;
		};
	};

	technique FullscreenNoBlendTechnique
	{
		pass p0
		{
			OutputBuffer = DefaultSurface;

			DepthTest = False;
			DepthFunc = Lequal;
			DepthWrite = False;

			CullEnable = False;
			CullFace = Back;
			FrontFace = CCW;
			
			VertexShader = vertNoBlend;
			FragmentShader = fragNoBlend;
		};
	};
}
