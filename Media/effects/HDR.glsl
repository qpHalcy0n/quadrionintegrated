QuadrionShader HDR
{
    #include CameraUniforms.glslh
    #include HDRUniforms.glslh
    #include RenderBuffers.glslh
    #version 410 core

    uniform sampler2D   s0;
    uniform sampler2D   s1;
    uniform sampler2D   s2;
    uniform vec2        g_staticSampleOffsets[16];
    uniform vec4        g_staticSampleWeights[16];
    uniform float       g_elapsedTime;

    const int		    MAX_SAMPLES   = 16;
    const vec3  	    LUM_CONV	  = vec3(0.2126, 0.7152, 0.0722);
    const float	 	    MAX_LUMINANCE = 20.0;

    

    interface VertIn
    {
        vec3 position : 0;
		vec2 uv0 : 1;
    };
    
    interface FragIn
    {
        vec2 oTexCoords;
    };
  
    interface FragOut
    {
        vec4 oColor : 0;
    };


    vec3 tonemap_op(vec3 shit)
    {
	    float A = 0.15;
	    float B = 0.50;
	    float C = 0.10;
	    float D = 0.20;
	    float E = 0.02;
	    float F = 0.30;
    
	    return ((shit * (A * shit + C * B) + D * E) / (shit * (A * shit + B) + D * F)) - E / F;
    }

	
    void vert(in VertIn, out FragIn)
    {
	    gl_Position = vec4(position, 1.0);
	    oTexCoords = uv0;
    }

    void downScale4Frag(in FragIn, out FragOut)
    {
        vec4 samp = vec4(0.0);
	    for( int i = 0; i < 16; i++ )
	    {
		    samp += texture(s0, oTexCoords + g_staticSampleOffsets[i]);
	    }
	
	    samp /= 16;
	    oColor = samp;
    }

    void sampleLuminance(in FragIn, out FragOut)
    {
        vec4 samp;
	    float logLumSamp = 0.0;
	
	    for( int i = 0; i < 9; ++i )
	    {
		    samp = texture(s0, oTexCoords + g_staticSampleOffsets[i]);
		    logLumSamp += log( dot( samp.rgb, LUM_CONV ) + 0.0001 );
	    }
	
	    logLumSamp /= 9.0;
	    if(logLumSamp > MAX_LUMINANCE)
		    logLumSamp = -1.0;
	

	    oColor = vec4( logLumSamp, 1.0, 1.0, 1.0 );
    }

    void resampleLuminance(in FragIn, out FragOut)
    {
        float resampleSum = 0.0;
	
	    for( int i = 0; i < 16; ++i )
	    {
	    	resampleSum += texture( s0, oTexCoords + g_staticSampleOffsets[i] ).r;
	    }

	    resampleSum /= 16.0;
	    if(resampleSum > MAX_LUMINANCE)
	    	resampleSum = -1.0;
    
	    oColor = vec4( resampleSum, 1.0F, 1.0F, 1.0F );
    }

    void finalLuminance(in FragIn, out FragOut)
    {
        float samp = 0.0;
	
	    for( int i = 0; i < 16; ++i )
	    {
	    	samp += texture( s0, oTexCoords + g_staticSampleOffsets[i] ).r;
	    }
    
	    samp = exp( samp / 16.0 );
	    if(samp > MAX_LUMINANCE)
	    	samp = 1.0;
    
	    oColor = vec4( samp, 1.0, 1.0, 1.0 );
    }

    void calcAdaptation(in FragIn, out FragOut)
    {
        float adaptedLum, curLum;
	    adaptedLum = texture( s0, vec2( 0.5, 0.5 ) ).r;
	    curLum     = texture( s1, vec2( 0.5, 0.5 ) ).r;

	    if( adaptedLum > MAX_LUMINANCE )
	    	adaptedLum = -2.0;
	    if( curLum > MAX_LUMINANCE )
	    	curLum = -2.0;
    
	    float newAdaptation = adaptedLum + ( curLum - adaptedLum ) * ( 1.0 - pow( 0.98, g_hdr_adaptationFactor * g_elapsedTime ) );
	    oColor = vec4( newAdaptation, 1.0, 1.0, 1.0 );
    }

    void bright(in FragIn, out FragOut)
    {
        vec4 samp = texture( s0, oTexCoords );
	    if(length(samp.xyz) < g_hdr_brightnessThreshold)
	    {
	    	oColor = vec4(0.0, 0.0, 0.0, samp.w);
	    	return;
	    }

	    float lum;
	    lum = texture( s1, vec2( 0.5, 0.5 ) ).r;
    
	    samp.rgb *= g_hdr_middleGrey / ( lum + 0.001 );
	    samp.rgb -= g_hdr_brightnessThreshold;
	    samp = max( samp, vec4(0.0) );
	    samp.rgb /= ( g_hdr_brightnessOffset + samp.rgb );

	    oColor = samp;
    }

    void gauss5x5(in FragIn, out FragOut)
    {
        vec3 samp = vec3(0.0);
	    for( int i = 0; i < 12; ++i )
	    {
	    	samp += g_staticSampleWeights[i].xyz * texture( s0, oTexCoords + g_staticSampleOffsets[i] ).rgb;
	    }

	    oColor = vec4(samp, texture(s0, oTexCoords).a);
    }

    void downScale2Frag(in FragIn, out FragOut)
    {
        vec4 samp = vec4(0.0);
	    for( int i = 0; i < 4; ++i )
	    {
	    	samp += texture( s0, oTexCoords + g_staticSampleOffsets[i] );
	    }
    
	    oColor = samp / 4.0;    
    }

    void bloom(in FragIn, out FragOut)
    {
        vec4 asamp = vec4(0.0);
	    vec2 sampPos;
    
	    for( int i = 0; i < 12; ++i )
	    {
	    	sampPos = oTexCoords + g_staticSampleOffsets[i];
	    	asamp += g_staticSampleWeights[i] * texture( s0, sampPos );
	    }
    
	    oColor = asamp;
    }

    void final(in FragIn, out FragOut)
    {
        vec4 samp = texture( s0, oTexCoords );
	    vec4 bloom = texture( s1, oTexCoords );
 
    	// float lum;
    	// lum = texture2D( s2, vec2( 0.5, 0.5 ) ).r;
        // lum = max(0.0001, lum);

    	// samp.rgb *= vec3(g_hdr_middleGrey) / vec3(lum + 0.001);
    	// samp.rgb /= (g_hdr_finalGrey + samp.rgb);   // 1.0 + samp.rgb

	    samp = vec4(tonemap_op(samp.xyz * g_hdr_exposure) / tonemap_op(vec3(g_hdr_whitePoint)), samp.w);

	    samp += g_hdr_bloomScale * bloom;
        samp.w = 1.0;
	    oColor = samp;
    }

    technique HDRTechnique
	{
		pass downScale4Pass
		{
			OutputBuffer = ScaledHDRTexture;

            s0 = HDRSurface;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

			DepthTest = False;
			CullEnable = False;

			VertexShader = vert;
			FragmentShader = downScale4Frag;
		};

        pass sampleLuminancePass
        {
            OutputBuffer = LuminanceTex3;

            s0 = ScaledHDRTexture;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = sampleLuminance;
        };

        pass resampleLuminancePass1
        {
            OutputBuffer = LuminanceTex2;

            s0 = LuminanceTex3;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = resampleLuminance;  
        };

        pass resampleLuminancePass2
        {
            OutputBuffer = LuminanceTex1;

            s0 = LuminanceTex2;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = resampleLuminance;      
        };

        pass finalLuminancePass
        {
            OutputBuffer = LuminanceTex0;

            s0 = LuminanceTex1;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = finalLuminance;      
        }; 

        pass adaptationPass
        {
            OutputBuffer = LumAdaptCurTex;

            s0 = LumAdaptLastTex;
            s0[MinFilter] = Nearest;
            s0[MagFilter] = Nearest;
            s0[Anisotropy] = 1.0;

            s1 = LuminanceTex0;
            s1[MinFilter] = Nearest;
            s1[MagFilter] = Nearest;
            s1[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = calcAdaptation;
        };

      
        pass adaptationPass1
        {
            OutputBuffer = LumAdaptLastTex;

            s0 = LumAdaptCurTex;
            s0[MinFilter] = Nearest;
            s0[MagFilter] = Nearest;
            s0[Anisotropy] = 1.0;

            s1 = LuminanceTex0;
            s1[MinFilter] = Nearest;
            s1[MagFilter] = Nearest;
            s1[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = calcAdaptation;
        };

        pass brightPass
        {
            OutputBuffer = BrightTex;

            s0 = HDRSurface;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            s1 = LuminanceTex0;
            s1[MinFilter] = Nearest;
            s1[MagFilter] = Nearest;
            s1[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = bright;
        };

        pass gaussPass
        {
            OutputBuffer = IntermediateBloomTex;

            s0 = BrightTex;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = gauss5x5;
        };

        pass downScale2Pass
        {
            OutputBuffer = BloomTex;

            s0 = IntermediateBloomTex;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = downScale2Frag;
        };

        pass gaussPass2
        {
            OutputBuffer = TempBloomTex2;

            s0 = BloomTex;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = gauss5x5;
        };

        pass bloomPass
        {
            OutputBuffer = TempBloomTex1;

            s0 = TempBloomTex2;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = bloom;
        };

        pass bloomPass2
        {
            OutputBuffer = TempBloomTex0;

            s0 = TempBloomTex1;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = bloom;    
        };

        pass finalPass
        {
            OutputBuffer = DefaultSurface;

            s0 = HDRSurface;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            s1 = TempBloomTex0;
            s1[MinFilter] = Linear;
            s1[MagFilter] = Linear;
            s1[Anisotropy] = 1.0;

            s2 = LumAdaptCurTex;
            s2[MinFilter] = Nearest;
            s2[MagFilter] = Nearest;
            s2[Anisotropy] = 1.0;

            DepthTest = False;
            CullEnable = False;

            VertexShader = vert;
            FragmentShader = final;
        };
	};
}