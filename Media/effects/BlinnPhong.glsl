QuadrionShader BlinnPhong
{
    #include CameraUniforms.glslh
	#include LightUniforms.glslh
	#include GBufferUniforms.glslh
	#version 410 core

	interface VertIn
	{
        vec3 position : 0;
		vec2 uv0 : 1;
        vec3 normal : 2;
	};

	interface VertInFullscreen
	{
		vec3 position : 0;
		vec2 uv0 : 1;
	};

	interface FragIn
	{
        vec4 worldSpaceNorm;
        vec4 worldSpacePos;
		vec2 texCoord0;
		vec4 worldSpaceBinormal;
 	};

	interface FragInDeferred
	{
		vec2 texCoord0;
	};

	interface FragOut
	{
        vec4 color : 0;
	};

    uniform mat4 ModelMatrix;
	uniform bool isGuiHighlighted;
//	uniform sampler2D GBufferFBO0;
//	uniform sampler2D GBufferFBO1;
//	uniform sampler2D GBufferFBO2;
//	uniform sampler2D GBufferFBO3;

	//////////////////////////////////////////////////////////////////////
	//
	//			SHADER FUNCS 
	//
	//////////////////////////////////////////////////////////////////////
	void vert(in VertIn, out FragIn)
	{
        vec4 ndcVertex = g_cam_ProjectionMatrix * g_cam_ViewMatrix * ModelMatrix * vec4(position, 1.0);
        worldSpacePos = ModelMatrix * vec4(position, 1.0);
        worldSpaceNorm = ModelMatrix * vec4(normal, 0.0);
		texCoord0 = uv0;

        gl_Position = ndcVertex;
	}

	void vertFullscreen(in VertInFullscreen, out FragInDeferred)
	{
		texCoord0 = uv0;
		gl_Position = vec4(position, 1.0);	
	}

	void frag(in FragIn, out FragOut)
	{
        float NdL = max(dot(worldSpaceNorm.xyz, g_dirLight_LightVec), 0.0);
		vec3 finalColor = vec3(NdL * g_dirLight_LightColor);

		if(isGuiHighlighted)
			finalColor = mix(finalColor, vec3(0.0f, 1.0f, 0.0f), 0.25);

        color = vec4(finalColor, 1.0);
	}
	
	void fragDeferred(in FragInDeferred, out FragOut)
	{
		vec4 viewSpaceNorm = vec4(texture(GBufferFBO1, texCoord0).rgb, 0.0);

		if(viewSpaceNorm.x == 0.0 && viewSpaceNorm.y == 0.0 &&  viewSpaceNorm.z == 0.0)
			discard;

		vec4 viewSpacePos = vec4(texture(GBufferFBO0, texCoord0).rgb, 0.0);
		vec4 surfaceColor = texture(GBufferFBO2, texCoord0);

		vec4 viewSpaceL = g_cam_ViewMatrix * vec4(g_dirLight_LightVec, 0.0);
		float NdL = max(dot(viewSpaceNorm.xyz, viewSpaceL.xyz), 0.0);
		vec3 finalColor = vec3(NdL * g_dirLight_LightColor * surfaceColor.rgb);

		// INSERT GUI HIGHLIGHTING CODE HERE
//		if(texture(GBufferFBO1, texCoord0).w == 1.0)
//			finalColor = mix(finalColor, vec3(0.0, 1.0, 0.0), 0.3);

		color = vec4(finalColor, 1.0);
//		color = vec4(1.0, 0.0, 0.0, 1.0);
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	//				TECHNIQUES 
	//
	////////////////////////////////////////////////////////////////////////////////
	technique BlinnPhong
	{
		pass p0
		{
			DepthTest = True;
			DepthFunc = Lequal;

			CullEnable = True;
			CullFace = Back;
			FrontFace = CCW;
			
			VertexShader = vert;
			FragmentShader = frag;
		};
	};

	technique BlinnPhongDeferred
	{
		pass p0
		{
			OutputBuffer = DefaultSurface;

			DepthTest = False;
			CullEnable = False;

			VertexShader = vertFullscreen;
			FragmentShader = fragDeferred;
		};
	};
}
