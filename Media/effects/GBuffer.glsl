QuadrionShader GBuffer
{
	#include CameraUniforms.glslh
	#include PBRMaterialUniforms.glslh
	#include PBRTextures.glslh
	#include TerrainUniforms.glslh
	#include LightUniforms.glslh
	#include RenderBuffers.glslh
	#include TerrainFunctions.glslh

	#version 410 core

	interface VertIn
	{
		vec3 iPosition : 0;
		vec2 iTexCoords : 1;
		vec3 iNormal : 2;
		vec3 iTangent : 3;
		vec4 iColor : 4;
	};

	interface SlimFragIn
	{
		vec4 vsNormal;
		vec4 vsPosition;
	};

	interface FragIn
	{
		vec3 outWSNormal;
		vec2 outTexCoords;
		vec3 outWSPos;
		vec4 outVSPos;
		vec4 outColor;
		vec3 outWSTangent;
	};

	interface FragOut
	{
		vec4 buf0 : 0;
		vec4 buf1 : 1;
		vec4 buf2 : 2;
		vec4 buf3 : 3;
	};

	interface SlimFragOut
	{
		vec4 buf0 : 0;
	};

	////////////////////////////////////////////////////////////////
	interface TerrainVertIn
	{
        vec3 position : 0;
        vec3 normal : 1;
        vec2 texCoord0 : 2;
	};

	layout(vertices = 3) out : tcs;
	interface TerrainTCSIn
	{
		vec3 outPositionVS;
		vec3 outNormalVS;
		vec2 outTexCoordsVS;
	};

	layout(triangles, equal_spacing, ccw) in : tes;
	interface TerrainTESIn
	{
		vec3 outPositionTCS;
		vec3 outNormalTCS;
		vec2 outTexCoordsTCS;
	};

	layout(triangles) in : geometry;
	layout(triangle_strip, max_vertices = 12) out : geometry;
	interface TerrainGeomIn
	{
		vec3 outPositionTES[3];
		vec3 outNormalTES[3];
		vec2 outTexCoordsTES[3];	
	};

	interface TerrainFragIn
	{
		vec3 outNormalGS;
		vec2 outTexCoordsGS;
		vec4 outPositionGS;
		vec4 outLightViewPositionGS[3];

//		float clipSpaceZ;
	};

	const mat2 M2 = mat2(0.8, -0.6, 0.6, 0.8);

	uniform mat4 ModelMatrix;
	uniform vec3 surfaceColor : GUI_DEBUG_FLOAT3(0.0, 1.0);
	uniform float RenderObjectID;
	uniform float isGuiSelected;
	uniform samplerBuffer 	patchInstanceBuffer;
	uniform sampler2D		heightmap;
	uniform sampler2D		noiseTex;
	uniform sampler2D		rockTexture;
	uniform sampler2D		grassTexture;
	uniform sampler2D		snowTexture;

	/////////////////// GLOBAL FUNCS ///////////////////////////////
	vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2) : tes
	{
		return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
	}

	vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2) : tes 
	{
		return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;	
	}

	

	float GetTessLevelShadowmap(float d0, float d1) : tcs
	{
		float avg = (d0 + d1) / 2.0;
		if(avg <= 1000.0)
			return 3.0;
		
		else if(avg <= 5000.0)
			return 2.0;

		return 1.0;
	}

	vec3 GenNormal(vec2 uv, float heightScale)
	{
		// vec2 d = terrainAspect * 2.0;
		float h0 = texture(heightmap, uv + g_terrain_texelSize * vec2(-1, -1)).r;
		float h1 = texture(heightmap, uv + g_terrain_texelSize * vec2(0, -1)).r;
		float h2 = texture(heightmap, uv + g_terrain_texelSize * vec2(1, -1)).r;
		float h3 = texture(heightmap, uv + g_terrain_texelSize * vec2(-1, 0)).r;
		float h4 = texture(heightmap, uv + g_terrain_texelSize * vec2(0, 0)).r;
		float h5 = texture(heightmap, uv + g_terrain_texelSize * vec2(1, 0)).r;
		float h6 = texture(heightmap, uv + g_terrain_texelSize * vec2(-1, 1)).r;
		float h7 = texture(heightmap, uv + g_terrain_texelSize * vec2(0, 1)).r;
		float h8 = texture(heightmap, uv + g_terrain_texelSize * vec2(1, 1)).r;

		vec3 n;
		n.x = -(h2 - h0 + 2.0 * (h5 - h3) + h8 - h6);
		n.z = -(h6 - h0 + 2.0 * (h7 - h1) + h8 - h2);
		n.y = 0.1;
		n = normalize(n);
		return n;

		// vec3 a = normalize(vec3(d.x, h0 - h1, 0.0));
		// vec3 b = normalize(vec3(0.0, h3 - h2, d.y));

		// vec3 n = normalize(cross(b, a));
		// return n;
	}

	float rndPerlin(vec2 p)
	{
    	float f = 0.0;
    	f += 0.5 * texture(noiseTex, p / 256.0).r; p = M2 * p * 2.02;
    	f += 0.25 * texture(noiseTex, p / 256.0).r; p = M2 * p * 2.03;
    	f += 0.125 * texture(noiseTex, p / 256.0).r; p = M2 * p * 2.01;
    	f += 0.0625 * texture(noiseTex, p / 256.0).r;
    	return f / 0.9375;
	}

	float intersect(vec3 pointOnPlane, vec3 N, vec3 rayDir, vec3 rayOrigin)
	{
		float d = dot(N, rayDir);
		float t = -1.0;
		if(abs(d) > 0.00001)
		{
			vec3 CO = pointOnPlane - rayOrigin;
			t = dot(N, CO) / d;
		}

		if(t < 0.0)
			t = 0.0;

		return t;
	}

	vec3 fogFunc(vec3 rgb, float dist, vec3 V, vec3 L, vec3 P)
	{
	    float b = 1.0 / 60000.0;
	    float offs = -500.0;
	    float c = 0.01;

	    float sunAmt = clamp(dot(V, L), 0.0, 1.0);
	    vec3 color = mix(vec3(0.4, 0.65, 1.0), 
	                     vec3(1.0, 0.7, 0.3),
	                     pow(sunAmt, 8.0));		//8

	    float amt = 1.0 - exp((-dist + offs) * b);
	    return mix(rgb, color, amt);
	}

	vec3 textureBlend2(vec4 tex1, float a1, vec4 tex2, float a2)
	{
	    return tex1.xyz * a1 + tex2.xyz * a2;
	}

	vec3 textureBlend3(vec4 tex1, float a1, vec4 tex2, float a2, vec4 tex3, float a3)
	{
		return tex1.xyz * a1 + tex2.xyz * a2 + tex3.xyz * a3;
	}
	


	////////////////////////// SHADER FUNCS ///////////////////////////
	void vert(in VertIn, out FragIn)
	{
		vec3 pos = iPosition;
        vec4 ndcPos = g_cam_ProjectionMatrix * g_cam_ViewMatrix * ModelMatrix * vec4(pos, 1.0);
		outColor = iColor * vec4(surfaceColor, 1.0);
		outColor = vec4(surfaceColor, 1.0);

		outTexCoords = iTexCoords;
		outWSNormal = vec4(ModelMatrix * vec4(iNormal, 0.0)).xyz;
        outWSPos = vec4(ModelMatrix * vec4(pos, 1.0)).xyz;
		outVSPos = g_cam_ViewMatrix * ModelMatrix * vec4(pos, 1.0);
		outWSTangent = vec4(ModelMatrix * vec4(iTangent, 0.0)).xyz;

		gl_Position = ndcPos;
	}

	void slimVert(in VertIn, out SlimFragIn)
	{
		vec4 ndcPos = g_cam_ProjectionMatrix * g_cam_ViewMatrix * ModelMatrix * vec4(iPosition, 1.0);
		vsPosition = g_cam_ViewMatrix * ModelMatrix * vec4(iPosition, 1.0);
		vsNormal = g_cam_ViewMatrix * ModelMatrix * vec4(iNormal, 0.0);

		gl_Position = ndcPos;
	}


	void slimFrag(in SlimFragIn, out SlimFragOut)
	{
		vec3 vsPos = vsPosition.xyz;
		float vsDepth = length(vsPos) / g_cam_ClipDistances.y;

		buf0 = vec4(vsNormal.xyz, vsDepth);
	}

	
	
	void frag(in FragIn, out FragOut)
	{
		// Position
		buf0 = vec4(outWSPos, RenderObjectID);

		// Normals
		// TODO: Use normal map
		buf1 = vec4(outWSNormal, isGuiSelected);

		// Albedo
		buf2 = outColor;

		buf3 = vec4(1.0, 1.0, 1.0, 1.0);
	}

	void GBufferFrag(in FragIn, out FragOut)
    {
        vec3 nSample = outWSNormal.xyz;
        if(g_pbr_hasNormalMap)
        {
            vec3 B = cross(outWSNormal.xyz, outWSTangent.xyz);
	        vec3 N = outWSNormal.xyz;
	        vec3 T = outWSTangent.xyz;
	        mat3 TBN = mat3(T, B, N);	        
            
            // Perturb normal by normalmap
	        nSample = texture(g_pbr_normalMap, outTexCoords).rgb;
	        nSample = nSample * 2.0 - 1.0;
	        nSample = (TBN * nSample);
	        nSample = normalize(nSample);
        }

		vec3 thisEmissive = vec3(0.0);
		if(g_pbr_hasEmissiveTexture)
			thisEmissive = texture(g_pbr_emissiveTexture, outTexCoords).rgb;
		thisEmissive *= 2.0;

        float thisRoughness = g_pbr_roughness;
        if(g_pbr_hasRoughnessTexture)
            thisRoughness = texture(g_pbr_roughnessTexture, outTexCoords).r;

        vec3 albedo = g_pbr_albedo.xyz;	// *= outColor
//		albedo = vec3(1.0, 0.0, 0.0);
        if(g_pbr_hasAlbedoTexture)
		{
            albedo = texture(g_pbr_albedoTexture, outTexCoords).rgb;
		}
		albedo *= outColor.xyz;

        float ao = 1.0;
        if(g_pbr_hasAOTexture)
            ao = texture(g_pbr_aoTexture, outTexCoords).r;

        float thisMetal = g_pbr_metal;
        if(g_pbr_hasMetallicTexture)
            thisMetal = texture(g_pbr_metallicTexture, outTexCoords).r;

		if(g_pbr_nRoughnessMetallicChannels > 1.0)
		{
			vec2 roughMetal = texture(g_pbr_metallicTexture, outTexCoords).gb;
			thisRoughness = roughMetal.x;
			thisMetal = roughMetal.y;
		}

		float vsDepth = outVSPos.z / g_cam_ClipDistances.y;
		buf0 = vec4(thisEmissive.xyz, thisMetal);			// emissiveR, emissiveG, emissiveB, metal
        buf1 = vec4(nSample, 0.0);                // Nx, Ny, Nz, alpha
        buf2 = vec4(albedo, thisRoughness);       // albedoR, albedoG, albedoB, roughness
       	buf3 = vec4(1.0, vsDepth, ao, RenderObjectID);        // shadow, vs depth, ao, Object ID (picking)
    }


	void terrain_vert(in TerrainVertIn, out TerrainTCSIn)
	{
		vec4 instanceOffset = texelFetch(patchInstanceBuffer, gl_InstanceID);
    	vec3 worldSpacePos = position + vec3(instanceOffset.x, 0.0, instanceOffset.y);

        outNormalVS = normal;
        outTexCoordsVS = mix(vec2(instanceOffset.zw), vec2(instanceOffset.zw) + g_terrain_uvPatchWidth, texCoord0); 	
		outPositionVS = worldSpacePos;
	}

	void terrain_tcs(in TerrainTCSIn, out TerrainTESIn)
	{
		outPositionTCS[gl_InvocationID] = outPositionVS[gl_InvocationID];
		outNormalTCS[gl_InvocationID] = outNormalVS[gl_InvocationID];
		outTexCoordsTCS[gl_InvocationID] = outTexCoordsVS[gl_InvocationID];

		float height0 = texture(heightmap, outTexCoordsVS[0]).r;
		float height1 = texture(heightmap, outTexCoordsVS[1]).r;
		float height2 = texture(heightmap, outTexCoordsVS[2]).r;

		vec3 pos0 = outPositionVS[0] + vec3(0.0, height0 * g_terrain_maxElevation, 0.0);
		vec3 pos1 = outPositionVS[1] + vec3(0.0, height1 * g_terrain_maxElevation, 0.0);
		vec3 pos2 = outPositionVS[2] + vec3(0.0, height2 * g_terrain_maxElevation, 0.0);

		float distToVert0 = length(g_cam_CamPosWorldSpace.xyz - pos0);
		float distToVert1 = length(g_cam_CamPosWorldSpace.xyz - pos1);
		float distToVert2 = length(g_cam_CamPosWorldSpace.xyz - pos2);

		gl_TessLevelOuter[0] = GetTessLevel(distToVert1, distToVert2);
		gl_TessLevelOuter[1] = GetTessLevel(distToVert2, distToVert0);
		gl_TessLevelOuter[2] = GetTessLevel(distToVert0, distToVert1);
		gl_TessLevelInner[0] = gl_TessLevelOuter[2];
	}


	void terrain_tes(in TerrainTESIn, out TerrainGeomIn)
	{
		outPositionTES = interpolate3D(outPositionTCS[0], outPositionTCS[1], outPositionTCS[2]);
		outNormalTES = interpolate3D(outNormalTCS[0], outNormalTCS[1], outNormalTCS[2]);
		outTexCoordsTES = interpolate2D(outTexCoordsTCS[0], outTexCoordsTCS[1], outTexCoordsTCS[2]);
	}

	
	void terrain_geom(in TerrainGeomIn, out TerrainFragIn)
	{
		for(int i = 0; i < 3; ++i)
		{
			outNormalGS = outNormalTES[i];
			outTexCoordsGS = outTexCoordsTES[i];

			float height = texture(heightmap, outTexCoordsTES[i]).r;
			vec3 pos = outPositionTES[i];
			pos.y = g_terrain_maxElevation * height;

			outPositionGS = ModelMatrix * vec4(pos, 1.0);

//			outLightViewPositionGS[0] = ProjectionMatrix_Light * ViewMatrix_Light * ModelMatrix_Light * vec4(pos, 1.0);

//			gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(pos, 1.0);
			gl_Position = g_cam_ProjectionMatrix * g_cam_ViewMatrix * outPositionGS;
//			clipSpaceZ = gl_Position.z;
			EmitVertex();
		}

		EndPrimitive();
	}

	void slimTerrainFrag(in TerrainFragIn, out SlimFragOut)
	{
//		vec3 N = GenNormal(outTexCoordsGS, maxHeight);
		vec3 N = normalize(cross(dFdx(outPositionGS.xyz), dFdy(outPositionGS.xyz)));
		vec4 vsPos = g_cam_ViewMatrix * outPositionGS;
		float vsDepth = length(vsPos) / g_cam_ClipDistances.y;

		buf0 = vec4(N, vsDepth);
	}


	void terrain_frag(in TerrainFragIn, out FragOut)
	{
		float shadow = 1.0;
//		vec4 shadowCoords = outLightViewPositionGS[0];
//		shadowCoords = shadowCoords / shadowCoords.w;
//		shadowCoords.xyz = shadowCoords.xyz * 0.5 + 0.5;
//		shadow = chebyshevOperator(shadowmapTexture, shadowCoords);

		vec3 N = GenNormal(outTexCoordsGS, g_terrain_maxElevation);
//		if(clipForWater)
//			n.y = -n.y;
		float isWater = 0.0;

		// Terrain land type //
		float dpy = dot(N, vec3(0.0, 1.0, 0.0));
        float normalRamp = mix(0.0, 1.0, dpy);
        float rock = 1.0 - pow(normalRamp, 1.5);

		float ge = smoothstep(0.0, 3000.0, outPositionGS.y);      // 0 - 3000
        float grassSlope = pow(clamp(dot(N, vec3(0.0, 1.0, 0.0)), 0.0, 1.0), 4.0);
        float grass = min(grassSlope, 1.0 - ge);


		/////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		vec3 P = outPositionGS.xyz;
		vec3 V = normalize(g_cam_CamPosWorldSpace.xyz - P);
		vec3 L = normalize(-g_dirLight_LightVec);
		vec2 meshUV = (vec2(P.xz) - g_terrain_worldPos.xz) / g_terrain_span;

    	float fresnel = clamp(1.0 + dot(V, N), 0.0, 1.0);
    	vec3 refl = reflect(V, N);
    	float vis = 1.0;						// shadow

    	vec3 rgb = vec3(0.0, 0.0, 0.0);
    	float globalAlpha = 1.0;

		vec3 test;

    	float r = texture(noiseTex, (P.xz / 2048.0) * g_terrain_noiseRepeat).r;
		r = 1.0;
    	rgb = (r * 0.25 + 0.75) * 5.0 * mix(vec3(0.08, 0.05, 0.03), vec3(0.1, 0.09, 0.08),
    	                                    texture(noiseTex, 0.00001 * vec2(P.x, P.y * 48.0)).r);
    	rgb = mix(rgb, 0.683 * vec3(0.45, 0.3, 0.15) * (0.5 + 0.5 * r), smoothstep(0.7, 0.9, N.y));
    	rgb = mix(rgb, 3.396 * vec3(0.3, 0.3, 0.1) * (0.25 * 0.75 * r), smoothstep(0.95, 1.0, N.y));
		test = rgb;
		

    	// snow //
    	float h = smoothstep(g_terrain_snowElevations.x, g_terrain_snowElevations.y, P.y + 500.0 * rndPerlin(P.xz + (g_terrain_span.x * 0.5)));
    	float e = smoothstep(1.0 - 0.5 * h, 1.0 - 0.1 * h, N.y);
    	float o = 0.3 + 0.7 * smoothstep(0.0, 0.1, N.x + h * h);
    	float s = h * e * o;
    	rgb = mix(rgb, 1.29 * vec3(0.62, 0.65, 0.7), smoothstep(0.1, 0.9, s));

		// amb //
//		float amb = clamp(0.5 + 0.5 * N.y, 0.0, 1.0);
//        float dif = clamp(dot(N, L), 0.0, 1.0);
//        float bac = clamp(0.2 + 0.8 * dot(normalize(vec3(L.x, 0.0, L.z)), N), 0.0, 1.0);
//        float sh = vis;

		//light//
//        vec3 lin = vec3(0.0);
//        lin += dif * vec3(7.0, 5.0, 3.0) * 1.3 * vec3(sh, sh * sh * 0.5 + 0.5 * sh, sh * sh * 0.8 + 0.2 * sh);
//        lin += amb * vec3(0.4, 0.6, 1.0) * 1.2;
//        lin += bac * vec3(0.4, 0.5, 0.6);
//        rgb *= lin * LightColor * LightIntensity;

//        vec3 H = L + V;
//        rgb += s * (0.04 + 0.96 * pow(clamp(1.0 + dot(H, V), 0.0, 1.0), 5.0)) * vec3(7.0, 5.0, 3.0) * dif * sh * 
//                pow(clamp(dot(N, H), 0.0, 1.0), 16.0);
//        rgb += s * 0.1 * pow(fresnel, 4.0) * vec3(0.4, 0.5, 0.6) * smoothstep(0.0, 0.6, refl.y);

//        L = normalize(L);
//        vec3 V = normalize(P);
//        rgb = fogFunc(rgb, length(P), V, L, P);


    	// Dither 
    	rgb += vec3(mix(-0.5/255.0, 0.5/255.0, texture(noiseTex, outTexCoordsGS * g_terrain_noiseRepeat).r));

		vec4 rockTexSample = texture(rockTexture, meshUV * 1024.0);		// * rockTexRepeat
		vec4 grassTexSample = texture(grassTexture, meshUV * 1024.0);	// * grassTexRepeat
		vec4 snowTexSample = texture(snowTexture, meshUV * 1024.0);

//		grassTexSample = pow(grassTexSample, vec4(2.2));
//		snowTexSample = pow(snowTexSample, vec4(2.2));
//		rockTexSample = pow(rockTexSample, vec4(2.2));

        vec3 detailSample = textureBlend3(grassTexSample, grass, rockTexSample, rock, snowTexSample, s);
		rgb = mix(rgb, detailSample.rgb, 0.4);

		// gamma 
    	rgb = pow(rgb, vec3(1.0));

		////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		float roughness = 1.0;
		float metal = 0.0;
		float ao = 1.0;

		buf0 = vec4(outPositionGS.xyz, 1.0);
		buf1 = vec4(N, 0.0);
		buf2 = vec4(rgb, roughness);
		buf3 = vec4(shadow, metal, ao, 1.0);
	}


	technique SlimGBuffer
	{
		pass p0
		{
			DepthTest = True;
			DepthWrite = True;

			OutputBuffer = SlimGBufferFBO;

			VertexShader = slimVert;
			FragmentShader = slimFrag;
		};
	};

	technique GBuffer
	{
		pass p0
		{
			DepthTest = True;
			
			OutputBuffer = GBufferFBO;

			VertexShader = vert;
			FragmentShader = frag;
		};
	};

	technique GBufferPBR
	{
		pass p0
		{
			DepthTest = True;
			DepthWrite = True;

			OutputBuffer = GBufferFBO;

			VertexShader = vert;
			FragmentShader = GBufferFrag;
		};
	};

	technique SlimTerrainGBuffer
	{
		pass p0
		{
			OutputBuffer = SlimGBufferFBO;

			DepthTest = True;
			DepthFunc = Lequal;
			DepthWrite = True;

			CullEnable = True;
			FrontFace = CCW;
			CullFace = Back;

			VertexShader = terrain_vert;
			TCShader = terrain_tcs;
			TEShader = terrain_tes;
			GeometryShader = terrain_geom;
			FragmentShader = slimTerrainFrag;
		};
	};

	technique GBufferTerrain
	{
		pass p0
		{
			OutputBuffer = GBufferFBO;

//			PolygonMode = FrontAndBack Line

			DepthTest = True;
			DepthFunc = Lequal;
			DepthWrite = True;

			CullEnable = True;
			FrontFace = CCW;
			CullFace = Back;

			noiseTex = Media/textures/noise256.png;
			noiseTex[MinFilter] = Linear;
			noiseTex[MagFilter] = Linear;
			noiseTex[Anisotropy] = 1.0;

			rockTexture = Media/textures/terrain/rock.jpg;
			rockTexture[MinFilter] = LinearMipmapLinear;
			rockTexture[MagFilter] = Linear;
			rockTexture[Anisotropy] = 16.0;

			grassTexture = Media/textures/terrain/grass3.jpg;
			grassTexture[MinFilter] = LinearMipmapLinear;
			grassTexture[MagFilter] = Linear;
			grassTexture[Anisotropy] = 16.0;

			snowTexture = Media/textures/terrain/Snow.jpg;
			snowTexture[MinFilter] = LinearMipmapLinear;
			snowTexture[MagFilter] = Linear;
			snowTexture[Anisotropy] = 16.0;


			heightmap[MinFilter] = Linear;
			heightmap[MagFilter] = Linear;
			heightmap[Anisotropy] = 1.0;

			VertexShader = terrain_vert;
			TCShader = terrain_tcs;
			TEShader = terrain_tes;
			GeometryShader = terrain_geom;
			FragmentShader = terrain_frag;
		};
	};
}