QuadrionShader HBAO
{
    #version 410 core

    #include LightUniforms.glslh
    #include CameraUniforms.glslh
    #include GBufferUniforms.glslh
    #include RenderBuffers.glslh
    #include Constants.glslh
    #include AOUniforms.glslh

    interface VertIn
    {
        vec4    position;
        vec2    texCoord0;
    };

    interface FragIn
    {
        vec2    outTexCoords;
        vec4    outViewVec;
    };

    interface FragOut
    {
        vec4    color : 0;
    };

    const float NUM_STEPS   = 4;
    const float NUM_DIRS    = 8;

    uniform sampler2D   GBuffer0Tex;
    uniform sampler2D   GBuffer1Tex;
    uniform sampler2D   s0;
    // uniform vec4        frustumUL;
    // uniform vec4        frustumUR;
    // uniform vec4        frustumLL;
    // uniform vec4        frustumLR; 
    
    float rand(vec2 co)
    {
        float a = 12.9898;
        float b = 78.233;
        float c = 43758.5453;
        float dt= dot(co.xy ,vec2(a,b));
        float sn= mod(dt,3.14);
        return fract(sin(sn) * c);
    }

    vec3 UVtoView(vec2 UV, float depth)
    {
        float x = g_cam_frustumUL.x + (g_cam_frustumUR.x - g_cam_frustumUL.x) * UV.x;
        float y = g_cam_frustumLL.y + (g_cam_frustumUL.y - g_cam_frustumLL.y) * UV.y;
        vec3 ray = vec3(x, y, g_cam_frustumUL.z);

        return ray * depth;
    }

    vec3 getViewPosFromUV(vec2 UV)
    {
        float z = texture(GBuffer0Tex, UV).w;
        return UVtoView(UV, z);
    }

    vec3 getViewPosFromUVDeferred(vec2 UV)
    {
        vec3 wsPos = texture(GBuffer0Tex, UV).xyz;
        vec4 vsPos = g_cam_ViewMatrix * vec4(wsPos, 1.0);
        return vsPos.xyz;
    }

    vec2 rotateVec2(vec2 dir, vec2 cossin)
    {
        return vec2(dir.x * cossin.x - dir.y * cossin.y,
                    dir.x * cossin.y + dir.y * cossin.x);
    }

    float falloff(float dsq)
    {
        float niR2 = -1.0 / (g_ao_radius * g_ao_radius);
        return dsq * niR2 + 1.0;
    }

    float getAO(vec3 P, vec3 N, vec3 S)
    {
        vec3 V = S - P;
        float VdV = dot(V, V);
        float NdV = dot(N, V) * 1.0 / sqrt(VdV);

        return clamp(NdV - g_ao_bias, 0, 1) * clamp(falloff(VdV), 0, 1);
    }

    float computeAO(vec2 UV, float radiusPels, vec4 rand, vec3 ViewPos, vec3 ViewNorm, bool isDeferred)
    {
        float stepSizeInPels = radiusPels / (NUM_STEPS + 1);
        const float A = 2.0 * g_const_Pi / NUM_DIRS;
        float AO = 0.0;
        vec2 invScreenRes = vec2(1.0f / g_screenWidth, 1.0f / g_screenHeight);

        for(float i = 0.0; i < NUM_DIRS; i += 1.0)
        {
            float ang = A * i;
            vec2 dir = rotateVec2(vec2(cos(ang), sin(ang)), rand.xy);

            float rayPix = rand.z * stepSizeInPels + 1.0;

            for(float j = 0.0; j < NUM_STEPS; j += 1.0)
            {
                vec2 snappedUV = round(rayPix * dir) * invScreenRes + UV;
                vec3 S = vec3(0.0);
                if(!isDeferred)
                    S = getViewPosFromUV(snappedUV);
                else
                    S = getViewPosFromUVDeferred(snappedUV);

                rayPix += stepSizeInPels;
                AO += getAO(ViewPos, ViewNorm, S);
            }
        }

        float multiplier = 1.0f / (1.0f - g_ao_bias);
        AO *= multiplier / (NUM_DIRS * NUM_STEPS);
        return clamp(1.0 - AO * 2.0, 0, 1);
    }

    float blur(vec2 uv, float r, float center_c, float center_d, inout float w_total)
    {
        vec4 hbaoSamp = texture(s0, uv);
        float c = hbaoSamp.y;
        float d = hbaoSamp.x;

        float blurSigma = float(g_ao_blurRadius) * 0.5;
        float blurFalloff = 1.0 / (2.0 * blurSigma * blurSigma);

        float diff = (d - center_d) * g_ao_blurSharpness;
        float w = exp2(-r * r * blurFalloff - diff * diff);
        w_total += w;

        return c * w;
    }

    ////////////////////////////////////////////////////////////////////////////////////

    void vert(in VertIn, out FragIn)
    {
        outTexCoords = texCoord0;
        gl_Position = position;
    }

    void frag(in FragIn, out FragOut)
    {
        vec2 uv = outTexCoords;
        vec3 ViewPosition = getViewPosFromUV(uv);
        vec3 ViewNormal = normalize(texture(GBuffer0Tex, uv).rgb);

        float radiusInPels = g_ao_radToScreen / -ViewPosition.z;
        float rng1 = rand(uv);
        float rng2 = rand(uv);
        float ang = 2.0 * g_const_Pi * rng1 / NUM_DIRS;
        vec4 hbaoRand = vec4(cos(ang), sin(ang), rng2, 0.0);

        float AO = computeAO(uv, radiusInPels, hbaoRand, ViewPosition, ViewNormal, false);
        color = vec4(AO, pow(AO, g_ao_exponent), 0.0, 0.0);
    }

    void deferred_frag(in FragIn, out FragOut)
    {
        vec2 uv = outTexCoords;

        vec4 ViewPosition = g_cam_ViewMatrix * vec4(texture(GBuffer0Tex, uv).rgb, 1.0);
        vec4 ViewNormal = g_cam_ViewMatrix * vec4(texture(GBuffer1Tex, uv).rgb, 0.0);

//        vec3 ViewPosition = getViewPosFromUV(uv);
//        vec3 ViewNormal = normalize(texture(GBuffer0Tex, uv).rgb);

        float radiusInPels = g_ao_radToScreen / -ViewPosition.z;
        float rng1 = rand(uv);
        float rng2 = rand(uv);
        float ang = 2.0 * g_const_Pi * rng1 / NUM_DIRS;
        vec4 hbaoRand = vec4(cos(ang), sin(ang), rng2, 0.0);

        float AO = computeAO(uv, radiusInPels, hbaoRand, ViewPosition.xyz, ViewNormal.xyz, true);
        color = vec4(AO, pow(AO, g_ao_exponent), 0.0, 0.0);
    }

    void blurFrag(in FragIn, out FragOut)
    {
        vec2 invScreenRes = vec2(1.0) / vec2(g_screenWidth, g_screenHeight);
        vec4 hbaoSamp = texture(s0, outTexCoords);
        float center_c = hbaoSamp.y;
        float center_d = hbaoSamp.x;

        float c_total = center_c;
        float w_total = 1.0;

        for(float r = 1; r <= g_ao_blurRadius; ++r)
        {
            vec2 uv = outTexCoords + invScreenRes * r;
            c_total += blur(uv, r, center_c, center_d, w_total);
        }

        for(float r = 1; r <= g_ao_blurRadius; ++r)
        {
            vec2 uv = outTexCoords - invScreenRes * r;
            c_total += blur(uv, r, center_c, center_d, w_total);
        }

        color = vec4(c_total / w_total);
    }


    technique HBAOTechnique
    {
        pass p0
        {
            DepthTest = False;
            DepthWrite = False;

            CullEnable = False;

            OutputBuffer = HBAOFBO;

            GBuffer0Tex = SlimGBufferFBO;
            GBuffer0Tex[MinFilter] = Nearest;
            GBuffer0Tex[MagFilter] = Nearest;
            GBuffer0Tex[Anisotropy] = 1.0;

            VertexShader = vert;
            FragmentShader = frag;
        };
    };

    technique DeferredHBAOTechnique
    {
        pass p0
        {
            DepthTest = False;
            DepthWrite = False;
            CullEnable = False;

            OutputBuffer = HBAOFBO;

            GBuffer0Tex = GBufferFBO0;
            GBuffer0Tex[MinFilter] = Nearest;
            GBuffer0Tex[MagFilter] = Nearest;
            GBuffer0Tex[Anisotropy] = 1.0;

            GBuffer1Tex = GBufferFBO1;
            GBuffer1Tex[MinFilter] = Nearest;
            GBuffer1Tex[MagFilter] = Nearest;
            GBuffer1Tex[Anisotropy] = 1.0;

            VertexShader = vert;
            FragmentShader = deferred_frag;
        };
    };

    technique HBAOBlurTechnique
    {
        pass p0
        {
            DepthTest = False;
            DepthWrite = False;
            CullEnable = False;

            OutputBuffer = HBAOBlurFBO;

            s0 = HBAOFBO;
            s0[MinFilter] = Linear;
            s0[MagFilter] = Linear;
            s0[Anisotropy] = 1.0;

            VertexShader = vert;
            FragmentShader = blurFrag;
        };
    };
}