QuadrionShader TerrainLighting
{
    #version 410 core

    #include LightUniforms.glslh
    #include CameraUniforms.glslh
	#include GBufferUniforms.glslh
	#include Constants.glslh
    #include RenderBuffers.glslh
	#include TerrainUniforms.glslh
	#include TerrainFunctions.glslh
   

    interface VertIn
	{
        vec3 position : 0;
        vec3 viewVec : 1;
	};

    interface FragIn
	{
		vec3 outViewVec;
        vec2 outTexCoords;
	};

    interface FragOut
	{
        vec4 color : 0;
	};

    interface TerrainVertIn
	{
        vec3 position : 0;
        vec3 normal : 1;
        vec2 texCoord0 : 2;
	};

	layout(vertices = 3) out : tcs;
	interface TerrainTCSIn
	{
		vec3 outPositionVS;
		vec3 outNormalVS;
		vec2 outTexCoordsVS;
	};

	layout(triangles, equal_spacing, ccw) in : tes;
	interface TerrainTESIn
	{
		vec3 outPositionTCS;
		vec3 outNormalTCS;
		vec2 outTexCoordsTCS;
	};

	layout(triangles) in : geometry;
	layout(triangle_strip, max_vertices = 12) out : geometry;
	interface TerrainGeomIn
	{
		vec3 outPositionTES[3];
		vec3 outNormalTES[3];
		vec2 outTexCoordsTES[3];	
	};

	interface TerrainFragIn
	{
		vec3 outNormalGS;
		vec2 outTexCoordsGS;
		vec4 outPositionGS;
		vec4 outLightViewPositionGS[3];

//		float clipSpaceZ;
	};
    
	uniform samplerBuffer 	patchInstanceBuffer;
	uniform sampler2D		heightmap;
	uniform mat4			ModelMatrix;

    const float shininessConstant = 1.0;
    const float iorA = 1.000293;			
    const float iorB = 2.4;			
    const float ssaoEnable = 0.0;
	const vec3 ambient = vec3(0.1, 0.1, 0.1);
	const float epsilon = 0.00001;

	vec3 GenNormal(vec2 uv, float heightScale)
	{
		// vec2 d = terrainAspect * 2.0;
		// float h0 = texture(heightmap, uv + texelSize * vec2(-1, -1)).r;
		// float h1 = texture(heightmap, uv + texelSize * vec2(0, -1)).r;
		// float h2 = texture(heightmap, uv + texelSize * vec2(1, -1)).r;
		// float h3 = texture(heightmap, uv + texelSize * vec2(-1, 0)).r;
		// float h4 = texture(heightmap, uv + texelSize * vec2(0, 0)).r;
		// float h5 = texture(heightmap, uv + texelSize * vec2(1, 0)).r;
		// float h6 = texture(heightmap, uv + texelSize * vec2(-1, 1)).r;
		// float h7 = texture(heightmap, uv + texelSize * vec2(0, 1)).r;
		// float h8 = texture(heightmap, uv + texelSize * vec2(1, 1)).r;

		// vec3 n;
		// n.x = -(h2 - h0 + 2.0 * (h5 - h3) + h8 - h6);
		// n.z = -(h6 - h0 + 2.0 * (h7 - h1) + h8 - h2);
		// n.y = 0.1;
		// n = normalize(n);
		// return n;

		float patchWidth = g_terrain_span.x / 1024.0;
		float patchHeight = g_terrain_span.y / 1024.0;

		float ul = texture(heightmap, uv + g_terrain_uvPatchWidth * vec2(-1, 1)).r * g_terrain_maxElevation;
		float ur = texture(heightmap, uv + g_terrain_uvPatchWidth * vec2(1, 1)).r * g_terrain_maxElevation;
		float ll = texture(heightmap, uv + g_terrain_uvPatchWidth * vec2(-1, -1)).r * g_terrain_maxElevation;
		float lr = texture(heightmap, uv + g_terrain_uvPatchWidth * vec2(1, -1)).r * g_terrain_maxElevation;

		vec3 a = vec3(-2.0 * patchWidth, lr - ul, -2.0 * patchHeight);
		vec3 b = vec3(-2.0 * patchWidth, ur - ll, 2.0 * patchHeight);

		vec3 n = normalize(cross(b, a));
		return n;
	}

	vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2) : tes
	{
		return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
	}

	vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2) : tes 
	{
		return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;	
	}


    vec3 oren_nayar(vec3 N, vec3 V, vec3 L, float roughness, vec3 albedo)
    {
    	float NdL = dot(N, L);
    	float NdV = dot(N, V);
    
    	float angVN = acos(NdV);
    	float angLN = acos(NdL);
    
    	float a = max(angVN, angLN);
    	float b = min(angVN, angLN);
    	float y = dot(V - N * NdV, L - N * NdL);
    
    	float rSQ = roughness * roughness;
    	float A = 1.0 - 0.5 * (rSQ / (rSQ + 0.33));
    	float B = 0.45 * (rSQ / (rSQ + 0.09));
    	float C = sin(a) * tan(b);
    	float L1 = max(0.0, NdL) * (A + (B * max(0.0, y) * C));

    //	return (lightColor * L1) / (2.0 * Pi);
    	return (albedo / g_const_Pi) * L1;
    }

	vec3 Fresnel(float ang, vec3 F0)
	{
		return F0 + (vec3(1.0) - F0) * pow(1.0 - ang, 5.0);
	}

    void vert(in VertIn, out FragIn)
	{
	    outViewVec = viewVec;
	    outTexCoords = vec2(position.xy) * 0.5 + 0.5;
	    gl_Position = vec4(position.xyz, 1.0);
	}

    void frag(in FragIn, out FragOut)
    {
    	// Sample GBuffer //
        vec4 GB0Sample = texture(GBufferFBO0, outTexCoords);
        vec4 GB1Sample = texture(GBufferFBO1, outTexCoords);
        vec4 GB2Sample = texture(GBufferFBO2, outTexCoords);
        vec4 GB3Sample = texture(GBufferFBO3, outTexCoords);

		float alpha = 1.0;
        if(GB1Sample.w == 1.0)
            alpha = 0.0;

		vec3 albedo = GB2Sample.xyz;
    	vec3 normSample = GB1Sample.xyz;
        float AO = GB3Sample.z;
    	float shadowSample = GB3Sample.x;
    	vec3 shadow = vec3(shadowSample, shadowSample, shadowSample);
		float roughness = pow(GB2Sample.w, 2.2);
    	float metal = pow(GB3Sample.g, 2.2);
    
		vec3 P = GB0Sample.xyz;

		// Gen light vector (view space)
		vec3 L = normalize(-g_dirLight_LightVec);
    
		// Gen normal vector (View space)
    	vec3 N = normSample.xyz;
    	N = normalize(N);

		// Gen view vector (view space)
		vec3 V = normalize(g_cam_CamPosWorldSpace.xyz - P);
		vec3 H = normalize(L + V);

		// Oren Nayar (terrain)
		vec3 ambient = vec3(g_dirLight_LightAmbientIntensity) * albedo;
//		vec3 Kd = oren_nayar(N, V, L, roughness, albedo);
		vec3 Kd = vec3(dot(N, L));
		vec3 diffuse = Kd * shadow * g_dirLight_LightColor * g_dirLight_LightIntensity * albedo;
		vec3 final = pow(albedo, vec3(1.0 / g_gamma));
		final = albedo * Kd * g_dirLight_LightColor * g_dirLight_LightIntensity;
		final = pow(final, vec3(1.0 / g_gamma));
		color = vec4(final, alpha);	
    }

    ////////////////////////////////////// FORWARD PASSES /////////////////////////////////

    void terrain_vert(in TerrainVertIn, out TerrainTCSIn)
	{
		vec4 instanceOffset = texelFetch(patchInstanceBuffer, gl_InstanceID);
    	vec3 worldSpacePos = position + vec3(instanceOffset.x, 0.0, instanceOffset.y);

        outNormalVS = normal;
        outTexCoordsVS = mix(vec2(instanceOffset.zw), vec2(instanceOffset.zw) + g_terrain_uvPatchWidth, texCoord0); 	
		outPositionVS = worldSpacePos;
	}

    void terrain_tcs(in TerrainTCSIn, out TerrainTESIn)
	{
		outPositionTCS[gl_InvocationID] = outPositionVS[gl_InvocationID];
		outNormalTCS[gl_InvocationID] = outNormalVS[gl_InvocationID];
		outTexCoordsTCS[gl_InvocationID] = outTexCoordsVS[gl_InvocationID];

		float height0 = texture(heightmap, outTexCoordsVS[0]).r;
		float height1 = texture(heightmap, outTexCoordsVS[1]).r;
		float height2 = texture(heightmap, outTexCoordsVS[2]).r;

		vec3 pos0 = outPositionVS[0] + vec3(0.0, height0 * g_terrain_maxElevation, 0.0);
		vec3 pos1 = outPositionVS[1] + vec3(0.0, height1 * g_terrain_maxElevation, 0.0);
		vec3 pos2 = outPositionVS[2] + vec3(0.0, height2 * g_terrain_maxElevation, 0.0);

		float distToVert0 = length(g_cam_CamPosWorldSpace.xyz - pos0);
		float distToVert1 = length(g_cam_CamPosWorldSpace.xyz - pos1);
		float distToVert2 = length(g_cam_CamPosWorldSpace.xyz - pos2);

		gl_TessLevelOuter[0] = GetTessLevel(distToVert1, distToVert2);
		gl_TessLevelOuter[1] = GetTessLevel(distToVert2, distToVert0);
		gl_TessLevelOuter[2] = GetTessLevel(distToVert0, distToVert1);
		gl_TessLevelInner[0] = gl_TessLevelOuter[2];
	}


	void terrain_tes(in TerrainTESIn, out TerrainGeomIn)
	{
		outPositionTES = interpolate3D(outPositionTCS[0], outPositionTCS[1], outPositionTCS[2]);
		outNormalTES = interpolate3D(outNormalTCS[0], outNormalTCS[1], outNormalTCS[2]);
		outTexCoordsTES = interpolate2D(outTexCoordsTCS[0], outTexCoordsTCS[1], outTexCoordsTCS[2]);
	}

	
	void terrain_geom(in TerrainGeomIn, out TerrainFragIn)
	{
		for(int i = 0; i < 3; ++i)
		{
			outNormalGS = outNormalTES[i];
			outTexCoordsGS = outTexCoordsTES[i];

			float height = texture(heightmap, outTexCoordsTES[i]).r;
			vec3 pos = outPositionTES[i];
			pos.y = g_terrain_maxElevation * height;

			outPositionGS = ModelMatrix * vec4(pos, 1.0);

//			outLightViewPositionGS[0] = ProjectionMatrix_Light * ViewMatrix_Light * ModelMatrix_Light * vec4(pos, 1.0);

//			gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(pos, 1.0);
			gl_Position = g_cam_ProjectionMatrix * g_cam_ViewMatrix * outPositionGS;
//			clipSpaceZ = gl_Position.z;
			EmitVertex();
		}

		EndPrimitive();
	}

	void terrain_frag(in TerrainFragIn, out FragOut)
	{
		vec3 albedo = vec3(0.0, 1.0, 0.0);
//		vec3 N = GenNormal(outTexCoordsGS, maxHeight);
		vec3 N = normalize(cross(dFdx(outPositionGS.xyz), dFdy(outPositionGS.xyz)));
		vec3 L = normalize(-g_dirLight_LightVec);

		vec3 ambient = vec3(g_dirLight_LightAmbientIntensity) * albedo;
		vec3 Kd = vec3(max(dot(N, L), 0.0));
		vec3 diffuse = Kd * g_dirLight_LightColor * g_dirLight_LightIntensity * albedo;
		vec3 final = pow(albedo, vec3(1.0 / g_gamma));
		final = albedo * Kd * g_dirLight_LightColor * g_dirLight_LightIntensity;
		final = pow(Kd * vec3(0.0, 1.0, 0.0), vec3(1.0 / g_gamma));
		color = vec4(final, 1.0);
//		color = vec4(N, 1.0);	
//		color = vec4(1.0, 0.0, 0.0, 1.0);
	}

    ///////////////////////////////////// TECHNIQUES ///////////////////////////////////////////////

    technique TerrainLightingTechnique
    {
        pass p0
        {
			BlendEnabled = True;
			BlendFunc = SrcAlpha OneMinusSrcAlpha;

//			OutputBuffer = {DefaultSurface, 0, 0, 0, 0, 0, 0}
			OutputBuffer = HDRSurface;

            VertexShader = vert;
            FragmentShader = frag;
        };
    };

    technique ForwardTerrainLightingTechnique
    {
        pass p0
        {
//			PolygonMode = FrontAndBack Line

            DepthTest = True;
			DepthWrite = True;
			DepthMode = Equal;

			CullEnable = True;

			OutputBuffer = HDRSurface;

            VertexShader = terrain_vert;
			TCShader = terrain_tcs;
			TEShader = terrain_tes;
			GeometryShader = terrain_geom;
			FragmentShader = terrain_frag;
        };
    };
}