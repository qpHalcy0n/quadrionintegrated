# QuadrionIntegrated

Integrated project with Quadrion Engine and driver project.

Compiling Windows
-----------------
1) Run premake_vs2017.bat. This will generate solution and project files for VS2017/2019
2) Check config.ini in Media folder and make sure the resolution makes sense for you
3) When running debug, make sure DEBUG_TERRAIN_RES in SandboxGamestate.cpp is set to something small like 16 or less. This value must be a power of 2 greater than 1.
4) Any time you add or remove a file from the project, you must run the premake_vs2017 again to re-generate the solution and projects.


Compiling Linux (Debian)
---------------------------
1) Run linux_i386_install_script.sh   This will install any known dependencies that may not exist on your system.

2) Run premake_linux.sh   This will generate a make file

3) Run make

4) Execute qdriver located in build folder

5) Entire process looks thusly:

	bash linux_i386_install_script.sh
	bash premake_linux.sh
	make
	./build/qdriver

6) Check config.ini in Media folder and make sure the resolution makes sense for you

7) When running debug, make sure DEBUG_TERRAIN_RES in SandboxGamestate.cpp is set to something small like 16 or less. This value must be a power of 2 greater than 1.

8) Any time you add or remove a file from the project, you must run the premake_linux.sh again to re-generate the solution and projects.

9) DONT FORGET TO MAKE!

10) Run "make clean" if build fails

11) Occasionally you may have to kill the build folder  "rm -rf build" if you've added assets to Media or new effects


