#include "components/RenderComponent.h"


RenderComponent::RenderComponent(std::vector<std::shared_ptr<MeshNode>> aMeshes)
{
    _meshes = aMeshes;
    _numMeshes = aMeshes.size();
}

std::vector<std::shared_ptr<MeshNode>>& RenderComponent::GetMeshes()
{
    return _meshes;
}