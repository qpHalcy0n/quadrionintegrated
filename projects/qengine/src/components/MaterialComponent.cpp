#include "components/MaterialComponent.h"
#include "render/QGfx.h"

MaterialComponent::MaterialComponent()
{

}

MaterialComponent::MaterialComponent(Effect* aEffect, const std::string& aTechName)
{
    _effect = aEffect;
    _techniqueName = aTechName;

    _technique = _effect->getTechnique(_techniqueName.c_str());
//    QGfx::addEntityToTechniqueMap(gameObject, aTechName);
}

std::string MaterialComponent::GetTechniqueName()
{
    return _techniqueName;
}

std::string MaterialComponent::GetEffectName()
{
    return _effect->getName();
}

uint32_t MaterialComponent::GetNumPasses()
{
    if(_technique)
        return _technique->getNumPasses();

    return 0;
}

std::string MaterialComponent::GetPassNameByIndex(uint32_t aIndex)
{
    return _technique->getPass(aIndex)->getName();
}

Technique* MaterialComponent::GetTechnique()
{
    return _technique;
}

bool MaterialComponent::Bind(uint32_t aPass)
{
    Pass* pass = _technique->getPass(aPass);
    if(!pass)
        return false;

    OutputBufferInfo* obi = pass->getOutputBuffer();
	QGfx::bindRenderTarget(obi);

    vec4<float> clearColor = pass->getClearColor();
    QGfx::clearBuffer(QGfx::getRenderBuffer(obi->name).id, clearColor);

	pass->bind();
    return true;
}