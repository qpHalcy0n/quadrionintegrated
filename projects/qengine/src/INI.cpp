/***************************************************************************************************
* Copyright (c) 2008 Jonathan 'Bladezor' Bastnagel.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the GNU Lesser Public License v2.1
* which accompanies this distribution, and is available at
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
* 
* Contributors:
*     Jonathan 'Bladezor' Bastnagel - initial implementation and documentation
***************************************************************************************************/

#include "INI.h"

#include <algorithm>
#include <cstring>
#include <fstream>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

struct INIImpl
{
	std::map< std::string, std::map< std::string, std::string> > mapINIData;
	std::streampos uiLength;
	char* path;
};

INI::INI()
{
	mImpl = new INIImpl();
}

INI::~INI()
{
	if (mImpl)
	{
		delete mImpl;
		mImpl = nullptr;
	}
}

const char* INI::getString(const char* aSection, const char* aVariable, const char* aDef)
{
	if (exists(aSection, aVariable))
	{
		return mImpl->mapINIData[aSection][aVariable].c_str();
	}

	setString(aSection, aVariable, aDef);

	return aDef;
}

int INI::getInt(const char* aSection, const char* aVariable, const int aDef)
{
	if (exists(aSection, aVariable))
	{
		std::string value = mImpl->mapINIData[aSection][aVariable];
		const char* tempBuffer = value.c_str();
		int result = atoi(tempBuffer);
		return result;
	}

	setInt(aSection, aVariable, aDef);

	return aDef;
}

bool INI::getBool(const char* aSection, const char* aVariable, const bool aDef)
{
	if (exists(aSection, aVariable))
	{
		std::string value = mImpl->mapINIData[aSection][aVariable];

		std::transform(value.begin(), value.end(), value.begin(), tolower);

		if (value == "true")
			return true;
		return false;
	}

	setBool(aSection, aVariable, aDef);

	return aDef;
}

double INI::getDouble(const char* aSection, const char* aVariable, const double aDef)
{
	if (exists(aSection, aVariable))
	{
		std::string value = mImpl->mapINIData[aSection][aVariable];
		const char* tempBuffer = value.c_str();
		double result = atof(tempBuffer);
		return result;
	}

	setDouble(aSection, aVariable, aDef);

	return aDef;
}

float INI::getFloat(const char* aSection, const char* aVariable, const float aDef)
{
	if (exists(aSection, aVariable))
	{
		std::string value = mImpl->mapINIData[aSection][aVariable];
		const char* tempBuffer = value.c_str();
		float result = static_cast<float>(atof(tempBuffer));
		return result;
	}

	setFloat(aSection, aVariable, aDef);

	return aDef;
}

const int INI::parseINI(const char* aPath)
{
	std::vector<std::string> _vLines;

	std::ifstream *fsFile = new std::ifstream();
	fsFile->open(aPath);

	auto path_length = strlen(aPath)+1;

	mImpl->path = new char[path_length];


	std::strcpy(mImpl->path, aPath);

	if (!fsFile)
		return INI_LOAD_ERROR;

	if (!fsFile->is_open())
		return INI_LOAD_ERROR;

	if (!fsFile->is_open())
		return INI_LOAD_ERROR;

	//	Calculate the length of the file.
	fsFile->seekg(0, std::ios::end);
	mImpl->uiLength = fsFile->tellg();
	fsFile->seekg(0, std::ios::beg);

	while(!fsFile->eof())
	{
		fsFile->tellg();
		std::string line;
		std::getline(*fsFile,line);
		_vLines.push_back(line);
	}

	fsFile->close();

	delete fsFile;

	//	Remove leading and trailing whitespace.

	for(unsigned int i = 0; i < _vLines.size(); ++i)
	{
		//	Erase empty lines from the vector.
		if(_vLines[i].size()<=0)
		{
			_vLines.erase(_vLines.begin()+i);
			i--;
			continue;
		}
		std::string::iterator itCharacter = _vLines[i].begin();
		while(*(itCharacter)==' ')
		{
			_vLines[i].erase(itCharacter);
			itCharacter=_vLines[i].begin();
		}
		itCharacter = _vLines[i].end()-1;
		while(*(itCharacter)==' ')
		{
			_vLines[i].erase(itCharacter);
			itCharacter = _vLines[i].end()-1;
		}

		_vLines[i].erase(std::remove(_vLines[i].begin(), _vLines[i].end(), '\t'), _vLines[i].end());
	}

	//	Remove invalid lines
	for(unsigned int i = 0; i < _vLines.size(); ++i)
	{
		size_t equalIndex = static_cast<unsigned int>(_vLines[i].find_first_of("="));
		//	There's nothing important in this line so let's remove it.
		if(equalIndex == std::string::npos && _vLines[i][0]!='[')
		{
			_vLines.erase(_vLines.begin()+i);
			i--;
			continue;
		}
	}

	//	_build a map of quotes
	std::unordered_map< unsigned int, std::vector<unsigned int> > vecQuoteIndices;
	for( unsigned int i = 0; i < _vLines.size(); ++i)
	{
		for( unsigned int j = 0; j < _vLines[i].size(); ++j)
		{
			if(_vLines[i][j]=='\"'&&_vLines[i][j-1]!='\\')
				vecQuoteIndices[i].push_back(j);
		}
	}

	//	Strip Comments and clean it up...
	for(unsigned int i = 0; i < _vLines.size(); ++i)
	{
		size_t equalIndex = static_cast<size_t>(_vLines[i].find_first_of("="));
		size_t commentIndex = static_cast<size_t>(_vLines[i].find_first_of(';'));
		size_t LBraceIndex = static_cast<size_t>(_vLines[i].find_first_of('['));
		size_t RBraceIndex = static_cast<size_t>(_vLines[i].find_first_of(']'));

		//	This line is invalid, remove it.
		if(equalIndex == std::string::npos)
		{
			if(LBraceIndex == std::string::npos || RBraceIndex == std::string::npos)
			{
				_vLines.erase(_vLines.begin()+i);
				i--;
				continue;
			}

			if(commentIndex > LBraceIndex && commentIndex > RBraceIndex)
			{
				_vLines[i] = _vLines[i].substr(0, commentIndex);
				//	Strip whitespace
				_vLines[i] = _vLines[i].substr(0, _vLines[i].find_last_not_of(' ') + 1);
			}
			else 
				if(commentIndex != std::string::npos)
				{
					_vLines.erase(_vLines.begin()+i);
					i--;
					continue;
				}
		}
		else
		{
			if(commentIndex!=std::string::npos)
			{
				if(commentIndex<equalIndex)
				{
					_vLines.erase(_vLines.begin()+i);
					i--;
					continue;
				}
				else
				{
					while(commentIndex != std::string::npos)
					{
						if(vecQuoteIndices[i].size() > 0)
						{
							//	Is the delimiter between quotes?
							if( vecQuoteIndices[i][0] < commentIndex && vecQuoteIndices[i][1] > commentIndex )
							{
								if(commentIndex + 1 < _vLines[i].size())
									commentIndex = static_cast<size_t>(_vLines[i].find_first_of(';', commentIndex+1));
								else
									commentIndex = static_cast<size_t>(std::string::npos);
							}
							else
							{
								_vLines[i] = _vLines[i].substr(0, commentIndex);
								_vLines[i] = _vLines[i].substr(0, _vLines[i].find_last_not_of(' ') + 1);
								commentIndex = static_cast<size_t>(std::string::npos);
							}
						}
						else
						{
							_vLines[i] = _vLines[i].substr(0, commentIndex);
							_vLines[i] = _vLines[i].substr(0, _vLines[i].find_last_not_of(' ') + 1);
							commentIndex = static_cast<size_t>(std::string::npos);
						}
					}
				}
			}
		}
	}

	//	Parse
	std::string section="null";
	for(size_t i = 0; i < _vLines.size(); ++i)
	{
		if(_vLines[i][0]=='[')
		{
			size_t endBrace = static_cast<unsigned int>(_vLines[i].find_last_of("]"));
			if(endBrace!=std::string::npos)
			{
				if(endBrace>1)
				{
					section = _vLines[i].substr(1,endBrace-1);
				}
			}
		}
		for(size_t j = 0; j < _vLines[i].size(); ++j)
		{
			if(_vLines[i][j]=='=')
			{
				if(j+1<_vLines[i].size())
				{
					std::string left = _vLines[i].substr(0,j);
					int il = static_cast<int>(left.find_first_not_of(' '));
					int ir = static_cast<int>(left.find_last_not_of(' '));
					left = left.substr(il, ir+1);
					//	Don't include the delimiter on the right side
					std::string right = _vLines[i].substr(j+1,_vLines[i].size()-j-1);
					il = static_cast<int>(right.find_last_of(' '));
					ir = static_cast<int>(right.find_last_not_of(' '));
					right = right.substr(il+1, ir);
					mImpl->mapINIData[section].insert(make_pair(left,right));
				}
			}
		}
	}
	return INI_LOAD_SUCCESS;
}

void INI::setString(const char* aSection, const char* aVariable, const char* aValue)
{
	mImpl->mapINIData[aSection][aVariable] = aValue;
}

void INI::setInt(const char* aSection, const char* aVariable, const int aValue)
{
	mImpl->mapINIData[aSection][aVariable] = std::to_string(aValue);
}

void INI::setBool(const char* aSection, const char* aVariable, const bool aValue)
{
	mImpl->mapINIData[aSection][aVariable] = aValue ? "true" : "false";
}
void INI::setDouble(const char* aSection, const char* aVariable, const double aValue)
{
	mImpl->mapINIData[aSection][aVariable] = std::to_string(aValue);
}

void INI::setFloat(const char* aSection, const char* aVariable, const float aValue)
{
	mImpl->mapINIData[aSection][aVariable] = std::to_string(aValue);
}

const int INI::save()
{
	std::map<std::string, std::map<std::string, std::string>>::iterator outterIt;
	std::map<std::string, std::string>::iterator innerIt;

	std::ofstream iniFile;
	iniFile.open(mImpl->path);
	if (iniFile.is_open())
	{
		for (outterIt = mImpl->mapINIData.begin(); outterIt != mImpl->mapINIData.end(); ++outterIt)
		{
			iniFile << "[" << outterIt->first << "]" << std::endl;

			for (innerIt = outterIt->second.begin(); innerIt != outterIt->second.end(); ++innerIt)
			{
				iniFile << innerIt->first << " = " << innerIt->second << std::endl;
			}

			iniFile << std::endl;
		}
	}

	iniFile.close();

	return INI_SAVE_SUCCESS;
}

bool INI::exists(const char* aSection, const char* aVariable)
{
	bool section_exists = mImpl->mapINIData.find(aSection) != mImpl->mapINIData.end();
	bool variable_exists = false;

	if (section_exists)
	{
		auto secn = mImpl->mapINIData.at(aSection);

		variable_exists = secn.find(aVariable) != secn.end();
	}

	return section_exists && variable_exists;
}
