#include "EngineContext.h"

#include "Application.h"

EngineContext::EngineContext(GameEngine* const aEngine, Application* const aApplication) :
							engine(aEngine), 
							application(aApplication)
{
}
