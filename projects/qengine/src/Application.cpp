#include "Application.h"
#include "core/qstring.h"
#include "core/QLog.h"

#if defined(_WIN32) || defined(_WIN64)
#include <direct.h>
#else
#include <unistd.h>
#endif

#if !defined(_WIN32) || !defined(_WIN64)
	typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
#endif

#if defined(_WIN32) || defined(_WIN64)
static LRESULT CALLBACK defaultEventCallback(HWND aHWnd, UINT aUMsg, WPARAM aWParam, LPARAM aLParam)
{
	switch(aUMsg)
	{	
		case WM_SYSCOMMAND:
		{
			if(aWParam == SC_SCREENSAVE || aWParam == SC_MONITORPOWER)
				return 0;
			
			break;
		}
		
		case WM_DESTROY:
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}

		default:
		{
			return DefWindowProc(aHWnd, aUMsg, aWParam, aLParam);
		}	
	}
	
	return DefWindowProc(aHWnd, aUMsg, aWParam, aLParam);
}
#endif

ApplicationWindow::ApplicationWindow()
{
	mIsQuitRequested = false;
	curMouseX = 0;
	curMouseY = 0;
}

#if defined(_WIN32) || defined(_WIN64)
ApplicationWindow::ApplicationWindow(INI* aIni, GameEngineImpl* aGameEngine, WNDPROC aWndProc)
{
	mIsQuitRequested = false;
	mCursorDisplayCount = 0;
	loadFromINI(aIni, aGameEngine, aWndProc);
	curMouseX = curMouseY = 0;
}
#else
ApplicationWindow::ApplicationWindow(INI* aIni, GameEngineImpl* aGameEngine)
{
	mIsQuitRequested = false;
	mCursorDisplayCount = 0;
	loadFromINI(aIni, aGameEngine);
	curMouseX = curMouseY = 0;
}
#endif

ApplicationWindow::~ApplicationWindow()
{
	#if defined(_WIN32)
		DestroyWindow(mHandle);
		mInstance = NULL;
	#else
	    if(mWin.context)
	    {
//			if (!glXMakeCurrent(mWin.display, None, NULL))
//			{
//				printf("Could not free OpenGL context\n");
//			}

			glXDestroyContext(mWin.display, mWin.context);
			mWin.context = NULL;
	    }

	    if(mFullscreen)
	    {
			XF86VidModeSwitchToMode(mWin.display, mWin.screen, &mWin.vidMode);
			XF86VidModeSetViewPort(mWin.display, mWin.screen, 0, 0);
	    }

	    XCloseDisplay(mWin.display);
	#endif	
}

void ApplicationWindow::bufferSwap()
{
	#if defined(_WIN32)
		SwapBuffers(mHDC);
	#else
		glXSwapBuffers(mWin.display, mWin.window);
	#endif
}

void ApplicationWindow::hideCursor()
{
	#if defined(_WIN32)
		CURSORINFO cursorInfo = {0};
		cursorInfo.cbSize = sizeof(cursorInfo);

		GetCursorInfo(&cursorInfo);

		if (cursorInfo.flags == 0x00000001 || this->mCursorDisplayCount >= 0)
		{
			while (this->mCursorDisplayCount >= 0)
				this->mCursorDisplayCount = ::ShowCursor(false);
		}
	#else
		Cursor invisibleCursor;
		Pixmap bitmapNoData;
		XColor black;
		static char noData[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
		black.red = black.green = black.blue = 0;

		bitmapNoData = XCreateBitmapFromData(mWin.display, mWin.window, noData, 8, 8);
		invisibleCursor = XCreatePixmapCursor(mWin.display, bitmapNoData, bitmapNoData, &black, &black, 0, 0);
		XDefineCursor(mWin.display, mWin.window, invisibleCursor);
		XFreeCursor(mWin.display, invisibleCursor);
	#endif		
}

void ApplicationWindow::showCursor()
{
	#if defined(_WIN32)
		CURSORINFO  cursorInfo = { 0 };
		cursorInfo.cbSize = sizeof(cursorInfo);

		GetCursorInfo(&cursorInfo);

		if (cursorInfo.flags <= 0x00000000 || this->mCursorDisplayCount < 0)
		{
			while (this->mCursorDisplayCount < 0)
				this->mCursorDisplayCount = ::ShowCursor(true);
		}
	#else
		Cursor cursor;
		cursor = XCreateFontCursor(mWin.display, XC_left_ptr);
		XDefineCursor(mWin.display, mWin.window, cursor);
		XFreeCursor(mWin.display, cursor);
	#endif
}

ApplicationWindow* Application::getApplicationWindow()
{
	return mAppWindow;
}

bool ApplicationWindow::peekEvents()
{
#if defined(_WIN32) || defined(_WIN64)
    MSG msg;
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
		if (msg.message == WM_DESTROY)
		{
			PostQuitMessage(0);
		}
		else if (msg.message == WM_QUIT)
        {
			mIsQuitRequested = true;
        }

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
#else
	int nMessages = XPending(mWin.display);
	int mouseMessages = 0;
	float mouseX, mouseY;
	mouseX = mouseY = 0.0f;
    if(nMessages > 0)
    {
			for(uint32_t i = 0; i < nMessages; ++i)
			{
    	    	XPeekEvent(mWin.display, &mEvent);
//						XNextEvent(mWin.display, &mEvent);
    	    	switch(mEvent.type)
    	    	{
								case MotionNotify:
								{
										int mx = mEvent.xmotion.x;
										int my = mEvent.xmotion.y;
										mouseX += mx;
										mouseY += my;
										curMouseX = mx;
										curMouseY = my;
//										QUADRION_TRACE("mx: {0}  my:{1}", curMouseX, curMouseY);
										mouseMessages++;
										break;
								}

    	        	case DestroyNotify:
								{
    	            	mIsQuitRequested = true;
										break;
								}

    	        	case ClientMessage:
								{
    	           		if(*XGetAtomName(mWin.display, mEvent.xclient.message_type) == *"WM_PROTOCOLS")
    	                	mIsQuitRequested = true;
										break;
								}
    	    	}
			}
  }
#endif

//	printf("messages: %d    mouse: %d\n", nMessages, mouseMessages);

//	if(mouseMessages > 0)
//	{
//			curMouseX = static_cast<int>(mouseX / static_cast<float>(mouseMessages));
//			curMouseY = static_cast<int>(mouseY / static_cast<float>(mouseMessages));
//	}
	return true;
}

bool ApplicationWindow::isQuitRequested()
{
	return mIsQuitRequested;
}

void ApplicationWindow::requestQuit()
{
	mIsQuitRequested = true;
}

void ApplicationWindow::getCursorPosition(int& aX, int& aY)
{
	aX = curMouseX;
	aY = curMouseY;
}

#if defined(_WIN32) || defined(_WIN64)
std::wstring s2ws(const std::string& aS)
{
	int len;
	int slength = static_cast<int>(aS.length() + 1);
	len = MultiByteToWideChar(CP_ACP, 0, aS.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, aS.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}
#endif

#if defined(_WIN32) || defined(_WIN64)
void ApplicationWindow::loadFromINI(INI* aIni, GameEngineImpl* aGameEngine, WNDPROC aWndProc)
{
	QLog::construct();

	GLuint pixFormat;

	// Populate window class
	WNDCLASS wc;
	DWORD dwExStyle;
	DWORD dwStyle;
	RECT windowRect;
	windowRect.left = static_cast<long>(0);
	windowRect.right = static_cast<long>(aIni->getInt("video", "res_x", 800));
	windowRect.top = static_cast<long>(0);
	windowRect.bottom = static_cast<long>(aIni->getInt("video", "res_y", 600));
	
	WNDPROC cbFunc = ( aWndProc ) ? aWndProc : static_cast<WNDPROC>(defaultEventCallback);

	mInstance = GetModuleHandle(NULL);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
	wc.lpfnWndProc = cbFunc;//(WNDPROC)DefaultEventCallback;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = mInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = L"QENGINE";
	
	// Register window class
	if (!RegisterClass(&wc))
		QUADRION_INTERNAL_CRITICAL("Failed to register windows class");
	
	// set DEVMODE settings for fullscreen
	if(aIni->getBool("video", "fullscreen", false))
	{
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = aIni->getInt("video", "res_x");
		dmScreenSettings.dmPelsHeight = aIni->getInt("video", "res_y");
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		
		if(ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			bool INI_FULLSCREEN = aIni->getBool("video", "fullscreen");

			if (aIni->getBool("video", "fullscreen"))
				QUADRION_INTERNAL_CRITICAL("Program will exit");
			INI_FULLSCREEN = !INI_FULLSCREEN;

			aIni->setBool("video", "fullscreen", INI_FULLSCREEN);
		}

		mFullscreen = true;	
	}
	
	if (aIni->getBool("video", "fullscreen"))
	{
		dwExStyle = WS_EX_APPWINDOW;
		dwStyle = WS_POPUP;
	}
	
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = WS_OVERLAPPED | \
			WS_CAPTION | \
			WS_SYSMENU | \
			WS_MINIMIZEBOX;
	}
	
	// size window and _create a window object
	AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);
	
	std::string sname = aIni->getString("player", "name", "Player");

	std::wstring wsname = s2ws(sname);

	LPCWSTR name = wsname.c_str();

	if(!(mHandle = CreateWindowEx(dwExStyle, L"QENGINE", name, dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
							     0, 0, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top,
							     NULL, NULL, mInstance, NULL)))
	{
		QUADRION_INTERNAL_CRITICAL("System could not create window");
	}

	SetWindowLongPtr(mHandle, GWLP_USERDATA, (LONG_PTR)(aGameEngine));

	static PIXELFORMATDESCRIPTOR pix_format_desc =
	{
		sizeof(PIXELFORMATDESCRIPTOR), 1, PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
		32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 8, 0, PFD_MAIN_PLANE, 0, 0, 0, 0
	};

	if (!(mHDC = GetDC(mHandle)))
		return;

	if (!(pixFormat = ChoosePixelFormat(mHDC, &pix_format_desc)))
		return;

	if (!SetPixelFormat(mHDC, pixFormat, &pix_format_desc))
		return;

//	if (!(mHRC = wglCreateContext(mHDC)))
//		return;

	const int attribList[7] = { WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
								WGL_CONTEXT_MINOR_VERSION_ARB, 2, 
								WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
								0};

	HGLRC fuckThisShit = wglCreateContext(mHDC);
	if (!fuckThisShit)
		return;

	if (!wglMakeCurrent(mHDC, fuckThisShit))
		return;

	initExtensions();

	mHRC = wglCreateContextAttribsARB(mHDC, 0, attribList);
	if (!mHRC)
		return;

	if (!wglMakeCurrent(mHDC, mHRC))
		return;
	

	ShowWindow(mHandle, SW_SHOW);
	SetForegroundWindow(mHandle);
	SetFocus(mHandle);
	
	glViewport(0, 0, aIni->getInt("video", "res_x"), aIni->getInt("video", "res_y"));
	
	RECT winRect;
	GetClientRect(mHandle, &winRect);
	mWinWidth = winRect.right;
	mWinHeight = winRect.bottom;

	aIni->save();
}

#else
static bool ctxErrorOccurred = false;
static int ctxErrorHandler(Display* dpy, XErrorEvent* ev)
{
	ctxErrorOccurred = true;
	return 0;
}

void ApplicationWindow::loadFromINI(INI* aIni, GameEngineImpl* aGameEngine)
{
	QLog::construct();
	mWinWidth = aIni->getInt("video", "res_x", 800);
    mWinHeight = aIni->getInt("video", "res_y", 600);

	const char* disp = ":1";
	Display* display = XOpenDisplay(disp);
	if(!display)
	{
		QUADRION_CRITICAL("Could not create X Window");
		exit(1);
	}

	static int visAttribs[] =
	{
		GLX_X_RENDERABLE, 		True,
		GLX_DRAWABLE_TYPE,		GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,		GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,		GLX_TRUE_COLOR,
		GLX_RED_SIZE,			8,
		GLX_GREEN_SIZE,			8,
		GLX_BLUE_SIZE,			8,
		GLX_ALPHA_SIZE,			8,
		GLX_DEPTH_SIZE,			24,
		GLX_STENCIL_SIZE,		8,
		GLX_DOUBLEBUFFER,		True,
		None
	};

	int glx_major, glx_minor;
	if(!glXQueryVersion(display, &glx_major, &glx_minor) ||
		((glx_major == 1) && (glx_minor < 3)) || (glx_major < 1))
	{
		QUADRION_CRITICAL("GLX Out of date, could not init");
		exit(1);
	}

	int fbCount = 0;
	GLXFBConfig* fbc = glXChooseFBConfig(display, DefaultScreen(display), visAttribs, &fbCount);
	if(!fbc)
	{
		QUADRION_CRITICAL("Could not locate FB Config");
		exit(1);	
	}

	int best_fbc = -1, worst_fbc = -1, best_num_samp = -1, worst_num_samp = 999;
	int i;
	for(i = 0; i < fbCount; i++)
	{
		XVisualInfo* vi = glXGetVisualFromFBConfig(display, fbc[i]);
		if(vi)
		{
			int samp_buf, samples;
			glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
			glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLES, &samples);

			if((best_fbc < 0 || samp_buf) && (samples > best_num_samp))
				best_fbc = i, best_num_samp = samples;
			
			if(worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
				worst_fbc = i, worst_num_samp = samples;
		}

		XFree(vi);
	}

	GLXFBConfig bestFbc = fbc[best_fbc];
	XFree(fbc);

	XVisualInfo* vi = glXGetVisualFromFBConfig(display, bestFbc);

	XSetWindowAttributes swa;
	Colormap cmap;
	swa.colormap = cmap = XCreateColormap(display, RootWindow(display, vi->screen), vi->visual, AllocNone);
	swa.background_pixmap = None;
	swa.border_pixel = 0;
	swa.event_mask = StructureNotifyMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask |
					 PointerMotionMask;

	Window win = XCreateWindow(display, RootWindow(display, vi->screen),
								0, 0, mWinWidth, mWinHeight, 0, vi->depth, InputOutput,
								vi->visual, CWBorderPixel | CWColormap | CWEventMask, &swa);

	if(!win)
	{
		QUADRION_CRITICAL("Could not create window");
		exit(1);
	}

	XFree(vi);

	XStoreName(display, win, "GL Window");
	XMapWindow(display, win);

	const char* glxExtensionsStr = glXQueryExtensionsString(display, DefaultScreen(display));
	glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((const GLubyte*)"glXCreateContextAttribsARB");

	GLXContext ctx = 0;
	ctxErrorOccurred = false;
	int(*oldHandler)(Display*, XErrorEvent*) = XSetErrorHandler(&ctxErrorHandler);

	if(!isExtensionSupported("GLX_ARB_create_context", glxExtensionsStr))
	{
		QUADRION_ERROR("Could not create core context, falling back");
		ctx = glXCreateNewContext(display, bestFbc, GLX_RGBA_TYPE, 0, True);
	}

	else
	{
		int ctxAttribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			None
		};

		ctx = glXCreateContextAttribsARB(display, bestFbc, 0, True, ctxAttribs);
		XSync(display, False);
		if(!ctxErrorOccurred && ctx)
			QUADRION_INFO("Created core 3.2 context");
		else
		{
			ctxAttribs[1] = 1;
			ctxAttribs[3] = 0;
			ctxErrorOccurred = false;
			ctx = glXCreateContextAttribsARB(display, bestFbc, 0, True, ctxAttribs);
		}
	}

	XSync(display, False);
	XSetErrorHandler(oldHandler);

	if(ctxErrorOccurred || !ctx)
	{
		QUADRION_CRITICAL("Could not create contetx");
		exit(1);
	}

	if(!glXIsDirect(display, ctx))
		QUADRION_INFO("Indirect rendering context created");
	else
		QUADRION_INFO("Direct context created");



	if(!glXMakeCurrent(display, win, ctx))
	{
		QUADRION_CRITICAL("Could not make the context current");
		exit(1);
	}

	QUADRION_INFO("initExtensions");
	initExtensions();

//	fprintf(f, "done initExtensions\n");
//	fclose(f);

	mWin.window = win;
	mWin.display = display;
	mWin.context = ctx;

	QUADRION_INFO("glXSwapIntervalEXT");
	glXSwapIntervalEXT(mWin.display, mWin.window, 0);

	QUADRION_TRACE("Finished app init");

//     mWinWidth = aIni->getInt("video", "res_x", 800);
//     mWinHeight = aIni->getInt("video", "res_y", 600);

// 	XVisualInfo* VisInfo;
// 	Colormap colourMap;
// 	int h, w;
// 	int glxMajor, glxMinor;
// 	int vidMajor, vidMinor;
// 	XF86VidModeModeInfo** modes;

// 	int NumModes;
// 	Atom wmDelete;
// 	Window winDummy;
// 	unsigned int borderDummy;

// 	mFullscreen = aIni->getBool("video", "fullscreen", false);
// 	//win.fullscreen = fullscreen;
// 	mWin.title = const_cast<char*>(std::string("SMSENGINE").c_str());
// 	int curMode = 0;

// 	char* disp = ":1";
// 	mWin.display = XOpenDisplay(disp);
// 	mWin.screen = DefaultScreen(mWin.display);
// 	XF86VidModeQueryVersion(mWin.display, &vidMajor, &vidMinor);
// 	printf("XF86 version %d.%d\n", vidMajor, vidMinor);

// 	XF86VidModeGetAllModeLines(mWin.display, mWin.screen, &NumModes, &modes);
// 	mWin.vidMode = *modes[0];

// 	int i;
// 	for(i = 0; i < NumModes; ++i)
// 	{
//         if((modes[i]->hdisplay == mWinWidth) && (modes[i]->vdisplay == mWinHeight))
//         {
//             curMode = i;
//             break;
//         }
// 	}

// 	VisInfo = glXChooseVisual(mWin.display, mWin.screen, mAttribDbl);
// 	if(VisInfo == NULL)
// 	{
//         VisInfo = glXChooseVisual(mWin.display, mWin.screen, mAttribSgl);
//         printf("Can only use single buffering\n");
// 	}

	
// 	glXQueryVersion(mWin.display, &glxMajor, &glxMinor);
// 	printf("GLX version %d.%d\n", glxMajor, glxMinor);

// 	// Create context
// 	int nConfigs = 0;
// 	GLXFBConfig* fbConfig = glXChooseFBConfig(mWin.display, mWin.screen, mAttribDbl, &nConfigs);
// 	GLXFBConfig selectedFBConfig = fbConfig[1];

// //	VisInfo = glXGetVisualFromFBConfig(mWin.display, selectedFBConfig);
// //	if(!VisInfo)
// //	{
// //		printf("VisInfo not found\n");
// //	}


// 	const int attribList[7] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
// 								GLX_CONTEXT_MINOR_VERSION_ARB, 2, 
// 								GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
// 								0};
// //	mWin.context = glXCreateContextAttribsARB(mWin.display, fbConfig[0], NULL, True, attribList);
// 	mWin.context = glXCreateContext(mWin.display, VisInfo, 0, GL_TRUE);


// 	colourMap = XCreateColormap(mWin.display, RootWindow(mWin.display, VisInfo->screen), VisInfo->visual, AllocNone);
// 	mWin.attrib.colormap = colourMap;
// 	mWin.attrib.border_pixel = 0;

// 	if(mFullscreen)
// 	{
//         XF86VidModeSwitchToMode(mWin.display, mWin.screen, modes[curMode]);
//         XF86VidModeSetViewPort(mWin.display, mWin.screen, 0, 0);
//         w = modes[curMode]->hdisplay;
//         h = modes[curMode]->vdisplay;
//         XFree(modes);
// 		mWin.attrib.override_redirect = True;
// 		mWin.attrib.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask | KeyReleaseMask | ButtonReleaseMask | PointerMotionMask;
// 		mWin.window = XCreateWindow(mWin.display, RootWindow(mWin.display, VisInfo->screen),
//                                    0, 0, w, h, 0, VisInfo->depth, InputOutput, VisInfo->visual,
//                                    CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect, &mWin.attrib);
//         XWarpPointer(mWin.display, None, mWin.window, 0, 0, 0, 0, 0, 0);
//         XMapRaised(mWin.display, mWin.window);
//         XGrabKeyboard(mWin.display, mWin.window, True, GrabModeAsync, GrabModeAsync, CurrentTime);
//         XGrabPointer(mWin.display, mWin.window, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, mWin.window, None, CurrentTime);
// 	}

// 	else
// 	{
// 		mWin.attrib.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask | KeyReleaseMask | PointerMotionMask | ButtonReleaseMask;
// 		mWin.window = XCreateSimpleWindow(mWin.display, RootWindow(mWin.display, VisInfo->screen), 0,0, mWinWidth, mWinHeight, 0,0,0);
// //        win.window = XCreateWindow(win.display, RootWindow(win.display, VisInfo->screen),
// //                                   0, 0, winWidth, winHeight, 0, VisInfo->depth, InputOutput, VisInfo->visual,
// //                                   CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect, &win.attrib);
//         wmDelete = XInternAtom(mWin.display, "WM_DELETE_WINDOW", True);
//         XSetWMProtocols(mWin.display, mWin.window, &wmDelete, 1);
//         XSetStandardProperties(mWin.display, mWin.window, mWin.title, mWin.title, None, NULL, 0, NULL);

// 		// set WM Hints to avoid issues w/ auto resizing on 21:9 and ultrawide displays //
// 		// X11 will attempt to do a best-fit, which is inappropriate //
// 		XSizeHints sh;
// 		sh.width = sh.min_width = mWinWidth;
// 		sh.height = sh.min_height = mWinHeight;
// 		sh.flags = PSize | PMinSize;
// 		XSetWMNormalHints(mWin.display, mWin.window, &sh);

//         XMapRaised(mWin.display, mWin.window);
// 		XSelectInput(mWin.display, mWin.window, mWin.attrib.event_mask);
// 	}

	

// 	int aX = 0;
// 	int aY = 0;
// 	unsigned int ww = mWinWidth;
// 	unsigned int wh = mWinHeight;
// 	unsigned int winDepth;

// 	bool mcRes = false;
// 	mcRes = glXMakeCurrent(mWin.display, mWin.window, mWin.context);
// 	initExtensions();

	

// 	mWin.context = glXCreateContextAttribsARB(mWin.display, selectedFBConfig, NULL, True, attribList);
// 	VisInfo = glXGetVisualFromFBConfig(mWin.display, selectedFBConfig);
// 	if(!VisInfo)
// 	{
// 		printf("VisInfo not found\n");
// 	}
// 	//////////////////////////////////////////////////////////////
// 	///////////////////////////////////////////////////////////////	
			
// 		mWin.attrib.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask | KeyReleaseMask | PointerMotionMask | ButtonReleaseMask;
// 		mWin.window = XCreateSimpleWindow(mWin.display, RootWindow(mWin.display, VisInfo->screen), 0,0, mWinWidth, mWinHeight, 0,0,0);
// //        win.window = XCreateWindow(win.display, RootWindow(win.display, VisInfo->screen),
// //                                   0, 0, winWidth, winHeight, 0, VisInfo->depth, InputOutput, VisInfo->visual,
// //                                   CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect, &win.attrib);
//         wmDelete = XInternAtom(mWin.display, "WM_DELETE_WINDOW", True);
//         XSetWMProtocols(mWin.display, mWin.window, &wmDelete, 1);
//         XSetStandardProperties(mWin.display, mWin.window, mWin.title, mWin.title, None, NULL, 0, NULL);

// 		// set WM Hints to avoid issues w/ auto resizing on 21:9 and ultrawide displays //
// 		// X11 will attempt to do a best-fit, which is inappropriate //
// 		XSizeHints sh;
// 		sh.width = sh.min_width = mWinWidth;
// 		sh.height = sh.min_height = mWinHeight;
// 		sh.flags = PSize | PMinSize;
// 		XSetWMNormalHints(mWin.display, mWin.window, &sh);

//         XMapRaised(mWin.display, mWin.window);
// 		XSelectInput(mWin.display, mWin.window, mWin.attrib.event_mask);
// 	//////////////////////////////////////////////////////////////////////////////
// 	//////////////////////////////////////////////////////////////////////////////

// 	mcRes = glXMakeCurrent(mWin.display, mWin.window, mWin.context);

// //	glXMakeCurrent(mWin.display, mWin.window, mWin.context);
// //	initExtensions();
// 	XGetGeometry(mWin.display, mWin.window, &winDummy, &aX, &aY,
//                  &ww, &wh, &borderDummy, &winDepth);

// 	glXSwapIntervalEXT(mWin.display, mWin.window, 0);
}
#endif

Application::Application()
{
	mAppINI = nullptr;
	loadDefaults();
}

#if defined(_WIN32) || defined(_WIN64)
Application::Application(const char* aFileName, GameEngineImpl* aGameEngine, WNDPROC aWndProc)
{
	mAppINI = new INI();
	mAppWindow = nullptr;

	if( aFileName )
	{
		mAppINI->parseINI(aFileName);
		mAppWindow = new ApplicationWindow(mAppINI, aGameEngine, aWndProc );
	}
	else
		loadDefaults();
}
#else
Application::Application(const char* aFileName, GameEngineImpl* aGameEngine)
{
	mAppINI = new INI();
    if(aFileName)
    {
		mAppINI->parseINI(aFileName);
		mAppWindow = new ApplicationWindow(mAppINI, aGameEngine);

    }
    else
        loadDefaults();
}

#endif

Application::~Application()
{
	delete mAppINI;
	mAppINI = nullptr;
	delete mAppWindow;
	mAppWindow = nullptr;
}

void Application::loadDefaults()
{
	mAppWindow = new ApplicationWindow();
}

INI* const Application::getApplicationINI()
{
	return mAppINI;
}

int Application::getWindowWidth()
{
	if (mAppWindow)
	{
		return mAppWindow->getWidth();
	}
	return 0;
}

int Application::getWindowHeight()
{
	if (mAppWindow)
		return mAppWindow->getHeight();
	else
		return 0;
}

void Application::getMousePosition(int& aX, int& aY)
{
#if defined(_WIN32) || defined(_WIN64)
	POINT pt;
	if( GetCursorPos( &pt ) )
	{
		HWND hWnd = mAppWindow->getWindowHandle();
		ScreenToClient(hWnd, &pt);

		aX = pt.x;
		aY = pt.y;
	}
#else
    	Window rootRet, childRet;
    	int rootX, rootY, winX, winY;
    	unsigned int maskRet;

		mAppWindow->getCursorPosition(aX, aY);
//    	XQueryPointer(mAppWindow->getDisplay(), mAppWindow->getWindow(), &rootRet, &childRet, &rootX, &rootY, &winX, &winY, &maskRet);
//    	aX = winX;
//    	aY = winY;
#endif
}

void Application::setMousePosition(int aX, int aY)
{
#if defined(_WIN32) || defined(_WIN64)
	HWND hWnd = mAppWindow->getWindowHandle();

	POINT pt;
	pt.x = static_cast<LONG>(aX);
	pt.y = static_cast<LONG>(aY);

	ClientToScreen(hWnd, &pt);

	SetCursorPos(static_cast<int>(pt.x), static_cast<int>(pt.y));
#else
	XWarpPointer(mAppWindow->getDisplay(), mAppWindow->getWindow(), mAppWindow->getWindow(), 0, 0,
		getWindowWidth(), getWindowHeight(), aX, aY);
#endif
}

vec2<int32_t> Application::getWindowCenter()
{
    WindowLocation size = getWindowDimensions();

    int dw = (size.left + size.right) / 2;
    int dh = (size.top + size.bottom) / 2;

    return vec2<int32_t>(dw, dh);
}

bool Application::peekEvents()
{
	return mAppWindow->peekEvents();
}

#if defined(_WIN32) || defined(_WIN64)
HWND ApplicationWindow::getWindowHandle()
{
	return mHandle;
}
#else
unsigned int ApplicationWindow::getWindowHandle()
{
	return 0;
}
#endif

WindowLocation ApplicationWindow::getWindowDimensions()
{
	WindowLocation ret;

#if defined(_WIN32) || defined(_WIN64)
	RECT rcClient;

	GetClientRect(mHandle, &rcClient);

	ret.left = rcClient.left;
	ret.right = rcClient.right;
	ret.bottom = rcClient.bottom;
	ret.top = rcClient.top;

#else
    	XWindowAttributes xwa;
    	XGetWindowAttributes(mWin.display, mWin.window, &xwa);
    	ret.left = xwa.x;
    	ret.right = xwa.x + xwa.width;
    	ret.bottom = xwa.y + xwa.height;
    	ret.top = xwa.y;

		if ((ret.right - ret.left) > mWinWidth)
			ret.right = ret.left + mWinWidth;

		if ((ret.bottom - ret.top) > mWinHeight)
			ret.bottom = ret.top + mWinHeight;
#endif
	
	return ret;
}

WindowLocation ApplicationWindow::getWindowPosition()
{
	WindowLocation ret;

#if defined(_WIN32) || defined(_WIN64)
	WINDOWPLACEMENT wp;
	GetWindowPlacement(mHandle, &wp);

	ret.left = wp.rcNormalPosition.left;
	ret.right = wp.rcNormalPosition.right;
	ret.bottom = wp.rcNormalPosition.bottom;
	ret.top = wp.rcNormalPosition.top;
#else
	XWindowAttributes xwa;
	XGetWindowAttributes(mWin.display, mWin.window, &xwa);
	ret.left = xwa.x;
	ret.right = xwa.x + xwa.width;
	ret.bottom = xwa.y + xwa.height;
	ret.top = xwa.y;
#endif

	return ret;
}

bool Application::isQuitRequested()
{
	return mAppWindow->isQuitRequested();
}

void Application::requestQuit()
{
	mAppWindow->requestQuit();
}

#if !defined(_WIN32) || !defined(_WIN64)
Display* Application::getDisplay() 
{ 
	return mAppWindow->getDisplay(); 
}

Window Application::getWindow() 
{ 
	return mAppWindow->getWindow();
}	
#endif

QString getCWD()
{
	QString result;
	char* buffer;

#if defined(_WIN32) || defined(_WIN64)
	if ((buffer = _getcwd(nullptr, 0)) == nullptr)
		result = "";
	else
	{
		result = static_cast<const char*>(buffer);
		free(buffer);
	}

#else

	if ((buffer = getcwd(nullptr, 0)) == nullptr)
		result = "";
	else
	{
		result = static_cast<const char*>(buffer);
		free(buffer);
	}
#endif

	return result;
}
