#include <stack>
#include "render/MatrixStack.h"


struct MatrixStackImpl
{
	std::stack<mat4<float>> mStack;	
};


MatrixStack::MatrixStack()
{
	mImpl = new MatrixStackImpl;	
}

MatrixStack::~MatrixStack() 
{
	if(mImpl)
		delete mImpl;	
}

void MatrixStack::pushMatrix(const mat4<float>& aMat) const
{
	if(mImpl->mStack.empty())
		mImpl->mStack.push(aMat);

	else
	{
		mat4<float> repl(mImpl->mStack.top());
		mImpl->mStack.push(aMat);
	}
}



mat4<float> MatrixStack::top() const
{
	return mImpl->mStack.top();
}

mat4<float> MatrixStack::popMatrix() const
{
	mat4<float> ret;
	if(mImpl->mStack.empty())
	{
		ret.loadIdentity();
		return ret;
	}

	else
	{
		ret = mImpl->mStack.top();
		mImpl->mStack.pop();

		return ret;
	}
}

void MatrixStack::multiplyMatrix(const mat4<float>& aMat) const
{
	if(mImpl->mStack.empty())
		return;
	
	const mat4<float> top = mImpl->mStack.top();
	const mat4<float> mult = top * aMat;

	mImpl->mStack.pop();
	mImpl->mStack.push(mult);
}
