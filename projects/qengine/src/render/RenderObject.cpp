#include "render/RenderObject.h"
#include "assets/MeshAsset.h"
#include "assets/AssetManager.h"
#include "core/QLog.h"


RenderObject::RenderObject()
{
    mWorldTransform.loadIdentity();
	mIsGuiSelected = false;
	mId = 0;
	mName = nullptr;
}

RenderObject::RenderObject(const char* aMeshAssetName, const char* aName, uint32_t aId)
{
	std::shared_ptr<MeshAsset> meshAsset = AssetManager::instance().get<MeshAsset>(aMeshAssetName);
	mMeshes = meshAsset->getMeshes();
    mName = aName;
	mIsGuiSelected = false;
	mWorldTransform.loadIdentity();
	mId = aId;
}

RenderObject::RenderObject(const char* aName, std::vector<std::shared_ptr<MeshNode>> aMeshes, uint32_t aId)
{
	mMeshes = aMeshes;
	mName = aName;
	mIsGuiSelected = false;
	mWorldTransform.loadIdentity();
	mId = aId;
}

RenderObject::~RenderObject()
{

}

// Change to take translation, scale, rotation
void RenderObject::setWorldTransform(const mat4<float>& aMatrix)
{
    mWorldTransform = aMatrix;
	mat4<float> NT, S, T, DT; 

	size_t nMeshes = mMeshes.size();
	for (size_t i = 0; i < nMeshes; i++)
	{
		vec3<float> pos = mMeshes[i]->position;
		vec3<float> scale = mMeshes[i]->scale;
		mat4<float> rot = mMeshes[i]->rotation;
		vec3<float> center = mMeshes[i]->center;
		rot.transpose();
		
		NT.loadTranslation(vec3<float>(-center.x, -center.y, -center.z));
		S.loadScale(scale);
		T.loadTranslation(center);
		DT.loadTranslation(pos);

		mMeshes[i]->transform = DT * rot;
	}
}

mat4<float> RenderObject::getWorldTransform()
{
    return mWorldTransform;
}


std::vector<std::shared_ptr<MeshNode>>& RenderObject::getMeshes()
{
	return mMeshes;
}

const char* RenderObject::getName()
{
    return mName;
}

void RenderObject::setGuiSelected(bool aIsSelected)
{
	mIsGuiSelected = aIsSelected;
}

bool RenderObject::isGuiSelected()
{
	return mIsGuiSelected;
}

uint32_t RenderObject::getID()
{
	return mId;
}

void RenderObject::onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods)
{
	if(mIsGuiSelected)
		QUADRION_TRACE("Object ID {0} triggered key event", mId);	
}