#include "render/QGfx.h"
#include "render/Material.h"
#include "core/Timer.h"
#include "core/QLog.h"
#include <algorithm>

#include <unordered_map>

#include "Extensions.h"

#include "StringUtils.h"

#include <map>


Material::Material()
{
	mName = std::string("");
	mMemory = nullptr;
	mUBOHandle = 0;
	mIsDirty = false;
}

void Material::setUniform(const std::string& aName, const void* aData)
{
//	for(auto uniform : mUniforms)
//	{
//		if(uniform->name == aName)
//		{
//			const uint32_t offset = uniform->offset;
//			const uint32_t size = uniform->size;
//
//			memcpy(static_cast<uint8_t*>(mMemory) + static_cast<size_t>(offset), aData, static_cast<size_t>(size));
//
//			mIsDirty = true;
//		}
//	}

	auto iter = mUniforms.find(aName);
	if(iter != mUniforms.end())
	{
		const uint32_t offset = iter->second->offset;
		const uint32_t size = iter->second->size;

		memcpy(static_cast<uint8_t*>(mMemory) + static_cast<size_t>(offset), aData, static_cast<size_t>(size));

		mIsDirty = true;
	}
}

void Material::copy(const void* aData)
{
	memcpy(mMemory, aData, mSizeInBytes);
	mIsDirty = true;
}

UniformBufferElement* const Material::getUniform(const std::string& aName)
{
	auto uniform = mUniforms.find(aName);
	if(uniform == mUniforms.end())
		return nullptr;

	return uniform->second;
}

void Material::addUniforms(const std::vector<UniformBufferElement*>& aUniforms)
{
//	mUniforms = aUniforms;
//	const uint32_t size = mUniforms[mUniforms.size() - 1]->offset;
//	mMemory = malloc(static_cast<size_t>(size));

	if (aUniforms.size() <= 0)
		return;

	uint32_t nUniforms = static_cast<uint32_t>(aUniforms.size());
	const uint32_t size = aUniforms[aUniforms.size() - 1]->offset + aUniforms[aUniforms.size() - 1]->size;
	mMemory = malloc(static_cast<size_t>(size));
	mSizeInBytes = size;
	for(uint32_t i = 0; i < nUniforms; ++i)
	{
		std::string key(aUniforms[i]->name);
		mUniforms.insert(std::make_pair(key, aUniforms[i]));	
	}

	if(mUBOHandle <= 0)
	{
		glGenBuffers(1, &mUBOHandle);
		glBindBuffer(GL_UNIFORM_BUFFER, mUBOHandle);
		glBufferData(GL_UNIFORM_BUFFER, size, nullptr, GL_STATIC_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	mIsDirty = true;
}

void Material::setSize(uint32_t aSizeInBytes)
{
	mSizeInBytes = aSizeInBytes;	
}

void Material::setName(const std::string& aName)
{
	mName = aName;
}

uint32_t Material::getSize()
{
	return mSizeInBytes;
}

std::string Material::getName()
{
	return mName;
}

uint32_t Material::getUBOHandle()
{
	return mUBOHandle;
}

size_t Material::getUniformOffset(const std::string& aName)
{
	auto iter = mUniforms.find(aName);
	if (iter != mUniforms.end())
	{
		return iter->second->offset;	
	}

	return 0;
}

size_t Material::getUniformSize(const std::string& aName)
{
	auto iter = mUniforms.find(aName);
	if (iter != mUniforms.end())
	{
		return iter->second->size;
	}

	return 0;
}

bool Material::isDirty()
{
	return mIsDirty;
}

void Material::bind(Shader* aShader)
{
	uint32_t programID = aShader->getProgramID();
	if(programID <= 0)
		return;

	uint32_t bindPoint = aShader->getUBOBindPoint(mName);
	glBindBufferBase(GL_UNIFORM_BUFFER, bindPoint, mUBOHandle);
	glBindBuffer(GL_UNIFORM_BUFFER, mUBOHandle);

	if(mIsDirty)	
		glBufferData(GL_UNIFORM_BUFFER, mSizeInBytes, mMemory, GL_STATIC_DRAW);		
		
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	const char* mNameCStr = mName.c_str();
//	uint32_t uboIndex = glGetUniformBlockIndex(programID, mNameCStr);
	uint32_t uboIndex = aShader->getUBOBindLocation(mName);

	glUniformBlockBinding(programID, uboIndex, bindPoint);	

	mIsDirty = false;
}


ObjectMaterial::ObjectMaterial(Effect* aEffect, const std::string& aTechName, const std::string& aPassName)
{
	mEffect = aEffect;

	mTechName = aTechName;
	mPassName = aPassName;


//	strcpy((char*)mName, aName);
}

ObjectMaterial::~ObjectMaterial()
{

}

void ObjectMaterial::bind()
{
	const auto mPass = mEffect->getTechnique(mTechName.c_str())->getPass(mPassName.c_str());
	mPass->bind();
	for(const auto &data : mMaterialData)
	{
		MaterialData* d = data.second;

		if (d->count == 1)
		{
			switch (d->type)
			{
				case EShaderDataType::QBool: mPass->getShader()->setUniform1i(data.first, d->b); break;
				case EShaderDataType::Float: mPass->getShader()->setUniform1f(data.first, d->f); break;
				case EShaderDataType::Int: mPass->getShader()->setUniform1i(data.first, d->i); break;
				case EShaderDataType::Vec2: mPass->getShader()->setUniform2f(data.first, d->v2); break;
				case EShaderDataType::Vec3: mPass->getShader()->setUniform3f(data.first, d->v3); break;
				case EShaderDataType::Vec4: mPass->getShader()->setUniform4f(data.first, d->v4); break;
				case EShaderDataType::Mat3: mPass->getShader()->setUniformMat3(data.first, d->m3); break;
				case EShaderDataType::Mat4: mPass->getShader()->setUniformMat4(data.first, d->m4); break;
				case EShaderDataType::Texture: mPass->getShader()->setUniform1i(data.first, d->i); break;
				case EShaderDataType::Cubemap: mPass->getShader()->setUniform1i(data.first, d->i); break;
				case EShaderDataType::TextureBuffer: mPass->getShader()->setUniform1i(data.first, d->i); break;
				default:;
			}
		}
		else
		{
			switch(d->type)
			{
				case EShaderDataType::QBool: break;
				case EShaderDataType::Float: mPass->getShader()->setUniform1fv(data.first, d->fv, d->count); break;
				case EShaderDataType::Int: mPass->getShader()->setUniform1iv(data.first, d->iv, d->count); break;
				case EShaderDataType::Vec2: mPass->getShader()->setUniform2fv(data.first, d->v2f, d->count); break;
				case EShaderDataType::Vec3: break;
				case EShaderDataType::Vec4: mPass->getShader()->setUniform4fv(data.first, d->v4f, d->count); break;
				case EShaderDataType::Mat3: break;
				case EShaderDataType::Mat4: break;
				case EShaderDataType::Texture: break;
				case EShaderDataType::Cubemap: break;
				case EShaderDataType::TextureBuffer: break;
				default: ;
			}
		}
	}
}

void ObjectMaterial::unbind()
{
	const auto mPass = mEffect->getTechnique(mTechName.c_str())->getPass(mPassName.c_str());
	mPass->unbind();
}

Effect* ObjectMaterial::getEffect() const
{
	return mEffect;
}

Pass* ObjectMaterial::getPass() const
{
	return mPass;
}

void ObjectMaterial::setUniform1b(const std::string& aName, bool aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::QBool;
		d->b = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->b = aValue;
	}
}

void ObjectMaterial::setUniform1f(const std::string& aName, float aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Float;
		d->f = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->f = aValue;
	}
}

void ObjectMaterial::setUniform1fv(const std::string& aName, float* aValue, const int32_t aCount)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = aCount;
		d->type = EShaderDataType::Float;
		d->fv = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->fv = aValue;
	}
}

void ObjectMaterial::setUniform1i(const std::string& aName, int32_t aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Int;
		d->i = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->i = aValue;
	}
}

void ObjectMaterial::setUniform1iv(const std::string& aName, int32_t* aValue, const int32_t aCount)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = aCount;
		d->type = EShaderDataType::Int;
		d->iv = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->iv = aValue;
	}
}

void ObjectMaterial::setUniform2f(const std::string& aName, vec2<float>& aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Vec2;
		d->v2 = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->v2 = aValue;
	}
}

void ObjectMaterial::setUniform2fv(const std::string& aName, vec2<float>* aValues, const int32_t aCount)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = aCount;
		d->type = EShaderDataType::Vec2;
		d->v2f = aValues;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->v2f = aValues;
	}
}

void ObjectMaterial::setUniform3f(const std::string& aName, vec3<float>& aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Vec3;
		d->v3 = aValue;
		mMaterialData[aName] = d;
	}
	
	else
	{
		mMaterialData[aName]->v3 = aValue;
	}
}

void ObjectMaterial::setUniform4f(const std::string& aName, vec4<float>& aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Vec4;
		d->v4 = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->v4 = aValue;
	}
}

void ObjectMaterial::setUniform4fv(const std::string& aName, vec4<float>* aValues, const int32_t aCount)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Vec4;
		d->v4f = aValues;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->v4f = aValues;
	}
}

void ObjectMaterial::setUniformMat3(const std::string& aName, mat3<float>& aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Mat3;
		d->m3 = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->m3 = aValue;
	}
}

void ObjectMaterial::setUniformMat4(const std::string& aName, mat4<float>& aValue)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Mat4;
		d->m4 = aValue;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->m4 = aValue;
	}
}

void ObjectMaterial::setTexture(const std::string& aName, uint32_t aTextureId)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Texture;
		d->i = aTextureId;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->i = aTextureId;
	}
}

void ObjectMaterial::setCubemapTexture(const std::string& aName, uint32_t aTextureId)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::Cubemap;
		d->i = aTextureId;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->i = aTextureId;
	}
}

void ObjectMaterial::setTextureBuffer(const std::string& aName, uint32_t aTextureId)
{
	auto loc = mMaterialData.find(aName);
	if (loc == mMaterialData.end())
	{
		MaterialData* d = new MaterialData();
		d->count = 1;
		d->type = EShaderDataType::TextureBuffer;
		d->i = aTextureId;
		mMaterialData[aName] = d;
	}

	else
	{
		mMaterialData[aName]->i = aTextureId;
	}
}


const char* ObjectMaterial::getPassName()
{
	return mPassName.c_str();
}

const char* ObjectMaterial::getTechniqueName()
{
	return mTechName.c_str();
}

const char* ObjectMaterial::getName()
{
	std::string concat(mTechName + mPassName);
	return concat.c_str();
}
