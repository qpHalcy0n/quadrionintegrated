#include "render/CubemapCamera.h"


void GenerateCubemapCamera(vec3<float> aPos, vec3<float> aViewDir, uint32_t aWinX, uint32_t aWinY,
						   CubemapCamera& aCam)
{
	vec3<float> lookAt = aPos + aViewDir;
	float fov = deg2Rad(90.0f);
	float aspect = (float)aWinX / aWinY;

	vec3<float> upVec(0.0f, 1.0f, 0.0f);
	if (aViewDir.y > 0.0f)
		upVec.set(0.0f, 0.0f, 1.0f);
	if (aViewDir.y < 0.0f)
		upVec.set(0.0f, 0.0f, -1.0f);

	aCam.worldPos = aPos;
//	aCam.screenX = aWinX;
//	aCam.screenY = aWinY;
	float width = static_cast<float>(aWinX);
	float height = static_cast<float>(aWinY);
	aCam.viewportDims.set(width, height);
	aCam.viewDir = aViewDir;
	loadRHView(aPos, lookAt, upVec, aCam.viewMat);
	loadRHPerspective(fov, aspect, 1.0f, 1000.0f, aCam.projMat);
}