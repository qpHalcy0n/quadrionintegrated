#include "render/Technique.h"
#include "render/Material.h"

class TechniqueImpl
{
public:

	~TechniqueImpl()
	{
		for(size_t i = 0; i < passes.size(); i++)
		{
			delete passes[i];
		}

		passes.clear();
	}
	std::vector<Pass*> passes;

	std::string name;
	std::string source;

	Effect* effect;

	//		QHandle<QEntityManager> entityManager;

	std::map<std::string, std::vector<UniformBufferElement*>> ubos;
	std::vector<std::string> uboNames;

	std::map<std::string, Material*> materialMap;
};

Technique::Technique(const std::string& aName, Effect* aEffect)
{
	mImpl = new TechniqueImpl();
	mImpl->name = aName;
	mImpl->effect = aEffect;
}

/*
Technique::Technique(const std::string& aName,
						   Effect* aEffect)
{
	mImpl = new TechniqueImpl();
	mImpl->name = aName;
	mImpl->effect = aEffect;
//	mImpl->entityManager = aEntityManager;
}
*/
Technique::~Technique()
{
	delete mImpl;
}

size_t Technique::getNumPasses() const
{
	return mImpl->passes.size();
}

Pass** Technique::getPasses() const
{
	return mImpl->passes.data();
}

Pass* Technique::getPass(const char* aName) const
{
	for (uint32_t i = 0; i < mImpl->passes.size(); i++)
	{
		if (mImpl->passes[i]->getName() == aName)
		{
			return mImpl->passes[i];
		}
	}

	return nullptr;
}

Pass* Technique::getPass(uint32_t aIndex) const
{
	return mImpl->passes[aIndex];
}

void Technique::addPass(Pass* pass)
{
	mImpl->passes.push_back(pass);
}

void Technique::addUBO(const std::string& name, std::vector<UniformBufferElement*> ubo)
{
	if (mImpl->ubos.find(name) == mImpl->ubos.end())
	{
		mImpl->ubos[name] = ubo;
		mImpl->uboNames.push_back(name);
	}
	else
	{
		//trying to add a ubo with the same name
		return;
	}
}

std::map<std::string, std::vector<UniformBufferElement*>> Technique::getUBOs() const
{
	return mImpl->ubos;
}

std::vector<UniformBufferElement*> Technique::getUBO(const std::string& name) const
{
	if (mImpl->ubos.find(name) != mImpl->ubos.end())
	{
		return mImpl->ubos[name];
	}

	assert(false);

	std::vector<UniformBufferElement*> v;
	return v;
}

std::vector<std::string> Technique::getUBONames() const
{
	return mImpl->uboNames;
}

bool Technique::hasUBO(const std::string& name) const
{
	for (const auto& ubo : mImpl->uboNames)
	{
		if (ubo == name)
		{
			return true;
		}
	}

	return false;
}

std::string& Technique::getName() const
{
	return mImpl->name;
}

void Technique::_setName(const std::string& aName) const
{
	mImpl->name = aName;
}

void Technique::_setSource(const std::string& aSource) const
{
	mImpl->source = aSource;
}

void Technique::_addPass(Pass* aPass) const
{
	mImpl->passes.push_back(aPass);
}

Effect* Technique::getEffect() const
{
	return mImpl->effect;
}

void Technique::addMaterial(const std::string& aName, Material* const aMaterial)
{
	if (mImpl->materialMap.find(aName) == mImpl->materialMap.end())
	{
		mImpl->materialMap[aName] = aMaterial;
	}
}

Material* const Technique::getMaterial(const std::string& aName)
{
	const auto iter = mImpl->materialMap.find(aName);
	if (iter != mImpl->materialMap.end())
		return mImpl->materialMap.at(aName);

	return nullptr;
}
