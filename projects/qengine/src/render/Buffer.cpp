#include <cstdint>
#include "render/Buffer.h"




GLuint createArrays(const VertexAttribute* attribs, 
					const void** bufs, 
					const uint32_t sizeOfVert, 
					const uint32_t nBuffers, 
					const uint32_t dynamic)
{
	GLuint vao;
	GLuint* buffers;
	GLenum usage;
	uint32_t n = 0;

	if (!attribs || !bufs || nBuffers < 0)
		return 0;

	if (dynamic & BUFFER_STATIC)
		usage = GL_STATIC_DRAW;
	else if (dynamic & BUFFER_DYNAMIC)
		usage = GL_DYNAMIC_DRAW;
	else
		usage = GL_STATIC_DRAW;

	buffers = (GLuint*)malloc(sizeof(GLuint) * nBuffers);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(nBuffers, buffers);
	for (n = 0; n < nBuffers; ++n)
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffers[n]);
		glBufferData(GL_ARRAY_BUFFER, sizeOfVert, bufs[n], usage);
		glEnableVertexAttribArray(attribs[n].loc);
		glVertexAttribPointer(attribs[n].loc, 
							  attribs[n].size, 
							  attribs[n].type, 
							  attribs[n].normalized, 
							  attribs[n].stride, 
							  BUFFER_OFFSET(attribs[n].dataOffset));
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return vao;
}


GLuint createInterleavedArray(const VertexAttribute* attribs, 
							  const uint32_t nAttribs, 
							  const void* buf, 
							  const uint32_t sizeOfVert, 
							  const uint32_t dynamic)
{
	GLuint vao;
	GLuint buffer;
	GLenum usage;
	uint32_t n = 0;
	
	if (!attribs || !buf || nAttribs <= 0 || nAttribs > GL_MAX_VERTEX_ATTRIBS)
		return 0;

	if (dynamic & BUFFER_STATIC)
		usage = GL_STATIC_DRAW;
	else if (dynamic & BUFFER_DYNAMIC)
		usage = GL_DYNAMIC_DRAW;
	else
		usage = GL_STATIC_DRAW;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeOfVert, buf, usage);
	for (n = 0; n < nAttribs; ++n)
	{
		glEnableVertexAttribArray(attribs[n].loc);
		glVertexAttribPointer(attribs[n].loc, 
							  attribs[n].size, 
							  attribs[n].type, 
							  attribs[n].normalized, 
							  attribs[n].stride, 
							  BUFFER_OFFSET(attribs[n].dataOffset));
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return vao;
}

GLuint createIndexedArrays(const VertexAttribute* attribs, 
						   const void** bufs, 
						   const uint32_t sizeOfVert, 
						   const uint32_t nBuffers, 
						   const uint32_t dynamic, 
						   const uint32_t* indices, 
						   const uint32_t nIndices)
{
	GLuint vao;
	GLuint* buffers;
	GLuint ibo;
	GLenum usage;
	uint32_t n = 0;

	if (!attribs || !bufs || nBuffers < 0 || nIndices <= 0 || !indices)
		return 0;

	if (dynamic & BUFFER_STATIC)
		usage = GL_STATIC_DRAW;
	else if (dynamic & BUFFER_DYNAMIC)
		usage = GL_DYNAMIC_DRAW;
	else
		usage = GL_STATIC_DRAW;

	buffers = (GLuint*)malloc(sizeof(GLuint) * nBuffers);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(nBuffers, buffers);
	for (n = 0; n < nBuffers; ++n)
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffers[n]);
		glBufferData(GL_ARRAY_BUFFER, sizeOfVert, bufs[n], usage);
		glEnableVertexAttribArray(attribs[n].loc);
		glVertexAttribPointer(attribs[n].loc, attribs[n].size, attribs[n].type, attribs[n].normalized, attribs[n].stride, BUFFER_OFFSET(attribs[n].dataOffset));
	}

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * nIndices, indices, usage);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return vao;
}

GLuint createIndexedArray(const VertexAttribute attribs, 
						  const void* buf, 
						  const uint32_t sizeOfVert, 
						  const uint32_t dynamic, 
						  const uint32_t* indices, 
						  const uint32_t nIndices)
{
	GLuint vao;
	GLuint ibo;
	GLuint buffer;
	GLenum usage;
	uint32_t n = 0;

	if (!buf || !indices || nIndices <= 0)
		return 0;

	if (dynamic & BUFFER_STATIC)
		usage = GL_STATIC_DRAW;
	else if (dynamic & BUFFER_DYNAMIC)
		usage = GL_DYNAMIC_DRAW;
	else
		usage = GL_STATIC_DRAW;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeOfVert, buf, usage);

	glEnableVertexAttribArray(attribs.loc);
	glVertexAttribPointer(attribs.loc, 
						  attribs.size, 
						  attribs.type, 
						  attribs.normalized, 
						  attribs.stride, 
						  BUFFER_OFFSET(attribs.dataOffset));

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * nIndices, indices, usage);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return vao;	
}

GLuint createIndexedInterleavedArray(const VertexAttribute* attribs, 
									 const uint32_t nAttribs, 
									 const void* buf, 
									 const uint32_t sizeOfVert, 
									 const uint32_t dynamic, 
									 const uint32_t* indices, 
									 const uint32_t nIndices)
{
	GLuint vao;
	GLuint ibo;
	GLuint buffer;
	GLenum usage;
	uint32_t n = 0;

	if (!attribs || !buf || nAttribs <= 0 || nAttribs > GL_MAX_VERTEX_ATTRIBS || !indices || nIndices <= 0)
		return 0;

	if (dynamic & BUFFER_STATIC)
		usage = GL_STATIC_DRAW;
	else if (dynamic & BUFFER_DYNAMIC)
		usage = GL_DYNAMIC_DRAW;
	else
		usage = GL_STATIC_DRAW;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeOfVert, buf, usage);
	for (n = 0; n < nAttribs; ++n)
	{
		glEnableVertexAttribArray(attribs[n].loc);
		glVertexAttribPointer(attribs[n].loc, attribs[n].size, attribs[n].type, attribs[n].normalized, attribs[n].stride, BUFFER_OFFSET(attribs[n].dataOffset));
	}

	GLenum err = glGetError();

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * nIndices, indices, usage);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return vao;
}

void bindBuffer(GLuint id)
{
	glBindVertexArray(id);
}

void bindDefaultBuffer()
{
	glBindVertexArray(0);
}

void deleteBuffer(GLuint id)
{
	glDeleteBuffers(1, &id);
}