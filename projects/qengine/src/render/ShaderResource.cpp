#include "render/ShaderResource.h"

ShaderResourceDeclaration::ShaderResourceDeclaration(const Type aType, const std::string& aName, const uint32_t aCount) :
	 mRegister(0), mCount(aCount), mType(aType)
{
	mName = const_cast<char*>(aName.c_str());
}

ShaderResourceDeclaration::Type ShaderResourceDeclaration::stringToType(const std::string& aType)
{
	if (aType == "sampler2D")		return Type::TEXTURE2D;
	if (aType == "samplerCube")		return Type::TEXTURECUBE;
	if (aType == "samplerShadow")	return Type::TEXTURESHADOW;
	if (aType == "samplerBuffer")   return Type::TEXTUREBUFFER;

	return Type::NONE;
}

std::string ShaderResourceDeclaration::typeToString(const Type aType)
{
	switch (aType)
	{
		case Type::TEXTURE2D:		return "sampler2D";
		case Type::TEXTURECUBE:		return "samplerCube";
		case Type::TEXTURESHADOW:	return "samplerShadow";
		case Type::TEXTUREBUFFER:   return "samplerBuffer";
		default:					return "Invalid Type";
	}
}
