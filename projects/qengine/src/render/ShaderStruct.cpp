#include "render/ShaderStruct.h"
#include "render/ShaderUniform.h"

class ShaderStructImpl
{
	public:
		std::vector<ShaderUniformDeclaration*> fields;
};

ShaderStruct::ShaderStruct(const std::string& aName): mSize(0), mOffset(0)
{
	mImpl = new ShaderStructImpl();
	mName = const_cast<char*>(aName.c_str());
}

ShaderStruct::~ShaderStruct()
{
	delete mImpl;
}

void ShaderStruct::addField(ShaderUniformDeclaration* aField)
{
	mSize += aField->getSize();
	uint32_t offset = 0;
	if (mImpl->fields.size())
	{
		ShaderUniformDeclaration* previous = mImpl->fields.back();
		offset = previous->getOffset() + previous->getSize();
	}
	aField->_setOffset(offset);
	mImpl->fields.push_back(aField);
}

const std::vector<ShaderUniformDeclaration*>& ShaderStruct::getFields() const
{
	return mImpl->fields;
}
