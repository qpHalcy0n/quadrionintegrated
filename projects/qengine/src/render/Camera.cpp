#include <cassert>
#include "render/Camera.h"
#include "render/Render.h"
#include "render/QGfx.h"
//#include "events/CameraUpdateEvent.h"

/*
class CameraImpl
{
	public:
		mat4<float> viewMatrix;
};
*/

Camera* Camera::current;

Camera::Camera()
{
	mCurFov = 0.0F;
	mCurAspect = 0.0F;
	mZNear = 0.0F;
	mZFar = 0.0F;

	mTheta = 0;
	mPhi = 0;

	mIsOrtho = false;

//	mImpl = new CameraImpl();
}

Camera::Camera(const Camera& aCamera)
{
	viewMatrix = aCamera.viewMatrix;
	mCurPosition = aCamera.mCurPosition;
	mCurLookAt = aCamera.mCurLookAt;
	mCurUpVec = aCamera.mCurUpVec;
	mAdjustedUpVec = aCamera.mAdjustedUpVec;
	mProjRight = aCamera.mProjRight;
	mProjLeft = aCamera.mProjLeft;
	mProjNear = aCamera.mProjNear;
	mProjFar = aCamera.mProjFar;
	mCurFov = aCamera.mCurFov;
	mCurHorizFov = aCamera.mCurHorizFov;
	mCurAspect = aCamera.mCurAspect;
	mZNear = aCamera.mZNear;
	mZFar = aCamera.mZFar;
	mTheta = aCamera.mTheta;
	mPhi = aCamera.mPhi;
	mLeft = aCamera.mLeft;
	mRight = aCamera.mRight;
	mTop = aCamera.mTop;
	mBottom = aCamera.mBottom;
	mFarClipHeight = aCamera.mFarClipHeight;
	mFarClipWidth = aCamera.mFarClipWidth;
	mIsOrtho = false;
}

Camera::~Camera()
{
	mCurFov = 0.0F;
	mCurAspect = 0.0F;
	mZNear = 0.0F;
	mZFar = 0.0F;

//	delete mImpl;
}

void Camera::operator= (const Camera& aCamera)
{
	viewMatrix = aCamera.viewMatrix;
	mCurPosition = aCamera.mCurPosition;
	mCurLookAt = aCamera.mCurLookAt;
	mCurUpVec = aCamera.mCurUpVec;
	mAdjustedUpVec = aCamera.mAdjustedUpVec;
	mProjRight = aCamera.mProjRight;
	mProjLeft = aCamera.mProjLeft;
	mProjNear = aCamera.mProjNear;
	mProjFar = aCamera.mProjFar;
	mCurFov = aCamera.mCurFov;
	mCurHorizFov = aCamera.mCurHorizFov;
	mCurAspect = aCamera.mCurAspect;
	mZNear = aCamera.mZNear;
	mZFar = aCamera.mZFar;
	mTheta = aCamera.mTheta;
	mPhi = aCamera.mPhi;
	mLeft = aCamera.mLeft;
	mRight = aCamera.mRight;
	mTop = aCamera.mTop;
	mBottom = aCamera.mBottom;
	mFarClipHeight = aCamera.mFarClipHeight;
	mFarClipWidth = aCamera.mFarClipWidth;
	mIsOrtho = aCamera.mIsOrtho;
}


void Camera::setCamera(const vec3<float> aCamPos, const vec3<float> aLookAt, const vec3<float> aUp)
{
	assert(!isnan(aCamPos.x + aCamPos.y + aCamPos.z));
	assert(!isnan(aLookAt.x + aLookAt.y + aLookAt.z));
	assert(!isnan(aUp.x + aUp.y + aUp.z));

	mCurPosition = aCamPos;
	mCurLookAt = aLookAt;
	mCurUpVec = aUp;

	vec3<float> direction = mCurLookAt - mCurPosition;
	direction.normalize();

	mPhi = asin(direction.y);

	float d = direction.z / (cosf(mPhi) + 0.00001f);

	if (d < -1.0f)
		d = -1.0f;
	else if (d > 1.0f)
		d = 1.0f;

	if (direction.x >= 0.0f)
		mTheta = acos(d);
	else
		mTheta = -acos(d);
}

/*
static void rotate(float* camPos, float* lookAtPos, float* upVec, float xDir)
{
	quat qRot, qView, qNewView;
	quat_rotate(qRot, deg2Rad(xDir), upVec);

	qView[0] = lookAtPos[0] - camPos[0];
	qView[1] = lookAtPos[1] - camPos[1];
	qView[2] = lookAtPos[2] - camPos[2];
	qView[3] = 0.0f;

	quat_mul(qRot, qView, qNewView);
	quat_conjugate(qRot);
	quat_mul(qNewView, qRot, qNewView);

	lookAtPos[0] = camPos[0] + qNewView[0];
	lookAtPos[1] = camPos[1] + qNewView[1];
	lookAtPos[2] = camPos[2] + qNewView[2];
}
*/

void Camera::update(const int aMouseX,
					const int aMouseY,
					float aSensitivity)
{
	if (aMouseX == 0 && aMouseY == 0)
		return;

	//	Sensitivity should be something...
	if (aSensitivity == 0)
		aSensitivity = 0.001f;

	mTheta -= aMouseX * aSensitivity;
	mPhi += aMouseY * aSensitivity;

	if (mPhi > mPi2)
		mPhi = mPi2;

	if (mPhi < -mPi2)
		mPhi = -mPi2;

	const float x = sin(mTheta) * cos(mPhi);
	const float y = sin(mPhi);
	const float z = cos(mTheta) * cos(mPhi);

	vec3<float> direction(x, y, z);
	direction.normalize();

	mCurLookAt = mCurPosition + direction;
}

void Camera::render()
{
	if (mIsOrtho)
	{
		renderOrtho();
		return;
	}

	current = this;
//	vec3 pos;
//	vec3 lookAt;
//	vec3 upVec;
	vec3<float> pos;
	vec3<float> lookAt;
	vec3<float> upVec;

	pos.x = mCurPosition.x;
	pos.y = mCurPosition.y;
	pos.z = mCurPosition.z;

	lookAt.x = mCurLookAt.x;
	lookAt.y = mCurLookAt.y;
	lookAt.z = mCurLookAt.z;

	upVec.x = mCurUpVec.x;
	upVec.y = mCurUpVec.y;
	upVec.z = mCurUpVec.z;

	mForwardVec = lookAt - pos;
	mRightVec = mForwardVec.crossProd(upVec);

//	mat4 la;
//	loadRHView(pos, lookAt, upVec, la);
	mat4<float> la;
	loadRHView(pos, lookAt, upVec, la);

	mAdjustedUpVec.set(la.m[1], la.m[5], la.m[9]);

//	mImpl->viewMatrix = la;
	viewMatrix = la;

	QGfx::setMatrix(VIEW, la);

	mat4<float> p;
//	loadRHPerspective(mCurFov, mCurAspect, mZNear, mZFar, p);
	loadRHPerspective(mCurFov, mCurAspect, mZNear, mZFar, p);
	QGfx::setMatrix(PROJECTION, p);

	const float halfFOV = mCurFov * 0.5f;
	mFarClipHeight = 2.0f * mZFar * tanf(halfFOV);
	mFarClipWidth = mFarClipHeight * mCurAspect;

	mCurHorizFov = 2.0f * atanf(mFarClipWidth * 0.5f / mZFar);
	const float biasAng = deg2Rad(8.0f);

	// generate projected clip planes //
	const float rlNearScale = mZNear / cosf(mCurHorizFov * 0.5f);
	const float rlFarScale = mZFar / cosf(mCurHorizFov * 0.5f);
	const vec2<float> projPos(mCurPosition.x, mCurPosition.z);
	const vec3<float> lookVec = mCurLookAt - mCurPosition;
	vec2<float> look(lookVec.x, lookVec.z);
	look.normalize();
//	mat2 rot;
	mat2<float> rot;
	rot.rotate((mCurHorizFov + biasAng) * 0.5f);
	vec2<float> rightVec = rot * look;
	rightVec.normalize();
	rightVec *= rlNearScale;
	
	mProjRight.v0.set(mCurPosition.x, mCurPosition.z);
	mProjRight.v1.set(mCurPosition.x + rightVec.x, mCurPosition.z + rightVec.y);

	rot.rotate((-mCurHorizFov - biasAng) * 0.5f);
	vec2<float> leftVec = rot * look;
	leftVec.normalize();
	leftVec *= rlNearScale;
	mProjLeft.v0.set(mCurPosition.x, mCurPosition.z);
	mProjLeft.v1.set(mCurPosition.x + leftVec.x, mCurPosition.z + leftVec.y);

	mProjNear.v0.set(projPos + rightVec);
	mProjNear.v1.set(projPos + leftVec);

	rightVec.normalize();
	leftVec.normalize();
	rightVec *= rlFarScale;
	leftVec *= rlFarScale;

	mProjFar.v0.set(projPos + rightVec);
	mProjFar.v1.set(projPos + leftVec);
}

void Camera::renderFromPose(mat3<float> pose, vec3<float> camPos)
{
	current = this;
	mCurPosition = camPos;

	mat4<float> la;
	loadRHViewFromPose(pose, camPos, la);
	mAdjustedUpVec.set(la.m[1], la.m[5], la.m[9]);
	viewMatrix = la;
	QGfx::setMatrix(VIEW, la);

	mat4<float> p;
	loadRHPerspective(mCurFov, mCurAspect, mZNear, mZFar, p);
	QGfx::setMatrix(PROJECTION, p);

	const float halfFOV = mCurFov * 0.5f;
	mFarClipHeight = 2.0f * mZFar * tanf(halfFOV);
	mFarClipWidth = mFarClipHeight * mCurAspect;

	mCurHorizFov = 2.0f * atanf(mFarClipWidth * 0.5f / mZFar);
	const float biasAng = deg2Rad(8.0f);

	// generate projected clip planes //
	const float rlNearScale = mZNear / cosf(mCurHorizFov * 0.5f);
	const float rlFarScale = mZFar / cosf(mCurHorizFov * 0.5f);
	const vec2<float> projPos(mCurPosition.x, mCurPosition.z);
	const vec3<float> lookVec = mCurLookAt - mCurPosition;
	vec2<float> look(lookVec.x, lookVec.z);
	look.normalize();
	mat2<float> rot;
	rot.rotate((mCurHorizFov + biasAng) * 0.5f);
	vec2<float> rightVec = rot * look;
	rightVec.normalize();
	rightVec *= rlNearScale;

	mProjRight.v0.set(mCurPosition.x, mCurPosition.z);
	mProjRight.v1.set(mCurPosition.x + rightVec.x, mCurPosition.z + rightVec.y);

	rot.rotate((-mCurHorizFov - biasAng) * 0.5f);
	vec2<float> leftVec = rot * look;
	leftVec.normalize();
	leftVec *= rlNearScale;
	mProjLeft.v0.set(mCurPosition.x, mCurPosition.z);
	mProjLeft.v1.set(mCurPosition.x + leftVec.x, mCurPosition.z + leftVec.y);

	mProjNear.v0.set(projPos + rightVec);
	mProjNear.v1.set(projPos + leftVec);

	rightVec.normalize();
	leftVec.normalize();
	rightVec *= rlFarScale;
	leftVec *= rlFarScale;

	mProjFar.v0.set(projPos + rightVec);
	mProjFar.v1.set(projPos + leftVec);
}

bool Camera::pointInProjFrustum(const vec2<float> aPt)
{
	bool out = true;

	if(mProjRight.pointIntersect(aPt) > 0.0f)
		out = false;

	if(mProjLeft.pointIntersect(aPt) < 0.0f)
		out = false;

	if(mProjNear.pointIntersect(aPt) < 0.0f)
		out = false;

	if(mProjFar.pointIntersect(aPt) > 0.0f)
		out = false;

	return out;
}

void Camera::renderOrtho()
{
	current = this;
	vec3<float> pos;
	vec3<float> lookAt;
	vec3<float> upVec;

	pos.x = mCurPosition.x;
	pos.y = mCurPosition.y;
	pos.z = mCurPosition.z;

	lookAt.x = mCurLookAt.x;
	lookAt.y = mCurLookAt.y;
	lookAt.z = mCurLookAt.z;

	upVec.x = mCurUpVec.x;
	upVec.y = mCurUpVec.y;
	upVec.z = mCurUpVec.z;

	mat4<float> LA;
	loadRHView(pos, lookAt, upVec, LA);
	mAdjustedUpVec.set(LA[1], LA[5], LA[9]);
	viewMatrix = LA;
	QGfx::setMatrix(VIEW, LA);

	mat4<float> O;
	loadRHOrthographic(mLeft, mRight, mBottom, mTop, mZNear, mZFar, O);
	QGfx::setMatrix(PROJECTION, O);
}

void Camera::setPerspective(const float fovy, 
								   const float aspect, 
								   const float nearClip, 
								   const float farClip)
{
	mCurFov = fovy;
	mCurAspect = aspect;
	mZNear = nearClip;
	mZFar = farClip;

	mIsOrtho = false;
}

void Camera::setOrtho(const float aLeft, 
							 const float aRight, 
							 const float aTop, 
							 const float aBottom, 
							 const float aNearClip, 
							 const float aFarClip)
{
	mZNear = aNearClip;
	mZFar = aFarClip;
	mLeft = aLeft;
	mRight = aRight;
	mBottom = aBottom;
	mTop = aTop;

	mIsOrtho = true;
}

void Camera::moveForward(const float aFMul)
{
	vec3<float> direction = mCurLookAt - mCurPosition;
	direction.normalize();
	direction *= aFMul;
	mCurPosition += direction;
	mCurLookAt += direction;
}

void Camera::moveBack(const float aFMul)
{
	vec3<float> direction = mCurLookAt - mCurPosition;
	direction.normalize();
	direction *= aFMul;
	mCurPosition -= direction;
	mCurLookAt -= direction;
}

void Camera::moveRight(const float aFMul)
{
	vec3<float> up = mCurUpVec;
	vec3<float> direction = mCurLookAt - mCurPosition;
	direction.normalize();

	vec3<float> right = direction.crossProd(up);
	right.normalize();
	right *= aFMul;

	mCurPosition += right;
	mCurLookAt += right;
}

void Camera::moveLeft(const float aFMul)
{
	vec3<float> up = mCurUpVec;
	vec3<float> direction = mCurLookAt - mCurPosition;
	direction.normalize();

	vec3<float> left = up.crossProd(direction);
	left.normalize();
	left *= aFMul;

	mCurPosition += left;
	mCurLookAt += left;
}

void Camera::setPosition(const vec3<float> pos)
{
	vec3<float> direction = mCurLookAt - mCurPosition;
	direction.normalize();
	mCurPosition = pos;
	mCurLookAt = mCurPosition + direction;
}

vec4<float> Camera::getVecFromScreenspace(const uint32_t aX, 
										   const uint32_t aY, 
										   const uint32_t aWinWidth, 
										   const uint32_t aWinHeight) const
{
	const float percX = aX / static_cast<float>(aWinWidth);
	const float percY = aY / static_cast<float>(aWinHeight);

	vec3<float> farPCenter(0.0f, 0.0f, -mZFar);
	vec3<float> vpUp(0.0f, 1.0f, 0.0f);
	const float farPHeight = 2.0f * tanf(mCurFov / 2.0f) * mZFar;
	const float farPWidth = farPHeight * mCurAspect;

	const float interpX = (-farPWidth * 0.5f) + (farPWidth * percX);
	const float interpY = (farPHeight * 0.5f) - (farPHeight * percY);

	vec3<float> z(interpX, interpY, -mZFar);
	z.normalize();
	vec4<float> z4(z.x, z.y, z.z, 0.0f);
	mat4<float> V = QGfx::getMatrix(VIEW);
	V.invert();

	vec4<float> res = V * z4;

	return res;
}

mat4<float> Camera::getViewMatrix() const
{
//	return mImpl->viewMatrix;
	return viewMatrix;
}


//////////////////////////////////////////////////////////////////////////


