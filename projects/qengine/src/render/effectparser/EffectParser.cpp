#include "render/effectparser/EffectParser.h"

#include <cctype>
#include <algorithm>
#include "render/effectparser/EffectKeywords.h"
#include "render/QGfx.h"

#include "io/FileSystem.h"

static std::vector<std::string> split(const std::string& str, const char* regex)
{
	size_t begin = 0;
	size_t next = str.find(regex, begin);
	std::vector<std::string> res;

	if (str.length() == 0)
	{
		return res;
	}

	do
	{
		std::string val = str.substr(begin, next - begin);
		if (next == begin || val.size() == 0)
		{
			begin = next + 1;
			next = str.find(regex, begin + 1);
			continue;
		}
		else if (std::string(regex).find(val[0]) != std::string::npos)
		{
			begin++;
			continue;
		}
		res.push_back(val);
		if (next >= str.length())
		{
			break;
		}
		begin = next + 1;
		next = str.find(regex, begin + 1);

	} while (begin < str.length());

	return res;
}

std::vector<std::string> splitOnAny(std::string& str, const char* regex)
{
	size_t begin = 0;
	size_t next = str.find_first_of(regex, begin);
	std::vector<std::string> res;


	if (str.length() == 0)
	{
		return res;
	}

	do
	{
		std::string val = str.substr(begin, next - begin);
		if (next == begin || val.size() == 0)
		{
			begin = next + 1;
			next = str.find_first_of(regex, begin + 1);
			continue;
		}
		else if (std::string(regex).find(val[0]) != std::string::npos)
		{
			begin++;
			continue;
		}
		res.push_back(val);
		if (next >= str.length())
		{
			break;
		}
		begin = next + 1;
		next = str.find_first_of(regex, begin + 1);

	} while (begin < str.length());

	return res;
}

static std::string trimFront(const std::string& str)
{
	size_t i = 0;
	while (i < str.length() && isspace(str[i]))
	{
		i++;
	}
	if (!i)
	{
		return str;
	}

	if (i == str.length())
		return std::string();

	return str.substr(i);
}

static std::string getBlock(std::string& block)
{
	size_t openCount = 1;
	size_t closeCount = 0;

	size_t i = block.find("{") + 1;
	const size_t begin = i;

	while (openCount != closeCount)
	{
		if (block[i] == '{')
		{
			openCount++;
		}
		else if (block[i] == '}')
		{
			closeCount++;
		}
		i++;
	}

	return block.substr(begin + 1, i - (begin + 2));
}

static std::string stripFirst(const std::string& aString, const std::string& aDelimiter)
{
	std::string finalString = "";

	bool found = false;
	size_t index;
	for (index = 0; index < aString.length(); index++)
	{
		uint32_t matchCount = 0;

		for (size_t j = 0; j < aDelimiter.length(); j++)
		{
			if (aString[index + j] == aDelimiter[j])
			{
				matchCount++;
			}
		}

		const bool match = (matchCount == aDelimiter.length());

		if (match)
		{
			index += matchCount;
			found = true;
			break;
		}

		finalString += aString[index];
	}

	if (found)
	{
		finalString += aString.substr(index + 1);
	}

	return finalString;
}

const std::string WHITESPACE = " \n\r\t\f\v";
static std::string stripComments(const std::string& aStr)
{
	auto lines = split(aStr, "\n");
	std::string ret = "";

	for(const auto& line : lines)
	{
		if(line.find("//") != std::string::npos)
		{
			// contains comment
			const size_t start = line.find_first_not_of(WHITESPACE);
			auto lineCopy = line.substr(start);
			if(lineCopy.rfind("//", 0) == 0)
			{
				// starts with comment
				// skip line
				continue;
			}
			else
			{
				// ends with comment
//				const size_t commentLocation = line.find_first_of("//");
				const size_t commentLocation = line.find("//");
				ret += line.substr(0, commentLocation);
				ret += "\n";
			}
		}
		else
		{
			// no comments
			ret += line;
			ret += "\n";
		}
	}

	return ret;
}

static bool isPreprocessorDirective(std::string& str)
{
	return str.size() && str[0] == EFFECT_PREPROCESSOR_DIRECTIVE;
}

static bool isVariable(std::string& str)
{
	auto tokens = splitOnAny(str, " ()\t");
    if(!tokens.empty() && tokens.size() > 2)
    {
        if(tokens[0] == EFFECT_LAYOUT && tokens[2] == EFFECT_UNIFORM)
        {
			return true;
        }
    }

	if (!tokens.empty() && (tokens[0] == EFFECT_UNIFORM || tokens[1] == EFFECT_BUFFER))
	{
		return true;
	}
	if (strncmp(tokens[0].c_str(), EFFECT_LAYOUT, strlen(EFFECT_LAYOUT)) == 0)
	{
		return false;
	}
	if(strncmp(tokens[0].c_str(), EFFECT_OUTPUTBUFFER, strlen(EFFECT_OUTPUTBUFFER)) == 0)
	{
		return false;
	}
	if (strncmp(tokens[0].c_str(), EFFECT_DEPTHBUFFER, strlen(EFFECT_DEPTHBUFFER)) == 0)
	{
		return false;
	}

    const size_t brackLoc = str.find("{");
	return brackLoc == std::string::npos;
}

static bool isFunction(std::string& str)
{
	auto tokens = split(str, " ");
    if(tokens.empty())
    {
		return false;
    }

	const auto startP = str.find('(');
	const auto endP = str.find(')');
    if (startP == std::string::npos || endP == std::string::npos || startP > str.find('{'))
    {
		return false;
    }

	bool isConst = false;

    if (tokens[0] == EFFECT_CONST)
    {
		isConst = true;
    }

    if (getEffectVariableType(tokens[isConst ? 1 : 0]) != EffectVariableType::UNKNOWN)
    {
		return true;
    }

	return false;
}

static bool isMainFunction(std::string& str)
{
	const bool isFunc = isFunction(str);
	if (isFunc)
	{
		const auto startP = str.find('(');
		const auto endP = str.find(')');
		std::string params = str.substr(startP + 1, endP - (startP + 1));

		if (params.find("in") != std::string::npos && params.find("out") != std::string::npos)
		{
			if(params.find("inout") != std::string::npos)
				return false;

			return true;
		}
		return false;
	}
	return false;
}

static bool isInterface(std::string& str)
{
	auto tokens = split(str, " ");
	if (tokens.empty())
	{
		return false;
	}

    return tokens[0] == EFFECT_INTERFACE;
}

static bool isTechnique(std::string& str)
{
	auto tokens = split(str, " ");
	return tokens[0] == EFFECT_TECHNIQUE;
}

static bool isLayout(std::string& str)
{
	return strncmp(str.c_str(), EFFECT_LAYOUT, strlen(EFFECT_LAYOUT)) == 0;
}

static bool isOutputBuffer(std::string& str)
{
	return strncmp(str.c_str(), EFFECT_OUTPUTBUFFER, strlen(EFFECT_OUTPUTBUFFER)) == 0;
}

static bool isDepthBuffer(std::string& str)
{
	return strncmp(str.c_str(), EFFECT_DEPTHBUFFER, strlen(EFFECT_DEPTHBUFFER)) == 0;
}

static bool processPreprocessorDirective(std::string& str, IParseNode** node)
{
	std::string directive = str.substr(1);
	auto tokens = split(directive, " ");
	std::string type = directive.substr(0, directive.find(' '));
	if (tokens[0] == EFFECT_VERSION)
	{
		std::string version = directive.substr(directive.find(' ') + 1);
		ParseEffectVersion* n = new ParseEffectVersion;
		*node = n;
		n->version.version = tokens[1];
		n->version.profile = tokens[2];
		return true;
	}
    if(tokens[0] == EFFECT_EXTENSION)
    {
		std::string extension = directive.substr(directive.find(' ') + 1);
		ParseEffectExtension* n = new ParseEffectExtension;
		*node = n;
		n->extension.name = tokens[1];
		n->extension.mode = tokens[2];
		return true;
    }
    /*if(tokens[0] == EFFECT_INCLUDE)
    {
		ParseEffectInclude* n = new ParseEffectInclude;
		*node = n;
		n->include.path = tokens[1];
		return true;
    }*/
	return false;
}

static bool parseVariable(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ()=;\n\t");
	if (tokens[0] == EFFECT_UNIFORM || tokens[0] == EFFECT_LAYOUT)
	{
		// process uniform
		if (str.find('{') != std::string::npos && str.find('}') != std::string::npos)
		{
			tokens = splitOnAny(str, "()=;\n{},\t ");
			// block
			auto n = new ParseUniformBlock;
			*node = n;
			size_t tok = 1;
			if (tokens[0] == EFFECT_LAYOUT)
			{
				n->block.layout = tokens[1];
				tok = 3;
			}
			n->block.blockName = tokens[tok++];

			if (tokens[tok] == ":")
			{
				n->block.shader = tokens[++tok];
			}

			while (tok < tokens.size())
			{
				if (getEffectVariableType(tokens[tok]) != EffectVariableType::UNKNOWN)
				{
					EffectUniform uni;
					uni.type = getEffectVariableType(tokens[tok++]);
					uni.uniformName = tokens[tok++];

					if (tok < tokens.size())
					{
						if (tokens[tok] == ":")
						{
							// GUI stuff
							tok++;
							uni.guiDebugElement = tokens[tok++];

							if (tok < tokens.size())
							{
								if(getEffectVariableType(tokens[tok]) == EffectVariableType::UNKNOWN)
								{
									uni.min = static_cast<float>(atof(tokens[tok++].c_str()));
									uni.max = static_cast<float>(atof(tokens[tok++].c_str()));
								}
							}

							if(tok < tokens.size())
							{
								if (getEffectVariableType(tokens[tok]) == EffectVariableType::UNKNOWN)
								{
									// This has a default value
									uni.guiDefaultValue = tokens[tok];
								}
							}
						}
					}

					n->block.uniforms.push_back(uni);
				}
				else
				{
					tok++;
				}
			}

			return true;
		}

		// single line
		auto n = new ParseUniform;
		*node = n;
		size_t tok = 1;
		n->uniform.type = getEffectVariableType(tokens[tok++]);
		n->uniform.uniformName = tokens[tok++];

		if (tok < tokens.size())
		{
			if (tokens[tok++] == ":")
			{
				//n->uniform.shader = tokens[tok];
				n->uniform.guiDebugElement = tokens[tok];

				if (tok + 1 < tokens.size())
				{
					n->uniform.min = static_cast<float>(atof(tokens[tok + 1].c_str()));
					n->uniform.max = static_cast<float>(atof(tokens[tok + 2].c_str()));
				}

				if (tok + 3 < tokens.size())
				{
					// This has a default value
					n->uniform.guiDefaultValue = tokens[tok + 3];
				}
			}
			else
			{
				tok--;
				std::string defaultValue = tokens[tok++];

				if(tok < tokens.size())
				{
					defaultValue += "(";

					while (tok < tokens.size())
					{
						std::string token = trimFront(tokens[tok]);
						if (token == ":" || token == ";")
						{
							break;
						}
						defaultValue += token;
						tok++;
					}

					defaultValue += ")";
				}
				n->uniform.defaultValue = defaultValue;
			}
		}
		if (tok < tokens.size())
		{
			const std::string token = trimFront(tokens[tok++]);
			if (token == ":")
			{
				//n->uniform.shader = tokens[tok];
				n->uniform.guiDebugElement = tokens[tok];
				if (tok + 1 < tokens.size())
				{
					n->uniform.min = static_cast<float>(atof(tokens[tok + 1].c_str()));
					n->uniform.max = static_cast<float>(atof(tokens[tok + 2].c_str()));
				}
			}
		}
		return true;
	}
	if (tokens[0] == EFFECT_BUFFER)
	{
		if (str.find('{') != std::string::npos && str.find('}') != std::string::npos)
		{
			tokens = splitOnAny(str, "()=;\n{} ");
			// block
			auto n = new ParseBufferBlock;
			*node = n;
			size_t tok = 1;
			n->block.blockName = tokens[tok++];

			if (tokens[tok] == ":")
			{
				n->block.shader = tokens[++tok];
			}

			while (tok < tokens.size())
			{
				if (getEffectVariableType(tokens[tok]) != EffectVariableType::UNKNOWN)
				{
					EffectUniform uni;
					uni.type = getEffectVariableType(tokens[tok++]);
					uni.uniformName = tokens[tok++];
					n->block.uniforms.push_back(uni);
				}
				else
				{
					tok++;
				}
			}

			return true;
		}

		return false;
	}
	
    // process variable
	auto n = new ParseVariable;
	*node = n;
	size_t tok = 0;
	EffectTypeQualifier qualifier = getEffectTypeQualifier(tokens[tok]);
	while (qualifier != EffectTypeQualifier::UNKNOWN)
	{
		tok++;
		n->variable.typeQualifiers.push_back(qualifier);
		qualifier = getEffectTypeQualifier(tokens[tok]);
	}
	n->variable.type = getEffectVariableType(tokens[tok++]);
	n->variable.variableName = tokens[tok++];
	if (tok < tokens.size())
	{
		if (tok < (tokens.size() - 1) && tokens[tok + 1] == ":")
		{
			n->variable.shader = tokens[tok++];
		}
		else
		{
			std::string defaultValue = tokens[tok++];

            if(tok < (tokens.size() - 1))
            {
                // multi parameter default value
			    defaultValue += "(";

			    while (tok < tokens.size())
			    {
				    std::string token = trimFront(tokens[tok]);
				    if (token == ":" || token == ";")
				    {
					    break;
				    }
				    defaultValue += token;
				    tok++;
			    }
			    
			    defaultValue += ")";
            }
			n->variable.defaultValue = defaultValue;
		}
	}
	if (tok < tokens.size())
	{
		const std::string token = trimFront(tokens[tok++]);
		if (token == ":")
		{
			n->variable.shader = tokens[tok];
		}
	}

	return true;
}

static bool parseFunction(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ()=;\n\t/");
	auto n = new ParseFunction;
	*node = n;
	size_t tok = 0;
	EffectTypeQualifier qualifier = getEffectTypeQualifier(tokens[tok]);
	while (qualifier != EffectTypeQualifier::UNKNOWN)
	{
		tok++;
		n->function.typeQualifiers.push_back(qualifier);
		qualifier = getEffectTypeQualifier(tokens[tok]);
	}

	n->function.type = getEffectVariableType(tokens[tok++]);
	n->function.name = tokens[tok++];

    while(tok < tokens.size())
    {
		if (tokens[tok] == ":" || tokens[tok] == ";" || tokens[tok] == "{")
		{
			break;
		}

		EffectVariable variable;
		qualifier = getEffectTypeQualifier(tokens[tok]);
		while (qualifier != EffectTypeQualifier::UNKNOWN)
		{
			tok++;
			variable.typeQualifiers.push_back(qualifier);
			qualifier = getEffectTypeQualifier(tokens[tok]);
		}

		variable.type = getEffectVariableType(tokens[tok++]);
		variable.variableName = tokens[tok++];

		n->function.parameters.push_back(variable);
    }

	if (tok < tokens.size())
	{
		const std::string token = trimFront(tokens[tok++]);
		if (token == ":")
		{
			while (tok < tokens.size())
			{
				if (trimFront(tokens[tok]) == "{")
					break;
				n->function.shaders.push_back(tokens[tok++]);
			}
		}
	}

	n->function.block = getBlock(str);

	return true;
}

static bool parseMainFunction(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ()=;\n\t,");
	auto n = new ParseMainFunction;
	*node = n;
	size_t tok = 0;

	n->function.type = getEffectVariableType(tokens[tok++]);
	n->function.name = tokens[tok++];

	while (tok < tokens.size())
	{
		EffectMainVariable variable;

		variable.specifier = getEffectInterfaceSpecifier(tokens[tok++]);
		variable.name = tokens[tok++];

		n->function.parameters.push_back(variable);

		if (tokens[tok] == ":" || tokens[tok] == ";" || tokens[tok] == "{")
		{
			break;
		}
	}

	if (tok < tokens.size())
	{
		const std::string token = trimFront(tokens[tok++]);
		if (token == ":")
		{
			n->function.shader = tokens[tok];
		}
	}

	n->function.block = getBlock(str);

	return true;
}

static bool parseInterface(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ()=;\n\t{}");
	auto n = new ParseInterface;
	*node = n;
	size_t tok = 1;

	n->effectInterface.name = tokens[tok++];

	while (tok < tokens.size())
	{
		EffectInterfaceVariable variable;
		EffectTypeQualifier qualifier = getEffectTypeQualifier(tokens[tok]);
		while (qualifier != EffectTypeQualifier::UNKNOWN)
		{
			tok++;
			variable.typeQualifiers.push_back(qualifier);
			qualifier = getEffectTypeQualifier(tokens[tok]);
		}

		variable.type = getEffectVariableType(tokens[tok++]);
		variable.variableName = tokens[tok++];
		variable.location = EffectSemantic::UNKNOWN;

		if (tok < tokens.size())
		{
			const std::string token = trimFront(tokens[tok]);
			if (token == ":")
			{
				variable.location = getEffectSemanticType(tokens[tok + 1]);
				tok += 2;
			}
		}

		n->effectInterface.variables.push_back(variable);
	}

	return true;
}

static bool parseLayout(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ();\t,");
	auto n = new ParseLayout;
	*node = n;

	size_t tok = 1;
    while(getEffectInterfaceSpecifier(tokens[tok]) == EffectInterfaceSpecifier::UNKNOWN)
    {
		EffectLayoutOperand operand;
		operand.name = tokens[tok++];

        if(tokens[tok] == "=")
        {
			operand.defaultValue = tokens[++tok];
			tok++;
        }

		n->layout.operands.push_back(operand);
    }

	n->layout.specifier = getEffectInterfaceSpecifier(tokens[tok++]);
	n->layout.shader = tokens[++tok];

	return true;
}

static std::string merge(const std::vector<std::string>& strs)
{
	std::string res = "";
	for (size_t i = 1; i < strs.size(); i++) res += strs[i];
	return res;
}

static bool parsePass(std::string& str, ParsePass* node)
{
	std::string block = trimFront(str);
	size_t i = block.find_first_of(" \t\n");
	std::string pass = block.substr(0, i);
	const std::string name = trimFront(block.substr(i + 1, (block.find_first_of(" \t\n", i + 1)) - (i + 1)));
	node->pass.name = name;
	block = getBlock(block);
	
	auto lines = splitOnAny(block, ";\n");

	for (std::string& line : lines)
	{
        if(line.find('[') != std::string::npos && line.find(']') != std::string::npos)
        {
		    auto tokens = splitOnAny(line, " =;[]\n\t");

			std::vector<std::string> toks;
			toks.push_back(tokens[0]);
			toks.push_back(tokens[2]);

			node->pass.other.insert({ tokens[1], toks});
			continue;
        }

		auto tokens = splitOnAny(line, " =\t\n;|,{}");
		if (tokens.size() < 2)
		{
			continue;
		}
		if (tokens[0] == EFFECT_VERTEX_SHADER)
		{
			node->pass.vertexFunction = merge(tokens);
		}
		else if (tokens[0] == EFFECT_GEOMETRY_SHADER)
		{
			node->pass.geometryFunction = merge(tokens);
		}
		else if (tokens[0] == EFFECT_TCS_SHADER)
		{
			node->pass.tcsFunction = merge(tokens);
		}
		else if (tokens[0] == EFFECT_TES_SHADER)
		{
			node->pass.tesFunction = merge(tokens);
		}
		else if (tokens[0] == EFFECT_FRAGMENT_SHADER)
		{
			node->pass.fragmentFunction = merge(tokens);
		}
		else
		{
			std::string tok = tokens[0];
			tokens.erase(tokens.begin());
			node->pass.other.insert({ tok, tokens });
		}
	}

	return true;
}

static bool parseTechnique(std::string& str, IParseNode** node)
{
	ParseTechnique* tech = new ParseTechnique;
	*node = tech;
	std::string block = trimFront(str);
	const size_t i = block.find_first_of(" \t\n");
	std::string technique = block.substr(0, i);
	const std::string name = trimFront(block.substr(i + 1, (block.find_first_of(" \t\n", i + 1)) - (i + 1)));

	tech->technique.name = name;

	block = trimFront(getBlock(block));

	std::vector<std::string> lines = splitOnAny(block, "\n");
	block = "";
	for(const auto& line : lines)
	{
		std::string l = line.c_str();
		l.erase(std::remove_if(l.begin(), l.end(), [](unsigned char x) { return std::isspace(x); }), l.end());
		if(l[0] == '/' && l[1] == '/')
		{
			continue;
		}

		block += line;
		block += "\n";
	}
	
	size_t passStart = block.find("pass");

	while (passStart < block.size())
	{
		size_t passEnd = block.find("pass", passStart + 1);

		std::string pass = block.substr(passStart, passEnd - passStart);
		passStart = passEnd;

		ParsePass* n = new ParsePass;
		parsePass(pass, n);

		tech->technique.passes.push_back(n->pass);

		n->parent = *node;
		tech->children.push_back(n);
	}

	return true;
}

static bool parseOutputBuffer(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ,{}=|");

	ParseOutputBuffer* buffer = new ParseOutputBuffer();
	buffer->buffer.name = tokens[1];
	buffer->buffer.numBuffers = atoi(tokens[2].c_str());
	buffer->buffer.width = atoi(tokens[3].c_str());
	buffer->buffer.height = atoi(tokens[4].c_str());
	buffer->buffer.pixelType = tokens[5];
	buffer->buffer.depthStencilType = tokens[6];

	for(size_t i = 7; i < tokens.size(); i++)
	{
		buffer->buffer.flags.push_back(tokens[i]);
	}

	*node = buffer;
	return true;
}

static bool parseDepthBuffer(std::string& str, IParseNode** node)
{
	std::vector<std::string> tokens = splitOnAny(str, " ,{}=|;");

	ParseDepthBuffer* buffer = new ParseDepthBuffer();
	buffer->buffer.name = tokens[1];
	buffer->buffer.depthStencilType = tokens[2];

	*node = buffer;
	return true;
}

IParseNode* EffectParser::parse(std::string& aStr) const
{
	auto str = stripComments(aStr);

	const size_t it = str.find_first_of(" ");
	std::string qs = str.substr(0, it);
	const size_t effectNameEnd = str.find("{", it);
	std::string effectName = str.substr(it + 1, effectNameEnd - (it + 1));
	size_t nameLength = 0;
	
	for (auto i = effectName.length(); i > 0; i--)
	{
		nameLength++;
		if (!isalnum(effectName[i - 1] && !(effectName[i - 1] == '\\')))
		{
			break;
		}
	}
	effectName = effectName.substr(0, effectName.length() - nameLength);

	IParseNode * node = new IParseNode;
	node->name = effectName;
	node->sourceCopy = aStr;

	std::string sTemp = getBlock(str);
	_parseRoot(sTemp, node);

	return node;
}

static void replaceAll(std::string& str, const std::string& from, const std::string& to) 
{
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

void EffectParser::_parseRoot(std::string& str, IParseNode* node) const
{
	size_t lineBegin = 0;
	size_t end = str.find_last_of("}");

	// preprocess includes
	while (lineBegin < end)
	{
		if (lineBegin + 1 >= str.length())
		{
			break;
		}
		size_t lineEnd = str.find('\n', lineBegin + 1);
		std::string line = trimFront(str.substr(lineBegin, lineEnd - lineBegin));
		if (line.length() == 0)
		{
			lineBegin++;
			continue;
		}
		if (line[0] == '/' && line[1] == '/')
		{
			lineBegin = str.find('\n', lineBegin + 1);
			continue;
		}

		if (isPreprocessorDirective(line))
		{
			std::string directive = line.substr(1);
			auto tokens = split(directive, " ");
			if (tokens[0] == EFFECT_INCLUDE)
			{
				std::string includePath = tokens[1];

				// Todo: Don't make this a static path to Media/effects
				std::string includeData = FileSystem::readFile("Media/effects/" + includePath) + "\n";

				str.erase(lineBegin, lineEnd - lineBegin);
				str.insert(lineBegin, includeData);

				lineEnd = lineBegin + includeData.length();
			}
		}

		lineBegin = lineEnd;
	}

	end = str.find_last_of("}");

	lineBegin = 0;

	replaceAll(str, "$SCREEN_WIDTH", std::to_string(QGfx::getDefaultViewportDims().z));
	replaceAll(str, "$SCREEN_HEIGHT", std::to_string(QGfx::getDefaultViewportDims().w));

	end = str.find_last_of("}");

	lineBegin = 0;

	while (lineBegin < end)
	{
		if (lineBegin + 1 >= str.length())
		{
			break;
		}
        const size_t lineEnd = str.find('\n', lineBegin + 1);
		std::string line = trimFront(str.substr(lineBegin, lineEnd - lineBegin));
		if (line.length() == 0)
		{
			lineBegin++;
			continue;
		}
		if (line[0] == '/' && line[1] == '/')
		{
			lineBegin = str.find('\n', lineBegin + 1);
			continue;
		}
		if (isPreprocessorDirective(line))
		{
			IParseNode* ver;
			if (processPreprocessorDirective(line, &ver))
			{
				node->children.push_back(ver);
				ver->parent = node;
			}
			lineBegin = lineEnd;
			continue;
		}

		const size_t openBrace = str.find('{', lineBegin);
		size_t closeBrace = str.find('}', lineBegin);
		const size_t blockEnd = str.find(';', lineBegin);

		// process block
		if (openBrace < blockEnd)
		{
//			line = trimFront(str.substr(lineBegin, (closeBrace + 1) - (lineBegin - 1)));

			size_t openCount = 1, closeCount = 0;

			size_t i = openBrace + 1;
			while (!(openCount == closeCount))
			{
				const char c = str[i++];
				if (c == '{') openCount++;
				else if (c == '}') closeCount++;
			}

			line = trimFront(str.substr(lineBegin, i - lineBegin));

			if (isVariable(line))
			{
				IParseNode* n;
				parseVariable(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
            else if (isFunction(line) && !isMainFunction(line))
            {
				IParseNode* n;
				parseFunction(line, &n);
				node->children.push_back(n);
				n->parent = node;
            }
			else if (isMainFunction(line))
			{
				IParseNode* n;
				parseMainFunction(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
			else if (isInterface(line))
			{
				IParseNode* n;
				parseInterface(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
			else if (isTechnique(line))
			{
				IParseNode* n;
				parseTechnique(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}

			lineBegin = i + 1;
		}
		// process single line var
		else
		{
			line = trimFront(str.substr(lineBegin + 1, blockEnd - lineBegin));
			if (line.size() == 0)
			{
				lineBegin++;
				continue;
			}
			if (line[0] == '}' || line[0] == ';')
			{
				lineBegin++;
				continue;
			}
			if (isVariable(line))
			{
				IParseNode* n;
				parseVariable(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
			else if (isLayout(line))
			{
				IParseNode* n;
				parseLayout(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
			else if(isOutputBuffer(line))
			{
				IParseNode* n;
				parseOutputBuffer(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
			else if (isDepthBuffer(line))
			{
				IParseNode* n;
				parseDepthBuffer(line, &n);
				node->children.push_back(n);
				n->parent = node;
			}
			lineBegin = blockEnd + 1;
		}		
	}
}
