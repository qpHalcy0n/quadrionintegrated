#include "render/effectparser/EffectVariableType.h"

#include <assert.h>
#include "render/effectparser/EffectKeywords.h"

EffectVariableType getEffectVariableType(const std::string& aName)
{
    if(aName == EFFECT_VOID)
    {
		return EffectVariableType::VOID_TYPE;
    }
	if (aName == EFFECT_BOOL)
	{
		return EffectVariableType::BOOL;
	}
	if (aName == EFFECT_INT)
	{
		return EffectVariableType::INT;
	}
	if (aName == EFFECT_UINT)
	{
		return EffectVariableType::UINT;
	}
	if (aName == EFFECT_FLOAT)
	{
		return EffectVariableType::FLOAT;
	}
	if (aName == EFFECT_DOUBLE)
	{
		return EffectVariableType::DOUBLE;
	}
	if (aName == EFFECT_VEC2)
	{
		return EffectVariableType::VEC2;
	}
	if (aName == EFFECT_VEC3)
	{
		return EffectVariableType::VEC3;
	}
	if (aName == EFFECT_VEC4)
	{
		return EffectVariableType::VEC4;
	}
	if (aName == EFFECT_IVEC2)
	{
		return EffectVariableType::IVEC2;
	}
	if (aName == EFFECT_IVEC3)
	{
		return EffectVariableType::IVEC3;
	}
	if (aName == EFFECT_IVEC4)
	{
		return EffectVariableType::IVEC4;
	}
	if (aName == EFFECT_BVEC2)
	{
		return EffectVariableType::BVEC2;
	}
	if (aName == EFFECT_BVEC3)
	{
		return EffectVariableType::BVEC3;
	}
	if (aName == EFFECT_BVEC4)
	{
		return EffectVariableType::BVEC4;
	}
	if (aName == EFFECT_UVEC2)
	{
		return EffectVariableType::UVEC2;
	}
	if (aName == EFFECT_UVEC3)
	{
		return EffectVariableType::UVEC3;
	}
	if (aName == EFFECT_UVEC4)
	{
		return EffectVariableType::UVEC4;
	}
	if (aName == EFFECT_DVEC2)
	{
		return EffectVariableType::DVEC2;
	}
	if (aName == EFFECT_DVEC3)
	{
		return EffectVariableType::DVEC3;
	}
	if (aName == EFFECT_DVEC4)
	{
		return EffectVariableType::DVEC4;
	}
	if (aName == EFFECT_MAT2)
	{
		return EffectVariableType::MAT2;
	}
	if (aName == EFFECT_MAT3)
	{
		return EffectVariableType::MAT3;
	}
	if (aName == EFFECT_MAT4)
	{
		return EffectVariableType::MAT4;
	}
	if (aName == EFFECT_DMAT2)
	{
		return EffectVariableType::DMAT2;
	}
	if (aName == EFFECT_DMAT3)
	{
		return EffectVariableType::DMAT3;
	}
	if (aName == EFFECT_DMAT4)
	{
		return EffectVariableType::DMAT4;
	}
	if (aName == EFFECT_MAT2x3)
	{
		return EffectVariableType::MAT2x3;
	}
	if (aName == EFFECT_MAT2x4)
	{
		return EffectVariableType::MAT2x3;
	}
	if (aName == EFFECT_MAT3x2)
	{
		return EffectVariableType::MAT3x2;
	}
	if (aName == EFFECT_MAT3x4)
	{
		return EffectVariableType::MAT3x4;
	}
	if (aName == EFFECT_MAT4x2)
	{
		return EffectVariableType::MAT4x2;
	}
	if (aName == EFFECT_MAT4x3)
	{
		return EffectVariableType::MAT4x3;
	}
	if (aName == EFFECT_DMAT2x3)
	{
		return EffectVariableType::DMAT2x3;
	}
	if (aName == EFFECT_DMAT2x4)
	{
		return EffectVariableType::DMAT2x4;
	}
	if (aName == EFFECT_DMAT3x2)
	{
		return EffectVariableType::DMAT3x2;
	}
	if (aName == EFFECT_DMAT3x4)
	{
		return EffectVariableType::DMAT3x4;
	}
	if (aName == EFFECT_DMAT4x2)
	{
		return EffectVariableType::DMAT4x2;
	}
	if (aName == EFFECT_DMAT4x3)
	{
		return EffectVariableType::DMAT4x3;
	}
	if (aName == EFFECT_SAMPLER1D)
	{
		return EffectVariableType::SAMPLER1D;
	}
	if (aName == EFFECT_SAMPLER1DSHADOW)
	{
		return EffectVariableType::SAMPLER1DSHADOW;
	}
	if (aName == EFFECT_SAMPLER1DARRAY)
	{
		return EffectVariableType::SAMPLER1DARRAY;
	}
	if (aName == EFFECT_SAMPLER1DARRAYSHADOW)
	{
		return EffectVariableType::SAMPLER1DARRAYSHADOW;
	}
	if (aName == EFFECT_ISAMPLER1D)
	{
		return EffectVariableType::ISAMPLER1D;
	}
	if (aName == EFFECT_ISAMPLER1DARRAY)
	{
		return EffectVariableType::ISAMPLER1DARRAY;
	}
	if (aName == EFFECT_USAMPLER1D)
	{
		return EffectVariableType::USAMPLER1D;
	}
    if (aName == EFFECT_USAMPLER1DARRAY)
	{
		return EffectVariableType::USAMPLER1DARRAY;
	}
	if (aName == EFFECT_SAMPLER2D)
	{
		return EffectVariableType::SAMPLER2D;
	}
	if (aName == EFFECT_SAMPLER2DSHADOW)
	{
		return EffectVariableType::SAMPLER2DSHADOW;
	}
	if (aName == EFFECT_SAMPLER2DARRAY)
	{
		return EffectVariableType::SAMPLER2DARRAY;
	}
	if (aName == EFFECT_SAMPLER2DARRAYSHADOW)
	{
		return EffectVariableType::SAMPLER2DARRAYSHADOW;
	}
	if (aName == EFFECT_ISAMPLER2D)
	{
		return EffectVariableType::ISAMPLER2D;
	}
	if (aName == EFFECT_ISAMPLER2DARRAY)
	{
		return EffectVariableType::ISAMPLER2DARRAY;
	}
	if (aName == EFFECT_USAMPLER2D)
	{
		return EffectVariableType::USAMPLER2D;
	}
	if (aName == EFFECT_USAMPLER2DARRAY)
	{
		return EffectVariableType::USAMPLER2DARRAY;
	}
	if (aName == EFFECT_SAMPLER2DRECT)
	{
		return EffectVariableType::SAMPLER2DRECT;
	}
	if (aName == EFFECT_SAMPLER2DRECTSHADOW)
	{
		return EffectVariableType::SAMPLER2DRECTSHADOW;
	}
	if (aName == EFFECT_ISAMPLER2DRECT)
	{
		return EffectVariableType::ISAMPLER2DRECT;
	}
	if (aName == EFFECT_USAMPLER2DRECT)
	{
		return EffectVariableType::USAMPLER2DRECT;
	}
	if (aName == EFFECT_SAMPLER2DMS)
	{
		return EffectVariableType::SAMPLER2DMS;
	}
	if (aName == EFFECT_ISAMPLER2DMS)
	{
		return EffectVariableType::ISAMPLER2DMS;
	}
	if (aName == EFFECT_USAMPLER2DMS)
	{
		return EffectVariableType::USAMPLER2DMS;
	}
	if (aName == EFFECT_SAMPLER2DMSARRAY)
	{
		return EffectVariableType::SAMPLER2DMSARRAY;
	}
	if (aName == EFFECT_ISAMPLER2DMSARRAY)
	{
		return EffectVariableType::ISAMPLER2DMSARRAY;
	}
	if (aName == EFFECT_USAMPLER2DMSARRAY)
	{
		return EffectVariableType::USAMPLER2DMSARRAY;
	}
	if (aName == EFFECT_SAMPLER3D)
	{
		return EffectVariableType::SAMPLER3D;
	}
	if (aName == EFFECT_ISAMPLER3D)
	{
		return EffectVariableType::ISAMPLER3D;
	}
	if (aName == EFFECT_USAMPLER3D)
	{
		return EffectVariableType::USAMPLER3D;
	}
	if (aName == EFFECT_SAMPLERCUBE)
	{
		return EffectVariableType::SAMPLERCUBE;
	}
	if (aName == EFFECT_SAMPLERSHADOWCUBE)
	{
		return EffectVariableType::SAMPLERSHADOWCUBE;
	}
	if (aName == EFFECT_ISAMPLERCUBE)
	{
		return EffectVariableType::ISAMPLERCUBE;
	}
	if (aName == EFFECT_USAMPLERCUBE)
	{
		return EffectVariableType::USAMPLERCUBE;
	}
	if (aName == EFFECT_SAMPLERCUBEARRAY)
	{
		return EffectVariableType::SAMPLERCUBEARRAY;
	}
	if (aName == EFFECT_SAMPLERCUBESHADOWARRAY)
	{
		return EffectVariableType::SAMPLERCUBESHADOWARRAY;
	}
	if (aName == EFFECT_ISAMPLERCUBEARRAY)
	{
		return EffectVariableType::ISAMPLERCUBEARRAY;
	}
	if (aName == EFFECT_USAMPLERCUBEARRAY)
	{
		return EffectVariableType::USAMPLERCUBEARRAY;
	}
	if (aName == EFFECT_SAMPLERBUFFER)
	{
		return EffectVariableType::SAMPLERBUFFER;
	}
	if (aName == EFFECT_ISAMPLERBUFFER)
	{
		return EffectVariableType::ISAMPLERBUFFER;
	}
	if (aName == EFFECT_USAMPLERBUFFER)
	{
		return EffectVariableType::USAMPLERBUFFER;
	}
	if (aName == EFFECT_IMAGE1D)
	{
		return EffectVariableType::IMAGE1D;
	}
	if (aName == EFFECT_IIMAGE1D)
	{
		return EffectVariableType::IIMAGE1D;
	}
	if (aName == EFFECT_UIMAGE1D)
	{
		return EffectVariableType::UIMAGE1D;
	}
	if (aName == EFFECT_IMAGE1DARRAY)
	{
		return EffectVariableType::IMAGE1DARRAY;
	}
	if (aName == EFFECT_IIMAGE1DARRAY)
	{
		return EffectVariableType::IIMAGE1DARRAY;
	}
	if (aName == EFFECT_UIMAGE1DARRAY)
	{
		return EffectVariableType::UIMAGE1DARRAY;
	}
	if (aName == EFFECT_IMAGE2D)
	{
		return EffectVariableType::IMAGE2D;
	}
	if (aName == EFFECT_IIMAGE2D)
	{
		return EffectVariableType::IIMAGE2D;
	}
	if (aName == EFFECT_UIMAGE2D)
	{
		return EffectVariableType::UIMAGE2D;
	}
	if (aName == EFFECT_IMAGE2DARRAY)
	{
		return EffectVariableType::IMAGE2DARRAY;
	}
	if (aName == EFFECT_IIMAGE2DARRAY)
	{
		return EffectVariableType::IIMAGE2DARRAY;
	}
	if (aName == EFFECT_UIMAGE2DARRAY)
	{
		return EffectVariableType::UIMAGE2DARRAY;
	}
	if (aName == EFFECT_IMAGE2DRECT)
	{
		return EffectVariableType::IMAGE2DRECT;
	}
	if (aName == EFFECT_IIMAGE2DRECT)
	{
		return EffectVariableType::IIMAGE2DRECT;
	}
	if (aName == EFFECT_UIMAGE2DRECT)
	{
		return EffectVariableType::UIMAGE2DRECT;
	}
	if (aName == EFFECT_IMAGE2DMS)
	{
		return EffectVariableType::IMAGE2DMS;
	}
	if (aName == EFFECT_IIMAGE2DMS)
	{
		return EffectVariableType::IIMAGE2DMS;
	}
	if (aName == EFFECT_UIMAGE2DMS)
	{
		return EffectVariableType::UIMAGE2DMS;
	}
	if (aName == EFFECT_IMAGE2DMSARRAY)
	{
		return EffectVariableType::IMAGE2DMSARRAY;
	}
	if (aName == EFFECT_IIMAGE2DMSARRAY)
	{
		return EffectVariableType::IIMAGE2DMSARRAY;
	}
	if (aName == EFFECT_UIMAGE2DMSARRAY)
	{
		return EffectVariableType::UIMAGE2DMSARRAY;
	}
	if (aName == EFFECT_IMAGE3D)
	{
		return EffectVariableType::IMAGE3D;
	}
	if (aName == EFFECT_IIMAGE3D)
	{
		return EffectVariableType::IIMAGE3D;
	}
	if (aName == EFFECT_UIMAGE3D)
	{
		return EffectVariableType::UIMAGE3D;
	}
	if (aName == EFFECT_IMAGECUBE)
	{
		return EffectVariableType::IMAGECUBE;
	}
	if (aName == EFFECT_IIMAGECUBE)
	{
		return EffectVariableType::IIMAGECUBE;
	}
	if (aName == EFFECT_UIMAGECUBE)
	{
		return EffectVariableType::UIMAGECUBE;
	}
	if (aName == EFFECT_IMAGECUBEARRAY)
	{
		return EffectVariableType::IMAGECUBEARRAY;
	}
	if (aName == EFFECT_IIMAGECUBEARRAY)
	{
		return EffectVariableType::IIMAGECUBEARRAY;
	}
	if (aName == EFFECT_UIMAGECUBEARRAY)
	{
		return EffectVariableType::UIMAGECUBEARRAY;
	}
	if (aName == EFFECT_IMAGEBUFFER)
	{
		return EffectVariableType::IMAGEBUFFER;
	}
	if (aName == EFFECT_UIMAGEBUFFER)
	{
		return EffectVariableType::UIMAGEBUFFER;
	}
	if (aName == EFFECT_BUFFER)
	{
		return EffectVariableType::BUFFER;
	}
    if (aName == EFFECT_INTERFACE)
    {
		return EffectVariableType::INTERFACE;
    }

	//assert("Could not determine effect type");
	return EffectVariableType::UNKNOWN;
}

std::string getEffectVariableTypeString(const EffectVariableType& aType)
{
    switch(aType)
    {
	case EffectVariableType::VOID_TYPE: return EFFECT_VOID;  break;
	case EffectVariableType::BOOL: return EFFECT_BOOL; break;
	case EffectVariableType::INT: return EFFECT_INT; break;
	case EffectVariableType::UINT: return EFFECT_UINT;  break;
	case EffectVariableType::FLOAT: return EFFECT_FLOAT; break;
	case EffectVariableType::DOUBLE: return EFFECT_DOUBLE; break;
	case EffectVariableType::VEC2: return EFFECT_VEC2; break;
	case EffectVariableType::VEC3: return EFFECT_VEC3; break;
	case EffectVariableType::VEC4: return EFFECT_VEC4; break;
	case EffectVariableType::IVEC2: return EFFECT_IVEC2; break;
	case EffectVariableType::IVEC3: return EFFECT_IVEC3; break;
	case EffectVariableType::IVEC4: return EFFECT_IVEC4; break;
	case EffectVariableType::BVEC2: return EFFECT_BVEC2; break;
	case EffectVariableType::BVEC3: return EFFECT_BVEC3; break;
	case EffectVariableType::BVEC4: return EFFECT_BVEC4; break;
	case EffectVariableType::UVEC2: return EFFECT_UVEC2; break;
	case EffectVariableType::UVEC3: return EFFECT_UVEC3; break;
	case EffectVariableType::UVEC4: return EFFECT_UVEC4; break;
	case EffectVariableType::DVEC2: return EFFECT_DVEC2; break;
	case EffectVariableType::DVEC3: return EFFECT_DVEC3; break;
	case EffectVariableType::DVEC4: return EFFECT_DVEC4; break;
	case EffectVariableType::MAT2: return EFFECT_MAT2; break;
	case EffectVariableType::MAT3: return EFFECT_MAT3; break;
	case EffectVariableType::MAT4: return EFFECT_MAT4; break;
	case EffectVariableType::MAT2x3: return EFFECT_MAT2x3; break;
	case EffectVariableType::MAT2x4: return EFFECT_MAT2x4; break;
	case EffectVariableType::MAT3x2: return EFFECT_MAT3x2; break;
	case EffectVariableType::MAT3x4: return EFFECT_MAT3x4; break;
	case EffectVariableType::MAT4x2: return EFFECT_MAT4x2; break;
	case EffectVariableType::MAT4x3: return EFFECT_MAT4x3; break;
	case EffectVariableType::DMAT2: return EFFECT_DMAT2; break;
	case EffectVariableType::DMAT3: return EFFECT_DMAT3; break;
	case EffectVariableType::DMAT4: return EFFECT_DMAT4; break;
	case EffectVariableType::DMAT2x3: return EFFECT_DMAT2x3; break;
	case EffectVariableType::DMAT2x4: return EFFECT_DMAT2x4; break;
	case EffectVariableType::DMAT3x2: return EFFECT_DMAT3x2; break;
	case EffectVariableType::DMAT3x4: return EFFECT_DMAT3x4; break;
	case EffectVariableType::DMAT4x2: return EFFECT_DMAT4x2; break;
	case EffectVariableType::DMAT4x3: return EFFECT_DMAT4x3; break;
	case EffectVariableType::SAMPLER1D: return EFFECT_SAMPLER1D; break;
	case EffectVariableType::SAMPLER1DSHADOW: return EFFECT_SAMPLER1DSHADOW; break;
	case EffectVariableType::SAMPLER1DARRAY: return EFFECT_SAMPLER1DARRAY; break;
	case EffectVariableType::SAMPLER1DARRAYSHADOW: return EFFECT_SAMPLER1DARRAYSHADOW; break;
	case EffectVariableType::ISAMPLER1D: return EFFECT_ISAMPLER1D; break;
	case EffectVariableType::ISAMPLER1DARRAY: return EFFECT_ISAMPLER1DARRAY; break;
	case EffectVariableType::USAMPLER1D: return EFFECT_USAMPLER1D; break;
	case EffectVariableType::USAMPLER1DARRAY: return EFFECT_USAMPLER1DARRAY; break;
	case EffectVariableType::SAMPLER2D: return EFFECT_SAMPLER2D; break;
	case EffectVariableType::SAMPLER2DSHADOW: return EFFECT_SAMPLER2DSHADOW; break;
	case EffectVariableType::SAMPLER2DARRAY: return EFFECT_SAMPLER2DARRAY; break;
	case EffectVariableType::SAMPLER2DARRAYSHADOW: return EFFECT_SAMPLER2DARRAYSHADOW; break;
	case EffectVariableType::ISAMPLER2D: return EFFECT_ISAMPLER2D; break;
	case EffectVariableType::ISAMPLER2DARRAY: return EFFECT_ISAMPLER2DARRAY; break;
	case EffectVariableType::USAMPLER2D: return EFFECT_USAMPLER2D; break;
	case EffectVariableType::USAMPLER2DARRAY: return EFFECT_USAMPLER2DARRAY; break;
	case EffectVariableType::SAMPLER2DRECT: return EFFECT_SAMPLER2DRECT; break;
	case EffectVariableType::SAMPLER2DRECTSHADOW: return EFFECT_SAMPLER2DRECTSHADOW; break;
	case EffectVariableType::ISAMPLER2DRECT: return EFFECT_ISAMPLER2DRECT; break;
	case EffectVariableType::USAMPLER2DRECT: return EFFECT_USAMPLER2DRECT; break;
	case EffectVariableType::SAMPLER2DMS: return EFFECT_SAMPLER2DMS; break;
	case EffectVariableType::ISAMPLER2DMS: return EFFECT_ISAMPLER2DMS; break;
	case EffectVariableType::USAMPLER2DMS: return EFFECT_USAMPLER2DMS; break;
	case EffectVariableType::SAMPLER2DMSARRAY: return EFFECT_SAMPLER2DMSARRAY; break;
	case EffectVariableType::ISAMPLER2DMSARRAY: return EFFECT_ISAMPLER2DMSARRAY; break;
	case EffectVariableType::USAMPLER2DMSARRAY: return EFFECT_USAMPLER2DMSARRAY; break;
	case EffectVariableType::SAMPLER3D: return EFFECT_SAMPLER3D; break;
	case EffectVariableType::ISAMPLER3D: return EFFECT_ISAMPLER3D; break;
	case EffectVariableType::USAMPLER3D: return EFFECT_USAMPLER3D; break;
	case EffectVariableType::SAMPLERCUBE: return EFFECT_SAMPLERCUBE; break;
	case EffectVariableType::SAMPLERSHADOWCUBE: return EFFECT_SAMPLERSHADOWCUBE; break;
	case EffectVariableType::ISAMPLERCUBE: return EFFECT_ISAMPLERCUBE; break;
	case EffectVariableType::USAMPLERCUBE: return EFFECT_USAMPLERCUBE; break;
	case EffectVariableType::SAMPLERCUBEARRAY: return EFFECT_SAMPLERCUBEARRAY; break;
	case EffectVariableType::SAMPLERCUBESHADOWARRAY: return EFFECT_SAMPLERCUBESHADOWARRAY; break;
	case EffectVariableType::ISAMPLERCUBEARRAY: return EFFECT_ISAMPLERCUBEARRAY; break;
	case EffectVariableType::USAMPLERCUBEARRAY: return EFFECT_USAMPLERCUBEARRAY; break;
	case EffectVariableType::SAMPLERBUFFER: return EFFECT_SAMPLERBUFFER; break;
	case EffectVariableType::ISAMPLERBUFFER: return EFFECT_ISAMPLERBUFFER; break;
	case EffectVariableType::USAMPLERBUFFER: return EFFECT_USAMPLERBUFFER; break;
	case EffectVariableType::IMAGE1D: return EFFECT_IMAGE1D; break;
	case EffectVariableType::IIMAGE1D: return EFFECT_IIMAGE1D; break;
	case EffectVariableType::UIMAGE1D: return EFFECT_UIMAGE1D; break;
	case EffectVariableType::IMAGE1DARRAY: return EFFECT_IMAGE1DARRAY; break;
	case EffectVariableType::IIMAGE1DARRAY: return EFFECT_IIMAGE1DARRAY; break;
	case EffectVariableType::UIMAGE1DARRAY: return EFFECT_UIMAGE1DARRAY; break;
	case EffectVariableType::IMAGE2D: return EFFECT_IMAGE2D; break;
	case EffectVariableType::IIMAGE2D: return EFFECT_IIMAGE2D; break;
	case EffectVariableType::UIMAGE2D: return EFFECT_UIMAGE2D; break;
	case EffectVariableType::IMAGE2DARRAY: return EFFECT_IMAGE2DARRAY; break;
	case EffectVariableType::IIMAGE2DARRAY: return EFFECT_IIMAGE2DARRAY; break;
	case EffectVariableType::UIMAGE2DARRAY: return EFFECT_UIMAGE2DARRAY; break;
        // TODO (Roderick, Nick): Finish this
        case EffectVariableType::IMAGE2DRECT: break;
        case EffectVariableType::IIMAGE2DRECT: break;
        case EffectVariableType::UIMAGE2DRECT: break;
        case EffectVariableType::IMAGE2DMS: break;
        case EffectVariableType::IIMAGE2DMS: break;
        case EffectVariableType::UIMAGE2DMS: break;
        case EffectVariableType::IMAGE2DMSARRAY: break;
        case EffectVariableType::IIMAGE2DMSARRAY: break;
        case EffectVariableType::UIMAGE2DMSARRAY: break;
        case EffectVariableType::IMAGE3D: break;
        case EffectVariableType::IIMAGE3D: break;
        case EffectVariableType::UIMAGE3D: break;
        case EffectVariableType::IMAGECUBE: break;
        case EffectVariableType::IIMAGECUBE: break;
        case EffectVariableType::UIMAGECUBE: break;
        case EffectVariableType::IMAGECUBEARRAY: break;
        case EffectVariableType::IIMAGECUBEARRAY: break;
        case EffectVariableType::UIMAGECUBEARRAY: break;
        case EffectVariableType::IMAGEBUFFER: break;
        case EffectVariableType::UIMAGEBUFFER: break;
        case EffectVariableType::BUFFER: break;
        case EffectVariableType::INTERFACE: break;
        case EffectVariableType::UNKNOWN: break;
        default: ;
    }

	return "ERROR";
}
