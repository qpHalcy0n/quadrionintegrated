#include "render/effectparser/EffectInterfaceSpecifier.h"

#include <assert.h>
#include "render/effectparser/EffectKeywords.h"

EffectInterfaceSpecifier getEffectInterfaceSpecifier(const std::string& aString)
{
	if (aString == EFFECT_IN)
	{
		return EffectInterfaceSpecifier::INTERFACE_IN;
	}
	else if (aString == EFFECT_OUT)
	{
		return EffectInterfaceSpecifier::INTERFACE_OUT;
	}

	assert("Could not determine effect type");
	return EffectInterfaceSpecifier::UNKNOWN;
}

std::string getEffectInterfaceSpecifierString(const EffectInterfaceSpecifier& specifier)
{
    switch(specifier)
    {
	    case EffectInterfaceSpecifier::INTERFACE_IN: return EFFECT_IN;  break;
	    case EffectInterfaceSpecifier::INTERFACE_OUT: return EFFECT_OUT; break;
	    case EffectInterfaceSpecifier::UNKNOWN: return "ERROR";  break;
        default: return "ERROR";  break;
    }
}
