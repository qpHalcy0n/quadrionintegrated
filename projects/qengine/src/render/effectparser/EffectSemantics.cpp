#include "render/effectparser/EffectSemantics.h"

#include <cctype>
#include "render/effectparser/EffectKeywords.h"

EffectSemantic getEffectSemanticType(const std::string& aName)
{
    if(isdigit(aName.c_str()[0]))
    {
		return static_cast<EffectSemantic>(atoi(aName.c_str()));
    }

    if(aName == EFFECT_POSITION)
    {
		return EffectSemantic::POSITION;
    }
    if(aName == EFFECT_NORMAL0)
    {
		return EffectSemantic::NORMAL0;
    }
	if (aName == EFFECT_NORMAL1)
	{
		return EffectSemantic::NORMAL1;
	}
	if (aName == EFFECT_TEXCOORD0)
	{
		return EffectSemantic::TEXCOORD0;
	}
	if (aName == EFFECT_TEXCOORD1)
	{
		return EffectSemantic::TEXCOORD1;
	}
	if (aName == EFFECT_COLOR0)
	{
		return EffectSemantic::COLOR0;
	}
	if (aName == EFFECT_COLOR1)
	{
		return EffectSemantic::COLOR1;
	}

	return EffectSemantic::UNKNOWN;
}
