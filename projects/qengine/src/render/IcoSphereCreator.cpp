#include "render/IcoSphereCreator.h"

class IcoSphereCreatorImpl
{
	public:

		GeomData data;

		int mIndex;
		std::map<int64_t, int> middlePointIndexCache;

		std::vector<vec3<float>> positions;
		std::vector<uint32_t> triangles;

		float mRadius;

		int addVertex(vec3<float> aP)
		{
			aP.normalize();
			positions.push_back(aP);
			return mIndex++;
		}

		int getMiddlePoint(const int aP1, const int aP2)
		{
			const bool firstIsSmaller = aP1 < aP2;
			const int64_t smallerIndex = firstIsSmaller ? aP1 : aP2;
			const int64_t greaterIndex = firstIsSmaller ? aP2 : aP1;
			const int64_t key = (smallerIndex << 32) + greaterIndex;

			if (middlePointIndexCache.find(key) != middlePointIndexCache.end())
			{
				return middlePointIndexCache[key];
			}

			const vec3<float> point1 = positions[aP1];
			const vec3<float> point2 = positions[aP2];
			const vec3<float> middle = vec3<float>((point1.x + point2.x) / 2.0f,
				(point1.y + point2.y) / 2.0f,
				(point1.z + point2.z) / 2.0f);

			const int i = addVertex(middle);
			middlePointIndexCache[key] = i;

			return i;
		}
};

IcoSphereCreator::IcoSphereCreator()
{
	mImpl = new IcoSphereCreatorImpl;
}

IcoSphereCreator::~IcoSphereCreator()
{
	if (mImpl)
	{
		delete mImpl;
		mImpl = nullptr;
	}
}

const GeomData* const IcoSphereCreator::getData()
{
	if(mImpl)
		return &(mImpl->data);

	return nullptr;
}

void IcoSphereCreator::create(const int aRecursion, const float aRadius)
{
	mImpl->mIndex = 0;
	mImpl->mRadius = aRadius;

	const float t = (1.0f + sqrtf(5.0f)) / 2.0f;

	mImpl->addVertex(vec3<float>(-1, t, 0));
	mImpl->addVertex(vec3<float>(1, t, 0));
	mImpl->addVertex(vec3<float>(-1, -t, 0));
	mImpl->addVertex(vec3<float>(1, -t, 0));

	mImpl->addVertex(vec3<float>(0, -1, t));
	mImpl->addVertex(vec3<float>(0, 1, t));
	mImpl->addVertex(vec3<float>(0, -1, -t));
	mImpl->addVertex(vec3<float>(0, 1, -t));

	mImpl->addVertex(vec3<float>(t, 0, -1));
	mImpl->addVertex(vec3<float>(t, 0, 1));
	mImpl->addVertex(vec3<float>(-t, 0, -1));
	mImpl->addVertex(vec3<float>(-1, 0, 1));

	std::vector<TriangleIndices*> faces;
	faces.push_back(new TriangleIndices(0, 11, 5));
	faces.push_back(new TriangleIndices(0, 5, 1));
	faces.push_back(new TriangleIndices(0, 1, 7));
	faces.push_back(new TriangleIndices(0, 7, 10));
	faces.push_back(new TriangleIndices(0, 10, 11));

	faces.push_back(new TriangleIndices(1, 5, 9));
	faces.push_back(new TriangleIndices(5, 11, 4));
	faces.push_back(new TriangleIndices(11, 10, 2));
	faces.push_back(new TriangleIndices(10, 7, 6));
	faces.push_back(new TriangleIndices(7, 1, 8));

	faces.push_back(new TriangleIndices(3, 9, 4));
	faces.push_back(new TriangleIndices(3, 4, 2));
	faces.push_back(new TriangleIndices(3, 2, 6));
	faces.push_back(new TriangleIndices(3, 6, 8));
	faces.push_back(new TriangleIndices(3, 8, 9));

	faces.push_back(new TriangleIndices(4, 9, 5));
	faces.push_back(new TriangleIndices(2, 4, 11));
	faces.push_back(new TriangleIndices(6, 2, 10));
	faces.push_back(new TriangleIndices(8, 6, 7));
	faces.push_back(new TriangleIndices(9, 8, 1));

	for (int i = 0; i < aRecursion; i++)
	{
		std::vector<TriangleIndices*> faces2;
		for (TriangleIndices* tri : faces)
		{
			const int a = mImpl->getMiddlePoint(tri->v1, tri->v2);
			const int b = mImpl->getMiddlePoint(tri->v2, tri->v3);
			const int c = mImpl->getMiddlePoint(tri->v3, tri->v1);

			faces2.push_back(new TriangleIndices(tri->v1, a, c));
			faces2.push_back(new TriangleIndices(tri->v2, b, a));
			faces2.push_back(new TriangleIndices(tri->v3, c, b));
			faces2.push_back(new TriangleIndices(a, b, c));
		}
		faces.assign(faces2.begin(), faces2.end());
	}

	for (TriangleIndices* tri : faces)
	{
		mImpl->triangles.push_back(tri->v1);
		mImpl->triangles.push_back(tri->v2);
		mImpl->triangles.push_back(tri->v3);
	}

	std::vector<float> posData;
	for (const vec3<float> pos : mImpl->positions)
	{
		posData.push_back(pos.x * mImpl->mRadius);
		posData.push_back(pos.y * mImpl->mRadius);
		posData.push_back(pos.z * mImpl->mRadius);
	}

	mImpl->data.nIndices = static_cast<uint32_t>(mImpl->triangles.size());
	mImpl->data.nVertices = static_cast<uint32_t>(mImpl->positions.size());

	mImpl->data.indices = new uint32_t[mImpl->triangles.size()];
	mImpl->data.vertices = new float[posData.size()];

	std::memcpy(mImpl->data.indices, &mImpl->triangles[0], 
				sizeof(uint32_t) * mImpl->triangles.size());
	std::memcpy(mImpl->data.vertices, &posData[0], sizeof(float) * posData.size());
}