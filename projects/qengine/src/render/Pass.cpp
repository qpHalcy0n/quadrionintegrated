#include "render/Pass.h"
#include "render/Effect.h"
#include "render/QGfx.h"
#include "StringUtils.h"
#include "assets/AssetManager.h"
#include "assets/TextureAsset.h"

namespace detail
{
	GLint lastPolygonMode[2];
	GLint lastViewport[4];
	GLint lastScissorBox[4];
	GLenum lastBlendSrcRGB;
	GLenum lastBlendDstRGB;
	GLenum lastBlendSrcAlpha;
	GLenum lastBlendDstAlpha;
	GLenum lastBlendEquationRGB;
	GLenum lastBlendEquationAlpha;
	GLboolean lastEnableBlend;
	GLboolean lastEnableCullFace;
	GLboolean lastEnableDepthTest;
	GLboolean lastEnableScissorTest;
	GLboolean lastEnableStencilTest;
	GLboolean lastColorWrite[4];
}

void RasterizerState::enable() const
{
	glGetIntegerv(GL_POLYGON_MODE, detail::lastPolygonMode);
	glGetIntegerv(GL_SCISSOR_BOX, detail::lastScissorBox);
	glGetIntegerv(GL_VIEWPORT, detail::lastViewport);
	glGetIntegerv(GL_BLEND_SRC_RGB, reinterpret_cast<GLint*>(&detail::lastBlendSrcRGB));
	glGetIntegerv(GL_BLEND_DST_RGB, reinterpret_cast<GLint*>(& detail::lastBlendDstRGB));
	glGetIntegerv(GL_BLEND_SRC_ALPHA, reinterpret_cast<GLint*>(& detail::lastBlendSrcAlpha));
	glGetIntegerv(GL_BLEND_DST_ALPHA, reinterpret_cast<GLint*>(& detail::lastBlendDstAlpha));
	glGetIntegerv(GL_BLEND_EQUATION_RGB, reinterpret_cast<GLint*>(&detail::lastBlendEquationRGB));
	glGetIntegerv(GL_BLEND_EQUATION_ALPHA, reinterpret_cast<GLint*>(& detail::lastBlendEquationAlpha));
	glGetIntegerv(GL_COLOR_WRITEMASK, reinterpret_cast<GLint*>(& detail::lastColorWrite));

	detail::lastEnableBlend = glIsEnabled(GL_BLEND);
	detail::lastEnableCullFace = glIsEnabled(GL_CULL_FACE);
	detail::lastEnableDepthTest = glIsEnabled(GL_DEPTH_TEST);
	detail::lastEnableScissorTest = glIsEnabled(GL_SCISSOR_TEST);
	detail::lastEnableStencilTest = glIsEnabled(GL_STENCIL_TEST);

	if(polygon.length() > 0)
		glPolygonMode(StringUtils::parseToGLEnum(polygon), StringUtils::parseToGLEnum(polygonMode));

	if (depthTest == "True")
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}

	if (depthWrite == "True")
	{
		glDepthMask(true);
	}
	else
	{
		glDepthMask(false);
	}

	if(depthFunc.length() > 0)
		glDepthFunc(StringUtils::parseToGLEnum(depthFunc));

	if (cullEnable == "True")
	{
		glEnable(GL_CULL_FACE);
	}
	else
	{
		glDisable(GL_CULL_FACE);
	}

	if(cullFace.length() > 0)
		glCullFace(StringUtils::parseToGLEnum(cullFace));

	if(frontFace.length() > 0)
		glFrontFace(StringUtils::parseToGLEnum(frontFace));

	if(colorR.length() > 0)
		glColorMask(StringUtils::parseToGLEnum(colorR), StringUtils::parseToGLEnum(colorG), StringUtils::parseToGLEnum(colorB), StringUtils::parseToGLEnum(colorA));

	if (alphaBlend == "True")
	{
		glEnable(GL_BLEND);
	}
	else
	{
		glDisable(GL_BLEND);
	}

	if(alphaSrc.length() > 0)
		glBlendFunc(StringUtils::parseToGLEnum(alphaSrc), StringUtils::parseToGLEnum(alphaDst));

	if (stencilTest == "True")
	{
		glEnable(GL_STENCIL_TEST);
	}
	else
	{
		glDisable(GL_STENCIL_TEST);
	}

	if (stencilFunc != "")
		glStencilFunc(StringUtils::parseToGLEnum(stencilFunc), stencilRef, stencilMask);

	if (stencilsFail != "")
		glStencilOp(StringUtils::parseToGLEnum(stencilsFail), StringUtils::parseToGLEnum(stencildpFail), StringUtils::parseToGLEnum(stencilPass));
}

void RasterizerState::disable() const
{
	glPolygonMode(GL_FRONT_AND_BACK, (GLenum)detail::lastPolygonMode[0]);
	glViewport(detail::lastViewport[0], detail::lastViewport[1], (GLsizei)detail::lastViewport[2], (GLsizei)detail::lastViewport[3]);
	glScissor(detail::lastScissorBox[0], detail::lastScissorBox[1], (GLsizei)detail::lastScissorBox[2], (GLsizei)detail::lastScissorBox[3]);
	glBlendFunc(detail::lastBlendSrcRGB, detail::lastBlendDstRGB);
	glAlphaFunc(detail::lastBlendSrcAlpha, GLclampf(detail::lastBlendDstAlpha));
	glBlendEquation(detail::lastBlendEquationRGB);

	if(detail::lastEnableBlend)
	{
		glEnable(GL_BLEND);
	}
	else
	{
		glDisable(GL_BLEND);
	}

	if(detail::lastEnableCullFace)
	{
		glEnable(GL_CULL_FACE);
	}
	else
	{
		glDisable(GL_CULL_FACE);
	}

	if(detail::lastEnableDepthTest)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}

	if(detail::lastEnableScissorTest)
	{
		glEnable(GL_SCISSOR_TEST);
	}
	else
	{
		glDisable(GL_SCISSOR_TEST);
	}

	if(detail::lastEnableStencilTest)
	{
		glEnable(GL_STENCIL_TEST);
	}
	else
	{
		glDisable(GL_STENCIL_TEST);
	}
}

void SamplerObject::bind(uint32_t unit) const
{
	glBindSampler(unit, samplerID);
}

class PassImpl
{
public:
	PassImpl() = default;
	~PassImpl()
	{
		delete shader;
	}

	Shader* shader;

	std::string name;

	std::string passSource;

	std::string vertexSource;
	std::string fragmentSource;
	std::string tcsSource;
	std::string tesSource;
	std::string geometrySource;

	Technique* technique;

	RasterizerState* rasterizerState;
	std::vector<SamplerObject*> samplerObjects;

	//		QHandle<QEntityManager> entityManager;

	std::unordered_map<std::string, std::pair<EffectVariableType, std::string>> samplerLoadPaths;

	uint32_t clearMode;
	vec4<float> clearColor;
};

Pass::Pass(const std::string& aName, Technique* aTechnique)
{
	mImpl = new PassImpl();
	mImpl->name = aName;

	mOutputBufferInfo = nullptr;

	mImpl->technique = aTechnique;
}

/*
Pass::Pass(const std::string& aName,
				 Technique* aTechnique)
{
	mImpl = new PassImpl();
	mImpl->name = aName;
	mImpl->technique = aTechnique;
//	mImpl->entityManager = aEntityManager;
}
*/
Pass::~Pass()
{
	delete mImpl;
}

vec4<float> Pass::getClearColor()
{
	return mImpl->clearColor;
}

uint32_t Pass::getClearMode()
{
	return mImpl->clearMode;
}

void Pass::compile() const
{
	mImpl->shader = new Shader(mImpl->technique, const_cast<Pass*>(this), mImpl->technique->getEffect(), mImpl->name, mImpl->vertexSource, mImpl->fragmentSource, mImpl->geometrySource, mImpl->tcsSource, mImpl->tesSource);
}

void Pass::bind() const
{
	// TODO: Verify pass bind events
//	QEventManager::getInstance()->send<PassBoundEvent>(mImpl->technique->getName().c_str(), this->getName().c_str());
//	Effect::current = mImpl->technique->getEffect();
	mImpl->shader->bind();

	// automatically bind sampler objects //
	for (auto samplerObj : mImpl->samplerLoadPaths)
	{
		if(samplerObj.second.first == EffectVariableType::SAMPLER2D)
		{
			auto textureAsset = AssetManager::instance().get<TextureAsset>(samplerObj.second.second);
			if (textureAsset)
			{
				mImpl->shader->setTexture(samplerObj.first, textureAsset->getTextureID());
			}
		}
	}

	if (mImpl->rasterizerState != nullptr)
	{
		mImpl->rasterizerState->enable();
	}

	glClearColor(mImpl->clearColor.x, mImpl->clearColor.y, mImpl->clearColor.z, mImpl->clearColor.w);

	GLboolean depthWriteEnabled;
	glGetBooleanv(GL_DEPTH_WRITEMASK, &depthWriteEnabled);
	glDepthMask(true);
	glClearDepth(1.0);
	glClear(mImpl->clearMode);
	if (!depthWriteEnabled)
		glDepthMask(false);
}

void Pass::unbind() const
{
	mImpl->shader->unbind();

	if (mImpl->rasterizerState != nullptr)
	{
		//mImpl->rasterizerState->disable();
	}

	QGfx::setDefaultState();
}

Shader* Pass::getShader() const
{
	return mImpl->shader;
}

bool Pass::hasSampler(const std::string& aName) const
{
	for (auto sampler : mImpl->samplerObjects)
	{
		if (sampler->name == aName.c_str())
		{
			return true;
		}
	}

	return false;
}

std::string& Pass::getName() const
{
	return mImpl->name;
}

void Pass::_setName(const std::string& aName) const
{
	mImpl->name = aName;
}

void Pass::_setVertexSource(const std::string& aSource) const
{
	mImpl->vertexSource = aSource;
}

void Pass::_setTCSSource(const std::string& aSource) const
{
	mImpl->tcsSource = aSource;
}

void Pass::_setTESSource(const std::string& aSource) const
{
	mImpl->tesSource = aSource;
}

void Pass::_setGeometrySource(const std::string& aSource) const
{
	mImpl->geometrySource = aSource;
}

void Pass::_setFragmentSource(const std::string& aSource) const
{
	mImpl->fragmentSource = aSource;
}

void Pass::_setSource(const std::string& aSource) const
{
	mImpl->passSource = aSource;
}

void Pass::_setClearColor(vec4<float> aColor)
{
	mImpl->clearColor = aColor;
}

void Pass::_setClearMode(uint32_t aMode)
{
	mImpl->clearMode = aMode;
}

std::string& Pass::getVertexSource() const
{
	return mImpl->vertexSource;
}

std::string& Pass::getGeometrySource() const
{
	return mImpl->geometrySource;
}

std::string& Pass::getFragmentSource() const
{
	return mImpl->fragmentSource;
}

std::string& Pass::getPassSource() const
{
	return mImpl->passSource;
}

void Pass::setRasterizerState(RasterizerState* state)
{
	mImpl->rasterizerState = state;
}

RasterizerState* Pass::getRasterizerState() const
{
	return mImpl->rasterizerState;
}

void Pass::addSamplerState(SamplerObject* state)
{
	mImpl->samplerObjects.push_back(state);
}

SamplerObject* Pass::getSamplerState(const std::string& textureName)
{
	for (auto sampler : mImpl->samplerObjects)
	{
		if (sampler->name == textureName)
		{
			return sampler;
		}
	}

	return nullptr;
}

void Pass::setOutputBuffer(OutputBufferInfo* aInfo)
{
	mOutputBufferInfo = aInfo;
}

OutputBufferInfo* Pass::getOutputBuffer() const
{
	return mOutputBufferInfo;
}

void Pass::setSamplerLoadPaths(const std::unordered_map<std::string, std::pair<EffectVariableType, std::string>>& aPaths) const
{
	mImpl->samplerLoadPaths = aPaths;
}

//TODO: Make this not suck (Rod)
std::unordered_map<std::string, std::pair<EffectVariableType, std::string>>& Pass::getSamplerLoadPaths()
{
	return mImpl->samplerLoadPaths;
}

