#include "render/Mesh.h"
#include "render/QGfx.h"

Mesh::Mesh(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer)
{
	mVAO = aVertexArray;
	mIBO = aIndexBuffer;
	mNumMeshInstances = 0;
	mInstanceBufferHandle = 0;
}

Mesh::Mesh(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer,
		   const MeshInstance* aMeshInstances, uint32_t aNumMeshInstances)
{
	mVAO = aVertexArray;
	mIBO = aIndexBuffer;

	mNumMeshInstances = aNumMeshInstances;

	QGfx::GL::gl_GenBuffers(1, &mInstanceBufferHandle);
	QGfx::GL::gl_BindBuffer(GL_TEXTURE_BUFFER, mInstanceBufferHandle);
	QGfx::GL::gl_BufferData(GL_TEXTURE_BUFFER, sizeof(MeshInstance) * mNumMeshInstances, 
							nullptr, GL_STATIC_DRAW);

	glGenTextures(1, &mInstanceTextureHandle);
	glBindTexture(GL_TEXTURE_BUFFER, mInstanceTextureHandle);
	QGfx::GL::gl_TexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, mInstanceBufferHandle);
	MeshInstance* InstancedMem = (MeshInstance*)QGfx::GL::gl_MapBufferRange(GL_TEXTURE_BUFFER,
								0,
								sizeof(vec4<float>) * mNumMeshInstances,
								GL_MAP_WRITE_BIT | \
								GL_MAP_INVALIDATE_BUFFER_BIT | \
								GL_MAP_UNSYNCHRONIZED_BIT);

	for (uint32_t i = 0; i < mNumMeshInstances; ++i)
	{
		InstancedMem[static_cast<size_t>(i)].position.x = aMeshInstances[static_cast<size_t>(i)].position.x;
		InstancedMem[static_cast<size_t>(i)].position.y = aMeshInstances[static_cast<size_t>(i)].position.y;
		InstancedMem[static_cast<size_t>(i)].position.z = aMeshInstances[static_cast<size_t>(i)].position.z;
		InstancedMem[static_cast<size_t>(i)].position.w = aMeshInstances[static_cast<size_t>(i)].position.w;
	}

	QGfx::GL::gl_UnmapBuffer(GL_TEXTURE_BUFFER);
	QGfx::GL::gl_BindTexture(GL_TEXTURE_BUFFER, 0);
	QGfx::GL::gl_BindBuffer(GL_TEXTURE_BUFFER, 0);
}

Mesh::~Mesh()
{
	delete mVAO;
	delete mIBO;
}

void Mesh::bind() const
{
	mVAO->bind();
	mIBO->bind();
}

void Mesh::unbind() const
{
	mIBO->unbind();
	mVAO->unbind();
}

uint32_t Mesh::getVertexCount() const
{
	// TODO (Roderick): Implement
	return 0;
}

uint32_t Mesh::getIndexCount() const
{
	return mIBO->getCount();
}

uint32_t Mesh::getNumMeshInstances() const
{
	return mNumMeshInstances;
}

uint32_t Mesh::getInstanceTextureHandle() const
{
	return mInstanceTextureHandle;
}