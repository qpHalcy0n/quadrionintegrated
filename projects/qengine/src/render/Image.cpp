#define NOMINMAX

#include <algorithm>
#include <vector>
#include <functional>
#include <cctype>
#include <string.h>
#include "QMath.h"
#include "render/Render.h"
#include "render/Image.h"
#include "FreeImage.h"
#include "StringUtils.h"
#include "render/QGfx.h"


const unsigned int DDPF_ALPHAPIXELS = 0x00000001;
const unsigned int DDPF_FOURCC      = 0x00000004;
const unsigned int DDPF_RGB         = 0x00000040;
const unsigned int DDSD_CAPS        = 0x00000001;
const unsigned int DDSD_HEIGHT      = 0x00000002;
const unsigned int DDSD_WIDTH       = 0x00000004;
const unsigned int DDSD_PITCH       = 0x00000008;
const unsigned int DDSD_PIXELFORMAT = 0x00001000;
const unsigned int DDSD_MIPMAPCOUNT = 0x00020000;
const unsigned int DDSD_LINEARSIZE  = 0x00080000;
const unsigned int DDSD_DEPTH       = 0x00800000;
const unsigned int DDSCAPS_COMPLEX  = 0x00000008;
const unsigned int DDSCAPS_TEXTURE  = 0x00001000;
const unsigned int DDSCAPS_MIPMAP   = 0x00400000;
const unsigned int DDSCAPS2_VOLUME  = 0x00200000;
const unsigned int DDSCAPS2_CUBEMAP = 0x00000200;
const unsigned int DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400;
const unsigned int DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800;
const unsigned int DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000;
const unsigned int DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000;
const unsigned int DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000;
const unsigned int DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000;
#define DDSCAPS2_CUBEMAP_ALL_FACES (DDSCAPS2_CUBEMAP_POSITIVEX |		\
									DDSCAPS2_CUBEMAP_NEGATIVEX |		\
									DDSCAPS2_CUBEMAP_POSITIVEY |		\
									DDSCAPS2_CUBEMAP_NEGATIVEY |		\
									DDSCAPS2_CUBEMAP_POSITIVEZ |		\
									DDSCAPS2_CUBEMAP_NEGATIVEZ)


unsigned int textureFourCc(const uint8_t aC0,
                           const uint8_t aC1,
                           const uint8_t aC2,
                           const uint8_t aC3)
{
	return static_cast<unsigned int>(aC0 | (aC1 << 8) | (aC2 << 16) | (aC3 << 24));
}

bool textureIsDepthFormat(const ETexturePixelFormat& aFmt)
{
	return ((aFmt >= QTEXTURE_FORMAT_DEPTH16) && (aFmt <= QTEXTURE_FORMAT_DEPTH24));
}

bool textureIsPlainFormat(const ETexturePixelFormat& aFmt)
{
	return (aFmt <= QTEXTURE_FORMAT_RGBA32F);
}

bool textureIsCompressedFormat(const ETexturePixelFormat& aFmt)
{
	return (aFmt >= QTEXTURE_FORMAT_DXT1);
}

unsigned int textureGetBytesPerBlock(const ETexturePixelFormat& aFmt)
{
	return (aFmt == QTEXTURE_FORMAT_DXT1 || aFmt == QTEXTURE_FORMAT_ATI1N) ? 8 : 16;
}

unsigned int textureGetBytesPerPixel(const ETexturePixelFormat& aFmt)
{
	static const unsigned int bpp[] = { 0, 1, 2, 3, 4, 2, 4, 6, 8, 2,
									   4, 6, 8, 4, 8, 12, 16, 24, 1, 1,
									   1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
									   1, 2, 4, 4 };
	return bpp[aFmt];
}

unsigned int textureGetBytesPerChannel(const ETexturePixelFormat& aFmt)
{
	return(aFmt <= QTEXTURE_FORMAT_RGBA8) ? 1 : (aFmt <= QTEXTURE_FORMAT_RGBA16F) ? 2 : 4;
}

unsigned int textureGetChannelCount(const ETexturePixelFormat& aFmt)
{
	static const unsigned int channels[] = { 0, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 1, 3, 3, 4, 4, 2, 1, 4 };
	return channels[aFmt];
}

int getFace(const vec2<float> aUv)
{
	const int x = int(floor(aUv.x * 3.0));
	const int y = 1 - int(floor(aUv.y * 2.0));
	return y * 3 + x;
}

int getImageSize(const ETexturePixelFormat& aFmt, const unsigned int& aW, const unsigned int& aH, const unsigned int& aD, const unsigned int& aNLevels)
{
	unsigned int width = aW;
	unsigned int height = aH;
	unsigned int depth = aD;
	unsigned int levels = aNLevels;

	const bool isCubemap = (aD == 0);
	if(isCubemap) depth = 1;

	int size = 0;
	while(levels)
	{
		if(textureIsCompressedFormat(aFmt))
			size += ((width + 3) >> 2) * ((height + 3) >> 2) * depth;
		else
			size += width * height * depth;

		if(width == 1 && height == 1 && depth == 1)
			break;

		if(width > 1) width >>= 1;
		if(height > 1) height >>= 1;
		if(depth > 1) depth >>= 1;
		--levels;
	}

	if(textureIsCompressedFormat(aFmt))
		size *= textureGetBytesPerBlock(aFmt);
	else
		size *= textureGetBytesPerPixel(aFmt);

	if(isCubemap)
		size *= 6;

	return size;
}

bool isPowerOfTwo(const int aVal)
{
	return (aVal & -aVal) == aVal;
}

int getMipMapCount2(const int& aMaxDimension)
{
	int i = 0;
	int maxDim = aMaxDimension;
	while(maxDim > 0)
	{
		maxDim >>= 1;
		++i;
	}

	return i;
}

bool hasMipMapFlags(const unsigned int& aFlags)
{
	if(aFlags & QTEXTURE_FILTER_BILINEAR || aFlags & QTEXTURE_FILTER_TRILINEAR ||
	   aFlags & QTEXTURE_FILTER_BILINEAR_ANISO || aFlags & QTEXTURE_FILTER_TRILINEAR_ANISO)
		return true;

	return false;
}


bool isPlainFormat(const ETexturePixelFormat& aFmt)
{
	return (aFmt <= QTEXTURE_FORMAT_RGBA32F);
}

bool isUnsignedFormat(const ETexturePixelFormat& aFmt)
{
	return (aFmt <= QTEXTURE_FORMAT_RGBA16);
}


int getChannelCount(const ETexturePixelFormat& aFmt)
{
	static const unsigned int channels[] = { 0, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 1, 3, 3, 4, 4, 2, 1 };
	return channels[aFmt];
}


int getFilterIndex(const unsigned int& aFlags)
{
	if(aFlags & QTEXTURE_FILTER_NEAREST) return 0;
	if(aFlags & QTEXTURE_FILTER_LINEAR) return 1;
	if(aFlags & QTEXTURE_FILTER_BILINEAR) return 2;
	if(aFlags & QTEXTURE_FILTER_TRILINEAR) return 3;
	if(aFlags & QTEXTURE_FILTER_BILINEAR_ANISO) return 4;
	if(aFlags & QTEXTURE_FILTER_TRILINEAR_ANISO) return 5;
	return -1;
}

template <typename DataType>
inline DataType clamp(const DataType aX, const float aLower, const float aUpper)
{
	return std::max(std::min(aX, DataType(aUpper)), DataType(aLower));
}

template <typename DataType>
inline DataType saturate(DataType aX)
{
	return clamp((aX), 0, 1);
}




#pragma pack(push, 1)

// Direct Draw Surface header format
struct SDdsHeader
{
	unsigned int dwMagic;
	unsigned int dwSize;
	unsigned int dwFlags;
	unsigned int dwHeight;
	unsigned int dwWidth;
	unsigned int dwPitchOrLinearSize;
	unsigned int dwDepth;
	unsigned int dwMipMapCount;
	unsigned int dwReserved[11];

	struct
	{
		unsigned int dwSize;
		unsigned int dwFlags;
		unsigned int dwFourCc;
		unsigned int dwRgbBitCount;
		unsigned int dwRBitMask;
		unsigned int dwGBitMask;
		unsigned int dwBBitMask;
		unsigned int dwRgbAlphaBitMask;
	}ddpfPixelFormat;

	struct
	{
		unsigned int dwCaps1;
		unsigned int dwCaps2;
		unsigned int reserved[2];
	}ddsCaps;

	unsigned int dwReserved2;
};

#pragma pack(pop)


struct TextureFileImpl
{
	GLuint id;
	GLuint heightId;
	GLuint normalMapId;

	GLuint bIsLoaded;

	FboId copyFbo;

	unsigned char* pixels;

	std::vector<unsigned char*> normalMap;
	unsigned char* greyscale;

	std::string fileName;							// file name and extension
	std::string pathName;							// Path name

	unsigned int width;								// width in pixels
	unsigned int height;							// height in pixels
	unsigned int bpp;								// bits per pixel
	unsigned int depth;								// depth in pixels
	uint32_t nMipMaps;								// number of mipmaps associated with raw data ("pixels")
	bool generateMips;

	float gamma;
	uint32_t texFilter;

	ETexturePixelFormat pixelFormat;				// pixel format descriptor
};

Image::Image()
{
	mImpl = new TextureFileImpl();

	mImpl->pixels = nullptr;
	mImpl->pixelFormat = QTEXTURE_FORMAT_NONE;
	mImpl->width = mImpl->height = mImpl->bpp = mImpl->depth = 0;
	mImpl->nMipMaps = 0;
	mImpl->bIsLoaded = false;
	mImpl->greyscale = nullptr;

	memset(&mImpl->copyFbo, 0, sizeof(FboId));
	mImpl->gamma = 2.2f;
	mImpl->texFilter = 0;
}


Image::Image(const Image& aTex)
{
	mImpl = new TextureFileImpl();

	mImpl->pixels = nullptr;
	mImpl->pixelFormat = aTex.mImpl->pixelFormat;
	mImpl->width = aTex.mImpl->width;
	mImpl->height = aTex.mImpl->height;
	mImpl->bpp = aTex.mImpl->bpp;
	mImpl->depth = aTex.mImpl->depth;
	mImpl->nMipMaps = aTex.mImpl->nMipMaps;
	mImpl->bIsLoaded = aTex.mImpl->bIsLoaded;

	mImpl->gamma = aTex.mImpl->gamma;
	mImpl->texFilter = 0;
}


Image& Image::operator= (const Image& aTex)
{
	mImpl->pixelFormat = aTex.mImpl->pixelFormat;
	mImpl->width = aTex.mImpl->width;
	mImpl->height = aTex.mImpl->height;
	mImpl->depth = aTex.mImpl->depth;
	mImpl->bpp = aTex.mImpl->bpp;
	mImpl->nMipMaps = aTex.mImpl->nMipMaps;
	mImpl->bIsLoaded = aTex.mImpl->bIsLoaded;

	mImpl->gamma = aTex.mImpl->gamma;
	mImpl->texFilter = aTex.mImpl->texFilter;

	return *this;
}

Image::~Image()
{
	if(mImpl->pixels)
	{
		delete[] mImpl->pixels;
		mImpl->pixels = nullptr;
	}

	for(unsigned int i = 0; i < mImpl->normalMap.size(); ++i)
	{
		if(mImpl->normalMap[i])
		{
			delete[] mImpl->normalMap[i];
			mImpl->normalMap[i] = nullptr;
		}
	}

	if(mImpl->greyscale)
	{
		delete[] mImpl->greyscale;
		mImpl->greyscale = nullptr;
	}

	mImpl->pixelFormat = QTEXTURE_FORMAT_NONE;
	mImpl->width = mImpl->height = mImpl->depth = mImpl->bpp = 0;
	mImpl->nMipMaps = 0;

	mImpl->bIsLoaded = false;

	if (mImpl)
	{
		delete mImpl;
		mImpl = nullptr;
	}
}


static vec2<float> CUBE_OFFSETS[6] = 
{
	vec2<float>(2.0, 1.0),
	vec2<float>(0.0, 1.0),
	vec2<float>(1.0, 0.0),
	vec2<float>(1.0, 2.0),
	vec2<float>(1.0, 1.0),
	vec2<float>(3.0, 1.0),
};

void Image::setTexFilter(const uint32_t& aT) const
{
	mImpl->texFilter = aT;
}

//////////////////////////////////////////////////////////////////////
// loadTexture
// fname- file name with path and extension
//
// load a texture file from filename
bool Image::loadTexture(const char* aFname, const bool aKeepLocal, const char* aPname,
						const bool aFlipVertical) const
{
	if (mImpl->pixels)
	{
		delete[] mImpl->pixels;
		mImpl->pixels = nullptr;
	}

	const char* ext = strrchr(aFname, '.');
	if (!ext)
		return false;

	mImpl->pathName = aPname;
	std::string full = mImpl->pathName + aFname;
	mImpl->fileName = aFname;

	++ext;

	FIBITMAP* dib(nullptr);


	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(full.c_str(), 0);
	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(full.c_str());
	if (fif == FIF_UNKNOWN)
		return false;

	if (FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, full.c_str());
	if (!dib)
		return false;


	uint8_t * bits = FreeImage_GetBits(dib);
	const uint32_t width = FreeImage_GetWidth(dib);
	const uint32_t height = FreeImage_GetHeight(dib);
	mImpl->bpp = FreeImage_GetBPP(dib);
	unsigned pitch = FreeImage_GetPitch(dib);

	if (aFlipVertical)
	{
		FreeImage_FlipVertical(dib);
	}

	mImpl->width = width;
	mImpl->height = height;
	if (bits == nullptr || width == 0 || height == 0)
		return false;

	const FREE_IMAGE_TYPE type = FreeImage_GetImageType(dib);

	if (aKeepLocal)
	{
		mImpl->pixels = new uint8_t[width * height * (mImpl->bpp / 8)];
		memcpy(mImpl->pixels, bits, width * height * (mImpl->bpp / 8));
	}


	TextureInfo texInfo;
	texInfo.width = width;
	texInfo.height = height;
	texInfo.depth = 1;
	// TODO: DO NOT HARD CODE TEXTURE SAMPLERS
	unsigned int filter;
	if (mImpl->texFilter > 0)
		filter = mImpl->texFilter;
	else
		filter = Render::getPreferredTextureFilter();
	texInfo.flags = SAMPLE_WRAP | filter;
	texInfo.isCubemap = 0;
	if(strcmp(ext, "dds") == 0 || strcmp(ext, "DDS") == 0)
		texInfo.isS3TC = 1;
	else 
		texInfo.isS3TC = 0;
	texInfo.size = width * height * (mImpl->bpp / 8);
	texInfo.colorSamples = 0;
	texInfo.coverageSamples = 0;
	if (mImpl->bpp == 24)
	{
		texInfo.pixelFormat = QTEXTURE_FORMAT_RGB8;
		mImpl->pixelFormat = QTEXTURE_FORMAT_RGB8;
	}
		
	else if (mImpl->bpp == 48)
	{
		if (type == FIT_RGBF)
		{
			texInfo.pixelFormat = QTEXTURE_FORMAT_RGB16F;
			mImpl->pixelFormat = QTEXTURE_FORMAT_RGB16F;
		}
			
		else
		{
			texInfo.pixelFormat = QTEXTURE_FORMAT_RGB16;
			mImpl->pixelFormat = QTEXTURE_FORMAT_RGB16;
		}
			
	}
	else if (mImpl->bpp == 32)
	{
		texInfo.pixelFormat = QTEXTURE_FORMAT_RGBA8;
		mImpl->pixelFormat = QTEXTURE_FORMAT_RGBA8;
	}
		
	else if (mImpl->bpp == 96)
	{
		if (type == FIT_RGBF)
		{
			texInfo.pixelFormat = QTEXTURE_FORMAT_RGB32F;
			mImpl->pixelFormat = QTEXTURE_FORMAT_RGB32F;
		}
			
	}
	else if (mImpl->bpp == 128)
	{
		if (type == FIT_RGBAF)
		{
			texInfo.pixelFormat = QTEXTURE_FORMAT_RGBA32F;
			mImpl->pixelFormat = QTEXTURE_FORMAT_RGBA32F;
		}
			
	}

	else if (mImpl->bpp == 8)
	{
		texInfo.pixelFormat = QTEXTURE_FORMAT_R8;
		mImpl->pixelFormat = QTEXTURE_FORMAT_R8;
	}
		

	else if (mImpl->bpp == 16)
	{
		texInfo.pixelFormat = QTEXTURE_FORMAT_R16;
		mImpl->pixelFormat = QTEXTURE_FORMAT_R16;
	}

	else if (mImpl->bpp == 64)
	{
		texInfo.pixelFormat = QTEXTURE_FORMAT_RGBA16;
		mImpl->pixelFormat = QTEXTURE_FORMAT_RGBA16;
	}
		

	else
	{
		FreeImage_Unload(dib);
		return false;
	}


	// Check if it's a cubemap //
	std::transform(full.begin(), full.end(), full.begin(), std::function<int(int)>(toupper));
	if (full.find("CUBEMAP") != std::string::npos)
		texInfo.isCubemap = true;
	
	// We have to flip B/R channels for JPG/JPEG and TGA //
	if(qstricmp(ext, "jpg") == false || qstricmp(ext, "jpeg") == false ||
	   qstricmp(ext, "tga") == false || qstricmp(ext, "png") == false ||
	   qstricmp(ext, "psd") == false)
	{
	
		if(mImpl->bpp != 8 && mImpl->bpp != 16 && mImpl->bpp <= 32)
		{

			uint32_t stride;
			if(mImpl->bpp == 24)
				stride = 3;
			else 
				stride = 4;
	
			uint8_t* src = bits;
			for(uint32_t i = 0; i < width * height; ++i)
			{
				const uint8_t tmp = *src;
				*src = *(src + 2);
				*(src + 2) = tmp;	
				src += stride; 
			}
		}
	}

	// Apply gamma correction //
	mImpl->generateMips = false;
	if (hasMipmapFlag(filter))
		mImpl->generateMips = true;

	if (strcmp(ext, "dds") == 0 || strcmp(ext, "DDS") == 0)
	{
		const int nChannels = textureGetChannelCount(static_cast<ETexturePixelFormat>(texInfo.pixelFormat));
		swapChannel(bits, width * height, nChannels, 0, 2);

		for (uint32_t i = 0; i < width * height; ++i)
		{
			float r = bits[i * 4 + 0] / 255.0f;
			float g = bits[i * 4 + 1] / 255.0f;
			float b = bits[i * 4 + 2] / 255.0f;

			r = pow(r, mImpl->gamma);
			g = pow(g, mImpl->gamma);
			b = pow(b, mImpl->gamma);

			bits[i * 4 + 0] = static_cast<uint8_t>(r * 255.0f);
			bits[i * 4 + 1] = static_cast<uint8_t>(g * 255.0f);
			bits[i * 4 + 2] = static_cast<uint8_t>(b * 255.0f);
		}
	}

//	else
//	{
//		BOOL gammaAdjust = FreeImage_AdjustGamma(dib, mImpl->gamma);
//	}

	if(!texInfo.isCubemap)
		mImpl->id = addTexture(texInfo, (void**)(&bits));
	else
	{
		const uint32_t xDiv = texInfo.width / 4;
		const uint32_t yDiv = texInfo.height / 3;
		const uint32_t bpp = mImpl->bpp / 8;
		uint8_t** cubemapArray = new uint8_t*[6];
		for (uint32_t i = 0; i < 6; ++i)
			cubemapArray[i] = new uint8_t[xDiv * yDiv * bpp];

		uint32_t arrayIndex = 0;
		uint32_t loc = 0;
		for (uint32_t i = 0; i < 6; ++i)
		{
			loc = (static_cast<uint32_t>(CUBE_OFFSETS[i].y) * texInfo.width * bpp * yDiv) + (static_cast<uint32_t>(CUBE_OFFSETS[i].x) * xDiv * bpp);
			for (uint32_t y = 0; y < yDiv; ++y)
			{
				for (uint32_t x = 0; x < xDiv; ++x)
				{
					cubemapArray[i][arrayIndex++] = bits[loc++];
					cubemapArray[i][arrayIndex++] = bits[loc++];
					cubemapArray[i][arrayIndex++] = bits[loc++];
					if(bpp > 3)
						cubemapArray[i][arrayIndex++] = bits[loc++];
				}
				 
				loc += ((texInfo.width - xDiv) * bpp);
			}

			arrayIndex = 0;
		}

		// TODO: Do not hardcode sampler state
		texInfo.flags = SAMPLE_CLAMP | SAMPLE_LINEAR;

		mImpl->id = addTexture(texInfo, (void**)(cubemapArray));
		for(uint32_t i = 0; i < 6; ++i)
			delete[] cubemapArray[i];
		delete[] cubemapArray;
	}

	if (mImpl->id <= 0)
	{
		FreeImage_Unload(dib);
		return false;
	}

	FreeImage_Unload(dib);
	return true;
}


bool Image::loadTexture(const uint32_t aWidth, 
						const uint32_t aHeight,
                        const ETexturePixelFormat aFmt,
                        const bool aKeepLocal,
						void* aBuf) const
{
	if(!aBuf)
		return false;

	if(mImpl->pixels)
	{
		delete[] mImpl->pixels;
		mImpl->pixels = nullptr;
	}

	const unsigned int bytesPerPel = textureGetBytesPerPixel(aFmt);
	const unsigned int size = aWidth * aHeight * bytesPerPel;

	if (aKeepLocal)
	{
		mImpl->pixels = new unsigned char[size];
		memcpy(mImpl->pixels, aBuf, size);
	}

	TextureInfo texInfo;
	texInfo.width = aWidth;
	texInfo.height = aHeight;
	texInfo.depth = 1;
	// TODO: DO NOT HARD CODE TEXTURE SAMPLERS
//	const unsigned int filter = getPreferredTexFilter();
//	texInfo.flags = SAMPLE_WRAP | filter;
	texInfo.flags = mImpl->texFilter;
	texInfo.isCubemap = 0;
	texInfo.isS3TC = 0;
	texInfo.size = size;
	texInfo.colorSamples = 0;
	texInfo.coverageSamples = 0;
	texInfo.pixelFormat = aFmt;

	mImpl->width = texInfo.width;
	mImpl->height = texInfo.height;
	mImpl->bpp = bytesPerPel * 8;
	mImpl->pixelFormat = aFmt;

	mImpl->generateMips = false;
	if (hasMipmapFlag(mImpl->texFilter))
		mImpl->generateMips = true;

	mImpl->pixelFormat = aFmt;
	if (aKeepLocal)
		mImpl->id = addTexture(texInfo, (void**)(&mImpl->pixels));
	else
		mImpl->id = addTexture(texInfo, static_cast<void**>(&aBuf));

	return (mImpl->id > 0);
}


bool Image::heightToNormal() const
{
	if(!mImpl->pixels)
		return false;

	const uint32_t bpp = textureGetBytesPerPixel(mImpl->pixelFormat);
	float* buf = new float[(mImpl->width - 2) * (mImpl->height - 2) * 3];

	uint32_t idx = 0;

	for(uint32_t y = 1; y < mImpl->height - 1; ++y)
	{
		for(uint32_t x = 1; x < mImpl->width - 1; ++x)
		{
			uint32_t center = (y * mImpl->width + x);
			uint32_t topRow = center - mImpl->width;
			uint32_t bottomRow = center + mImpl->width;

//			if (topRow < 0)
//				topRow = center + ((mImpl->width) * (mImpl->height - 1));
//			if (bottomRow > (mImpl->height - 1) * mImpl->width)
//				bottomRow = 1;

			center *= bpp;
			topRow *= bpp;
			bottomRow *= bpp;

			uint16_t ul, t, ur, l, r, ll, b, lr;
			memcpy(&ul, &mImpl->pixels[topRow - 2], 2);
			memcpy(&t, &mImpl->pixels[topRow], 2);
			memcpy(&ur, &mImpl->pixels[topRow + 2], 2);
			memcpy(&l, &mImpl->pixels[center - 2], 2);
			memcpy(&r, &mImpl->pixels[center + 2], 2);
			memcpy(&ll, &mImpl->pixels[bottomRow - 2], 2);
			memcpy(&b, &mImpl->pixels[bottomRow], 2);
			memcpy(&lr, &mImpl->pixels[bottomRow + 2], 2);

			const float ulV = ul / 65536.0f;
			const float tV = t / 65536.0f;
			const float urV = ur / 65536.0f;
			const float lV = l / 65536.0f;
			const float rV = r / 65536.0f;
			const float llV = ll / 65536.0f;
			const float bV = b / 65536.0f;
			const float lrV = lr / 65536.0f;

			float dx = urV + 2.0f * rV + lrV - ulV - 2.0f * lV - llV;
			float dy = llV + 2.0f * bV + lrV - ulV - 2.0f * tV - ulV;
			const vec3<float> xVec(dx, 0.0, 0.0);
			vec3<float> yVec(0.0f, dy, 0.0);
			vec3<float> zVec = yVec.crossProd(xVec);
			const float dz = 0.5f * sqrtf(1.0f - dx * dx - dy * dy);
			vec3<float> N(2.0f * dx, 2.0f * dy, 0.05f * dz);		// 0.25 * dz
//			vec3<float> N(2.0f * dx, 2.0f * dy, 2.0 * zVec.getLength());
			N.normalize();

			float tmp = N.y;
			N.y = N.z;
			N.z = tmp;

			memcpy(static_cast<void*>(buf + (idx * 3)), &N, 12);
			idx++;
		}
	}
	
	// load tex //
	TextureInfo texInfo;
	texInfo.width = mImpl->width - 2;
	texInfo.height = mImpl->height - 2;
	texInfo.depth = 1;
	// TODO: DO NOT HARD CODE TEXTURE SAMPLERS
	TextureFilter filter = Render::getPreferredTextureFilter();
	texInfo.flags = SAMPLE_WRAP | filter;
	texInfo.isCubemap = 0;
	texInfo.isS3TC = 0;
	texInfo.size = (mImpl->width - 2) * (mImpl->height - 2) * 12;
	texInfo.colorSamples = 0;
	texInfo.coverageSamples = 0;
	texInfo.pixelFormat = QTEXTURE_FORMAT_RGB32F;

	mImpl->normalMapId = addTexture(texInfo, (void**)(&buf));
	delete[] buf;

	return true;
}

bool Image::unloadTexture() const
{
	if (!mImpl || mImpl->id <= 0)
		return false;

	deleteTexture(mImpl->id);
	return true;
}

void Image::updateDirtyRegion(const int32_t aXOffset, 
							  const int32_t aYOffset, 
							  const int32_t aWidth, 
							  const int32_t aHeight, 
							  const void* aBuf) const
{
	if(!aBuf || mImpl->id <= 0)
		return;

	// TODO: Update local pixel storage ???
	updateTextureDirtyRegion(mImpl->id, 
							 aXOffset, 
							 aYOffset, 
							 aWidth, 
							 aHeight, 
							 mImpl->pixelFormat, 
							 mImpl->generateMips, aBuf);
}


////////////////////////////////////////////////////////////
// getWidth
// Get base level image width
int Image::getWidth() const
{
	return mImpl->width;
}



///////////////////////////////////////////////////////////
// getHeight
// Get base level image height
int Image::getHeight() const
{
	return mImpl->height;
}



////////////////////////////////////////////////////////
// getDepth
// Get base level image depth
int Image::getDepth() const
{
	return mImpl->depth;
}


//////////////////////////////////////////////////////////////
// GetBitsPerPel
// Get the number of bits per pixel
int Image::getBitsPerPel() const
{
	return mImpl->bpp;
}


///////////////////////////////////////////////////////////
// getWidth
// get width at mip level
int Image::getWidth(const unsigned int& aLevel) const
{
	const int a = mImpl->width >> aLevel;
	return (a == 0) ? 1 : a;
}


////////////////////////////////////////////////////////////
// getHeight
// Get height at mip level
int Image::getHeight(const unsigned int& aLevel) const
{
	const int a = mImpl->height >> aLevel;
	return (a == 0) ? 1 : a;
}


//////////////////////////////////////////////////////////
// getDepth
// Get depth at mip level
int Image::getDepth(const unsigned int& aLevel) const
{
	const int a = mImpl->depth >> aLevel;
	return (a == 0) ? 1 : a;
}



///////////////////////////////////////////////////////////
// isCubemap
// Is the texture a cubemap?
bool Image::isCubemap() const
{
	return (!mImpl || mImpl->depth == 0);
}

bool Image::isLoaded() const
{
	if (mImpl->bIsLoaded > 0)
		return true;
	return false;
}

GLuint Image::getOpenGlid() const
{
	return mImpl->id;
}

GLuint Image::getHeightOpenGlid() const
{
	return mImpl->heightId;
}

GLuint Image::getNormalOpenGlid() const
{
	return mImpl->normalMapId;
}

//////////////////////////////////////////////////////////////////////////////
// getData
// Get pixel surface pointer at a certain mip level
unsigned char* Image::getData(const unsigned int& aLevel) const
{
	if (!mImpl->pixels)
		return nullptr;

	int w = mImpl->width >> 0;
	if(w == 0) w = 1;
	int h = mImpl->height >> 0;
	if(h == 0) h = 1;
	int d = mImpl->depth >> 0;
	if(d == 0) d = 1;

//	unsigned int highest = pixels.size() - 1;

	const int mipMappedSize = getImageSize(mImpl->pixelFormat, w, h, (mImpl->depth > 0) * d, aLevel);
	return(aLevel <= mImpl->nMipMaps) ? static_cast<unsigned char*>(mImpl->pixels) + mipMappedSize : NULL;
}

////////////////////////////////////////////////////////////////////////////////
// swapChannels
// Swap channels from ch0 to ch1 in existing texture
bool Image::swapChannels(const uint32_t& aCh0, 
						 const uint32_t& aCh1,
                         const bool aUseNormalmap) const
{
	if(!textureIsPlainFormat(mImpl->pixelFormat))
		return false;

	const uint32_t nPixels = getPixelCount(0, mImpl->nMipMaps);
	const uint32_t nChannels = textureGetChannelCount(mImpl->pixelFormat);
//	unsigned int largest = (useNormalmap) ? normalMap.size() - 1 : pixels.size() - 1;

	if(mImpl->pixelFormat <= QTEXTURE_FORMAT_RGBA8)
	{
		if(!aUseNormalmap)
			swapChannel(static_cast<unsigned char*>(mImpl->pixels), nPixels, nChannels, aCh0, aCh1);
	}
	else if(mImpl->pixelFormat <= QTEXTURE_FORMAT_RGBA16F)
		swapChannel((uint32_t*)(mImpl->pixels), nPixels, nChannels, aCh0, aCh1);
	else
		swapChannel((float*)(mImpl->pixels), nPixels, nChannels, aCh0, aCh1);

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
// getPixelCount
// Get total pixel count in image from firstMip to nLevels mips
uint32_t Image::getPixelCount(const uint32_t& firstMip, 
											 uint32_t nLevels) const
{
	uint32_t w = mImpl->width >> firstMip;
	if(w == 0) w = 1;

	uint32_t h = mImpl->height >> firstMip;
	if(h == 0) h = 1;

	uint32_t d = mImpl->depth >> firstMip;
	if(d == 0) d = 1;

	w <<= 1;
	h <<= 1;
	d <<= 1;

	uint32_t size = 0;

	while(nLevels && (w != 1 || h != 1 || d != 1))
	{
		if(w > 1) w >>= 1;
		if(h > 1) h >>= 1;
		if(d > 1) d >>= 1;

		size += w * h * d;
		--nLevels;
	}

	if(mImpl->depth == 0)
		size *= 6;

	return size;
}


const char* Image::getFileName() const
{
	return mImpl->fileName.c_str();
}

void Image::setFileName(const char* aS) const
{
	mImpl->fileName = std::string(aS);
}

const char* Image::getFilePath() const
{
	return mImpl->pathName.c_str();
}

void Image::setFilePath(const char* aS) const
{
	mImpl->pathName = std::string(aS);
}

ETexturePixelFormat Image::getPixelFormat() const
{
	return mImpl->pixelFormat;
}


bool Image::addChannel(const float aVal) const
{
	const unsigned int nChannels = textureGetChannelCount(mImpl->pixelFormat);
	if(textureIsCompressedFormat(mImpl->pixelFormat) || nChannels == 4)
		return false;

	unsigned int value;
	if(mImpl->pixelFormat <= QTEXTURE_FORMAT_RGBA8)
		value = static_cast<unsigned int>(255.0F * saturate(aVal));
	else if(mImpl->pixelFormat <= QTEXTURE_FORMAT_RGBA16)
		value = static_cast<unsigned int>(65535.0F * saturate(aVal));
	else
		value = static_cast<unsigned int>(aVal);

	const uint32_t nPixels = getPixelCount(0, mImpl->nMipMaps);
	const int bpc = textureGetBytesPerChannel(mImpl->pixelFormat);
	const int cs = bpc * nChannels;
//	unsigned int largest = pixels.size() - 1;
	unsigned char* newPix = new unsigned char[nPixels * (nChannels + 1) * bpc];
	unsigned char* dest = newPix;

	unsigned char* src = mImpl->pixels;
	for(uint32_t i = 0; i < nPixels; ++i)
	{
		memcpy(dest, src, cs);
		dest += cs;
		src += cs;
		memcpy(dest, &value, bpc);
		dest += bpc;
	}

//	pixels.push_back(newPix);
	mImpl->pixelFormat = static_cast<ETexturePixelFormat>(mImpl->pixelFormat + 1);

	return true;
}


bool Image::is2D() const
{
	return(mImpl->height > 1 && mImpl->depth == 1);
}


bool Image::is3D() const
{
	return (mImpl->depth > 1);
}

void Image::GetAsBitmap(void** aOutputBuffer, int &aLength) const
{
/*
	const auto img_buff_size = mImpl->width * mImpl->height * 4;
	void* img = malloc(img_buff_size);
	aLength = sizeof(BmpFileHdr) + sizeof(BmpInfoHdr) + (img_buff_size);
	*aOutputBuffer = malloc(aLength);

	BmpFileHdr bmpFileHeader;
	bmpFileHeader.bfType = 0x4D42;
	bmpFileHeader.bfSize = aLength;
	bmpFileHeader.bfReserved1 = 0;
	bmpFileHeader.bfReserved2 = 0;
	bmpFileHeader.bfOffBits = sizeof(BmpFileHdr) + sizeof(BmpInfoHdr);

	BmpInfoHdr  bmpHeader;
	bmpHeader.biSize= sizeof(BmpInfoHdr);
	bmpHeader.biWidth = mImpl->width;
	bmpHeader.biHeight = mImpl->height;
	bmpHeader.biPlanes = 1;
	bmpHeader.biBitCount = 32;
	bmpHeader.biCompression = 0;   // BI_RGB
	bmpHeader.biSizeImage = 0;
	bmpHeader.biXPelsPerMeter = 0;
	bmpHeader.biYPelsPerMeter = 0;
	bmpHeader.biClrUsed = 0;
	bmpHeader.biClrImportant = 0;

	memcpy(*aOutputBuffer, &bmpFileHeader, sizeof(BmpFileHdr));
	memcpy(static_cast<unsigned char*>(*aOutputBuffer) + sizeof(BmpFileHdr), &bmpHeader, sizeof(BmpInfoHdr));

	if(mImpl->copyFbo.id <= 0)
	{
		GLuint colorTgt = addRenderTarget(mImpl->width, mImpl->height, QTEXTURE_FORMAT_RGBA8, SAMPLE_NEAREST | SAMPLE_CLAMP);
		DepthStencilId no;
		no.depthId = 0;
		no.stencilId = 0;
		mImpl->copyFbo = createRenderableSurface(&colorTgt, 1, no, mImpl->width, mImpl->height);
	}

	// RTT
	QGfx::disableDepthTest();
	bindRenderableSurface(mImpl->copyFbo);
	Render::renderFullscreenTexturedQuad(false, mImpl->id);
	bindDefaultSurface();

	// Read back
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, mImpl->copyFbo.id);
	glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
	glReadPixels(0, 0, mImpl->width, mImpl->height, GL_BGRA, GL_UNSIGNED_BYTE, img);
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);

	memcpy(static_cast<unsigned char*>(*aOutputBuffer) + sizeof(BmpFileHdr) + sizeof(BmpInfoHdr), img, img_buff_size);
	free(img);
*/
}


void Image::setGamma(const float aExp ) const
{
	mImpl->gamma = aExp;
}

