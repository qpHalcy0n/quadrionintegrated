#include "render/IndexBuffer.h"

#include "Extensions.h"

IndexBuffer::IndexBuffer(uint32_t* aData, const uint32_t aCount, const uint32_t aPrimType)
{
	mCount = aCount;
	mPrimType = aPrimType;

	glGenBuffers(1, &mId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mCount * sizeof(uint32_t), aData, GL_STATIC_DRAW);
}

IndexBuffer::IndexBuffer(void* aData, const uint32_t aSizeInBytes)
{
//	mCount = aCount;
//	mPrimType = aPrimType;

	glGenBuffers(1, &mId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, aSizeInBytes, aData, GL_STATIC_DRAW);	
}

IndexBuffer::~IndexBuffer()
{
	glDeleteBuffers(1, &mId);
}

void IndexBuffer::bind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mId);
}

void IndexBuffer::unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
