#include "entities/Entity.h"
//#include "scene/QScene.h"


Entity::Entity()
{
//	transform = addComponent<TransformComponentRod>();
}

Entity::Entity(const std::string& aName)
{
	name = aName;

	transform = AddComponent<TransformComponent>();
}

Entity::~Entity()
{
	auto iter = mComponents.begin();
	while (iter != mComponents.end())
	{
//		mScenePtr->destroyComponent(*iter);
		EntityFactory::instance().DestroyComponent(*iter);
	}
}

void Entity::removeComponent(Component* aComponent)
{
	auto iter = std::find(mComponents.begin(), mComponents.end(), aComponent);
	if (iter != mComponents.end())
	{
		mComponents.erase(iter);
	}
}
