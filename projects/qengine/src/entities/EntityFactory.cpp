#include "entities/EntityFactory.h"
#include "entities/Entity.h"

EntityFactory::EntityFactory()
{
    _entityPool = new PoolAllocator(2, MAX_ENTITIES, 256);       //32
    _componentPool = new PoolAllocator(2, MAX_COMPONENTS, 256);         //256
}

EntityFactory& EntityFactory::instance()
{
    static EntityFactory* instance = new EntityFactory();
	return *instance;
}

std::shared_ptr<Entity> EntityFactory::CreateEntity(const std::string& aName)
{
    void* raw = _entityPool->getBlock();
    Entity* tmp = (Entity*)raw;
    new(tmp) Entity(aName);
    auto ptr = std::shared_ptr<Entity>(tmp);

    auto find = _entityMap.find(aName);
    if(find == _entityMap.end())
    {
        _entityMap[aName] = ptr;
        return ptr;
    }

    else
        return nullptr;
}

void EntityFactory::RegisterComponent(std::type_index aType, Component* aMem)
{
    auto loc = _componentTypeMap.find(aType);
    if(loc == _componentTypeMap.end())
        _componentTypeMap[aType].reserve(MAX_COMPONENTS_PER_TYPE);
    _componentTypeMap[aType].push_back(aMem);
} 

void* EntityFactory::GetComponentMemBlock()
{
    return _componentPool->getBlock();
}


void EntityFactory::OnUpdate()
{
    for (auto comp : _componentTypeMap)
	{
		for (auto thisComponent : comp.second)
		{
			thisComponent->OnUpdate();
		}
	}   
}