#include "InputManager.h"
#include "QMath.h"
//#include "Log.h"
#include "core/QLog.h"

#include "Application.h"
#include <map>

#if defined(_WIN32)
#include <windowsx.h>
#endif

class InputManagerImpl
{
	public:
		InputManagerImpl(Application* application);


	/*
		void RegisterRawInputDevice()
		{
			RAWINPUTDEVICE rid;
			rid.usUsagePage = 0x01;
			rid.usUsage = 0x02;
			rid.dwFlags = RIDEV_NOLEGACY;
			rid.hwndTarget = 0;

			if (RegisterRawInputDevices(&rid, 1, sizeof(rid)) == FALSE)
			{
				std::string err("This Shouldnt happen");
			}

			_isRawInputRegistered = true;
		}

		void UnregisterRawInputDevice()
		{
			RAWINPUTDEVICE rid;
			rid.usUsagePage = 0x01;
			rid.usUsage = 0x02;
			rid.dwFlags = RIDEV_REMOVE;
			rid.hwndTarget = 0;

			if (RegisterRawInputDevices(&rid, 1, sizeof(rid)) == FALSE)
			{
				std::string err("This Shouldnt happen");
			}

			_isRawInputRegistered = false;
		}
	*/


	~InputManagerImpl();

	void registerInputListener(InputListener* aInputListener);
	void deregisterInputListener(InputListener* aInputListener);

	void setBindings(std::multimap<unsigned int, std::string> aBindingCollection);

#if defined(_WIN32)
	bool processInput(UINT aUMsg, WPARAM aWParam, LPARAM aLParam);
#else   
	bool processInput(Display* aDisp);
#endif

	void update(const UpdateContext& aUpdateContext);

	void setCenterCursor(bool aCenter);
	const bool isCursorCentering();
	void setMousePosition(int aX, int aY);
	void centerCursor();

	vec2<int32_t> const getMousePosition();
	vec2<int32_t> const getMousePositionFromCenter();

	//    vec2<int32_t>* const GetMouseTrace(int32_t& nSamples);
	//    std::vector<vec2<int32_t>> _mouseTrace;

	int32_t lastMouseX, lastMouseY;
	//vec2<int32_t> mousePosition = vec2<int32_t>(0, 0);
	bool isCursorCentered;
	bool lastCenterCursor;

	EKeyAction keyStates[255];



	//    bool _isRawInputRegistered;


	private:
		void onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction);
		void onMouseMove(const double aX, const double aY);
		void onMouseWheelScroll(const double aDeltaX, const double aDeltaY);
		void onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods);
		void onCharEvent(const int aCodepoint);
		void onWindowGotFocus();
		void onWindowLostFocus();
		bool isNumPadInput();

		std::vector<InputListener*> mVecInputListeners;
		std::multimap<unsigned int, std::string> mBinds;

		Application* mApplication;
};




InputManagerImpl::InputManagerImpl(Application* aApplication)
{
	mVecInputListeners = std::vector<InputListener*>();
	mBinds = std::multimap<unsigned int, std::string>();

	//    RegisterRawInputDevice();
	lastMouseX = lastMouseY = 0;

	mApplication = aApplication;

	isCursorCentered = true;

	memset(keyStates, static_cast<int32_t>(EKeyAction::RELEASE), sizeof(int) * 255);
}



InputManagerImpl::~InputManagerImpl()
{
	mBinds.clear();
}

void InputManagerImpl::registerInputListener(InputListener* aInputListener)
{
	if (aInputListener)
	{
		if (std::find(mVecInputListeners.begin(),
			mVecInputListeners.end(),
			aInputListener) != mVecInputListeners.end())
		{
			return;
		}

		mVecInputListeners.push_back(aInputListener);
	}
}

void InputManagerImpl::deregisterInputListener(InputListener* aInputListener)
{
	if (aInputListener)
	{
		auto loc = std::find(mVecInputListeners.begin(),
			mVecInputListeners.end(),
			aInputListener);

		if (loc != mVecInputListeners.end())
		{
			mVecInputListeners.erase(loc);
		}
	}
}

void InputManagerImpl::setBindings(std::multimap<unsigned int, std::string> aBindingCollection)
{

}

#if defined(_WIN32)
bool InputManagerImpl::processInput(UINT aUMsg, WPARAM aWParam, LPARAM aLParam)
{
	switch (aUMsg)
	{
		case WM_LBUTTONDOWN:
			onMouseButton(EMouseButton::LEFT, EMouseButtonAction::PRESS);
			return true;

		case WM_RBUTTONDOWN:
			onMouseButton(EMouseButton::RIGHT, EMouseButtonAction::PRESS);
			return true;

		case WM_MBUTTONDOWN:
			onMouseButton(EMouseButton::MIDDLE, EMouseButtonAction::PRESS);
			return true;

		case WM_LBUTTONUP:
			onMouseButton(EMouseButton::LEFT, EMouseButtonAction::RELEASE);
			return true;

		case WM_LBUTTONDBLCLK:
			onMouseButton(EMouseButton::LEFT, EMouseButtonAction::DBL_CLICK);
			return true;

		case WM_RBUTTONUP:
			onMouseButton(EMouseButton::RIGHT, EMouseButtonAction::RELEASE);
			return true;

		case WM_MBUTTONUP:
			onMouseButton(EMouseButton::MIDDLE, EMouseButtonAction::RELEASE);
			return true;

		case WM_MOUSEWHEEL:
		{
			short delta = GET_WHEEL_DELTA_WPARAM(aWParam);
			onMouseWheelScroll(0, delta);
			return true;
		}

		case WM_MOUSEHWHEEL:
		{
			short delta = GET_WHEEL_DELTA_WPARAM(aWParam);
			onMouseWheelScroll(delta, 0);
			return true;
		}

		case WM_MOUSEMOVE:
		{
			int xPos = GET_X_LPARAM(aLParam);
			int yPos = GET_Y_LPARAM(aLParam);

			/*POINT pt;
			GetCursorPos(&pt);

			HWND hwnd = mApplication->getApplicationWindow()->getWindowHandle();
			RECT rec;
			GetClientRect(hwnd, &rec);

			xPos = pt.x;
			yPos = -rec.top + rec.bottom - pt.y;*/

			//onMouseMove(xPos, yPos);
			return true;
		}

		case WM_INPUT:
		{
			//            UINT dwSize;
			//            GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));
			 //           LPBYTE lpb = new BYTE[dwSize];
			 //           if(!lpb)
			  //              return false;

			 //           if( GetRawInputData( ( HRAWINPUT )lParam, RID_INPUT, lpb, &dwSize, sizeof( RAWINPUTHEADER ) ) != dwSize )
			  //          {
			   //             std::string crap("This sucks");
				//        }

			 //          RAWINPUT* raw = (RAWINPUT*)lpb;
			 /*
						if( raw->header.dwType == RIM_TYPEMOUSE )
						{
							vec2<int32_t> trace;
							trace.x = raw->data.mouse.lLastX;
							trace.y = raw->data.mouse.lLastY;

							if( raw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN )
								OnMouseButton( MouseButton::LEFT, MouseButtonAction::PRESS );
							if(raw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN)
								OnMouseButton( MouseButton::RIGHT, MouseButtonAction::PRESS );
							if(raw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN)
								OnMouseButton( MouseButton::MIDDLE, MouseButtonAction::PRESS );
							if(raw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP)
								OnMouseButton( MouseButton::LEFT, MouseButtonAction::RELEASE );
							if(raw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP)
								OnMouseButton( MouseButton::RIGHT, MouseButtonAction::RELEASE );
							if(raw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP)
								OnMouseButton( MouseButton::MIDDLE, MouseButtonAction::RELEASE );
							if( raw->data.mouse.usButtonFlags & RI_MOUSE_WHEEL )
							{
								int16_t delta = raw->data.mouse.usButtonData;
								OnMouseWheelScroll(0, delta);
							}

							_mouseTrace.push_back(trace);

						}

						delete[] lpb;
			*/
			/*if (_centerCursor)
				_application->CenterCursor();*/
			return true;
		}

		case WM_CHAR:
		{
			onCharEvent(static_cast<int32_t>(aWParam));
			return true;
		}

		case WM_UNICHAR:
		{
			onCharEvent(static_cast<int32_t>(aWParam));
			return true;
		}

		case WM_KEYDOWN:
		{
			// Make sure this is the initial KEYDOWN event, and not the result of the user holding a key down. This can be done by checking the previous key state on the 30th bit in lParam.
			long previousState = (aLParam & (1 << 30)) >> 30;

			if (previousState != 0)
				return true;

			int keyModifiers = 0;

			if (HIWORD(GetAsyncKeyState(VKEY_SHIFT)))
			{
				keyModifiers |= (int)EKeyModifiers::SHIFT;
			};

			if (HIWORD(GetAsyncKeyState(VK_CONTROL)))
			{
				keyModifiers |= (int)EKeyModifiers::CTRL;
			};

			if (HIWORD(GetAsyncKeyState(VKEY_MENU)))
			{
				keyModifiers |= (int)EKeyModifiers::ALT;
			};

			if (HIWORD(GetAsyncKeyState(VKEY_LWIN)) || HIWORD(GetAsyncKeyState(VKEY_RWIN)))
			{
				keyModifiers |= (int)EKeyModifiers::META;
			};

			if (isNumPadInput())
			{
				keyModifiers |= (int)EKeyModifiers::NUMPAD;
			}

			onKeyEvent((const int)aWParam, EKeyAction::PRESS, keyModifiers);
			keyStates[(const int)aWParam] = EKeyAction::PRESS;

			return true;
		}

		case WM_KEYUP:
		{
			int keyModifiers = 0;

			if (HIWORD(GetAsyncKeyState(VKEY_SHIFT)))
			{
				keyModifiers |= (int)EKeyModifiers::SHIFT;
			};

			if (HIWORD(GetAsyncKeyState(VKEY_CONTROL)))
			{
				keyModifiers |= (int)EKeyModifiers::CTRL;
			};

			if (HIWORD(GetAsyncKeyState(VKEY_MENU)))
			{
				keyModifiers |= (int)EKeyModifiers::ALT;
			};

			if (HIWORD(GetAsyncKeyState(VKEY_LWIN)) || HIWORD(GetAsyncKeyState(VKEY_RWIN)))
			{
				keyModifiers |= (int)EKeyModifiers::META;
			};

			if (isNumPadInput())
			{
				keyModifiers |= (int)EKeyModifiers::NUMPAD;
			}

			onKeyEvent((const int)aWParam, EKeyAction::RELEASE, keyModifiers);
			keyStates[(const int)aWParam] = EKeyAction::RELEASE;

			return true;
		}

		case WM_ACTIVATE:
		{
			if (static_cast<int>(aWParam) == WA_ACTIVE || static_cast<int>(aWParam) == WA_CLICKACTIVE)
			{
				// Game window got focus.
				onWindowGotFocus();
				return true;
			}
			break;
		}

		case WM_KILLFOCUS:
		case WM_SYSCOMMAND:
		{
			if (aWParam == SC_MINIMIZE)
			{
				// Game window was minimized.
				onWindowLostFocus();
				return true;
			}
			else if (aWParam == SC_RESTORE)
			{
				// Game window was restored (from being minimized).
				onWindowGotFocus();
				return true;
			}

			break;
		}
	}

	return false;
}
#else
bool InputManagerImpl::processInput(Display* aDisp)
{
	int nMessages = XPending(aDisp);
	for(uint32_t i = 0; i < nMessages; ++i) //(XPending(aDisp) > 0)
	{
		XEvent event;
		XNextEvent(aDisp, &event);
		switch (event.type)
		{
			case KeyPress:
			{
				int keyModifiers = 0;
				onKeyEvent(XLookupKeysym(&event.xkey, 0), EKeyAction::PRESS, keyModifiers);
				keyStates[XLookupKeysym(&event.xkey, 0)] = EKeyAction::PRESS;
				break;
			}

			case KeyRelease:
			{
				int keyModifiers = 0;
				onKeyEvent(XLookupKeysym(&event.xkey, 0), EKeyAction::RELEASE, keyModifiers);
				keyStates[XLookupKeysym(&event.xkey, 0)] = EKeyAction::RELEASE;
				break;
			}

			case ButtonPress:
			{
				if(event.xbutton.button == Button1)
					onMouseButton(EMouseButton::LEFT, EMouseButtonAction::PRESS);
				else if(event.xbutton.button == Button3)
					onMouseButton(EMouseButton::RIGHT, EMouseButtonAction::PRESS);

				break;	
			}

			case ButtonRelease:
			{
				if(event.xbutton.button == Button1)
					onMouseButton(EMouseButton::LEFT, EMouseButtonAction::RELEASE);
				else if(event.xbutton.button == Button3)
					onMouseButton(EMouseButton::RIGHT, EMouseButtonAction::RELEASE);	

				break;
			}

//			case MotionNotify:
//			{
//				mousePosition.x = event.xmotion.x;
//				mousePosition.y = event.xmotion.y;
//				printf("X: %d   Y:%d\n", event.xmotion.x, event.xmotion.y);
//				return true;
//			}
		}
	}

	return false;
}
#endif

void InputManagerImpl::setMousePosition(int aX, int aY)
{
	//    SetCursorPos(x, y);
	mApplication->setMousePosition(aX, aY);

	/*mousePosition.x = aX;
	mousePosition.y = aY;*/
}

void InputManagerImpl::centerCursor()
{
	//int mx = 0;
	//int my = 0;
	//int sx = mApplication->getWindowWidth();
	//int sy = mApplication->getWindowHeight();

	/*int winX, winY;
	winX = winY = 0;*/

	/*WindowLocation loc = mApplication->getWindowLocation();
	int dw = (loc.left + loc.right) / 2;
	int dh = (loc.top + loc.bottom) / 2;*/

	vec2<int32_t> center = mApplication->getWindowCenter();

	setMousePosition(center.x, center.y);
//	QUADRION_TRACE("Center cursor");
}

void InputManagerImpl::setCenterCursor(const bool aCenter)
{

	isCursorCentered = aCenter;

#if defined(_WIN32)
	/*if (!_centerCursor)
	{
		if (_isRawInputRegistered)
		{
			UnregisterRawInputDevice();
		}
	}
	else
	{
		if (!_isRawInputRegistered)
		{
			RegisterRawInputDevice();
		}
	}*/
#endif
}

const bool InputManagerImpl::isCursorCentering()
{
	return isCursorCentered;
}

vec2<int32_t> const InputManagerImpl::getMousePosition()
{
	vec2<int32_t> mousePos;
	mApplication->getMousePosition(mousePos.x, mousePos.y);

	return vec2<int32_t>(mousePos.x, mousePos.y);
}

vec2<int32_t> const InputManagerImpl::getMousePositionFromCenter()
{
	vec2<int32_t> mousePos;
	mApplication->getMousePosition(mousePos.x, mousePos.y);
//	QUADRION_TRACE("mouse x: {0}    mouse y: {1}", mousePos.x, mousePos.y);

	vec2<int32_t> center = mApplication->getWindowCenter();

	// Y is inversed.
	vec2<int32_t> delta = vec2<int32_t>(mousePos.x - center.x, center.y - mousePos.y);
//	QUADRION_TRACE("dx: {0}   dy: {1}", delta.x, delta.y);

	return delta;
}

//vec2<int32_t>* const InputManager_Impl::GetMouseTrace(int32_t& nSamples)
//{
//    nSamples = _mouseTrace.size();
//
//	return _mouseTrace.data();
//}



void InputManagerImpl::update(const UpdateContext& aUpdateContext)
{
	//    int32_t nMouseSamples;
	//    vec2<int32_t>* mouseTrace = GetMouseTrace(nMouseSamples);
	vec2<int32_t> curPt = getMousePosition();

	//    for (int32_t i = 0; i < nMouseSamples; ++i)
	 //   {
	 //       curPt.x += mouseTrace[i].x;
	  //      curPt.y += -mouseTrace[i].y;
	  //  }

	//int mx, my;
	//mApplication->getMousePosition(mx, my);
	//curPt.x = mx;
	//curPt.y = my;
	//vec2<int32_t> oldMousePos = mousePosition;

	//vec2<int32_t> center = mApplication->getWindowCenter();
	//curPt.x = curPt.x - center.x;
	//curPt.y = curPt.y - center.y;
	//curPt.y *= -1;

	///*mousePosition.x = curPt.x;
	//mousePosition.y = curPt.y;*/

	if (curPt.x != 0 || curPt.y != 0)
	{
		onMouseMove(curPt.x, curPt.y);

		if (isCursorCentered && aUpdateContext.windowFocused)
			centerCursor();
	}
}

bool InputManagerImpl::isNumPadInput()
{
#if defined(_WIN32)
	int numPad = 0;
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD0));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD1));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD2));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD3));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD4));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD5));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD6));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD7));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD8));
	numPad |= HIWORD(GetAsyncKeyState(VKEY_NUMPAD9));

	return numPad > 0;
#else
	return false;
#endif
}

void InputManagerImpl::onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction)
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onMouseButton(aButton, aAction);
	}
}

void InputManagerImpl::onMouseMove(const double aX, const double aY)
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onMouseMove(aX, aY);
	}
}

void InputManagerImpl::onMouseWheelScroll(const double aDeltaX, const double aDeltaY)
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onMouseWheelScroll(aDeltaX, aDeltaY);
	}
}

void InputManagerImpl::onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods)
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onKeyEvent(aKey, aAction, aMods);
	}
}

void InputManagerImpl::onCharEvent(const int aCodepoint)
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onCharEvent(static_cast<int>(aCodepoint));
	}
}

void InputManagerImpl::onWindowGotFocus()
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onWindowGotFocus();
	}
}

void InputManagerImpl::onWindowLostFocus()
{
	for (auto inputListener : mVecInputListeners)
	{
		inputListener->onWindowLostFocus();
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



//InputManager::InputManager(Application* application)
//{
//	mImpl = new InputManagerImpl(application);
//}

InputManager& InputManager::instance()
{
	static InputManager* instance = new InputManager();
	return *instance;
}

InputManager::InputManager()
{
	mIsInitialized = false;
}


InputManager::~InputManager()
{
	if (mImpl)
	{
		delete mImpl;
		mImpl = NULL;
	}
}

void InputManager::initialize(Application* aApplication)
{
	mImpl = new InputManagerImpl(aApplication);
	mIsInitialized = true;
}

void InputManager::registerInputListener(InputListener* aInputListener)
{
	mImpl->registerInputListener(aInputListener);
}

void InputManager::deregisterInputListener(InputListener* aInputListener)
{
	mImpl->deregisterInputListener(aInputListener);
}

bool InputManager::isKeyPressed(const uint8_t aKeyCode, const bool aCaseSensitive) const
{
	if (aKeyCode < 65 || (aKeyCode > 90 && aKeyCode < 97) || aKeyCode > 122)
	{
		return mImpl->keyStates[aKeyCode] == EKeyAction::PRESS;
	}

	if(!aCaseSensitive)
	{
		if (aKeyCode - 32 < 65)
		{
			// key is already upper case
			return mImpl->keyStates[aKeyCode] == EKeyAction::PRESS || mImpl->keyStates[aKeyCode + 32] == EKeyAction::PRESS;
		}
		else
		{
			// key is lower case
			return mImpl->keyStates[aKeyCode] == EKeyAction::PRESS || mImpl->keyStates[aKeyCode - 32] == EKeyAction::PRESS;
		}
	}

	return mImpl->keyStates[aKeyCode] == EKeyAction::PRESS;
}

bool InputManager::isKeyReleased(const uint8_t aKeyCode, const bool aCaseSensitive) const
{
	if(aKeyCode < 65 || (aKeyCode > 90 && aKeyCode < 97) || aKeyCode > 122)
	{
		return mImpl->keyStates[aKeyCode] == EKeyAction::RELEASE;
	}

	if (!aCaseSensitive)
	{
		if (aKeyCode - 32 < 65)
		{
			// key is already upper case
			return mImpl->keyStates[aKeyCode] == EKeyAction::RELEASE || mImpl->keyStates[aKeyCode + 32] == EKeyAction::RELEASE;
		}
		else
		{
			// key is lower case
			return mImpl->keyStates[aKeyCode] == EKeyAction::RELEASE || mImpl->keyStates[aKeyCode - 32] == EKeyAction::RELEASE;
		}
	}

	return mImpl->keyStates[aKeyCode] == EKeyAction::RELEASE;
}


//void InputManager::SetBindings(std::multimap<unsigned int, std::string> bindingCollection)
//{
//	mImpl->SetBindings(bindingCollection);
//}

#if defined(_WIN32)
bool InputManager::processInput(UINT aUMsg, WPARAM aWParam, LPARAM aLParam)
{
	return mImpl->processInput(aUMsg, aWParam, aLParam);
}
#else
bool InputManager::processInput(Display* aDisp)
{
	return mImpl->processInput(aDisp);
}
#endif


void InputManager::update(const UpdateContext& aUpdateContext)
{
	mImpl->update(aUpdateContext);
}

void InputManager::setMousePosition(int32_t aX, int32_t aY)
{
	mImpl->setMousePosition(aX, aY);
}

void InputManager::setMousePosition(vec2<int32_t> aPos)
{
	mImpl->setMousePosition(aPos.x, aPos.y);
}

void InputManager::centerCursor()
{
	mImpl->centerCursor();
}

void InputManager::setCenterCursor(const bool aCenter)
{
	mImpl->setCenterCursor(aCenter);
}

const bool InputManager::isCursorCentering()
{
	return mImpl->isCursorCentering();
}

vec2<int32_t> const InputManager::getMousePosition()
{
	return mImpl->getMousePosition();
}

vec2<int32_t> const InputManager::getMousePositionFromCenter()
{
	return mImpl->getMousePositionFromCenter();
}



//#if defined(_WIN32)
//vec2<int32_t>* const InputManager::GetMouseTrace( int32_t& nSamples )
//{
//    return mImpl->GetMouseTrace(nSamples);
//}
//#endif

