#include <memory>
#include <stack>

#include "Application.h"
#include "core/Timer.h"
#include "GameEngine.h"
//#include "physics/PhysX/PhysicsEnginePhysX.h"
#include "InputManager.h"
//#include "qwebui.h"
//#include "render/EffectResource.h"
#include "render/Render.h"
#include "render/QGfx.h"
//#include "scripting/ScriptingEngine.h"
//#include "sound/SoundEngine.h"
#include "core/QLog.h"
#include "io/FileSystem.h"
#include "assets/AssetManager.h"
#include "assets/EffectAsset.h"
//#include "scene/QScene.h"
//#include "ThreadArena.h"

GameEngine* GameEngine::instance;

class GameEngineImpl : public IUpdatable, InputListener
{
public:
	GameEngineImpl(const char* aIniPath, GameEngine* aGameEngine);
	~GameEngineImpl();

	void setTickRate(const unsigned int aHz);
	const unsigned int getTickRate();

	void gameLoop();

	void quit();
	void initializeCore(GameEngine* aGameEngine);
	void deinitializeCore();

	void pushGameState(GameState* aGameState);
	void popGameState();
	
	const int getWindowWidth();
	const int getWindowHeight();

	INI* getApplicationINI();

#if defined(_WIN32)
	LRESULT windowProc(HWND aHWnd, UINT aUMsg, WPARAM aWParam, LPARAM aLParam);
#endif

	QScene* scene = nullptr;
	EngineContext* mEngineContext;

private:
#if defined (_WIN32)
	static LRESULT CALLBACK _windowProc(HWND aHWnd, UINT aUMsg, WPARAM aWParam, LPARAM aLParam);
#endif

	virtual void tick(const UpdateContext& aUpdateContext) override final;
	virtual void update(const UpdateContext& aUpdateContext) override final;
	void render(const UpdateContext& aUpdateContext);

	virtual void onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction) override;
	virtual void onMouseMove(const double aX, const double aY) override;
	virtual void onMouseWheelScroll(const double aDeltaX, const double aDeltaY) override;
	virtual void onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods) override;
	virtual void onCharEvent(const int aCodepoint) override;
	virtual void onWindowGotFocus() override;
	virtual void onWindowLostFocus() override;

	std::unique_ptr<Application> mApplication;

	bool mDone;
	Timer mTimer;
	unsigned int mTickRate;
	EGameEngineMode mEngineMode;

	std::stack<GameState*> mGameStateStack;


	//	Core sub-systems
//	std::unique_ptr<InputManager> mInputManager;
//	std::unique_ptr<QWebUI> mGui;
//	std::unique_ptr<EffectResourceManager> mEffectResourceManager;
//	std::unique_ptr<IPhysicsEngine> mPhysics;
//	std::unique_ptr<ISoundEngine> mSound;
//	std::unique_ptr<IScriptingEngine> mScripting;
//	std::unique_ptr<AssetManager> mAssetManager;

	const static unsigned int mWidth = 1920;
	const static unsigned int mHeight = 1080;

	//uint32_t mBuffer[mWidth * mHeight];

//	GameStateTransition*	currentTransition = nullptr;
};

GameEngineImpl::GameEngineImpl(const char* aIniPath, GameEngine* aGameEngine)
{

#if defined(_WIN32)
	mApplication = std::make_unique<Application>(aIniPath, this, _windowProc);
#else	
	mApplication = std::make_unique<Application>(aIniPath, this); 
#endif


	InputManager::instance().initialize(mApplication.get());
	InputManager::instance().registerInputListener(this);

	mDone = false;
	mTimer = Timer();
	mTickRate = 60;
	mEngineMode = EGameEngineMode::PRIMARY;

	initializeCore(aGameEngine);

	mGameStateStack = std::stack<GameState*>();
}

GameEngineImpl::~GameEngineImpl()
{
	deinitializeCore();
}

void GameEngineImpl::initializeCore(GameEngine* aGameEngine)
{
	// TODO: Force VSYNC Off
#if defined(_WIN32) || defined(_WIN64)
	if (wglSwapIntervalEXT) wglSwapIntervalEXT(0);
#endif

	auto ini = mApplication->getApplicationINI();
	auto textureFilter = (uint32_t)ini->getInt("video", "texture_filter", 
												TRILINEAR_ANISO);

	ini->getFloat("input", "sensitivity", 0.001f);

	QGfx::initializeGraphics();
	Render::initialize();
	Render::setPreferredTextureFilter(TextureFilter(textureFilter));
//	QLog::construct();


	mEngineContext = new EngineContext(aGameEngine, mApplication.get());
}

void GameEngineImpl::deinitializeCore()
{
	if (mApplication)
	{
		auto ini = mApplication->getApplicationINI();
		ini->save();
	}

	while (!mGameStateStack.empty())
	{
		auto top = mGameStateStack.top();
		delete top;
		mGameStateStack.pop();
	}

	InputManager::instance().deregisterInputListener(this);

	if (mEngineContext)
	{
		delete mEngineContext;
		mEngineContext = NULL;
	}
}

void GameEngineImpl::setTickRate(const unsigned int hz)
{
	mTickRate = hz;
}

const unsigned int GameEngineImpl::getTickRate()
{
	return mTickRate;
}

void GameEngineImpl::gameLoop()
{
	UpdateContext context = { 0.00f };
	double accumulation = 0.0f;
	bool	resetTime = false;

//	MSG msg;

//	mEngineContext->input->centerCursor();
	InputManager::instance().centerCursor();

	while (!mDone)
	{
		mTimer.start();

		if (!mApplication->peekEvents())
			mDone = true;

/*
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				return;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
*/	
#ifndef _WIN32
//		mInputManager->processInput(mApplication->getDisplay());
		InputManager::instance().processInput(mApplication->getDisplay());
#endif

		if (!mEngineContext)
			continue;

		// If we're too far behind, reset the timer at the end of the loop to prevent a scenario where the engine can never get caught up.
		if (accumulation > 1.0)
			resetTime = true;
		
		while (accumulation >= 1.0/(double)(mTickRate))
		{
			accumulation -= 1.0 / (double)(mTickRate);

			// If we're more than 1 second behind, skip over some ticks to catch back up.
			if (accumulation > 1.0)
				continue;

			tick(context);
		}

		if (resetTime)
			mTimer.start();

		update(context);
		render(context);

		mApplication->presentBackbuffer();

		context.elapsed = mTimer.getElapsedSec();
		accumulation += context.elapsed;
		context.lag = accumulation;
		context.windowFocused = mEngineMode != EGameEngineMode::BACKGROUND;		

		if(mApplication->isQuitRequested())
			mDone = true;
	}
}

void GameEngineImpl::quit()
{
	mApplication->requestQuit();
}

#if defined(_WIN32)
LRESULT CALLBACK GameEngineImpl::_windowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	GameEngineImpl *c = (GameEngineImpl*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	if (c == NULL)
		return DefWindowProc(hWnd, uMsg, wParam, lParam);

	return c->windowProc(hWnd, uMsg, wParam, lParam);
}
#endif

void GameEngineImpl::tick(const UpdateContext& updateContext)
{ 
//    if(scene != nullptr)
 //   {
//		scene->onTick();
 //   }

	if (!mGameStateStack.empty())
	{
		mGameStateStack.top()->tick(updateContext);
	}
//	if (currentTransition)
//	{
//		currentTransition->Update(1.0f / 60.0f);
//
//		if (currentTransition->HasCompleted())
//		{
//			delete currentTransition;
//			currentTransition = nullptr;
//		}
//	}
//	else if (!mGameStateStack.empty())
//	{
//		mGameStateStack.top()->tick(updateContext);
//	}	
}

void GameEngineImpl::update(const UpdateContext& updateContext)
{
//    if(scene != nullptr)
//    {
//		scene->onUpdate();
//		scene->onLateUpdate();
 //   }

	if (!mGameStateStack.empty())
		mGameStateStack.top()->update(updateContext);

//	mInputManager->update(updateContext);
	InputManager::instance().update(updateContext);
}

void GameEngineImpl::render(const UpdateContext& updateContext)
{
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	QGfx::changeDepthTest(LEQUAL);

//    if(scene != nullptr)
//    {
//		scene->onRender();
//		scene->onPostRender();
 //   }

	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->render(updateContext);
}

//////////////////////////////////////////////////////////////////////////
// We pass InputHandler events to the active game state manually
// because we don't want events from all game-states getting fired, only
// the active one.
//////////////////////////////////////////////////////////////////////////

void GameEngineImpl::onMouseButton(const EMouseButton button, const EMouseButtonAction action)
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onMouseButton(button, action);
}

void GameEngineImpl::onMouseMove(const double x, const double y)
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onMouseMove(x, y);
}

void GameEngineImpl::onMouseWheelScroll(const double deltaX, const double deltaY)
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onMouseWheelScroll(deltaX, deltaY);
}

void GameEngineImpl::onKeyEvent(const int key, const EKeyAction action, const int mods)
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onKeyEvent(key, action, mods);
}

void GameEngineImpl::onCharEvent(const int codepoint)
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onCharEvent(codepoint);
}

void GameEngineImpl::onWindowGotFocus()
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onWindowGotFocus();
}

void GameEngineImpl::onWindowLostFocus()
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.top()->onWindowLostFocus();
}

void GameEngineImpl::pushGameState(GameState* gameState)
{
	gameState->initialize(mEngineContext);

	gameState->onInitialize(nullptr, 0);

	gameState->onGainedFocus();
	
	mGameStateStack.push(gameState);
}

void GameEngineImpl::popGameState()
{
	if (mGameStateStack.empty())
		return;

	mGameStateStack.pop();
}

const int GameEngineImpl::getWindowWidth()
{
	return mApplication->getWindowWidth();
}

const int GameEngineImpl::getWindowHeight()
{
	return mApplication->getWindowHeight();
}

INI* GameEngineImpl::getApplicationINI()
{
	return mApplication->getApplicationINI();
}

#if defined(_WIN32)
LRESULT GameEngineImpl::windowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_ACTIVATE:
		{
			if ((int)wParam == WA_ACTIVE || (int)wParam == WA_CLICKACTIVE)
			{
				// Game window got focus.
				mEngineMode = EGameEngineMode::PRIMARY;

				//LOG("Window gained focus.");
			}
			break;
		}
		case WM_KILLFOCUS:
		case WM_SYSCOMMAND:
		{
			// TODO: Need to _create a GameState event for gaining and losing focus so that the gamestate can release the mouse, etc.
			if (wParam == SC_SCREENSAVE || wParam == SC_MONITORPOWER)
				return 0;
			else if (wParam == SC_MINIMIZE)
			{
				// Game window was minimized.
				mEngineMode = EGameEngineMode::BACKGROUND;
				QUADRION_INFO("Window was minimized");
			}
			else if (wParam == SC_RESTORE)
			{
				// Game window was restored (from being minimized).
				mEngineMode = EGameEngineMode::PRIMARY;
				QUADRION_INFO("Window was restored");
			}

			break;
		}
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
	}

//	if (mInputManager)
//	{
//		auto res = mInputManager->processInput(uMsg, wParam, lParam);
//		if (res)
//		{
//			return 0;
//		}
//	}

	if(InputManager::instance().isInitialized())
	{
		auto res = InputManager::instance().processInput(uMsg, wParam, lParam);
		if(res)
			return 0;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
#endif
GameEngine::GameEngine(const char* iniPath) : mImpl(new GameEngineImpl(iniPath, this))
{
	instance = this;
//	ThreadArena::create();
}

GameEngine::~GameEngine()
{
	if (mImpl)
	{
//		ThreadArena::destroy();
		delete mImpl;
		mImpl = NULL;
	}
}

void GameEngine::setTickRate(const unsigned int hz)
{
	mImpl->setTickRate(hz);
}

const unsigned int GameEngine::getTickRate()
{
	return mImpl->getTickRate();
}

void GameEngine::preloadGameState(GameState* gameState)
{
	//mImpl->PushGameState(gameState);
}

void GameEngine::pushGameState(GameState* gameState)
{
	mImpl->pushGameState(gameState);
}

void GameEngine::popGameState()
{
	mImpl->popGameState();
}

void GameEngine::setScene(QScene* aScene)
{
	mImpl->scene = aScene;
}

const int GameEngine::getWindowWidth()
{
	return mImpl->getWindowWidth();
}

const int GameEngine::getWindowHeight()
{
	return mImpl->getWindowHeight();
}

INI* GameEngine::getApplicationINI()
{
	return mImpl->getApplicationINI();
}

void GameEngine::gameLoop()
{
	mImpl->gameLoop();
}

void GameEngine::quit()
{
	mImpl->quit();
}

EngineContext* GameEngine::getEngineContext() const
{
	return mImpl->mEngineContext;
}
