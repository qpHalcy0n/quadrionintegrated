#include "GameState.h"

void GameState::initialize(const EngineContext* aEngineContext)
{
	mEngineContext = aEngineContext;
}

void GameState::onInitialize(const char** aArgs, size_t aNumArgs)
{

}

void GameState::onGainedFocus()
{

}

void GameState::onLostFocus()
{

}
