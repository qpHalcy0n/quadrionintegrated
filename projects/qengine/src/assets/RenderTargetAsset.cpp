#include "assets/RenderTargetAsset.h"

RenderTargetAsset::RenderTargetAsset(const std::string& aName,
									 uint32_t aWidth, uint32_t aHeight,
									 ETexturePixelFormat aFormat,
									 uint32_t aDepthBits, uint32_t aStencilBits, uint32_t aFlags, uint32_t aNumTargets) : Asset(aName)
{
	mLoaded = false;
	mWidth = aWidth;
	mHeight = aHeight;
	mFormat = aFormat;
	mDepthBits = aDepthBits;
	mStencilBits = aStencilBits;
	mFlags = aFlags;
	mNumTargets = aNumTargets;
	mTextures = nullptr;
}

bool RenderTargetAsset::load()
{
	if(mNumTargets < 0 || mNumTargets > 8)
	{
		mLoaded = false;
		return false;
	}

	mTextures = new GLuint[mNumTargets];
	for(uint32_t i = 0; i < mNumTargets; ++i)
		mTextures[i] = addRenderTarget(mWidth, mHeight, mFormat, mFlags);
	mDS.depthId = 0;
	mDS.stencilId = 0;
	if(mDepthBits > 0 && mStencilBits > 0)
		mDS = addDepthStencilTarget(mWidth, mHeight, mDepthBits, mStencilBits, mFlags);

	mFBO = createRenderableSurface(mTextures, mNumTargets, mDS, mWidth, mHeight);

	mLoaded = true;
	return mLoaded;
}