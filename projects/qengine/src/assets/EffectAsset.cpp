#include "assets/EffectAsset.h"

#include <fstream>
#include "render/effectparser/GLSLInterpreter.h"
#include "core/QLog.h"
#include "StringUtils.h"

EffectAsset::EffectAsset(const std::string& aName,
				         const std::string& aFileName) : Asset(aName)
{
	

	mFileName = aFileName;
	mName = aName;
	mEffect = nullptr;
	mIsEffect = false;
}

EffectAsset::~EffectAsset()
{

}

bool EffectAsset::load()
{
	FILE* pFile = nullptr;
	long fLen = 0;
	char* cstr = nullptr;

	do
	{
		pFile = fopen(mFileName.c_str(), "r");
		if (pFile)
		{
			fseek(pFile, 0, SEEK_END);
			fLen = ftell(pFile);
			rewind(pFile);
		}

		int err = ferror(pFile);

		cstr = new char[fLen];
		fread(cstr, 1, fLen, pFile);
		fclose(pFile);
	} while(fLen == 0);
	


//	std::ifstream stream;
//	do
//	{
//		stream.open(mFileName);
//	}while(!stream.good());

//	std::ifstream stream(mFileName);
//	auto shit = std::istreambuf_iterator<char>(stream);
//	auto cock = std::istreambuf_iterator<char>();
//	std::stringstream buf;
//	buf << stream.rdbuf();

	std::string str(cstr);
//	stream.close();

	EffectParser parser;
	const auto root = parser.parse(str);

	GLSLInterpreter interpreter;
	mEffect = interpreter.construct(root, str.c_str(), mName);

	if (mEffect)
	{
		mLoaded = true;
		return true;
	}

	else
	{
		mLoaded = false;
		return false;
	}
}

std::shared_ptr<Effect> EffectAsset::getEffect()
{
	return mEffect;
}

void EffectAsset::overrideEffect(std::shared_ptr<Effect> aEffect)
{
}
