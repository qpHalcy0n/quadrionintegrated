#include "assets/HeightmapTerrainMeshAsset.h"


HeightmapTerrainMeshAsset::HeightmapTerrainMeshAsset(const std::string& aName,
											         Mesh* aMesh) : Asset(aName)
{
	mMeshRef = aMesh;
	mLoaded = true;
}

bool HeightmapTerrainMeshAsset::load()
{
	return true;	
}

Mesh* const HeightmapTerrainMeshAsset::getMesh()
{
	return mMeshRef;
}