#include "assets/CubemapAsset.h"
#include "render/QGfx.h"


CubemapAsset::CubemapAsset(const std::string& aName,
						   uint32_t aWidth, uint32_t aHeight,
						   ETexturePixelFormat aFormat) : Asset(aName)
{
	mLoaded = false;
	mWidth = aWidth;
	mHeight = aHeight;
	mFormat = aFormat;
}

bool CubemapAsset::load()
{
	mCubemapTexture = addRenderTarget(mWidth, mHeight, mFormat, SAMPLE_CLAMP | SAMPLE_LINEAR | QTEXTURE_CUBEMAP);
	DepthStencilId skyboxDS;
	skyboxDS.depthId = 0;
	skyboxDS.stencilId = 0;
	mFBO = createRenderableCubemap(mCubemapTexture, skyboxDS, mWidth, mHeight);
	QGfx::addFramebuffer(mName, mFBO);

	mLoaded = true;
	return true;
}