#include "Solar.h"

#include <ctime>


Solar::Solar()
{
    time_t now = time(0);
    mCurrentTime = gmtime(&now);

    // default lat/lon is in Dallas, TX 
    mLatitude = 32.7767f;
    mLongitude = -96.7970f;
    mSelectedUtcDev = -5;

    mUseSystemTime = true;
}

Solar::~Solar()
{

}

void Solar::Update()
{
    time_t now = time(0);
    mCurrentTime = gmtime(&now);

    if (mUseSystemTime)
    {
        mSelectedDay = GetCurrentYearDay();
        mSelectedHour = GetCurrentHour();
        mSelectedMinute = GetCurrentMinute();

        #if !defined(_WIN32) || !defined(_WIN64)
            mSelectedHour += mSelectedUtcDev;
        #endif
    }

    int day = mSelectedDay;
    int hour = mSelectedHour;
    int minute = mSelectedMinute;
   
    float minuteFrac = (float)minute / 60.0f;
    mHour = (float)hour + minuteFrac;

    mDecAngle = rad2Deg(   asinf(0.39795f * cosf(deg2Rad(  0.98563f * (day - 173.0f)   )))      );
    float lstm = 15.0f * mSelectedUtcDev;
    float B = (360.0f / 365.0f) * (day - 81);
    float eot = 9.87f * sinf(2.0f * B) - 7.53f * cosf(B) - 1.5f * sinf(B);
    float tc = 4.0f * (mLongitude - lstm) + eot;
    float lst = mHour + (tc / 60.0f);

    mHourAngle = 15.0f * (lst - 12.0f);

    float a = sinf(deg2Rad(mDecAngle));
    float b = sinf(deg2Rad(mLatitude));
    float c = cosf(deg2Rad(mDecAngle));
    float d = cosf(deg2Rad(mHourAngle));
    float e = cosf(deg2Rad(mLatitude));

//    asin(  (a*b) + (c*d*e)  )

//    mSunElevation = asinf(    (sinf(deg2Rad(mDecAngle)) * sinf(deg2Rad(mLatitude))) + 
//                              (cosf(deg2Rad(mDecAngle) * cosf(deg2Rad(mHourAngle) * cosf(deg2Rad(mLatitude))))));
    mSunElevation = asinf((a*b) + (c*d*e));
    mSunElevation = rad2Deg(mSunElevation);

    float aa = sinf(deg2Rad(mDecAngle));
    float ab = cosf(deg2Rad(mLatitude));
    float ac = cosf(deg2Rad(mDecAngle));
    float ad = cosf(deg2Rad(mHourAngle));
    float ae = sinf(deg2Rad(mLatitude));
    float af = cosf(deg2Rad(mSunElevation));

    float acosArg = ((aa*ab) - (ac*ad*ae)) / af;

 //   float acosArg = (((sinf(deg2Rad(mDecAngle)) * cosf(deg2Rad(mLatitude))) - (cosf(deg2Rad(mDecAngle)) * cosf(deg2Rad(mHourAngle)) * sinf(deg2Rad(mLatitude)))) / cosf(deg2Rad(mSunElevation)));
    acosArg = std::clamp(acosArg, -0.99999f, 0.999999f);
    float A = acosf(acosArg);
    A = rad2Deg(A);

    if(mHourAngle <= 0)
        mSunAzimuth = A;
    else    
        mSunAzimuth = 360.0f - A;

    mLightVector.z = sinf(deg2Rad(mSunAzimuth));
    mLightVector.x = cosf(deg2Rad(mSunAzimuth));
    mLightVector.y = sinf(deg2Rad(mSunElevation));
    mLightVector.normalize();
}


void Solar::SetLatitude(float aLat)
{
    if(aLat >= -90.0f && aLat <= 90.0f)
        mLatitude = aLat;
}

void Solar::SetLongitude(float aLon)
{
    if(aLon >= -180.0f && aLon <= 180.0f)
        mLongitude = aLon;
}

void Solar::SetDay(int32_t aDay)
{
    if(aDay >= 0 && aDay <= 365)
        mSelectedDay = aDay;
}

void Solar::SetHour(int32_t aHour)
{
    if(aHour >= 0 && aHour <= 23)
        mSelectedHour = aHour;
}

void Solar::SetMinute(int32_t aMinute)
{
    if(aMinute >= 0 && aMinute <= 59)
        mSelectedMinute = aMinute;
}

void Solar::SetUTCOffset(int32_t aUTCOffset)
{
    if(aUTCOffset >= -12 && aUTCOffset <= 12)
        mSelectedUtcDev = aUTCOffset;
}

void Solar::UseSystemTime(bool aUseSystemTime)
{
    mUseSystemTime = aUseSystemTime;
}


vec3<float> Solar::GetLightVector()
{
    return mLightVector;
}

int Solar::GetCurrentYearDay()
{
    return mCurrentTime->tm_yday;
}

int Solar::GetCurrentHour()
{
    return mCurrentTime->tm_hour;
}

int Solar::GetCurrentMinute()
{
    return mCurrentTime->tm_min;
}

int32_t Solar::GetDay()
{
    return mSelectedDay;
}

int32_t Solar::GetHour()
{
    return mSelectedHour;
}

int32_t Solar::GetMinute()
{
    return mSelectedMinute;
}

float Solar::GetSunAzimuth()
{
    return mSunAzimuth;
}

float Solar::GetSunAltitude()
{
    return mSunElevation;
}
