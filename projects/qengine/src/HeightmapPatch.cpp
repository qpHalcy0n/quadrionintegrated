#include "HeightmapPatch.h"

/*
struct HeightmapPatchImpl
{

};
*/

HeightmapPatch::HeightmapPatch()
{
	numVertsX = 0;
	numVertsZ = 0;

	minElevation = 0xffff;
	maxElevation = 0;
}


HeightmapPatch::HeightmapPatch(const HeightfieldVertex* verts,
							   const uint32_t nVertsX,
							   const uint32_t nVertsZ,
							   vec2<double> worldMins,
							   vec2<double> worldMaxs)
{
	minElevation = 0xffff;
	maxElevation = 0;

	numVertsX = nVertsX;
	numVertsZ = nVertsZ;
	uint32_t numVerts = nVertsX * nVertsZ;

	uint32_t tmp = 0;
	for(uint32_t i = 0; i < numVerts; ++i)
	{
		if(verts[i].vertex > maxElevation)
			maxElevation = verts[i].vertex;

		if(verts[i].vertex < minElevation)
			minElevation = verts[i].vertex;

		HeightfieldVertex vert = verts[i];
		heightmapVertices.push_back(vert);
	}


//	startX = startVertX;
//	startZ = startVertZ;

	worldMinimums = worldMins;
	worldMaximums = worldMaxs;
}

HeightmapPatch::~HeightmapPatch()
{
	heightmapVertices.clear();
}

const uint32_t HeightmapPatch::getNumVertsX() const
{
	return numVertsX;
}

const uint32_t HeightmapPatch::getNumVertsZ() const
{
	return numVertsZ;
}

const HeightfieldVertex HeightmapPatch::getVertex(uint32_t idx) const
{
	HeightfieldVertex ret;
	ret.vertex = 0;
	if(idx < 0 || idx > numVertsX * numVertsZ)
		return ret;

	if(heightmapVertices[idx].vertex > 0)
		int p = 0;

	ret = heightmapVertices[idx];
	return ret;
}

const vec2<double> HeightmapPatch::getWorldMinimums() const
{
	return worldMinimums;
}

const vec2<double> HeightmapPatch::getWorldMaximums() const
{
	return worldMaximums;
}

const uint16_t HeightmapPatch::getMinimumElevation() const
{
	return minElevation;
}

const uint16_t HeightmapPatch::getMaximumElevation() const
{
	return maxElevation;
}