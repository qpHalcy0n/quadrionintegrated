#include "core/QLog.h"

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <string>
#if defined(_WIN32)
#include <direct.h>
#include <windows.h>
#include <wchar.h>
#else
#include <stdio.h>
#include <unistd.h>
#endif 

std::shared_ptr<spdlog::logger> QLog::mCoreLogger;
std::shared_ptr<spdlog::logger> QLog::mClientLogger;

void QLog::construct()
{
	const std::string patternString = "%^[%T], %t:%P - [%n]: [%l] %v%$";

	char* cwd;
#if defined(_WIN32)
	cwd = _getcwd(nullptr, 0);
#else
	cwd = getcwd(nullptr, 0);
#endif

	std::string coreLocation = std::string(cwd);
	coreLocation += "/quadrion.log";

	std::string clientLocation = std::string(cwd);
	clientLocation += "/client.log";

	std::string combinedLocation = std::string(cwd);
	combinedLocation += "/console.log";

	const auto combinedFileSink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(combinedLocation.c_str(), true);
	const auto intermediateSink = std::make_shared<intermediate_sink_mt>();

	combinedFileSink->set_pattern(patternString.c_str());
	intermediateSink->set_pattern(patternString.c_str());

	mCoreLogger = spdlog::basic_logger_mt("Quadrion", coreLocation.c_str(), true);
	mCoreLogger->sinks().push_back(intermediateSink);
	mCoreLogger->sinks().push_back(combinedFileSink);
	mCoreLogger->set_pattern(patternString.c_str());
	mCoreLogger->set_level(spdlog::level::trace);

	mClientLogger = spdlog::basic_logger_mt("Client", clientLocation.c_str(), true);
	mClientLogger->sinks().push_back(intermediateSink);
	mClientLogger->sinks().push_back(combinedFileSink);
	mClientLogger->set_pattern(patternString.c_str());
	mClientLogger->set_level(spdlog::level::trace);
}