#ifndef __entity_h_
#define __entity_h_


#include <string>
#include <list>
#include <functional>
#include <type_traits>
#include <typeindex>
#include <cstddef>

#include "EngineExport.h"
#include "components/Component.h"
//#include "components/TransformComponentRod.h"
#include "components/TransformComponent.h"
#include "QMath.h"
#include "core/QLog.h"

#include "entities/EntityFactory.h"

//#include "scene/QScene.h"


class QENGINE_API Entity
{
	public:
		Entity();
		Entity(const std::string& aName);
		~Entity();

		template<typename T, typename ... Arguments>
		T* AddComponent(Arguments&& ... aArgs);
	
		template<typename T>
		T* getComponent();

		template<typename T>
		std::list<T*> getComponents();
	
		std::list<Component*>::iterator begin() { return mComponents.begin(); }
		std::list<Component*>::iterator end() { return mComponents.end(); }
	
		std::string name;
//		TransformComponentRod* transform;
		TransformComponent* transform;
	
	private:

		friend class Component;

		void removeComponent(Component* aComponent);

		std::list<Component*> mComponents;
};



template<typename T, typename ... Arguments>
T* Entity::AddComponent(Arguments&& ... aArgs)
{
	static_assert(std::is_base_of<Component, T>::value, "T is not derived from Component");
	static_assert(sizeof(T) <= 512, "Component is too large");

	T* mem = (T*)EntityFactory::instance().GetComponentMemBlock();
	::new(mem) T(std::forward<Arguments>(aArgs)...);

	std::type_index tid = std::type_index(typeid(T));
	mem->componentTypeName = tid.name();
	mem->componentTypeHash = tid.hash_code();
	
	mem->gameObject = this;
	mem->OnConstruct();
	mem->OnStart();

	InputManager::instance().registerInputListener(mem);

	mComponents.push_back(mem);

	EntityFactory::instance().RegisterComponent(std::type_index(typeid(T)), mem);

	return mem;
}

template<typename T>
T* Entity::getComponent()
{
	static_assert(std::is_base_of<Component, T>::value, "T is not derived from Component");

	auto iter = mComponents.begin();
	while(iter != mComponents.end())
	{
		std::type_index TTypeInfo = std::type_index(typeid(T));
		size_t TTypeHash = TTypeInfo.hash_code();

		if ((*iter)->componentTypeHash == TTypeHash)
		{
			return static_cast<T*>(*iter);
		}

		iter++;
	}

	return nullptr;
}


template<typename T>
std::list<T*> Entity::getComponents()
{
	static_assert(std::is_base_of<Component, T>::value, "T is not derived from Component");

	std::list<T*> components;

	auto iter = mComponents.begin();
	while (iter != mComponents.end())
	{
		std::type_index TtypeInfo = std::type_index(typeid(T));
		size_t TTypeHash = TtypeInfo.hash_code();

		if((*iter)->componentTypeHash == TTypeHash)
		{
			T* ret = static_cast<T*>(*iter);
			if(ret)
				components.push_back(ret);
		}

		iter++;
	}

	return components;
}

#endif 


