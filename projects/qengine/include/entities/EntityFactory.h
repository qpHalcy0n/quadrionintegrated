#ifndef __entityfactory_h_
#define __entityfactory_h_

#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <vector>

constexpr uint32_t MAX_ENTITIES = 65536;
constexpr uint32_t MAX_COMPONENTS = 65536;
constexpr uint32_t MAX_COMPONENTS_PER_TYPE = 65536;

#include "core/PoolAllocator.h"
#include "components/Component.h"
#include "EngineExport.h"

class Entity;

class QENGINE_API EntityFactory
{
    public:

        EntityFactory();

        std::shared_ptr<Entity> CreateEntity(const std::string& aName);
        
        void*   GetComponentMemBlock();
        void    RegisterComponent(std::type_index aType, Component* aMem);

        template <typename T> 
        void    DestroyComponent(T*& aComponent);

		template <typename T>
		std::vector<Component*>& GetComponentsByType();

        // Runtime
		void onEnable() {};
		void onConstruct() {};
		void onStart() {};
	
		void onTick() {};
		void OnUpdate();
		void onLateUpdate() {};
	
		void onPreRender() {};
		void onRender() {};
		void onPostRender() {};
	
		void onDestroy() {};

        ////////////////////////////////////////////////////
        static EntityFactory& instance();

    private:

        std::unordered_map<std::string, std::shared_ptr<Entity>> _entityMap;
//        std::unordered_map<std::type_index, std::list<Component*>> _componentTypeMap;
		std::unordered_map<std::type_index, std::vector<Component*>> _componentTypeMap;

        PoolAllocator*      _entityPool;
        PoolAllocator*      _componentPool;
};

template <typename T> 
void EntityFactory::DestroyComponent(T*& aComponent)
{
    if (!aComponent)
		return;

	static_assert(std::is_base_of<Component, T>::value, "T is not derived from Component");

	std::type_index tid = std::type_index(typeid(*aComponent));
	auto components = _componentTypeMap.find(std::type_index(typeid(*aComponent)));
	if (components == _componentTypeMap.end())
		//	if(components->second.size() <= 0)
		return;

	auto iter = std::find(components->second.begin(), components->second.end(), aComponent);
	if (iter != components->second.end())
	{
		components->second.erase(iter);
	}

	_componentPool->freeBlock((void*)aComponent);
	aComponent = nullptr;    
}

template <typename T>
std::vector<Component*>& EntityFactory::GetComponentsByType()
{
	auto iter = _componentTypeMap.find(std::type_index(typeid(T)));
	if (iter != _componentTypeMap.end())
	{
		return (iter->second);
	}

	else
	{
		std::vector<Component*> empty;
		return empty;
	}
}


#endif