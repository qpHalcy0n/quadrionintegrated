/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// QTEXTURE.H
//
// Written by Shawn Simonson 08/2008 for Quadrion Engine
//
// This file contains texture object class, descriptors and related functions that pertain to
// actual texture asset files such as .JPG, .DDS, and .TGA
//
// The class is essentially abstracted by CQuadrionRenderer for texture loading, however
// the class can be used to manage raw texture data assets such as lightmaps, or run-time
// generated height and normal maps
//
// This description is only here for record keeping.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef QIMAGE_H__
#define QIMAGE_H__

#include <cstdint>

#include "EngineExport.h"
#include "render/Texture.h"

struct BmpFileHdr
{
    unsigned short      bfType;
    unsigned long       bfSize;
    unsigned short      bfReserved1;
    unsigned short      bfReserved2;
    unsigned long       bfOffBits;
};

struct BmpInfoHdr
{
    unsigned long       biSize;
    long                biWidth;
    long                biHeight;
    unsigned short      biPlanes;
    unsigned short      biBitCount;
    unsigned long       biCompression;
    unsigned long       biSizeImage;
    long                biXPelsPerMeter;
    long                biYPelsPerMeter;
    unsigned long       biClrUsed;
    unsigned long       biClrImportant;
};

QENGINE_API unsigned int textureFourCc(uint8_t aC0, uint8_t aC1, uint8_t aC2, uint8_t aC3);

QENGINE_API bool textureIsDepthFormat(const ETexturePixelFormat& aFmt);

QENGINE_API bool textureIsPlainFormat(const ETexturePixelFormat& aFmt);

QENGINE_API bool textureIsCompressedFormat(const ETexturePixelFormat& aFmt);

QENGINE_API unsigned int textureGetBytesPerBlock(const ETexturePixelFormat& aFmt);

QENGINE_API unsigned int textureGetBytesPerPixel(const ETexturePixelFormat& aFmt);

QENGINE_API unsigned int textureGetBytesPerChannel(const ETexturePixelFormat& aFmt);

QENGINE_API unsigned int textureGetChannelCount(const ETexturePixelFormat& aFmt);

template <typename Type>
static void swapChannel(Type* aPixels, 
						const int32_t& aNumPixels, 
						const int32_t& aNumChannels, 
						const int32_t& aCh0, 
						const int32_t& aCh1)
{
	int pixelCount = aNumPixels;

	do
	{
		Type tmp = aPixels[aCh1];
		aPixels[aCh1] = aPixels[aCh0];
		aPixels[aCh0] = tmp;
		aPixels += aNumChannels;
	} while (--pixelCount);
}

template <typename Type>
static void genMipMap(Type* aSrc, 
					  Type* aDest, 
					  const uint32_t aW, 
					  const uint32_t aH, 
					  const uint32_t aD, 
					  const uint32_t aC)
{
	const uint32_t xOffset = (aW < 2) ? 0 : aC;
	const uint32_t yOffset = (aH < 2) ? 0 : aC * aW;
	const uint32_t zOffset = (aD < 2) ? 0 : aC * aW * aH;

	for (uint32_t z = 0; z < aD; z += 2)
	{
		for (uint32_t y = 0; y < aH; y += 2)
		{
			for (uint32_t x = 0; x < aW; x += 2)
			{
				for (uint32_t i = 0; i < aC; ++i)
				{
					*aDest++ = (aSrc[0] + aSrc[xOffset] + aSrc[yOffset] + aSrc[yOffset + xOffset] + aSrc[zOffset] +
						aSrc[zOffset + xOffset] + aSrc[zOffset + yOffset] + aSrc[zOffset + yOffset + xOffset]) / 8;
					++aSrc;
				}
				aSrc += xOffset;
			}
			aSrc += yOffset;
		}
		aSrc += zOffset;
	}
}

static bool isPowerOfTwo(const int aVal);

static int getMipMapCount2(const int& aMaxDimension);

struct TextureFileImpl;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// cQuadrionTextureFile
//
// This class represents the actual texture file asset.
// cQuadrionTextureFile will open, load, release, and delete TGA, JPG, and DDS texture file formats.
// This class is also used to load texture assets into the renderer and ultimately onto the video board.
// Ultimately, this class is fully abstracted unless using it to manage raw texture assets such as lightmaps
//
// Update: 1/21/2018 By Renato Ciuffo
//  This class is now obselete. Use TextureAsset instead.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class QENGINE_API Image
{
	public:

		Image();
		Image(const Image& aTex);
		Image& operator= (const Image& aTex);
		~Image();

		// load texture from filename //
		bool loadTexture(const char* aFname, 
						 bool aKeepLocal, 
						 const char* aPname = "./", 
						 bool aFlipVertical = false) const;

		// load texture from raw buffer //
		bool loadTexture(const uint32_t aWidth, 
								const uint32_t aHeight, 
								ETexturePixelFormat aFmt, 
								bool aKeepLocal, 
								void* aBuf) const;

		bool unloadTexture() const;

		void updateDirtyRegion(const int32_t aXOffset, 
									  const int32_t aYOffset, 
									  const int32_t aWidth, 
									  const int32_t aHeight, 
									  const void* aBuf) const;

		bool heightToNormal() const;

		// Swap 2 channels in texture //
		bool swapChannels(const uint32_t& aCh0, 
								 const uint32_t& aCh1, 
								 bool aNormalMap = false) const;

		// Add channel //
		bool addChannel(const float aVal = 0) const;

		// Get pointer to pixel data at "level" mip level //
		unsigned char* getData(const unsigned int& aLevel = 0) const;

		// Get pixel format in ETexturePixelFormat format //
		ETexturePixelFormat getPixelFormat() const;

		// Obtain width, height, and depth of original format //
		int getWidth() const;
		int getHeight() const;
		int getDepth() const;
		int getBitsPerPel() const;

		// Obtain width, height, and depth at a particlar mip level //
		int	getWidth(const unsigned int& aLevel) const;
		int	getHeight(const unsigned int& aLevel) const;
		int	getDepth(const unsigned int& aLevel) const;

		// Obtain total pixel count //
		// firstMip- first mip level to query from (defaults to 0)
		// aNumLevels- number of mip levels to query (defaults to all mipmaps)
		uint32_t getPixelCount(const uint32_t& aFirstMip = 0, 
										  uint32_t aNumLevels = QTEXTURE_ALL_MIPMAPS) const;

		void GetAsBitmap(void** aOutputBuffer, int &aLength) const;

		// Obtain file name with path and extension //
		const char* getFileName() const;
		void setFileName(const char* aFileName) const;
		const char* getFilePath() const;
		void setFilePath(const char* aFilePath) const;
		//void				GetFileName(const std::string* fname);
		void setGamma(float aExp) const;
		void setTexFilter(const uint32_t &aTexFilter) const;

		// Query image type //
		bool is3D() const;
		bool is2D() const;
		bool isCubemap() const;


		bool isLoaded() const;

		GLuint getOpenGlid() const;
		GLuint getHeightOpenGlid() const;
		GLuint getNormalOpenGlid() const;


	protected:

	private:

		TextureFileImpl* mImpl = nullptr;		
};






#endif
