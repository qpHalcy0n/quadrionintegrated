//////////////////////////////////////////////////////////////////////////////////
//
// CAMERA.H
//
// Written by Shawn Simonson for Quadrion Engine 11/2005
//
// Provides facility for basic free-roaming camera object.
// Cameras support either Orthographic or Perspective projection. You will
// also find facility for frustum plane extraction herein.
//
//////////////////////////////////////////////////////////////////////////////////

#ifndef CAMERA_H__
#define CAMERA_H__

#include "QMath.h"
#include "EngineExport.h"





//class CameraImpl;
class QENGINE_API Camera
{
	public:
	
		Camera();
		Camera(const Camera& aCamera);
		~Camera();
	
		void setCamera(vec3<float> aCamPos, vec3<float> aLookAt, vec3<float> aUp);
	
		void setPerspective(const float aFovy, const float aAspect, const float aNearClip, const float aFarClip);
	
		void setOrtho(const float aLeft, const float aRight, const float aTop, const float aBottom, const float aNearClip, const float aFarClip);
	
		void setPosition(const vec3<float> aPos);
	
		void setLookAtPosition(const vec3<float> aPoint)
		{
			mCurLookAt = aPoint;
		}
	
		void update(const int aMouseX, const int aMouseY, float aSensitivity);
	
		void render();
		void renderFromPose(mat3<float> pose, vec3<float> camPos);
	
		void moveForward(const float aFMul = 0.1F);
		void moveBack(const float aFMul = 0.1F);
		void moveRight(const float aFMul = 0.1F);
		void moveLeft(const float aFMul = 0.1F);
	
		bool pointInProjFrustum(const vec2<float> aPt);
	
		inline float getAspectRatio() const { return mCurAspect; }
	
		inline float getFarClip() const { return mZFar; }
		inline float getNearClip() const { return mZNear; }
		inline vec3<float> getAdjustedUpVec() const { return mAdjustedUpVec; }
		inline float getFov() const { return mCurFov; }
		inline vec3<float> getPosition() const { return mCurPosition; }

		vec3<float> getForwardVec() const { return mForwardVec; }
		vec3<float> getRightVec() const { return mRightVec; }
	
		inline vec3<float> getDirection() const
		{
			return mCurLookAt - mCurPosition;
		}
	
		inline vec3<float> getLookAtPosition() const
		{
			return mCurLookAt;
		}
	
		inline line2<float> getProjectedLeftPlane() const
		{
			return mProjLeft;
		}
	
		inline line2<float> getProjectedRightPlane() const
		{
			return mProjRight;
		}
	
		inline line2<float> getProjectedNearPlane() const
		{
			return mProjNear;
		}
	
		inline line2<float> getProjectedFarPlane() const
		{
			return mProjFar;
		}
	
		vec4<float> getVecFromScreenspace(const uint32_t aX, const uint32_t aY, const uint32_t aWinWidth, const uint32_t aWinHeight) const;
	
		mat4<float> getViewMatrix() const;
	
		void operator= (const Camera& aCamera);
	
		static Camera* current;
	
	private:
	//	CameraImpl* mImpl;

		void renderOrtho();

		bool			mIsOrtho;
	
		mat4<float>		viewMatrix;
	
		vec3<float>        mCurPosition;      // Current camera worldspace position
		vec3<float>        mCurLookAt;        // Current camera look at position
		vec3<float>        mCurUpVec;         // Current worldspace up vector (relative)
		vec3<float>		   mRightVec;
		vec3<float>		   mForwardVec;

		vec3<float>		mAdjustedUpVec;	
	
		line2<float>		mProjRight;
		line2<float>		mProjLeft;
		line2<float>		mProjNear;
		line2<float>		mProjFar;
	
		float       mCurFov;           // Current camera field of view (degrees)
		float		mCurHorizFov;
		float       mCurAspect;        // Current camera frustum aspect ratio
		float       mZNear;            // Near clip distance
		float       mZFar;             // Far clip distance
		float		mTheta;
		float		mPhi;
	
		float		mLeft;
		float		mRight;
		float		mTop;
		float		mBottom;
	
		float		mFarClipHeight;
		float		mFarClipWidth;
	
		float mPi2 = PI2_F - 0.001f;
};


#endif
