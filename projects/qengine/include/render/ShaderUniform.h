#ifndef shaderuniform_h__
#define shaderuniform_h__

#include <string>
#include <vector>
#include <cassert>
#include "ShaderStruct.h"
#include "EngineExport.h"

struct UniformBufferElement
{
	uint32_t offset;
	uint32_t size;
	std::string name;
};

class QENGINE_API ShaderUniformDeclaration
{
	public:
		enum class Type : int
		{
			NONE, FLOAT32, VEC2, VEC3, VEC4, MAT3, MAT4, INT32, STRUCT, BOOL
		};

		ShaderUniformDeclaration(const Type aType, const std::string& aName, uint32_t aCount = 1);
		ShaderUniformDeclaration(ShaderStruct* aStruct, const std::string& aName, uint32_t aCount = 1);

		inline const char* getName() const { return mName; }
		inline uint32_t getSize() const { return mSize; }
		inline uint32_t getCount() const { return mCount; }
		inline uint32_t getOffset() const { return mOffset; }
		inline uint32_t getAbsoluteOffset() const { return mStruct ? mStruct->getOffset() + mOffset : mOffset; }

		int32_t getLocation() const { return mLocation; }
		inline Type getType() const { return mType; }
		inline const ShaderStruct& getShaderUniformStruct() const { assert(mStruct); return *mStruct; }

		static uint32_t sizeofUniformType(const Type aType);
		static uint32_t sizeofUniformBufferType(const Type aType, const bool aIsArray, const uint32_t aAmount);
		static uint32_t offsetofUniformBufferType(const Type aType, const bool aIsArray, const uint32_t aAmount);
		static Type stringToType(const std::string& aType);
		static std::string typeToString(const Type aType);

	private:
		friend class Shader;
		friend class ShaderUniformBufferDeclaration;
		friend class ShaderStruct;

		void _setOffset(const uint32_t aOffset);

		char* mName;
		uint32_t mSize;
		uint32_t mCount;
		uint32_t mOffset;

		Type mType;
		ShaderStruct* mStruct;
		mutable int32_t mLocation;
};

struct ShaderUniformField
{
	ShaderUniformDeclaration::Type type;
	std::string name;
	uint32_t count;
	mutable uint32_t size;
	mutable uint32_t location;
};

class ShaderUniformBufferDeclarationImpl;
class QENGINE_API ShaderUniformBufferDeclaration
{
	public:
		ShaderUniformBufferDeclaration(const std::string& aName, uint32_t aShaderType);
		~ShaderUniformBufferDeclaration();

		void pushUniform(ShaderUniformDeclaration* aUniform);

		inline const char* getName() const { return mName; }
		inline uint32_t getRegister() const { return mRegister; }
		inline uint32_t getShaderType() const { return mShaderType; }
		inline uint32_t getSize() const { return mSize; }
		std::vector<ShaderUniformDeclaration*> getUniformDeclarations() const;

		ShaderUniformDeclaration* findUniform(const std::string& aName) const;

	private:
		friend class Shader;

		ShaderUniformBufferDeclarationImpl* mImpl;

		char* mName;
		uint32_t mRegister;
		uint32_t mSize;
		uint32_t mShaderType;
};

#endif // shaderuniform_h__