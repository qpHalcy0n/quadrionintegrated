#ifndef effectinterface_h__
#define effectinterface_h__

#include <string>
#include <vector>
#include "render/effectparser/EffectInterfaceVariable.h"

struct EffectInterface
{
	std::string name;
	std::vector<EffectInterfaceVariable> variables;
};

#endif // effectinterface_h__