#ifndef effectoutputbuffer_h__
#define effectoutputbuffer_h__

#include <string>

struct EffectOutputBuffer
{
	std::string name;

	uint32_t numBuffers;
	uint32_t width;
	uint32_t height;

	std::string pixelType;
	std::string depthStencilType;
	std::vector<std::string> flags;
};


#endif // effectoutputbuffer_h__