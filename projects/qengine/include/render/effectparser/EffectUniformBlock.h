#ifndef effectuniformblock_h__
#define effectuniformblock_h__

#include <string>
#include <vector>
#include "render/effectparser/EffectVariableType.h"
#include "render/effectparser/EffectUniform.h"

struct EffectUniformBlock
{
	std::string layout;
	std::string blockName;
	std::string shader;
	std::vector<EffectUniform> uniforms;
};

#endif // effectuniformblock_h__