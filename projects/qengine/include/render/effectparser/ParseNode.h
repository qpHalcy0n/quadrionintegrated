#ifndef effectroot_h__
#define effectroot_h__

#include <vector>
#include <string>
#include "render/effectparser/EffectVariable.h"
#include "render/effectparser/EffectVersion.h"
#include "render/effectparser/EffectExtension.h"
#include "render/effectparser/EffectFunction.h"
#include "render/effectparser/EffectInterface.h"
#include "render/effectparser/EffectInterfaceVariable.h"
#include "render/effectparser/EffectUniform.h"
#include "render/effectparser/EffectUniformBlock.h"
#include "render/effectparser/EffectSemantics.h"
#include "render/effectparser/EffectLayout.h"
#include "render/effectparser/EffectTechnique.h"
#include "render/effectparser/EffectInclude.h"
#include "render/effectparser/EffectOutputBuffer.h"
#include "render/effectparser/EffectDepthBuffer.h"

enum class ParseNodeType : uint32_t
{
	ROOT,
	VERSION,
    EXTENSION,
	VARIABLE,
    MAINFUNCTION,
    FUNCTION,
	EINTERFACE,
	UNIFORM,
	UNIFORMBLOCK,
	BUFFERBLOCK,
    LAYOUT,
    TECHNIQUE,
	PASS,
    INCLUDE,
	OUTPUTBUFFER,
	DEPTHBUFFER
};

struct IParseNode
{
	std::string name;
	std::vector<IParseNode*> children;
	IParseNode* parent = nullptr;
	ParseNodeType type;

	std::string sourceCopy;
};

template<ParseNodeType T>
struct ParseNode : IParseNode
{
	ParseNode();
};

struct ParseEffectVersion : public ParseNode<ParseNodeType::VERSION>
{
	EffectVersion version;
};

struct ParseEffectExtension : public ParseNode<ParseNodeType::EXTENSION>
{
	EffectExtension extension;
};

struct ParseEffectInclude : public ParseNode<ParseNodeType::INCLUDE>
{
	EffectInclude include;
};

struct ParseVariable : public ParseNode<ParseNodeType::VARIABLE>
{
	EffectVariable variable;
};

struct ParseUniform : public ParseNode<ParseNodeType::UNIFORM>
{
	EffectUniform uniform;
};

struct ParseUniformBlock : public ParseNode<ParseNodeType::UNIFORMBLOCK>
{
	EffectUniformBlock block;
};

struct ParseBufferBlock : public ParseNode<ParseNodeType::BUFFERBLOCK>
{
	EffectUniformBlock block;
};

struct ParseFunction : public ParseNode<ParseNodeType::FUNCTION>
{
	EffectFunction function;
};

struct ParseMainFunction : public ParseNode<ParseNodeType::MAINFUNCTION>
{
	EffectMainFunction function;
};

struct ParseInterface : public ParseNode<ParseNodeType::EINTERFACE>
{
	EffectInterface effectInterface;
};

struct ParseLayout : public ParseNode<ParseNodeType::LAYOUT>
{
	EffectLayout layout;
};

struct ParseTechnique : public ParseNode<ParseNodeType::TECHNIQUE>
{
	EffectTechnique technique;
};

struct ParsePass : public ParseNode<ParseNodeType::PASS>
{
	EffectPass pass;
};

struct ParseOutputBuffer : public ParseNode<ParseNodeType::OUTPUTBUFFER>
{
	EffectOutputBuffer buffer;
};

struct ParseDepthBuffer : public ParseNode<ParseNodeType::DEPTHBUFFER>
{
	EffectDepthBuffer buffer;
};

template<ParseNodeType T>
inline ParseNode<T>::ParseNode()
{
	this->type = T;
}

#endif
