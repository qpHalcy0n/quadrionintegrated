#ifndef effectversion_h__
#define effectversion_h__

#include <string>

struct EffectVersion
{
	std::string version;
	std::string profile;
};

#endif // effectversion_h__