#ifndef effecttechnique_h__
#define effecttechnique_h__

#include <string>
#include <vector>
#include <functional>
#include <utility>
#include "render/effectparser/EffectPass.h"

struct EffectTechnique
{
	std::string name;
	std::vector<EffectPass> passes;

    inline bool operator == (const EffectTechnique& other) const
    {
		return name == other.name && passes == other.passes;
    }

	inline bool operator != (const EffectTechnique& other) const
	{
		return name != other.name || passes != other.passes;
	}
};


namespace std
{
	template <>
	struct hash<EffectTechnique>
	{
		size_t operator()(const EffectTechnique& key) const noexcept;
	};

//	template <>
    inline size_t hash<EffectTechnique>::operator()(const EffectTechnique& key) const noexcept
    {
        return hash<std::string>()(key.name.c_str());
    }
}

#endif // effecttechnique_h__