#ifndef effectpass_h__
#define effectpass_h__

#include <string>
#include <unordered_map>
#include <vector>
#include "render/effectparser/EffectFunction.h"

struct EffectPass
{
	std::string name;
	std::string vertexFunction;
	std::string tesFunction;
	std::string tcsFunction;
	std::string geometryFunction;
	std::string fragmentFunction;

	std::unordered_map<std::string, std::vector<std::string>> other;

    inline bool operator == (const EffectPass& o) const
    {
		return name == o.name && vertexFunction == o.vertexFunction && tesFunction == o.tesFunction && tcsFunction == o.tcsFunction &&
			geometryFunction == o.geometryFunction && fragmentFunction == o.fragmentFunction;
    }

	inline bool operator != (const EffectPass& o) const
    {
		return name != o.name || vertexFunction != o.vertexFunction || tesFunction != o.tesFunction || tcsFunction != o.tcsFunction ||
			geometryFunction != o.geometryFunction || fragmentFunction != o.fragmentFunction;
	}
};

#endif // effectpass_h__