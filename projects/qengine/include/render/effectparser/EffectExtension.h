#ifndef effectextension_h__
#define effectextension_h__

#include <string>

struct EffectExtension
{
	std::string name;
	std::string mode;
};

#endif // effectextension_h__