#ifndef effectsemantics_h__
#define effectsemantics_h__

#include <stdint.h>
#include <string>
#include "EngineExport.h"

enum class EffectSemantic : uint32_t
{
    POSITION,
    NORMAL0,
    NORMAL1,
    TEXCOORD0,
    TEXCOORD1,
    COLOR0,
    COLOR1,
    UNKNOWN
};

QENGINE_API EffectSemantic getEffectSemanticType(const std::string& aName);

#endif // effectsemantics_h__