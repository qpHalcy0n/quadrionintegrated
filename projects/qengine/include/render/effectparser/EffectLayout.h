#ifndef effectlayout_h__
#define effectlayout_h__

#include <string>
#include <vector>
#include "render/effectparser/EffectInterfaceSpecifier.h"

struct EffectLayoutOperand
{
	std::string name;
	std::string defaultValue;
};

struct EffectLayout
{
	std::vector<EffectLayoutOperand> operands;
	EffectInterfaceSpecifier specifier;
	std::string shader;
};

#endif // effectlayout_h__