#ifndef effectinterfacespecifier_h__
#define effectinterfacespecifier_h__

#include <stdint.h>
#include <string>
#include "EngineExport.h"

enum class EffectInterfaceSpecifier : uint32_t
{
	INTERFACE_IN,
	INTERFACE_OUT,
	UNKNOWN
};

QENGINE_API EffectInterfaceSpecifier getEffectInterfaceSpecifier(const std::string& aString);
QENGINE_API std::string getEffectInterfaceSpecifierString(const EffectInterfaceSpecifier& specifier);

#endif // effectinterfacespecifier_h__