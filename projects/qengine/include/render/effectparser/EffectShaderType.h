#ifndef effectshadertype_h__
#define effectshadertype_h__

#include <stdint.h>
#include "QMath.h"

enum class EffectShaderType : uint32_t
{
    VERTEX = bitLeft(1),
    FRAGMENT = bitLeft(2),
    GEOMETRY = bitLeft(3),
    TESSELATIONCONTROL = bitLeft(4),
    TESSELATIONEVALUATION = bitLeft(5)
};

#endif // effectshadertype_h__