#ifndef effectdepthbuffer_h__
#define effectdepthbuffer_h__

#include <string>

struct EffectDepthBuffer
{
	std::string name;

	std::string depthStencilType;
};


#endif // effectdepthbuffer_h__