#ifndef effecttypequalifier_h__
#define effecttypequalifier_h__

#include <string>
#include <stdint.h>
#include "EngineExport.h"

enum class EffectTypeQualifier : uint32_t
{
	CONSTANT,
	UNIFORM,
	VARYING,
	READONLY,
	WRITEONLY,
	VOLATILE,
	RESTRICT,
	CENTROID,
	FLAT,
	SMOOTH,
	NOPERSPECTIVE,
	LOWP,
	MEDIUMP,
	HIGHP,
	GLSLOUT,
	GLSLIN,
	GLSLINOUT,
	UNKNOWN
};

QENGINE_API EffectTypeQualifier getEffectTypeQualifier(const std::string& aString);
QENGINE_API std::string getEffectTypeQualifierString(const EffectTypeQualifier& aQualifier);

#endif // effecttypequalifier_h__