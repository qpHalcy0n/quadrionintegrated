#ifndef effectinclude_h__
#define effectinclude_h__

#include <string>

struct EffectInclude
{
	std::string path;
};

#endif // effectinclude_h__