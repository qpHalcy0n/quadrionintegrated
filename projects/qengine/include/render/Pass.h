#ifndef passrod_h__
#define passrod_h__

#include <string>
#include <unordered_map>
#include <map>
#include "EngineExport.h"
#include "Shader.h"

#include "Texture.h"
#include "render/effectparser/EffectVariableType.h"


struct OutputBufferInfo;

class QENGINE_API SamplerObject
{
public:
	std::string name;

	std::string minFilter;
	std::string magFilter;

	std::string wrapS;
	std::string wrapT;
	std::string wrapR;

	float anisotropy;

	uint32_t samplerID;

	SamplerObject()
	{
		minFilter = magFilter = wrapS = wrapT = wrapR = "NONE";
		samplerID = 0;
		anisotropy = 1;
	}

	void bind(uint32_t unit) const;
};

class QENGINE_API RasterizerState
{
public:
	std::string polygon;
	std::string polygonMode;

	std::string depthTest;
	std::string depthWrite;
	std::string depthFunc;

	std::string cullEnable;
	std::string cullFace;
	std::string frontFace;

	std::string colorR, colorG, colorB, colorA;

	std::string alphaSrc, alphaDst;
	std::string alphaBlend;

	std::string stencilFunc;
	uint32_t stencilRef, stencilMask;
	std::string stencilsFail, stencildpFail, stencilPass;
	std::string stencilTest;

	RasterizerState()
	{
		polygon = "";
		polygonMode = "";

		depthTest = "";
		depthWrite = "";
		depthFunc = "";

		cullEnable = "";
		cullFace = "";
		frontFace = "";

		colorR = colorG = colorB = colorA = "";

		alphaSrc = "";
		alphaDst = "";
		alphaBlend = "";

		stencilTest = "";
		stencilFunc = stencilsFail = stencildpFail = stencilPass = "";
		stencilRef = stencilMask = 0;
	}

	void enable() const;
	void disable() const;
};

class Technique;
class PassImpl;
class QENGINE_API Pass
{
public:
	friend class Effect;
	friend class EffectImpl;
	friend class GLSLInterpreter;

	Pass(const std::string& aName, Technique* aTechnique);
	//		Pass(const std::string& aName, 
	//				Technique* aTechnique);
	~Pass();

	void compile() const;

	void bind() const;
	void unbind() const;

	std::string& getName() const;

	Shader* getShader() const;

	bool hasSampler(const std::string& aName) const;

	std::string& getVertexSource() const;
	std::string& getGeometrySource() const;
	std::string& getFragmentSource() const;
	std::string& getPassSource() const;

	void setRasterizerState(RasterizerState* state);
	RasterizerState* getRasterizerState() const;
	void addSamplerState(SamplerObject* state);
	SamplerObject* getSamplerState(const std::string& textureName);

	void setOutputBuffer(OutputBufferInfo* aInfo);
	OutputBufferInfo* getOutputBuffer() const;
	
	void setSamplerLoadPaths(const std::unordered_map<std::string, std::pair<EffectVariableType, std::string>>& aPaths) const;
	std::unordered_map<std::string, std::pair<EffectVariableType, std::string>>& getSamplerLoadPaths();
	
	vec4<float> getClearColor();
	uint32_t getClearMode();

private:
	PassImpl* mImpl;

	void _setName(const std::string& aName) const;
	void _setVertexSource(const std::string& aSource) const;
	void _setTCSSource(const std::string& aSource) const;
	void _setTESSource(const std::string& aSource) const;
	void _setGeometrySource(const std::string& aSource) const;
	void _setFragmentSource(const std::string& aSource) const;
	void _setSource(const std::string& aSource) const;

	void _setClearColor(vec4<float> aColor);
	void _setClearMode(uint32_t aMode);

	OutputBufferInfo* mOutputBufferInfo;
};

#endif // passrod_h__