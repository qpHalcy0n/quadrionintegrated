#ifndef __meshnodematerial_h_
#define __meshnodematerial_h_

#include "QMath.h"

struct MeshNodeMaterial
{
	uint32_t albedoTexture;
	uint32_t emissiveTexture;
	uint32_t aoTexture;
	uint32_t roughnessTexture;
	uint32_t metallicTexture;
	uint32_t normalMap;
	uint32_t heightMap;

	vec3<float>		diffuseColor;
	vec3<float>		specularColor;
	vec3<float>		ambientColor;
	vec3<float>		emissiveColor;
	float			shininess;
	float			shininessStrength;
	float			nRoughnessMetallicChannels;
};

#endif