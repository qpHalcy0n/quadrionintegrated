#ifndef vertexbufferlayout_h__
#define vertexbufferlayout_h__

#include <assert.h>
#include <vector>
#include <string>
#include "Extensions.h"
#include "QMath.h"

struct BufferElement
{
	std::string name;
	uint32_t type;
	uint32_t size;
	uint32_t count;
	size_t offset;
	uint32_t location;
	bool normalized;
};

class QENGINE_API VertexBufferLayout
{
	public:
		VertexBufferLayout();

		template<typename T>
		void push(const std::string& aName, uint32_t aCount = 1, bool aNormalized = false)
		{
			assert(false);
		}

		inline const std::vector<BufferElement>& getLayout() const { return mLayout; }
		inline uint32_t getStride() const { return mSize; }

	private:
		void push(const std::string& aName, uint32_t aType, uint32_t aSize, uint32_t aCount, bool aNormalized);

		uint32_t mSize;
		std::vector<BufferElement> mLayout;
};

template<>
inline void VertexBufferLayout::push<float>(const std::string& aName, uint32_t aCount, bool aNormalized)
{
	push(aName, GL_FLOAT, sizeof(float), aCount, aNormalized);
}

template<>
inline void VertexBufferLayout::push<uint32_t>(const std::string& aName, uint32_t aCount, bool aNormalized)
{
	push(aName, GL_UNSIGNED_INT, sizeof(uint32_t), aCount, aNormalized);
}

template<>
inline void VertexBufferLayout::push<uint8_t>(const std::string& aName, uint32_t aCount, bool aNormalized)
{
	push(aName, GL_UNSIGNED_BYTE, sizeof(uint8_t), aCount, aNormalized);
}

template<>
inline void VertexBufferLayout::push<vec2<float>>(const std::string& aName, uint32_t aCount, bool aNormalized)
{
	push(aName, GL_FLOAT, sizeof(float), 2, aNormalized);
}

template<>
inline void VertexBufferLayout::push<vec3<float>>(const std::string& aName, uint32_t aCount, bool aNormalized)
{
	push(aName, GL_FLOAT, sizeof(float), 3, aNormalized);
}

template<>
inline void VertexBufferLayout::push<vec4<float>>(const std::string& aName, uint32_t aCount, bool aNormalized)
{
	push(aName, GL_FLOAT, sizeof(float), 4, aNormalized);
}

#endif // vertexbufferlayout_h__