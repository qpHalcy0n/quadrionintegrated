#ifndef __CUBEMAPCAMERA_H_
#define __CUBEMAPCAMERA_H_

#include "QMath.h"
#include "EngineExport.h"

struct CubemapCamera
{
	vec3<float>		worldPos;
	vec3<float>		viewDir;
//	uint32_t		screenX, screenY;
	vec2<float>		viewportDims;

	mat4<float>		viewMat, projMat;
};



QENGINE_API void GenerateCubemapCamera(vec3<float> aPos, vec3<float> aViewDir, uint32_t aWinX, uint32_t aWinY,
										  CubemapCamera& aCam);



#endif
