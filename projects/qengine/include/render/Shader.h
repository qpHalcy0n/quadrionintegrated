#ifndef SHADER_H__
#define SHADER_H__

#include <map>


//#include "EngineExport.h"
//#include "QMath.h"
//#include "Extensions.h"
//#include "render/QGfx.h"

#define LOC_POSITION		0
#define LOC_NORMAL			2
#define LOC_COLOR			3
#define LOC_COLOR2			4
#define LOC_FOGCOORD		5
#define LOC_TANGENT			6
#define LOC_BINORMAL		7
#define LOC_TEXCOORD0		8
#define LOC_TEXCOORD1		9
#define LOC_TEXCOORD2		10
#define LOC_TEXCOORD3		11
#define LOC_TEXCOORD4		12
#define LOC_TEXCOORD5		13
#define LOC_TEXCOORD6		14
#define LOC_TEXCOORD7		15


const unsigned int		MATRIX_IDENTITY = 0x00000010;
const unsigned int		MATRIX_TRANSPOSE = 0x00000020;
const unsigned int		MATRIX_INVERSE = 0x00000040;
const unsigned int		MATRIX_INVERSETRANSPOSE = 0x00000080;

/*
struct ShaderSource
{
	const char*		vertexPath;
	const char*		vertexSource;
	const char*		geometryPath;
	const char*		geometrySource;
	const char*		fragPath;
	const char*		fragSource;
};

struct ColorOutput
{
	const char*		outputSemantic;
	uint32_t		outputLocation;
};

struct AttribLoc
{
	uint32_t	loc;
	const char* name;
};

struct ShaderImpl;

class QENGINE_API Shader
{
	public:

		Shader();
		~Shader();

		bool create(const char* aVertexFile, 
					const char* aFragFile, 
					const char* aGeomFile, 
					const ColorOutput* aColorOuts, 
					uint32_t aNColorOuts, 
					const AttribLoc* aTtribs, 
					uint32_t aNAttribs, 
					const char** aTransformFeedbackVaryings,
					const uint32_t aNTransformFeedbackVaryings) const;

		bool bind() const;
		void unbind() const;

		// bind by name //
		bool bindTexture(const char* aParamName, GLuint aId) const;
		bool bindTexture1D(const char* aParamName, GLuint aId) const;
		bool bindTextureBuffer(const char* aParamName, GLuint aId) const;
		bool bindCubemap(const char* aParamName, GLuint aId) const;
		void evictTextures() const;

		bool bindFloat(const char* aParamName, const float* aF, int aNFloats) const;
		bool bindBool(const char* aParamName, const bool& aB) const;
		bool bindFloat2Array(const char* aParamName, const vec2<float>* aV, int aNVecs) const;
		bool bindFloat3Array(const char* aParamName, const vec3<float>* aV, int aNVecs) const;
		bool bindFloat4Array(const char* aParamName, const vec4<float>* aV, int aNVecs) const;
		bool bindInt(const char* aParamName, const int* aI, int aNInts) const;
		bool bindStateMatrix(const char* aParamName, const MatrixMode aMatrix) const;
//		bool bindUniformMatrix4X4(const char* aParamName, float* aMat) const;
//		bool bindUniformMatrix3X3(const char* aParamName, float* aMat) const;
		bool bindUniformMatrix4X4(const char* aParamName, mat4<float>& aMat) const;
		bool bindUniformMatrix3X3(const char* aParamName, mat3<float>& aMat) const;


		// bind by id //
		bool bindTexture(GLint aBindPoint, GLuint aId) const;
		bool bindCubemap(GLint aBindPoint, GLuint aId) const;

		bool bindFloat(const GLint aId, const float* aF, int aNFloats) const;
	    bool bindBool(const GLint aId, const bool& aB) const;
		bool bindFloat2Array(const GLint aId, const vec2<float>* aV, const int aNVecs) const;
		bool bindFloat3Array(GLint aId, const vec3<float>* aV, const int aNVecs) const;
		bool bindFloat4Array(GLint aId, const vec4<float>* aV, const int aNVecs) const;
		bool bindInt(const GLint aId, const int* aI, const int aNInts) const;
//		bool bindStateMatrix(const GLint aId, const unsigned int aMatrix) const;
		bool bindUniformMatrix4X4(const GLint aId, const float* aMat) const;
		bool bindUniformMatrix3X3(GLint aId, const float* aMat) const;


		GLint getTexUnit(const char* aParamName) const;
		GLint getUniformLoc(const char* aParamName) const;

		const char* getVertexFilePath() const;
		const char* getFragFilePath() const;
		const char* getGeomFilePath() const;

		unsigned int getOpenGLId() const;

	private:

		void _hashSamplers() const;
		void _hashAttributes() const;
		void _hashUniforms() const;

		ShaderImpl* mImpl = nullptr;
};

*/


#include <string>
#include <cstdint>
#include <string>
#include "EngineExport.h"
#include "QMath.h"
#include "ShaderUniform.h"
#include "ShaderResource.h"

class Effect;
class Technique;
class Pass;

struct ShaderSourceRod
{
	const char* vertexPath;
	const char* vertexSource;
	const char* geometryPath;
	const char* geometrySource;
	const char* fragPath;
	const char* fragSource;
};

struct ShaderErrorInfo
{
	uint32_t shader;
	std::string message[5];
	uint32_t line[2];
};

class ShaderImpl;
class QENGINE_API Shader
{
public:
	Shader(const std::string& aName,
		const std::string& aVertexSource,
		const std::string& aFragmentSource,
		const std::string& aGeometrySource = "",
		const std::string& aTCSource = "",
		const std::string& aTESource = "");

	Shader(Technique* aTechnique,
		Pass* aPass,
		Effect* aParent,
		const std::string& aName,
		const std::string& aVertexSource,
		const std::string& aFragmentSource,
		const std::string& aGeometrySource = "",
		const std::string& aTCSource = "",
		const std::string& aTESource = "");

	~Shader();

	void bind() const;
	void unbind() const;

	void bindMaterials();

	void setVSSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot = 0);
	void setPSSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot = 0);
	void setGSSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot = 0);
	void setTESystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot = 0);
	void setTCSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot = 0);

	void setVSUserUniformBuffer(uint8_t* aData, const uint32_t aSize);
	void setPSUserUniformBuffer(uint8_t* aData, const uint32_t aSize);
	void setGSUserUniformBuffer(uint8_t* aData, const uint32_t aSize);
	void setTEUserUniformBuffer(uint8_t* aData, const uint32_t aSize);
	void setTCUserUniformBuffer(uint8_t* aData, const uint32_t aSize);

	const std::string& getName() const;

	void setUniform(const std::string& aName, uint8_t* aData) const;
	void resolveAndSetUniformField(const ShaderUniformDeclaration& aField, uint8_t* aData, const int32_t aOffset) const;

	void setUniform1b(const std::string& aName, const bool aValue) const;
	void setUniform1f(const std::string& aName, const float aValue) const;
	void setUniform1fv(const std::string& aName, float* aValue, const int32_t aCount) const;
	void setUniform1i(const std::string& aName, const int32_t aValue) const;
	void setUniform1iv(const std::string& aName, int32_t* aValue, const int32_t aCount) const;
	void setUniform2f(const std::string& aName, const vec2<float>& aValue) const;
	void setUniform2fv(const std::string& aName, const vec2<float>* aValues, const int32_t aCount) const;
	void setUniform3f(const std::string& aName, const vec3<float>& aValue) const;
	void setUniform4f(const std::string& aName, const vec4<float>& aValue) const;
	void setUniform4fv(const std::string& aName, const vec4<float>* aValues, const int32_t aCount) const;
	void setUniformMat3(const std::string& aName, const mat3<float>& aValue) const;
	void setUniformMat4(const std::string& aName, const mat4<float>& aValue) const;
	void setTexture(const std::string& aName, const uint32_t aTextureId) const;
	void setCubemapTexture(const std::string& aName, const uint32_t aTextureId) const;
	void setTextureBuffer(const std::string& aName, const uint32_t aTextureId) const;

	uint32_t getTextureUnit(const std::string& aName) const;
	bool hasTexture(const std::string& aName) const;

	uint32_t getUBOBindLocation(const std::string& uboName);

	int32_t getUniformBindLocation(const std::string& aUniformName);

	void setUniform1b(const uint32_t aLocation, const bool aValue) const;
	void setUniform1f(const uint32_t aLocation, const float aValue) const;
	void setUniform1fv(const uint32_t aLocation, float* aValue, const int32_t aCount) const;
	void setUniform1i(const uint32_t aLocation, const int32_t aValue) const;
	void setUniform1iv(const uint32_t aLocation, int32_t* aValue, const int32_t aCount) const;
	void setUniform2f(const uint32_t aLocation, const vec2<float>& aValue) const;
	void setUniform2fv(const uint32_t aLocation, const vec2<float>* aValues, const int32_t aCount) const;
	void setUniform3f(const uint32_t aLocation, const vec3<float>& aValue) const;
	void setUniform4f(const uint32_t aLocation, const vec4<float>& aValue) const;
	void setUniform4fv(const uint32_t aLocation, const vec4<float>* aValues, const int32_t aCount) const;
	void setUniformMat3(const uint32_t aLocation, const mat3<float>& aValue) const;
	void setUniformMat4(const uint32_t aLocation, const mat4<float>& aValue) const;

	std::vector<ShaderUniformBufferDeclaration*>& getVSSystemUniformBuffer() const;
	std::vector<ShaderUniformBufferDeclaration*>& getPSSystemUniformBuffer() const;
	std::vector<ShaderUniformBufferDeclaration*>& getGSSystemUniformBuffer() const;
	std::vector<ShaderUniformBufferDeclaration*>& getTESystemUniformBuffer() const;
	std::vector<ShaderUniformBufferDeclaration*>& getTCSystemUniformBuffer() const;

	ShaderUniformBufferDeclaration* getVSUserUniformBuffer() const;
	ShaderUniformBufferDeclaration* getPSUserUniformBuffer() const;
	ShaderUniformBufferDeclaration* getGSUserUniformBuffer() const;
	ShaderUniformBufferDeclaration* getTEUserUniformBuffer() const;
	ShaderUniformBufferDeclaration* getTCUserUniformBuffer() const;

	std::vector<ShaderResourceDeclaration*>& getResources() const;
	std::vector<ShaderStruct*> getStructs() const;

	std::vector<std::string> getUboNames() const;
	uint32_t getUBOBindPoint(const std::string& aName);

	uint32_t getProgramID();

	//		static Shader* sCurrentlyBound;

private:
	void _init();
	void _shutdown() const;

	void _parseSamplers();


	uint32_t _compile(std::string** aShaders, ShaderErrorInfo& aInfo) const;

	void _parse(const std::string& aVertexSource, const std::string& aFragmentSource, const std::string& aGeometrySource, const std::string& aTCSource, const std::string& aTESource) const;
	void _parseUniform(const std::string& aStatement, uint32_t aShaderType) const;
	void _parseUniformStruct(const std::string& aBlock, uint32_t aShaderType) const;

	ShaderStruct* _findStruct(const std::string& aName) const;

	bool _isTypeStringResource(const std::string& aType) const;

	void _resolveUniforms();
	void _validateUniforms() const;

	bool _isSystemUniform(ShaderUniformDeclaration* aUniform) const;
	int32_t _getUniformLocation(const std::string& aName) const;

	ShaderUniformDeclaration* _findUniformDeclaration(const std::string& aName, const ShaderUniformBufferDeclaration* aBuffer) const;
	ShaderUniformDeclaration* _findUniformDeclaration(const std::string& aName) const;

	void _resolveAndSetUniform(ShaderUniformDeclaration* aUniform, uint8_t* aData, const uint32_t aSize) const;
	void _resolveAndSetUniforms(ShaderUniformBufferDeclaration* aBuffer, uint8_t* aData, const uint32_t aSize) const;

	void _setUniformStruct(ShaderUniformDeclaration* aUniform, uint8_t* aData, uint32_t aOffset) const;

	ShaderImpl* mImpl;
};

#endif 

