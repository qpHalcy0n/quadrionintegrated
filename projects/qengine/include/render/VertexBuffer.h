#ifndef vertexbuffer_h__
#define vertexbuffer_h__

#include "render/VertexBufferLayout.h"
#include "EngineExport.h"

class QENGINE_API VertexBuffer
{
	public:
		VertexBuffer(const void* aData, const size_t aSize);
		~VertexBuffer();

		void setLayout(const VertexBufferLayout& aLayout);
		inline const VertexBufferLayout& getLayout() const { return mLayout; }

		void bind() const;
		void unbind() const;

	private:
		size_t mSize;
		uint32_t mId;

		VertexBufferLayout mLayout;
};

#endif // core_vertexbuffer_h__