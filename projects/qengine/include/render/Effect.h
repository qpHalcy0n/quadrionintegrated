
#ifndef effectrod_h__
#define effectrod_h__

#include <map>
#include <string>
#include <vector>
#include <utility>
#include "EngineExport.h"
#include "Technique.h"
#include "render/effectparser/EffectOutputBuffer.h"
#include "render/effectparser/EffectDepthBuffer.h"

struct OutputBufferInfo
{
	std::string name;
	uint32_t colorBuffers;
	uint32_t width;
	uint32_t height;

	ETexturePixelFormat colorFormat;
	ETexturePixelFormat depthFormat;
	std::string depthBufferName;

	uint32_t flags;
};

//class EffectRodImpl;
class QENGINE_API Effect
{
public:

	Effect();
	~Effect();

	size_t getNumTechniques() const;
	std::vector<Technique*>& getTechniques();
	Technique* getTechnique(const char* aName) const;
	void addTechnique(Technique* tech);

	void reset();

	//		void addMaterialToEntity(const std::string& aTechName, QHandle<QEntity> aEntity);

	const char* getName() const;
	void setName(const std::string& aName);

	void setSourceCopy(const std::string& aSource);
	std::string getSourceCopy() const;
	std::vector<std::pair<std::string, std::string>> getIncludes();

	std::string getUniformGUIDebugElementType(const std::string& aUniformName);
	void setUniformGUIDebugElementType(const std::string& aUniformName, const std::string& aGUIElementType);

	std::pair<float, float> getUniformGUIDebugElementData(const std::string& aUniformName);
	void setUniformGUIDebugElementData(const std::string& aUniformName, std::pair<float, float> aData);

	float getUniformElementDefaultValue(const std::string& aUniformName);
	void setUniformElementDefaultValue(const std::string& aUniformName, float aData);

	void addUniformName(const std::string& aName);
	void addGUIUniformName(const std::string& aName);

	std::vector<std::string> getUniformNames() const;
	std::vector<std::string> getGUIUniformNames() const;

	void setOutputBuffers(const std::vector<OutputBufferInfo*>& aBuffers);
	std::vector<OutputBufferInfo*> getOutputBuffers() const;

	void setDepthBuffers(const std::vector<EffectDepthBuffer>& aBuffers);
	std::vector<EffectDepthBuffer> getDepthBuffers() const;

	//		static Effect* current;

private:

	std::map<std::string, std::pair<float, float>> mUniformGUIData;
	std::vector<Technique*> techniques;
	std::vector<std::pair<std::string, std::string>> includes;

	std::vector<OutputBufferInfo*> _outputBuffers;
	std::vector<EffectDepthBuffer> _depthBuffers;

	std::map<std::string, std::string> mUniformGUIElementType;

	std::vector<std::string> mUniformNames;
	std::vector<std::string> mGUIUniformNames;
	std::map<std::string, float> mGUIUniformDefaults;


	std::string name;
	std::string source;
	std::string sourceCopy;

	//		void parseEffect(const std::vector<std::string>& aSourceLines);

	//		QHandle<QEntityManager> entityManager;

	//		Effect* effect;	

	//		EffectImpl* mImpl;
};

#endif // effectrod_h__
