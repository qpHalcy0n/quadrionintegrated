#ifndef BUFFER_H__
#define BUFFER_H__


#include "EngineExport.h"
#include "Extensions.h"
#include "QMath.h"


#define BUFFER_STATIC		0x00000001
#define BUFFER_DYNAMIC		0x00000002

// Use the below VERTEX_ATTRIB_XXX for the "loc" parameter in vertex_attribute //
// All except for "POSITION" can be used generically. //
#define VERTEX_ATTRIB_POSITION		0x00000000
#define VERTEX_ATTRIB_NORMAL		0x00000002
#define VERTEX_ATTRIB_COLOR			0x00000003
#define VERTEX_ATTRIB_COLOR2		0x00000004
#define VERTEX_ATTRIB_FOGCOORD		0x00000005
#define VERTEX_ATTRIB_TANGENT		0x00000006
#define VERTEX_ATTRIB_BINORMAL		0x00000007
#define VERTEX_ATTRIB_TEXCOORD0		0x00000008
#define VERTEX_ATTRIB_TEXCOORD1		0x00000009
#define VERTEX_ATTRIB_TEXCOORD2		0x0000000A
#define VERTEX_ATTRIB_TEXCOORD3		0x0000000B
#define VERTEX_ATTRIB_TEXCOORD4		0x0000000C
#define VERTEX_ATTRIB_TEXCOORD5		0x0000000D
#define VERTEX_ATTRIB_TEXCOORD6		0x0000000E
#define VERTEX_ATTRIB_TEXCOORD7		0x0000000F


struct VertexAttribute
{
	GLuint			loc;
	GLint			size;			// Number of floats 1, 2, 3, 4
	GLenum			type;			
	GLboolean		normalized;
	GLsizei			stride;			
	uint32_t		dataOffset;

	void operator= (const VertexAttribute& aRhs)
	{
		loc = aRhs.loc;
		size = aRhs.size;
		type = aRhs.type;
		normalized = aRhs.normalized;
		stride = aRhs.stride;
		dataOffset = aRhs.dataOffset;
	}
};

struct GeometryFace
{
	uint32_t numIndices;
	uint32_t* indices;
};

struct GeometryMesh
{
	uint32_t numVertices;
	uint32_t numNormals;
	uint32_t numTangents;

	uint32_t numFaces;

	vec3<float>* vertices;
	vec3<float>* normals;
	vec3<float>* tangents;
	vec3<float>** colors;
	vec3<float>** texCoords;

	VertexAttribute* vertAttribs;	// Must be copied explicitly!!
	uint32_t	nVertAttribs;		// Must be copied explicitly!!

	GeometryFace* faces;

	uint32_t numTexCoordsPerVertex;
	uint32_t numIndices;
	uint32_t* getIndices() const
	{
		uint32_t * indices = new uint32_t[numIndices];

		uint32_t index = 0;

		for (uint32_t i = 0; i < numFaces; ++i)
		{
			for (uint32_t j = 0; j < faces[i].numIndices; ++j)
			{
				indices[index] = faces[i].indices[j];
				index++;
			}
		}

		return indices;
	}

	uint32_t geometryBufferId;	//	TODO: Deprecate
};

struct GeometryNode
{
	uint32_t* meshes;
	uint32_t numMeshes;

	GeometryNode* nodes;
	GeometryNode* parentNode;

	uint32_t numChildren;

	mat4<float> transformation;
};

struct GeometryBufferObject
{
	uint32_t			numMeshes;
	GeometryMesh*		meshes;
	GeometryNode*		rootNode;

	vec3<float> scale;
	vec3<float> modelCenter;
};





// Each buffer must have exactly one attribute //
// (VVVV)(CCCC)(NNNN)  <=== this style         //
QENGINE_API GLuint createArrays(const VertexAttribute* aAttribs, 
								const void** aBufs, 
								const uint32_t aSizeOfVert, 
								const uint32_t aNBuffers, 
								const uint32_t aDynamic);

// Makes a single buffer, but the buffer is interleaved with data //
// (VCN)(VCN)(VCN) <=== this style								  //
QENGINE_API GLuint createInterleavedArray(const VertexAttribute* aAttribs, 
										  const uint32_t aNumAttribs, 
										  const void* aBuf, 
										  const uint32_t aSizeOfVert, 
										  const uint32_t aDynamic);

// Identical to createArrays except it is indexed //
QENGINE_API GLuint createIndexedArrays(const VertexAttribute* aAttribs, 
									   const void** aBufs, 
									   const uint32_t aSizeOfVert, 
									   const uint32_t aNumBuffers, 
									   const uint32_t aDynamic, 
									   const uint32_t* aIndices, 
									   const uint32_t aNumIndices);

QENGINE_API GLuint createIndexedArray(const VertexAttribute aAttribs, 
									  const void* aBuf, 
									  const uint32_t aSizeOfVert, 
									  const uint32_t aDynamic, 
									  const uint32_t* aIndices, 
									  const uint32_t aNumIndices);

// Identical to createInterleavedArray except it is indexed //
QENGINE_API GLuint createIndexedInterleavedArray(const VertexAttribute* aAttribs, 
											     const uint32_t aNumAttribs, 
												 const void* aBuf, 
												 const uint32_t aSizeOfVert, 
												 const uint32_t aDynamic, 
												 const uint32_t* aIndices, 
												 const uint32_t aNumIndices);


QENGINE_API void bindBuffer(GLuint aId);
QENGINE_API void bindDefaultBuffer();
QENGINE_API void deleteBuffer(GLuint aId);




#endif