#ifndef shaderstruct_h__
#define shaderstruct_h__

#include <cstdint>
#include <string>
#include <vector>
#include "EngineExport.h"

class ShaderUniformDeclaration;
class ShaderStructImpl;
class QENGINE_API ShaderStruct
{
	public:
		explicit ShaderStruct(const std::string& aName);
		~ShaderStruct();

		void addField(ShaderUniformDeclaration* aField);
		inline void setOffset(const uint32_t aOffset) { mOffset = aOffset; }

		inline const char* getName() const { return mName; }
		inline uint32_t getSize() const { return mSize; }
		inline uint32_t getOffset() const { return mOffset; }
		const std::vector<ShaderUniformDeclaration*>& getFields() const;

	private:
		ShaderStructImpl* mImpl;

		char* mName;
		uint32_t mSize;
		uint32_t mOffset;
};

#endif // shaderstruct_h__