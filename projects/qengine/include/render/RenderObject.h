
#ifndef __RENDEROBJECT_H_
#define __RENDEROBJECT_H_

#include <memory>
#include "EngineExport.h"
#include "QMath.h"
#include "assets/MeshAsset.h"
#include "InputManager.h"


class QENGINE_API RenderObject : public InputListener
{
    public:

        RenderObject();
        RenderObject(const char* aMeshAssetName, const char* aName, uint32_t aId);
		RenderObject(const char* aName, std::vector<std::shared_ptr<MeshNode>> aMeshes, uint32_t aId);
        ~RenderObject();

        void setWorldTransform(const mat4<float>& aMatrix);
        mat4<float> getWorldTransform();

		std::vector<std::shared_ptr<MeshNode>>& getMeshes();

        const char* getName();

		void setGuiSelected(bool aIsSelected);
		bool isGuiSelected();

		uint32_t getID();

        // From InputListener
        void onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods);
        
    private:

		uint32_t mId;
        const char* mName;
        mat4<float> mWorldTransform;
		std::vector<std::shared_ptr<MeshNode>> mMeshes;

		bool mIsGuiSelected;
        
};


#endif