#ifndef _material_h__
#define _material_h__

#include <cstdint>
#include <unordered_map>
#include "Shader.h"
#include "core/QLog.h"
#include "render/Effect.h"
#include "EngineExport.h"


#define BIT(x) (1 << x)

class QENGINE_API Material
{
	public:
		Material();
		~Material()
		{
			int jonathan = 0;
		}

		void setName(const std::string& aName);
		void setSize(uint32_t aSizeInBytes);
		uint32_t getSize();
		void setUniform(const std::string& aName, const void* aData);
		void addUniforms(const std::vector<UniformBufferElement*>& aUniforms);
		UniformBufferElement* const getUniform(const std::string& aName);
		std::string getName();
		uint32_t getUBOHandle();
		bool isDirty();
		void bind(Shader* aShader);
		void unbind();

		void copy(const void* aData);

		size_t getUniformOffset(const std::string& aName);
		size_t getUniformSize(const std::string& aName);

	private:

		uint32_t mSizeInBytes;

		std::string mName;
		void* mMemory;
		uint32_t mUBOHandle;
		std::unordered_map<std::string, UniformBufferElement*> mUniforms;

		bool	mIsDirty;
};

class QENGINE_API ObjectMaterial
{
	public:
		ObjectMaterial(Effect* aEffect, const std::string& aTechName, const std::string& aPassName);
		~ObjectMaterial();

		void bind();
		void unbind();

		Effect* getEffect() const;
		Pass* getPass() const;
		const char* getPassName();

		const char* getName();
		const char* getTechniqueName();

		void setUniform1b(const std::string& aName, bool aValue);
		void setUniform1f(const std::string& aName, float aValue);
		void setUniform1fv(const std::string& aName, float* aValue, const int32_t aCount);
		void setUniform1i(const std::string& aName, int32_t aValue);
		void setUniform1iv(const std::string& aName, int32_t* aValue, const int32_t aCount);
		void setUniform2f(const std::string& aName, vec2<float>& aValue);
		void setUniform2fv(const std::string& aName, vec2<float>* aValues, const int32_t aCount);
		void setUniform3f(const std::string& aName, vec3<float>& aValue);
		void setUniform4f(const std::string& aName, vec4<float>& aValue);
		void setUniform4fv(const std::string& aName, vec4<float>* aValues, const int32_t aCount);
		void setUniformMat3(const std::string& aName, mat3<float>& aValue);
		void setUniformMat4(const std::string& aName, mat4<float>& aValue);
		void setTexture(const std::string& aName, uint32_t aTextureId);
		void setCubemapTexture(const std::string& aName, uint32_t aTextureId);
		void setTextureBuffer(const std::string& aName, uint32_t aTextureId);

	private:
		Effect* mEffect;
		Pass* mPass;


		std::string mTechName;
		std::string mPassName;

		enum class EShaderDataType
		{
			QBool,
			Float,
			Int,
			Vec2,
			Vec3,
			Vec4,
			Mat3,
			Mat4,
			Texture,
			Cubemap,
			TextureBuffer
		};

		struct MaterialData
		{
			int32_t count;
			EShaderDataType type;
			bool b;
			float f;
			float* fv;
			int32_t i;
			int32_t* iv;
			vec2<float> v2;
			vec3<float> v3;
			vec4<float> v4;
			vec2<float>* v2f;
			vec4<float>* v4f;
			mat3<float> m3;
			mat4<float> m4;
		};

		std::map<std::string, MaterialData*> mMaterialData;
};

#endif // material_h__