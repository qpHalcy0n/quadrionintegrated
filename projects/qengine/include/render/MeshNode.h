#ifndef __meshnode_h_
#define __meshnode_h_

#include <memory>
#include "QMath.h"
#include "render/MeshNodeMaterial.h"
#include "render/Mesh.h"

struct MeshNode
{
	mat4<float> transform;
	std::shared_ptr<Mesh> mesh = nullptr;
	std::string name;
	MeshNodeMaterial meshMaterial;
	vec3<float> scale, position;
	mat4<float> rotation;
	vec3<float> center;
};

#endif