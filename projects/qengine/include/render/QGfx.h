
#ifndef __QGFX_H_
#define __QGFX_H_

#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <queue>
#include "render/Effect.h"
#include "Extensions.h"
#include "EngineExport.h"
#include "QMath.h"
#include "render/Image.h"
#include "render/RenderObject.h"
#include "render/Material.h"
#include "assets/MeshAsset.h"
#include "render/Pass.h"
#include "render/MeshNode.h"
#include "entities/Entity.h"

enum CompareFunction
{
	NEVER		= 0x0200,
	LESS		= 0x0201,
	EQUAL		= 0x0202,
	LEQUAL		= 0x0203,
	GREATER		= 0x0204,
	NOTEQUAL	= 0x0205,
	GEQUAL		= 0x0206,
	ALWAYS		= 0x0207,
};

enum MatrixMode : int
{
	NONE				= 0,
	MODEL				= 1,
	VIEW				= 2,
	PROJECTION			= 4,
	TRANSPOSE			= 8,
	INVERSE				= 16,
	INVERSE_TRANSPOSE	= 32, 
};

static inline MatrixMode operator| (MatrixMode lhs, MatrixMode rhs)
{
	return static_cast<MatrixMode>(static_cast<int>(lhs) | static_cast<int>(rhs));
}

static inline MatrixMode operator& (MatrixMode lhs, MatrixMode rhs)
{
	return static_cast<MatrixMode>(static_cast<int>(lhs) & static_cast<int>(rhs));
}

enum CullMode
{
	FRONT			= 0x0404,
	BACK			= 0x0405,
	FRONT_AND_BACK	= 0x0408,
};

enum FrontFace
{
	CW		= 0x0900,
	CCW		= 0x0901, 
};

struct QENGINE_API QGfxImpl;

class QENGINE_API QGfx
{
	public:

		QGfx();
		~QGfx();

		static bool initializeGraphics();
		static bool setDefaultState();


		// Z-State //
		static void enableDepthTest();
		static void disableDepthTest();
		static void changeDepthTest(const CompareFunction aFunc);
		static void enableDepthWrite();
		static void disableDepthWrite();
		static bool isDepthTestEnabled();
		static bool isDepthWriteEnabled();

		// Matrix State //
		static bool setMatrix(MatrixMode aMode, const mat4<float>& aMat);
		static bool pushMatrix(MatrixMode aMode, const mat4<float>& aMat);
		static bool popMatrix(MatrixMode aMode);
		static mat4<float> getMatrix(MatrixMode aMode);
		static bool areMatricesDirty();

		// Culling State //
		static void enableBackfaceCulling();
		static void disableBackfaceCulling();
		static void changeFrontFaceWinding(FrontFace aFace);
		static void changeCullMode(CullMode aMode);

		// Raster State //
		static void enableWireframe();
		static void disableWireframe();

		// Texture state //
		static void addTexture(std::string aTag, GLuint aId);
		static GLuint getTexture(std::string aTag);

		// Shadowmap Management //
		static void addShadowmap(std::string aTag, ShadowmapId aId);
		static void addShadowmap(std::string aTag, MultiShadowmapId aId);
		static void bindShadowmap(const std::string& aTag);

		// Framebuffer state //
		static void addFramebuffer(std::string aTag, FboId aBufferId);
		static void createRenderBuffer(OutputBufferInfo* aObi);
		static FboId getRenderBuffer(const std::string& aName);
		static GLuint getRenderTarget(const std::string& aName);
		static std::vector<std::string> getRenderTargetNames();
		static void setBufferClearFlag(const std::string& aName, bool aClear);
		static void resetBufferClearFlags();
		static void bindRenderTarget(OutputBufferInfo* aObi);
		static uint32_t createRenderObjectID();
		static size_t getNumRenderTargets();
		


		static vec4<int32_t> getDefaultViewportDims();

		// Material state
//		static bool addObjectMaterial(Effect* aEffect, const std::string& aTechName, const std::string& aPassName, const char* aName);
		static bool addObjectMaterial(Effect* aEffect, const std::string& aTechPassName);
		static bool addRenderableToMaterial(const char* aMaterialName, 
											std::shared_ptr<MeshAsset> aMeshAsset, 
											mat4<float> a = mat4<float>());
		static bool addRenderableToMaterial(const char* aMaterialName, std::shared_ptr<RenderObject> aRenderObj);
		static ObjectMaterial* getObjectMaterial(const char* aName);

		static std::map<std::string, std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>>>& getObjectMaterialMap()
		{
			return mObjectMaterialMap;
		}

//		static std::pair<ObjectMaterial*, std::vector<RenderObject*>> getRenderablesFromMaterial(const char* aMaterialName);
		static std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>> getRenderablesFromMaterial(const char* aMaterialName);
		static std::vector<ObjectMaterial*> getObjectMaterialsFromEffect(const char* aEffectName);
		static std::vector<ObjectMaterial*>& getObjectMaterials();
		static std::shared_ptr<RenderObject> getRenderObject(const uint32_t aId);
		static ObjectMaterial* getObjectMaterial(const uint32_t aId);

		// render queue
		static void pushToRenderQueue(const std::string& aMaterialName);
		static void pushToRenderQueueNew(const std::string& aMaterialName);
		static std::queue<std::string>& getRenderQueue();
		static std::queue<std::string>& getRenderQueueNew();


		static void addEntityToTechniqueMap(Entity* aEntity, std::string aTechName);
		static std::vector<Entity*> getEntitiesByTechnique(const std::string& aTechName);

		static void beginRenderFrame();
		static void endRenderFrame();
		static void clearBuffer(uint32_t aId, vec4<float> aClearColor);

		class QENGINE_API GL
		{
			public:
				static void gl_GenBuffers(GLsizei aN, GLuint* aBuffers);

				static void gl_BindBuffer(GLenum aTarget, GLuint aBuffer);

				static void gl_BindBufferBase(GLenum aTarget, 
											  GLuint aIndex, 
											  GLuint aBuffer);

				static void gl_BufferData(GLenum aTarget,
										  GLsizeiptr aSize,
										  const GLvoid* aData,
										  GLenum aUsage);

				static void* gl_MapBufferRange(GLenum aTarget,
											   GLintptr aOffset,
											   GLsizeiptr aLength,
											   GLbitfield aAccess);

				static GLboolean gl_UnmapBuffer(GLenum aTarget);

				static void		 gl_DeleteBuffers(GLsizei aN, 
												  const GLuint* aBuffers);


				static void gl_GenVertexArrays(GLsizei aN, GLuint* aArrays);

				static void gl_BindVertexArray(GLuint aArray);

				static void gl_EnableVertexAttribArray(GLuint aIndex);

				static void gl_VertexAttribPointer(GLuint aIndex,
												   GLint aSize,
												   GLenum aType,
												   GLboolean aNormalized,
												   GLsizei aStride,
												   const GLvoid* aPointer);

				static void gl_DeleteVertexArrays(GLsizei aN, const GLuint* aRrays);

				static void gl_GenTextures(GLsizei aN, GLuint* aTextures);

				static void gl_BindTexture(GLenum aTarget, GLuint aTexture);

				static void gl_TexBuffer(GLenum aTarget, 
										 GLenum aInternalFormat, 
										 GLuint aBuffer);

				static void gl_GenQueries(GLsizei aN, GLuint* aIds);

				static void gl_DeleteQueries(GLsizei aN, const GLuint* aIds);

				static void gl_BeginQuery(GLenum aTarget, GLuint aId);

				static void gl_EndQuery(GLenum aTarget);

				static void gl_GetQueryObjectuiv(GLuint aId, 
												 GLenum aPname, 
												 GLuint* aParams);

				static void gl_BeginTransformFeedback(GLenum aPrimitiveMode);

				static void gl_EndTransformFeedback();

				static void gl_DrawArrays(GLenum aMode, 
										  GLint aFirst, 
										  GLsizei aCount);

				static void gl_DrawElementsInstanced(GLenum aMode,
													 GLsizei aCount,
													 GLenum aType,
													 const void* aIndices,
													 GLsizei aPrimcount);

				static void gl_DrawElements(GLenum mode,
											GLsizei count,
											GLenum type,
											const GLvoid* indices);

				static void gl_StencilOpSeparate(GLenum aFace, 
												 GLenum aSfail, 
												 GLenum aDpfail, 
												 GLenum aDppass);

				static void gl_BlendEquation(GLenum aMode);

				static void gl_ActiveTexture(GLenum aSlot);

				static void gl_BindFramebuffer(GLenum aTarget, GLuint aFramebuffer);

				static void gl_FramebufferTexture2D(GLenum aTarget,
													GLenum aTtachment,
													GLenum aTextarget,
													GLuint aTexture,
													GLint aLevel);

				static void gl_GenFramebuffers(GLsizei aN, GLuint* aIds);

				static void gl_BlitFramebuffer(GLint aSrcX0,
											   GLint aSrcY0,
											   GLint aSrcX1,
											   GLint aSrcY1,
											   GLint aDstX0,
											   GLint aDstY0,
											   GLint aDstX1,
											   GLint aDstY1,
											   GLbitfield aMask,
											   GLenum aFilter);

				static void gl_DrawBuffers(GLsizei aN, const GLenum* aBufs);

				static GLenum gl_CheckFramebufferStatus(GLenum aTarget);

				static void gl_Enablei(GLenum aCap, GLuint aIndex);

				static void gl_Disablei(GLenum aCap, GLuint aIndex);

				static void gl_BlendFunci(GLuint aBuf, 
										  GLenum aSfactor, 
										  GLenum aDfactor);

				static void gl_ClearBufferiv(GLenum aBuffer, 
											 GLint aDrawbuffer, 
											 const GLint* aValue);

				static void gl_ClearBufferuiv(GLenum aBuffer, 
											  GLint aDrawbuffer, 
											  const GLuint* aValue);

				static void gl_ClearBufferfv(GLenum aBuffer, 
											 GLint aDrawbuffer, 
											 const GLfloat* aValue);

				static void gl_ClearBufferfi(GLenum aBuffer, 
											 GLint aDrawbuffer, 
											 GLfloat aDepth, 
											 GLint aStencil);

				static void gl_GetIntegerv(GLenum pName, GLint* params);

				static void gl_ReadPixels(GLint x, 
										  GLint y,
										  GLsizei width,
										  GLsizei height,
										  GLenum format,
										  GLenum type,
										  GLvoid* data);

				static void gl_ReadBuffer(GLenum mode);

				static void gl_PatchParameteri(GLenum pname, GLint value);
		};

	private:

		// map to a pair of object materials to a list of strings->asset manager mesh assets
//		static std::map<std::string, std::pair<ObjectMaterial*, std::vector<RenderObject*>>> mObjectMaterialMap;
		static std::map<std::string, std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>>> mObjectMaterialMap; 
		static std::map<std::string, std::vector<ObjectMaterial*>> mEffectMaterialMap;
		static std::vector<ObjectMaterial*> mObjectMaterials;
		static std::map<std::string, DepthStencilId> mDSBufferMap;
		static std::map<std::string, FboId> mFBOMap;
		static std::map<std::string, GLuint> mRTMap;
		static std::map<uint32_t, std::shared_ptr<RenderObject>> mRenderObjectIDMap;
		static std::map<uint32_t, ObjectMaterial*> mRenderObjectMaterialMap;
		static std::queue<std::string> mRenderQueue;
		static std::queue<std::string> mRenderQueueNew;
		static std::map<std::string, std::vector<Entity*>> mEntityTechniqueMap;
		static std::vector<uint32_t> mClearedBuffers;

		static uint32_t mNumRenderObjects;
		static uint32_t mCurrentFramebuffer;
		static vec4<int32_t> mDefaultViewport;

		static CompareFunction currentDepthCompare;

		static QGfxImpl* mImpl;
};



#endif