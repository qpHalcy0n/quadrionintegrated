////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// HDRPIPELINE.H
//
// Written by Shawn Simonson for Quadrion Engine 05/2008
//
// This file implements a basic HDR pipeline in a manner that is very easy for the client to
// work with and implement. It involves initializing the HDR pipeline, giving it a base texture
// from which to send down the pipeline and finally presents the HDR asset to the backbuffer
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "QMath.h"
#include "core/Timer.h"
#include "render/Texture.h"
#include "render/Shader.h"
#include "render/Render.h"




#ifndef __QHDR_H_
#define __QHDR_H_

/*


////////////////////////////////////////////////////////////////////////////
// cHDRPipeline
// PRIMARY point of interaction for HDR rendering
class QENGINE_API CHDRPipeline
{

	public:

		CHDRPipeline();
		CHDRPipeline(unsigned int w, unsigned int h);
		~CHDRPipeline();
	
		// initHDRPipeline- must be called prior to any subsequent operation //
		bool		initialize();
	
		// explicit destructor //
		void		destroy();
	
	
		// setSceneTexture- must be called prior to pipeline rendering //
		// Present the pipeline with the full scene rendered to a floating point render target //
		// So the handle presented must be in a FP16 format //
		const inline void		SetDepthTarget(const int& depthid, const int& stencilid) { m_depthTarget = depthid; m_stencilTarget = stencilid; }
	
		// Run the pipeline and present the HDR asset to the backbuffer //
		void			render(fbo_id dest);
	
	
		// is the pipeline initialized? //
		const inline bool			IsInitialized() { return m_isInitialized; }
		const inline bool			IsEncoded() { return m_bUsingLogLuv; }
		const inline double			GetDeltaTime() { return m_deltaTime; }
		const inline fbo_id			GetHDRSurface() { return m_sceneHDRFBO; }
		const inline GLuint			GetHDRTexture() { return m_intermediateBloom; }
	
		const inline void			SetMiddleGrey(const float& mg) { m_middleGrey = mg; }
		const inline void			SetBloomScale(const float& sc) { m_bloomScale = sc; }
		const inline void			SetBrightnessThreshold(const float& thresh) { m_brightnessThreshold = thresh; }
		const inline void			SetBrightnessOffset(const float& offset) { m_brightnessOffset = offset; }
		const inline void			SetAdaptationFactor(const float& adap) { m_adaptationFactor = adap; }
		const inline void			SetFinalGrey(const float& fg) { m_finalGrey = fg; }
		const inline void			SetFramebufferWidth(const uint32_t w) { m_framebufferWidth = w; }
		const inline void			SetFramebufferHeight(const uint32_t h) { m_framebufferHeight = h; }
	
		inline CQuadrionEffect* GetGaussBlurEffect() { return m_pGaussEffect; }

	protected:

	private:

//		void			ClearTargets();
		void			MeasureLuminance();				// measure overall luminance
		void			BrightPass();					// perform full scene bright pass	
		void			ApplyBloom();					// _create bloom source
		void			RenderBloom();					// render bloom source
//		void			CalculateAdaptation();			// calculate light adaptation
	
		float			m_middleGrey;
		float			m_bloomScale;
		float			m_brightnessThreshold;
		float			m_brightnessOffset;
		float			m_adaptationFactor;
		float			m_finalGrey;

		float			m_whitePoint;
		float			m_exposure;
	
		// TEXTURE assets //
		GLuint			m_depthTarget;
		GLuint			m_stencilTarget;
		GLuint			m_sceneHDR;							// full resolution floating point texture of the scene
		fbo_id			m_sceneHDRFBO;
	
		GLuint			m_scaledHDRScene;					// scaled floating point texture of the scene
		fbo_id			m_scaledHDRSceneFBO;
	
		GLuint			m_luminance[4];						// progressively scaled array of average luminance textures
		fbo_id			m_luminanceFBO[4];
	
		GLuint			m_brightPass;						// overall bright pass texture
		fbo_id			m_brightPassFBO;
	
		GLuint			m_bloom;							// bloom source texture
		fbo_id			m_bloomFBO;
	
		GLuint			m_tempBloom[4];						// vertical and horizontal +/- bloom textures to calculate final bloom
		fbo_id			m_tempBloomFBO[4];
	
		GLuint			m_lumAdaptCur;						// adapted current luminance
		fbo_id			m_lumAdaptCurFBO;
	
		GLuint			m_lumAdaptLast;						// adapted last luminance
		fbo_id			m_lumAdaptLastFBO;
	
		GLuint			m_intermediateBloom;				// temp bloom texture
		fbo_id			m_intermediateBloomFBO;
	
		CQuadrionEffect* m_pCalcAdaptEffect;
		CQuadrionEffect* m_pSampleLumEffect;
		CQuadrionEffect* m_pResampleLumEffect;
		CQuadrionEffect* m_pFinalLumEffect;
		CQuadrionEffect* m_pBrightPassEffect;
		CQuadrionEffect* m_pGaussEffect;
		CQuadrionEffect* m_pDownScale2;
		CQuadrionEffect* m_pBloomEffect;
		CQuadrionEffect* m_pDownScale4;
		CQuadrionEffect* m_pFinalEffect;
		CQuadrionEffect* m_pReinhardEffect;
	
		//		CQuadrionEffect* m_pEffect;
		//		INT			m_effect;							// effect handle
	
		unsigned int	m_framebufferWidth;
		unsigned int	m_framebufferHeight;
		unsigned int	m_cropWidth;						// texture scale factor in U
		unsigned int	m_cropHeight;						// texture scale factor in V
	
		bool			m_bUsingLogLuv;
		bool			m_isInitialized;				// is the pipeline initialized?
														//		BOOL			m_isFPSupported;				// are floating point render targets supported?
	
		unsigned int			m_luminanceFormat;
		//		ETexturePixelFormat		m_luminanceFormat;			// greatest luminance format  I16F or I32F
		//		ETexturePixelFormat		m_floatingPointFormat;		// greatest floating point texture format
	
		CTimer		m_adaptedTimer;						// timer for adaptive luminance calculation
		double		m_deltaTime;						// change in time from last frame

};

*/



#endif