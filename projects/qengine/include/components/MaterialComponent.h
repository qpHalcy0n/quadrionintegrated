#ifndef __materialcomponent_h_
#define __materialcomponent_h_

#include <string>
#include "EngineExport.h"
#include "components/Component.h"
#include "render/Effect.h"

class QENGINE_API MaterialComponent : public Component 
{
    public:

        MaterialComponent();
        MaterialComponent(Effect* aEffect, const std::string& aTechName); 

        std::string     GetTechniqueName();
        std::string     GetEffectName();
        Technique*      GetTechnique();
    
        uint32_t        GetNumPasses();
        bool            Bind(uint32_t aPass);   
    
        std::string     GetPassNameByIndex(uint32_t aIndex); 

    private:

        Effect*             _effect;
        Technique*          _technique;
        std::string         _techniqueName; 
};



#endif