#ifndef __componentrod_h_
#define __componentrod_h_

#include <cstddef>
#include <type_traits>
#include <typeindex>
#include "InputManager.h"
#include "EngineExport.h"

class Entity;
class QENGINE_API Component : public InputListener
{
	public:
		Component() = default;
		~Component() = default;
	
		virtual void OnEnable() {};
		virtual void OnConstruct() {};
		virtual void OnStart() {};
	
		virtual void OnTick() {};
		virtual void OnUpdate(){};
		virtual void OnLateUpdate() {};
	
		virtual void OnPreRender() {};
		virtual void OnRender() {};
		virtual void OnPostRender() {};
	
		virtual void OnDestroy() {};
	
		Entity* gameObject;

		template <typename T>
		static void destroy(T*& aComponent);
		
		// Just for debugging //
		const char* componentTypeName;

		// Actually used for type evaluation //
		std::size_t		componentTypeHash;
};

template <typename T>
void Component::destroy(T*& aComponent)
{
	static_assert(std::is_base_of<Component, T>::value, "T is not derived from Component");

	aComponent->gameObject->removeComponent(aComponent);

	// This devilish syntax is required if the template specialization appears
	// after the -> or . operator
	aComponent->gameObject->getScenePtr()->template destroyComponent<T>(aComponent);
}



#endif