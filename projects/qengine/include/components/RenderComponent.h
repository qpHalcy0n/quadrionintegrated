#ifndef __rendercomponent_h_
#define __rendercomponent_h_

#include <memory>
#include <vector>
#include "assets/MeshAsset.h"
#include "components/Component.h"
#include "EngineExport.h"

class QENGINE_API RenderComponent : public Component
{
    public:

        RenderComponent() = default;
        RenderComponent(std::vector<std::shared_ptr<MeshNode>> aMeshes);

        std::vector<std::shared_ptr<MeshNode>>& GetMeshes();

    private:

        std::vector<std::shared_ptr<MeshNode>> _meshes;

        uint32_t    _numMeshes;
};


#endif