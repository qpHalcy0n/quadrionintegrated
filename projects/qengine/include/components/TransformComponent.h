#ifndef __transformcomponent_h_
#define __transformcomponent_h_

#include "components/Component.h"
#include "QMath.h"
#include "EngineExport.h"


struct QENGINE_API TransformComponent : public Component
{
    public:

		TransformComponent()
		{
			position = vec3<float>(0, 0, 0);
			scale = vec3<float>(1, 1, 1);
			rotation = vec3<float>(0, 0, 0);
			forward = vec3<float>(0, 0, 1);

			mMatrix.loadIdentity();
		}
	
		~TransformComponent() = default;

		void OnUpdate()
		{
			mat4<float> s, t, r, xr, yr, zr;
			xr.loadXRotation(rotation.x);
			yr.loadYRotation(rotation.y);
			zr.loadZRotation(rotation.z);
			s.loadScale(scale);
			t.loadTranslation(position);
			r = xr * yr * zr;

			mMatrix = t * s * r;
		}

		void onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods)
		{
			int p = 0;
		}
	
		vec3<float> position;
		vec3<float> scale;
		vec3<float> rotation;
		vec3<float> forward;
		vec3<float> right;
		vec3<float> up;
		mat4<float> mMatrix;
	
	private:
	
		
};

#endif