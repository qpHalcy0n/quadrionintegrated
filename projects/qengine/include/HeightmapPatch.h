#ifndef __HEIGHTMAPPATCH_H_
#define __HEIGHTMAPPATCH_H_


#include "EngineExport.h"
#include "QMath.h"


struct QENGINE_API HeightfieldVertex
{
	HeightfieldVertex()
	{
		vertex = 0;
	}
	
	HeightfieldVertex(const HeightfieldVertex& v)
	{
		vertex = v.vertex;
	}

	void operator= (const HeightfieldVertex& v)
	{
		vertex = v.vertex;
	}

	uint16_t vertex;
};


class QENGINE_API HeightmapPatch
{
	public:

		HeightmapPatch();

		HeightmapPatch(const HeightfieldVertex* verts,
					   const uint32_t nVertsX,
					   const uint32_t nVertsZ,  
					   vec2<double> worldMins,
					   vec2<double> worldMaxs);
		~HeightmapPatch();

		const uint32_t getNumVertsX() const;
		const uint32_t getNumVertsZ() const;

		const HeightfieldVertex getVertex(uint32_t idx) const;

		const vec2<double> getWorldMinimums() const;
		const vec2<double> getWorldMaximums() const;

		const uint16_t getMinimumElevation() const;
		const uint16_t getMaximumElevation() const;

	private:

		std::vector<HeightfieldVertex> heightmapVertices;

		uint32_t numVertsX;
		uint32_t numVertsZ;
		uint32_t startX;
		uint32_t startZ;

		uint16_t minElevation;
		uint16_t maxElevation;

		vec2<double> worldMinimums;
		vec2<double> worldMaximums;
};


#endif
