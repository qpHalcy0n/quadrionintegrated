#ifndef __HEIGHTMAP_H_
#define __HEIGHTMAP_H_

#include "EngineExport.h"
#include "QMath.h"
#include "render/Render.h"
#include "HeightmapPatch.h"
#include "render/Image.h"

struct HeightmapImpl;



class QENGINE_API Heightmap
{
	public:

		Heightmap();
		~Heightmap();

		bool loadFromFile(const char* fName, 
						  const char* path, 
						  const uint32_t nPixelsPerPatchX,
						  const uint32_t nPixelsPerPatchY,
						  vec2<double> worldMins,
						  vec2<double> worldMaxs);

		bool loadFromData(void* data,
						  const uint32_t nPixelsX,
						  const uint32_t nPixelsY,
						  const uint32_t nPixelsPerPatchX,
						  const uint32_t nPixelsPerPatchY,
						  vec2<double> worldMins,
						  vec2<double> worldMaxs);

		GLuint getHeightmapGlid();

		const size_t getNumPatches();

		const HeightmapPatch* const getPatch(const size_t idx);

		const Image* const getHeightmapSource() const;
		const size_t getNumSourcePixelsX();
		const size_t getNumSourcePixelsY();

	private:
		
		HeightmapImpl* mImpl = nullptr;
};


#endif