#ifndef __SOLAR_H_
#define __SOLAR_H_

#include <cstdint>
#include "EngineExport.h"
#include "QMath.h"


class QENGINE_API Solar 
{
    public:

        Solar();
        ~Solar();

        void        SetLatitude(float aLat);
        void        SetLongitude(float aLon);
        void        SetDay(int32_t aDay);
        void        SetHour(int32_t aHour);
        void        SetMinute(int32_t aMinute);
        void        SetUTCOffset(int32_t aUTCOffset);

        void        UseSystemTime(bool aUseSystemTime);

        vec3<float> GetLightVector();

        float       GetSunAzimuth();
        float       GetSunAltitude();

        int32_t     GetDay();
        int32_t     GetHour();
        int32_t     GetMinute();

        void        Update();


    private:

        int         GetCurrentYearDay();
        int         GetCurrentHour();
        int         GetCurrentMinute();

        vec3<float> mLightVector;

        float       mLatitude;
        float       mLongitude;
        float       mDecAngle;
        float       mSunriseHourAngle;
        float       mSunElevation;
        float       mSunAzimuth;
        float       mSunriseHour;
        float       mHour;
        float       mHourAngle;


        int32_t    mSelectedHour;
        int32_t    mSelectedMinute;
        int32_t    mSelectedDay;

        int32_t    mSelectedUtcDev;

        tm*         mCurrentTime;         

        bool        mUseSystemTime;
};


#endif