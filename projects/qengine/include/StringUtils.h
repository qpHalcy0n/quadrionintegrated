#ifndef stringutils_h__
#define stringutils_h__

#include <string>
#include <vector>
#include <cstdint>
#include "EngineExport.h"

QENGINE_API char qtolower(char aA);
QENGINE_API bool qstricmp(const char* aA, const char* aB);

class QENGINE_API StringUtils
{
    public:
	    static std::vector<std::string> split(const std::string& aString, const char aDelimiter);
	    static std::vector<std::string> split(const std::string& aString, const std::string& aDelimiter);
	    static std::vector<std::string> splitOnAny(const std::string& aString, const std::string& aDelimiters);
		static std::vector<std::string> splitWithRegex(const std::string& aString, const std::string& aDelimiters);

		static std::string reduceTrim(const std::string& str, const std::string& whitespace = " \t");
		static std::string reduce(const std::string& aStr, const std::string& aFill = " ", const std::string& aWhitespace = " \t");

	    static std::string trimFront(const std::string& aString, const uint32_t aAmount);
	    static std::string trimEnd(const std::string& aString, const uint32_t aAmount);

	    static std::string strip(const std::string& aString, const char aDelimiter);
	    static std::string strip(const std::string& aString, const std::string& aDelimiter);

	    static std::string stripFirst(const std::string& aString, const char aDelimiter);
	    static std::string stripFirst(const std::string& aString, const std::string& aDelimiter);

	    static std::string stripLast(const std::string& aString, const char aDelimiter);
	    static std::string stripLast(const std::string& aString, const std::string& aDelimiter);

	    static std::string replace(const std::string& aString, const char aSearch, const char aReplacement);
	    static std::string replace(const std::string& aString, const std::string& aSearch, const char aReplacement);
	    static std::string replace(const std::string& aString, const char aSearch, const std::string& aReplacement);
	    static std::string replace(const std::string& aString, const std::string& aSearch, const std::string& aReplacement);

	    static std::string replaceFirst(const std::string& aString, const char aSearch, const char aReplacement);
	    static std::string replaceFirst(const std::string& aString, const std::string& aSearch, const char aReplacement);
	    static std::string replaceFirst(const std::string& aString, const char aSearch, const std::string& aReplacement);
	    static std::string replaceFirst(const std::string& aString, const std::string& aSearch, const std::string& aReplacement);

	    static std::string replaceLast(const std::string& aString, const char aSearch, const char aReplacement);
	    static std::string replaceLast(const std::string& aString, const std::string& aSearch, const char aReplacement);
	    static std::string replaceLast(const std::string& aString, const char aSearch, const std::string& aReplacement);
	    static std::string replaceLast(const std::string& aString, const std::string& aSearch, const std::string& aReplacement);

	    static std::string toLowercase(const std::string& aString);
	    static std::string toUppercase(const std::string& aString);
	    static std::string swapCase(const std::string& aString);

	    static bool equalsIgnoreCase(const std::string& aString, const std::string& aOther);

	    static bool contains(const std::string& aString, const char aToContain);
	    static bool contains(const std::string& aString, const std::string& aToContain);

	    static bool containsAny(std::string aString, std::vector<char> aToContain);
	    static bool containsAny(std::string aString, std::vector<std::string> aToContain);

	    static std::string deleteWhitespace(const std::string& aString);

	    static std::string reverse(const std::string& aString);

	    static bool startsWith(const std::string& aString, const char aPrefix);
	    static bool startsWith(const std::string& aString, const std::string& aPrefix);
	    static bool startsWithAny(const std::string& aString, std::vector<char> aPrefixes);
	    static bool startsWithAny(const std::string& aString, std::vector<std::string> aPrefixes);

	    static bool endsWith(const std::string& aString, const char aSuffix);
	    static bool endsWith(const std::string& aString, const std::string& aSuffix);
	    static bool endsWithAny(const std::string& aString, std::vector<char> aSuffixes);
	    static bool endsWithAny(const std::string& aString, std::vector<std::string> aSuffixes);

//		static std::vector<std::string> split(const std::string& aString, const std::string& aDelimiters);

		static const char* findToken(const char* aStr, const std::string& aToken);
		static const char* findToken(const std::string& aString, const std::string& aToken);

		static std::vector<std::string> getLines(const std::string& aSource);

		static std::string getBlock(const char* aStr, const char** aOutPosition = nullptr);
		static std::string getBlock(const std::string& aString, const uint32_t aOffset = 0);

		static std::string getStatement(const char* aStr, const char** aOutPosition = nullptr);

		static std::vector<std::string> tokenize(const std::string& aString);

		static uint32_t findEndOfBlock(const std::vector<std::string> aSource, const uint32_t aStart, const uint32_t aEnd, std::string* aWriteBuffer);

		static uint32_t parseToGLEnum(const std::string& aValue);

		static std::string getSemanticLocationString(const std::string& aSemantic);

		static int32_t getSemanticLocation(const std::string& aSemantic);
};

#endif // stringutils_h__
