#ifndef gamestate_h__
#define gamestate_h__

#include "IUpdatable.h"
#include "EngineContext.h"
#include "EngineExport.h"
#include "InputManager.h"

class QENGINE_API GameState : public IUpdatable, public InputListener
{
public:
	virtual ~GameState(){};

	void initialize(const EngineContext* aEngineContext);
	virtual void render(const UpdateContext& aUpdateContext) = 0;

	virtual void onInitialize(const char** aArgs, size_t aNumArgs);
	virtual void onGainedFocus();
	virtual void onLostFocus();

protected:
	const EngineContext* mEngineContext;
};

#endif // gamestate_h__
