#ifndef enginecontext_h__
#define enginecontext_h__

#include "EngineExport.h"

//	Context
//	We utilize this to avoid using globals everywhere.
//
//	Only add *critical* sub-systems here please
//	otherwise we approach the Blob anti-pattern.
//
//	Multiple instances of engineContext are allowed (discouraged),
//	however, they must be consistent throughout their
//	lifetime. i.e. we don't want people swapping out
//	critical sub-systems between render calls.


class GameEngine;
class Application;


struct QENGINE_API EngineContext
{
	EngineContext( GameEngine* const aEngine, Application* const aApplication);


	GameEngine* const engine;
	Application* const application;
};

#endif // enginecontext_h__
