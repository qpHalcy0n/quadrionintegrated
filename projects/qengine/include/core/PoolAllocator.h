#ifndef __poolallocator_h_
#define __poolallocator_h_


#include <list>

#if !defined(_WIN32) || !defined(_WIN64)
	#include <stdlib.h>
#else
	#include <malloc.h>
#endif

#include "EngineExport.h"


class PoolAllocatorImpl;
class QENGINE_API PoolAllocator
{
	public:
		
		PoolAllocator();
		PoolAllocator(size_t aAlignment, size_t aNumBlocks, size_t aSizePerBlock);
		~PoolAllocator();

		void* getBlock();

		void freeBlock(void* aBlock);

	private:
		PoolAllocatorImpl* mImpl;

		void*		mMem;
		
		size_t		mAlignment;
		size_t		mNumBlocksTotal;
		size_t		mSizeofBlock;

};


#endif