/*
* qtimer.h
*
* Created on: 25/01/2008
*     Author: Jonathan 'Bladezor' Bastnagel
*
* Copyright (C) <2008>  <Odorless Entertainment & Quadrion Software>
*/

#ifndef timer_h__
#define timer_h__

#include "EngineExport.h"

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#endif

struct QTime
{
	double years;
	double days;
	double hours;
	double minutes;
	double seconds;
	double milliseconds;
	double microseconds;
};

class QENGINE_API Timer
{
public:
	Timer()
	{
#if defined(_WIN32) || defined(_WIN64)
		QueryPerformanceFrequency(&mFreq);
		mStartCount.QuadPart = 0;
		mEndCount.QuadPart = 0;
#else
		mStartCount.tv_sec = mStartCount.tv_usec = 0;
		mEndCount.tv_sec = mEndCount.tv_usec = 0;
#endif	
		mStartTime = 0;
		mEndTime = 0;
	}

	~Timer()
	{
	}

	void start();
	void stop();
	void reset();

	double getElapsedMicroSec();
	double getElapsedMilliSec();
	double getElapsedSec();
	QTime getElapsed();
	bool isRunning;
	
protected:
private:

#if defined(_WIN32) || defined(_WIN64)
	LARGE_INTEGER mFreq;
	LARGE_INTEGER mStartCount;
	LARGE_INTEGER mEndCount;
#else
	timeval mStartCount;
	timeval mEndCount;
#endif

	double mStartTime;
	double mEndTime;
};

#endif // timer_h__

