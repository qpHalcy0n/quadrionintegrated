#ifndef qarray_h__
#define qarray_h__

#include <cstdint>
#include <cstring>

#include "EngineExport.h"

template<typename Ty>
class QArray
{
	private:
		Ty**		mData = nullptr;
		size_t		mSize = 0;
		size_t		mAllocSize = 0;

	public:
		QArray()
		{
			mData = nullptr;
			mSize = 0;
			mAllocSize = 0;
		}

		~QArray()
		{
			clear();
		}

		QArray(const QArray<Ty>& aS)
		{
			for (size_t i = 0; i < aS.mSize; ++i)
			{
				this->push_back(aS[static_cast<int32_t>(i)]);
			}
		}

		QArray(QArray<Ty>&& aS)
		{
			for (size_t i = 0; i < aS.mSize; ++i)
			{
				this->push_back(aS[static_cast<int32_t>(i)]);
			}
		}

		const Ty& operator[] (int aIndex) const
		{
			Ty* val = (mData[aIndex]);
			return *val;
		}

		Ty& operator[] (int aIndex)
		{
			Ty* val = (mData[aIndex]);
			return *val;
		}

		QArray<Ty>& operator= (const QArray<Ty>& aF)
		{
			clear();

			for (size_t i = 0; i < aF.mSize; ++i)
			{
				this->push_back(aF[static_cast<int32_t>(i)]);
			}

			return *this;
		}

		// clear() should not affect reserved capacity of the vector //
		// only free memory of elements //
		void clear()
		{
			if (mData)
			{
				for (size_t i = 0; i < mSize; ++i)
				{
					if (mData[i])
					{
						delete mData[i];
						mData[i] = nullptr;
					}
				}

//				delete[] _data;
//				_data = nullptr;
			}

			mSize = 0;
//			_allocSize = 0;

		}

		const Ty& front() const
		{
			if (!mData)
				return nullptr;

			return mData[0];
		}

		void push_back(const Ty& aItem)
		{
			if ((mSize + 1) > mAllocSize)
				resize(mSize + 3);

			mData[mSize] = new Ty(aItem);
			mSize++;
		}

		void push_back(Ty& aItem)
		{
			if ((mSize + 1) > mAllocSize)
				resize(mSize + 3);

			mData[mSize] = new Ty(aItem);
			mSize++;
		}

		void resize(size_t aSize)
		{
			if (mAllocSize == aSize)
				return;

			Ty** newData = new Ty*[sizeof(Ty*) * aSize];

			if (mAllocSize <= aSize)
			{
				if (mAllocSize > 0)
					memcpy((void*)newData, (const void*)mData, sizeof(Ty*) * mAllocSize);
			}
			else
				memcpy((void*)newData, (const void*)mData, sizeof(Ty*) * aSize);

			if (mData)
				delete[] mData;

			mData = newData;
			mAllocSize = aSize;
		}

		size_t size() const
		{
			return mSize;
		}

		size_t capacity() const
		{
			return mAllocSize;
		}

		Ty* data()
		{
			return *(mData);
		}

		Ty& at(const uint32_t aI)
		{
			Ty ret;
			if(aI < 0 || aI > mSize)
				return ret;

			return *(mData[aI]);
		}
};

#endif // qarray_h__