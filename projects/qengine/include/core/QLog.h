#ifndef qlog_h__
#define qlog_h__

#pragma warning(push)
#pragma warning(disable: 4251)

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/sinks/sink.h>
#include <iostream>
#include <sstream>
#include <memory>
#include "EngineExport.h"


#if defined(_WIN32) || defined(_WIN64)
#define LOG_( s )            \
		{                             \
   			OutputDebugStringA( (s) ); \
		}
#else	
#define LOG_( s )		\
		{						\
			std::ostringstream os_;		\
			os_ << s; \
			std::cout << os_.str().c_str(); \
		}
#endif

class QENGINE_API QLog
{
public:
	static void construct();

	inline static std::shared_ptr<spdlog::logger>& getCoreLogger()
	{
		return mCoreLogger;
	}

	inline static std::shared_ptr<spdlog::logger>& getClientLogger()
	{
		return mClientLogger;
	}

private:
	static std::shared_ptr<spdlog::logger> mCoreLogger;
	static std::shared_ptr<spdlog::logger> mClientLogger;
};

template<typename Mutex>
class IntermediateSink : public spdlog::sinks::base_sink<Mutex>
{
    protected:
        void sink_it_(const spdlog::details::log_msg& aMsg) override
        {
			fmt::memory_buffer formatted;
			spdlog::sinks::sink::formatter_->format(aMsg, formatted);
			const auto s = fmt::to_string(formatted);
			LOG_(s.c_str());
        }

        void flush_() override
		{
			std::cout << std::flush;
		}
};

#include "spdlog/details/null_mutex.h"
#include <mutex>
using intermediate_sink_mt = IntermediateSink<std::mutex>;
using intermediate_sink_st = IntermediateSink<spdlog::details::null_mutex>;

// Core log macros
#define QUADRION_INTERNAL_TRACE(...)	QLog::getCoreLogger()->trace(__VA_ARGS__)
#define QUADRION_INTERNAL_INFO(...)		QLog::getCoreLogger()->info(__VA_ARGS__)
#define QUADRION_INTERNAL_WARN(...)		QLog::getCoreLogger()->warn(__VA_ARGS__)
#define QUADRION_INTERNAL_ERROR(...)	QLog::getCoreLogger()->error(__VA_ARGS__)
#define QUADRION_INTERNAL_CRITICAL(...)	QLog::getCoreLogger()->critical(__VA_ARGS__)

// Client log macros
#define QUADRION_TRACE(...)				QLog::getClientLogger()->trace(__VA_ARGS__)
#define QUADRION_INFO(...)				QLog::getClientLogger()->info(__VA_ARGS__)
#define QUADRION_WARN(...)				QLog::getClientLogger()->warn(__VA_ARGS__)
#define QUADRION_ERROR(...)				QLog::getClientLogger()->error(__VA_ARGS__)
#define QUADRION_CRITICAL(...)			QLog::getClientLogger()->critical(__VA_ARGS__)

#pragma warning(pop)

#endif 