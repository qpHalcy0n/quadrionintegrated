#ifndef __asset_h_
#define __asset_h_

#include <string>
#include "EngineExport.h"

class QENGINE_API Asset
{
	public:

		Asset(const std::string& aName);
		virtual ~Asset();
	
		virtual bool load() = 0;
		bool isLoaded() const { return mLoaded; }

		std::string getName() { return mName; }
	
	protected:
	
		bool mLoaded;
		char* mName;
};

#endif
