#ifndef _cubemapasset_h
#define _cubemapasset_h

#include <string>
#include "assets/Asset.h"
#include "render/Texture.h"
#include "EngineExport.h"

class QENGINE_API CubemapAsset : public Asset
{
	public:

		CubemapAsset(const std::string& aName, 
					 uint32_t aWidth, uint32_t aHeight,
					 ETexturePixelFormat aFormat);

		bool load() override;

		const FboId getFBO() const { return mFBO; }
		const GLuint getTexture() const { return mCubemapTexture; }

	private:

		uint32_t				mWidth, mHeight;
		ETexturePixelFormat		mFormat;
		FboId					mFBO;
		GLuint					mCubemapTexture;
};


#endif