#ifndef materialasset_h__
#define materialasset_h__

#include <string>
#include <vector>
#include "render/Material.h"
#include "assets/AssetManager.h"
#include "core/QLog.h"
#include "render/QGfx.h"

class MaterialAsset : public Asset
{
	public:

		MaterialAsset(const std::string& aName,
					  const std::vector<UniformBufferElement*> aElems) : Asset(aName)
		{
			mMaterial = nullptr;
			mUBOElements = aElems;

			QUADRION_TRACE("Material {0} constructor called", aName);
		}

		bool load() override
		{
			mMaterial = new Material();
			mMaterial->setName(mName);
			mMaterial->addUniforms(mUBOElements);

			mLoaded = true;

			QUADRION_TRACE("Material {0} load called", mName);

			return true;
		}

		Material* const		getMaterial()
		{
			return mMaterial;
		}

	private:
		
		std::vector<UniformBufferElement*>		mUBOElements;
		Material*							mMaterial;
};


#endif // materialasset_h__
