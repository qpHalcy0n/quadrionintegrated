#ifndef qinputmanager_h__
#define qinputmanager_h__

#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
#else
#include "X11/keysymdef.h"
#include "X11/Xlib.h"
#endif

#include <type_traits>
#include <cstdint>
#include "QMath.h"
#include "UpdateContext.h"
#include "EngineExport.h"


// Aliased to Win32 Virtual Key table values //
#if defined(_WIN32)
	#define VKEY_ESCAPE 		0x1B
	#define VKEY_TAB			0x09
	#define VKEY_SHIFT			0x10
	#define VKEY_CONTROL		0x11
	#define VKEY_MENU			0x12	// Alt Key
	#define VKEY_LWIN           0x5B
	#define VKEY_RWIN           0x5C

	#define VKEY_NUMPAD1		0x61
	#define VKEY_NUMPAD2		0x62
	#define VKEY_NUMPAD3		0x63
	#define VKEY_NUMPAD4		0x64
	#define VKEY_NUMPAD5		0x65
	#define VKEY_NUMPAD6		0x66
	#define VKEY_NUMPAD7		0x67
	#define VKEY_NUMPAD8		0x68
	#define VKEY_NUMPAD9		0x69
	#define VKEY_NUMPAD0		0x60

	#define VKEY_UP				0x26
	#define VKEY_DOWN			0x28
	#define VKEY_LEFT			0x25
	#define VKEY_RIGHT			0x27
	#define VKEY_PRIOR			0x21	// Page Up (I think?)
	#define VKEY_NEXT			0x22	// Page Down (I think?)
	#define VKEY_F11			0x7A
#else
	#define VKEY_ESCAPE 		XK_Escape
	#define VKEY_TAB			XK_Tab
	#define VKEY_SHIFT			XK_Shift_L		// TODO: Fix me?
	#define VKEY_CONTROL		XK_Control_L
	#define VKEY_MENU			0x12			// Alt Key
	#define VKEY_LWIN           0x5B
	#define VKEY_RWIN           0x5C

	#define VKEY_NUMPAD1		XK_KP_1
	#define VKEY_NUMPAD2		XK_KP_2
	#define VKEY_NUMPAD3		XK_KP_3
	#define VKEY_NUMPAD4		XK_KP_4
	#define VKEY_NUMPAD5		XK_KP_5
	#define VKEY_NUMPAD6		XK_KP_6
	#define VKEY_NUMPAD7		XK_KP_7
	#define VKEY_NUMPAD8		XK_KP_8
	#define VKEY_NUMPAD9		XK_KP_9
	#define VKEY_NUMPAD0		XK_KP_0

	#define VKEY_UP				XK_Up
	#define VKEY_DOWN			XK_Down
	#define VKEY_LEFT			XK_Left
	#define VKEY_RIGHT			XK_Right
	#define VKEY_PRIOR			XK_Page_Up		// Page Up (I think?)
	#define VKEY_NEXT			XK_Page_Down	// Page Down (I think?)
	#define VKEY_F11 XK_F11
#endif

class Application;

enum class EMouseButton : int
{
	LEFT,
	RIGHT,
	MIDDLE
};

enum class EMouseButtonAction : int
{
	PRESS,
	RELEASE,
	DBL_CLICK
};

enum class EKeyAction : int
{
	PRESS,
	RELEASE,
	REPEAT
};

enum class EKeyModifiers : int
{
	SHIFT = 1 << 0,
	CTRL = 1 << 1, 
	ALT = 1 << 2, 
	META = 1 << 3,
	NUMPAD = 1 << 4 
};

class QENGINE_API InputListener
{
	public:
		virtual void onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction) {};
		virtual void onMouseMove(const double aX, const double aY) {};
		virtual void onMouseWheelScroll(const double deltaX, const double deltaY) {};
		virtual void onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods) {};
		virtual void onCharEvent(const int aCodepoint) {};
		virtual void onWindowGotFocus() {};
		virtual void onWindowLostFocus() {};
};

class InputManagerImpl;

class QENGINE_API InputManager
{
	public:

		InputManager();
//		InputManager(Application* aApplication);
		~InputManager();

		void initialize(Application* aApplication);
		bool isInitialized() { return mIsInitialized; }

		static InputManager& instance();
	
		void registerInputListener(InputListener* aInputListener);
		void deregisterInputListener(InputListener* aInputListener);

		bool isKeyPressed(const uint8_t aKeyCode, const bool aCaseSensitive = false) const;
		bool isKeyReleased(const uint8_t aKeyCode, const bool aCaseSensitive = false) const;

	#if defined(_WIN32)
		bool processInput(UINT aUMsg, WPARAM aWParam, LPARAM aLParam);
	#else	
		bool processInput(Display* aDisp);
	#endif

		void update(const UpdateContext& aUpdateContext);

		void setMousePosition(int32_t aX, int32_t aY);
		void setMousePosition(vec2<int32_t> aPos);
		void centerCursor();

		void setCenterCursor(const bool aCenter);
		const bool isCursorCentering();
		vec2<int32_t> const getMousePosition();
		vec2<int32_t> const getMousePositionFromCenter();


	//	vec2<int32_t>* const GetMouseTrace(int32_t& nSamples);
	//	void   ClearMouseTrace();

	private:

		bool				mIsInitialized;
		InputManagerImpl* mImpl;
};

#endif // qinputhandler_h__
