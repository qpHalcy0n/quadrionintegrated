#ifndef app_h__
#define app_h__

#if defined(_WIN32) || defined(_WIN64)
	#include <Windows.h>
#else
	#include <X11/Xlib.h>
	#include <X11/cursorfont.h>
#endif

#include "core/qstring.h"
#include "Extensions.h"
#include "INI.h"
#include "EngineExport.h"
#include "InputManager.h"

struct WindowLocation
{
	int left;
	int right;
	int top;
	int bottom;
};

class GameEngineImpl;

#if !defined(_WIN32) || !defined(_WIN64)
struct XGameWindow
{
    Display* display;
    Window window;
    GLXContext context;
    XSetWindowAttributes attrib;
    XF86VidModeModeInfo vidMode;

    int screen;
    char* title;
};
#endif

class QENGINE_API ApplicationWindow
{
	public:
		ApplicationWindow();

#if defined(_WIN32) || defined(_WIN64)
		ApplicationWindow(INI* aIni, GameEngineImpl* aGameEngine, WNDPROC aWndProc = NULL);
#else
		ApplicationWindow(INI* aIni, GameEngineImpl* aGameEngine);
#endif
		~ApplicationWindow();

		int getWidth() { return mWinWidth; }
		int getHeight() { return mWinHeight; }

#if !defined(_WIN32) || !defined(_WIN64)
		Display* getDisplay() { return mWin.display; }
		Window getWindow() { return mWin.window; }
#endif

		bool isQuitRequested();

		void bufferSwap();
		void hideCursor();
		void showCursor();
		void requestQuit();
		bool peekEvents();
		void getCursorPosition(int& aX, int& aY);

#if defined(_WIN32) || defined(_WIN64)
		HWND getWindowHandle();
#else
		unsigned int getWindowHandle();
#endif

		WindowLocation getWindowDimensions();

		WindowLocation getWindowPosition();

	protected:
		int			mWinWidth;
		int			mWinHeight;
		int			mCursorDisplayCount;

	private:
		bool mFullscreen;
		bool mIsQuitRequested;
		int  curMouseX;
		int  curMouseY;

#if defined(_WIN32) || defined(_WIN64)

		void loadFromINI(INI* aIni, GameEngineImpl* aGameEngine, WNDPROC aWndProc = NULL);

		HGLRC mHRC;
		HDC	mHDC;
		HWND mHandle;
		HINSTANCE mInstance;

#else
//		Window mWin;

		XGameWindow mWin;
		XEvent mEvent;
        
		// int mAttribSgl[10] =
        // {
		//     GLX_RGBA,
		//     GLX_RED_SIZE, 4,
		//     GLX_GREEN_SIZE, 4,
		//     GLX_BLUE_SIZE, 4,
		//     GLX_DEPTH_SIZE, 16, None
		// };

		// int mAttribDbl[11] =
		// {
		//     GLX_RGBA, GLX_DOUBLEBUFFER,
		//     GLX_RED_SIZE, 4,
		//     GLX_GREEN_SIZE, 4,
		//     GLX_BLUE_SIZE, 4,
		//     GLX_DEPTH_SIZE, 16, None
		// };

		// int mAttribDbl[17] = 
		// {
		// 	GLX_RENDER_TYPE, GLX_RGBA_BIT, GLX_RED_SIZE, 4, GLX_GREEN_SIZE, 4, GLX_BLUE_SIZE, 4, 
		// 	GLX_ALPHA_SIZE, 4, 
		// 	GLX_DOUBLEBUFFER, True, GLX_DEPTH_SIZE, 24, GLX_STENCIL_SIZE, 8, None
		// };

		void loadFromINI(INI* aIni, GameEngineImpl* aGameEngine);
#endif

};

class QENGINE_API Application
{
	public:
		/**
		  *  Default constructor loads application and window from default (safe) settings
		**/
		Application();

		/**
		  *	Constructor with INI file name. Loads initial settings from valid INI file 
		  * If wndProc is NULL, the system will use a default callback which only handles exits
		**/
#if defined(_WIN32) || defined(_WIN64)
		Application(const char* aFileName, GameEngineImpl* aGameEngine, WNDPROC aWndProc = NULL);
#else		
		Application(const char* aFileName, GameEngineImpl* aGameEngine);
#endif

		~Application();

		/**
		  * Obtain the initialized INI file 
		**/
		INI* const getApplicationINI();

		int	getWindowWidth();
		int	getWindowHeight();

		void getMousePosition(int& aX, int& aY);

		void setMousePosition(int aX, int aY);
		bool peekEvents();
		void requestQuit();
		bool isQuitRequested();
		
		vec2<int32_t> getWindowCenter();

		void presentBackbuffer()
		{
			mAppWindow->bufferSwap();
		}

		WindowLocation getWindowDimensions()
		{
			return mAppWindow->getWindowDimensions();
		}

		WindowLocation getWindowPosition()
		{
			return mAppWindow->getWindowPosition();
}

#if !defined(_WIN32) || !defined(_WIN64)		
		Display* getDisplay();
		Window getWindow();	
#endif
		ApplicationWindow* getApplicationWindow();
	private:

		void loadDefaults();

		ApplicationWindow* mAppWindow;
		INI* mAppINI;
};

/**
* Gets the current working directory of the applicatio n.
*/
QString getCWD();

#endif // app_h__
