#include "SandboxGamestate.h"

#include <memory>
#include <string>
#include "Application.h"
#include "EngineContext.h"
#include "GameEngine.h"
#include "render/QGfx.h"
#include "assets/AssetManager.h"
#include "assets/TextureAsset.h"
#include "assets/CubemapAsset.h"
#include "render/VertexBufferLayout.h"
#include "render/VertexBuffer.h"
#include "render/IndexBuffer.h"
#include "render/QGfx.h"
#include "render/Render.h"
#include "entities/Entity.h"
#include "entities/EntityFactory.h"
#include "components/TransformComponent.h"
#include "components/RenderComponent.h"
#include "components/MaterialComponent.h"
#include "QMath.h"
#include "sml.h"


constexpr uint32_t DEBUG_TERRAIN_RES = 8;
constexpr int Forward = 1, Backward = 2, Left = 4, Right = 8, Up = 16, Down = 24;
constexpr float		WASD_SPEED = 1000.0f;

static double maxUpdateDt = 0.0;
static float selectedObj = -1.0f;
static float lastSelectedObj = 1.0f;
static int frameCount = 0;
static double avgTime = 0.0;
static double lastAvg = 0.0;
static vec2<int32_t> pushedMousePos;

float shit = 0.0f;
bool oddFrame = false;

static void SetPBRMaterials(ObjectMaterial* aObjMat, const char* aTechName, const char* aPassName,
							MeshNodeMaterial aMeshMat)
{
	Shader* shader = aObjMat->getEffect()->getTechnique(aTechName)->getPass(aPassName)->getShader();

	if (aMeshMat.albedoTexture > 0)
	{
		shader->setTexture("g_pbr_albedoTexture", aMeshMat.albedoTexture);
		aObjMat->setUniform1b("g_pbr_hasAlbedoTexture", true);
	}

	else
		aObjMat->setUniform1b("g_pbr_hasAlbedoTexture", false);

	if (aMeshMat.emissiveTexture > 0)
	{
		shader->setTexture("g_pbr_emissiveTexture", aMeshMat.emissiveTexture);
		aObjMat->setUniform1b("g_pbr_hasEmissiveTexture", true);
	}

	else
		aObjMat->setUniform1b("g_pbr_hasEmissiveTexture", false);

	if (aMeshMat.aoTexture > 0)
	{
		shader->setTexture("g_pbr_aoTexture", aMeshMat.aoTexture);
		aObjMat->setUniform1b("g_pbr_hasAOTexture", true);
	}

	else
		aObjMat->setUniform1b("g_pbr_hasAOTexture", false);

	if (aMeshMat.roughnessTexture > 0)
	{
		shader->setTexture("g_pbr_roughnessTexture", aMeshMat.roughnessTexture);
		aObjMat->setUniform1b("g_pbr_hasRoughnessTexture", true);
	}

	else
		aObjMat->setUniform1b("g_pbr_hasRoughnessTexture", false);

	if (aMeshMat.metallicTexture > 0)
	{
		shader->setTexture("g_pbr_metallicTexture", aMeshMat.metallicTexture);
		aObjMat->setUniform1b("g_pbr_hasMetallicTexture", true);
	}

	else
		aObjMat->setUniform1b("g_pbr_hasMetallicTexture", false);

	if (aMeshMat.normalMap > 0)
	{
		shader->setTexture("g_pbr_normalMap", aMeshMat.normalMap);
		aObjMat->setUniform1b("g_pbr_hasNormalMap", true);
	}

	else
		aObjMat->setUniform1b("g_pbr_hasNormalMap", false);

	aObjMat->setUniform1f("g_pbr_nRoughnessMetallicChannels", aMeshMat.nRoughnessMetallicChannels);
}

static bool createObjectMaterials(Effect* aEffect)
{
	std::vector<Technique*> techs = aEffect->getTechniques();	
	for(uint32_t i = 0; i < techs.size(); ++i)
	{
		size_t nPasses = techs[i]->getNumPasses();
		for(size_t j = 0; j < nPasses; ++j)
		{
			Pass* p = techs[i]->getPass(j);
			std::string concat(techs[i]->getName());
			concat.append("+");
			concat.append(p->getName());

			QGfx::addObjectMaterial(aEffect, concat);
		}
	}

	return true;
}

static void createForwardRenderQueueNew()
{
	QGfx::pushToRenderQueue("SkyLighting");
	QGfx::pushToRenderQueue("SkyLightingCubemap");
	QGfx::pushToRenderQueue("SkyIrradianceCubemap");
	QGfx::pushToRenderQueue("CTIntegrationTechnique");

	QGfx::pushToRenderQueue("SlimGBuffer");
	QGfx::pushToRenderQueue("HBAOTechnique");
	QGfx::pushToRenderQueue("HBAOBlurTechnique");

	QGfx::pushToRenderQueue("ForwardPBRTechnique");
	QGfx::pushToRenderQueue("HDRTechnique");
}

/*

static void createDeferredRenderQueue()
{
	
	QGfx::pushToRenderQueue("skyLightingMaterial");
	QGfx::pushToRenderQueue("skyLightingCubeMaterial");
	QGfx::pushToRenderQueue("skyIrradianceCubeMaterial");
	QGfx::pushToRenderQueue("ctIntegrationMaterial");
	QGfx::pushToRenderQueue("gBufferTerrainMaterial");
	QGfx::pushToRenderQueue("gBufferMaterial1");
	QGfx::pushToRenderQueue("gBufferMaterial2");
	QGfx::pushToRenderQueue("gBufferMaterial3");

	QGfx::pushToRenderQueue("deferredHBAOMaterial");
	QGfx::pushToRenderQueue("hbaoBlurMaterial");

	QGfx::pushToRenderQueue("pbrMaterial");

	QGfx::pushToRenderQueue("hdrDownScaleMaterial");
	QGfx::pushToRenderQueue("hdrSampleLuminanceMaterial");
	QGfx::pushToRenderQueue("hdrResampleLuminanceMaterial1");
	QGfx::pushToRenderQueue("hdrResampleLuminanceMaterial2");
	QGfx::pushToRenderQueue("hdrFinalLuminanceMaterial");
	QGfx::pushToRenderQueue("hdrAdaptationMaterial");
	QGfx::pushToRenderQueue("hdrBrightPassMaterial");
	QGfx::pushToRenderQueue("hdrGaussMaterial");
	QGfx::pushToRenderQueue("hdrDownScale2Material");
	QGfx::pushToRenderQueue("hdrGauss2Material");
	QGfx::pushToRenderQueue("hdrBloomMaterial");
	QGfx::pushToRenderQueue("hdrBloom2Material");
	QGfx::pushToRenderQueue("hdrFinalMaterial");
	
}
*/
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

SandboxGamestate::SandboxGamestate()
{
	mCharacterDirection = 0;
	mIsSprinting = false;
};

SandboxGamestate::~SandboxGamestate()
{
//	delete mSampleVAO;
	delete mEffectGui;
	delete mCamera;
};

bool SandboxGamestate::_isBufferCleared(uint32_t aId)
{
	for (uint32_t cleared : mClearedBuffers)
	{
		if(cleared == aId)
			return true;
	}

	return false;
}

void SandboxGamestate::_clearBuffer(uint32_t aId, vec4<float> aClearColor)
{
	if (!_isBufferCleared(aId))
	{
		glClearColor(aClearColor.x, aClearColor.y, aClearColor.z, aClearColor.w);
		GLboolean depthWriteEnabled;
		glGetBooleanv(GL_DEPTH_WRITEMASK, &depthWriteEnabled);
		glDepthMask(true);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if (!depthWriteEnabled)
			glDepthMask(false);
		mClearedBuffers.push_back(aId);
	}
}

void SandboxGamestate::_loadGeometry()
{
	mTestMesh = AssetManager::instance().load<MeshAsset>("TestMesh", ASSET_LOAD_MED_PRIO, "Media/models/Cabinet.FBX");

	TerrainInitializer terrainInit;
	terrainInit.worldMins = vec2<double>(0.0, 0.0);
	terrainInit.nPatchesX = DEBUG_TERRAIN_RES;				//96
	terrainInit.nPatchesZ = DEBUG_TERRAIN_RES;				//96
	terrainInit.nPixelsPerPatchX = DEBUG_TERRAIN_RES;		//64
	terrainInit.nPixelsPerPatchZ = DEBUG_TERRAIN_RES;		//64
	terrainInit.patchWidth = 1000.0f;
	terrainInit.patchDepth = 1000.0f;
	terrainInit.maxElevation = 4440.0f;				// 4440
	terrainInit.heightmapImagePath = "";
	terrainInit.heightmapImageFilename = "";
	terrainInit.entropy = 4.0;
	terrainInit.smoothing = 2.2;
	mTestTerrain = AssetManager::instance().load<ProceduralMeshAsset>("TestTerrain", ASSET_LOAD_MED_PRIO, terrainInit);
}

 
void SandboxGamestate::_loadEffects()
{
	std::string fxDir = "Media/effects";
	mEffectLoader = new CEffectLoader(fxDir.c_str());
}


void SandboxGamestate::onInitialize(const char** aArgs, size_t aNumArgs)
{
	GameEngine* ge = mEngineContext->engine;
	int32_t winWidth = ge->getWindowWidth();
	int32_t winHeight = ge->getWindowHeight();
	mViewportDimensions.set(static_cast<float>(winWidth), static_cast<float>(winHeight));

	mEngineContext->application->getApplicationWindow()->hideCursor();

	_loadEffects();
	_loadGeometry();
	_loadCamera(deg2Rad(65.0f), winWidth, winHeight);

	auto mColoredEffectAsset = AssetManager::instance().get<EffectAsset>("BlinnPhong");
	auto mColoredEffect = mColoredEffectAsset->getEffect();
	Effect* coloredEffect = mColoredEffect.get();

	auto mGBufferEffectAsset = AssetManager::instance().get<EffectAsset>("GBuffer");
	auto mGBufferEffect = mGBufferEffectAsset->getEffect();
	Effect* pGBufferEffect = mGBufferEffect.get();

	auto skyLightingEffectAsset = AssetManager::instance().get<EffectAsset>("SkyLightingEffect");
	auto skyLightingEffect = skyLightingEffectAsset->getEffect();
	Effect* pSkyLightingEffect = skyLightingEffect.get();

	auto PBREffectAsset = AssetManager::instance().get<EffectAsset>("PBRLighting");
	auto PBREffect = PBREffectAsset->getEffect();
	Effect* pPBREffect = PBREffect.get();

	auto HDREffectAsset = AssetManager::instance().get<EffectAsset>("HDR");
	auto HDREffect = HDREffectAsset->getEffect();
	Effect* pHDREffect = HDREffect.get();

	auto pTerrainLightingEffectAsset = AssetManager::instance().get<EffectAsset>("TerrainLighting");
	auto pTerrainLightingEffect = pTerrainLightingEffectAsset->getEffect();
	Effect* pTerrainLighting = pTerrainLightingEffect.get();

	auto pHBAOEffectAsset = AssetManager::instance().get<EffectAsset>("HBAO");
	auto pHBAOEffect = pHBAOEffectAsset->getEffect();
	Effect* pHBAO = pHBAOEffect.get();

	auto pFullscreenEffectAsset = AssetManager::instance().get<EffectAsset>("Fullscreen");
	auto pFullscreenEffect = pFullscreenEffectAsset->getEffect();
	Effect* pFullscreen = pFullscreenEffect.get();

	std::shared_ptr<MeshAsset> p = AssetManager::instance().get<MeshAsset>("TestMesh");
	_testEntity = EntityFactory::instance().CreateEntity("_testEntity");
	_testEntity->AddComponent<RenderComponent>(p->getMeshes());
	_testEntity->AddComponent<MaterialComponent>(pPBREffect, "ForwardPBRTechnique");
	_testEntity->AddComponent<MaterialComponent>(pGBufferEffect, "SlimGBuffer");
	_testEntity->transform->scale = vec3<float>(40.0f, 40.0f, 40.0f);
	_testEntity->transform->position = vec3<float>(50.0f, 0.0f, 0.0f);
	_testEntity->transform->rotation = vec3<float>(0.0f, deg2Rad(90.0f), 0.0f);
	QGfx::addEntityToTechniqueMap(_testEntity.get(), "ForwardPBRTechnique");
	QGfx::addEntityToTechniqueMap(_testEntity.get(), "SlimGBuffer");

	_testFullscreenEntity = EntityFactory::instance().CreateEntity("_testFullscreenEntity");
	_testFullscreenEntity->AddComponent<RenderComponent>(Render::getFullscreenMesh());
	_testFullscreenEntity->AddComponent<MaterialComponent>(pSkyLightingEffect, "SkyLighting");
	_testFullscreenEntity->AddComponent<MaterialComponent>(pSkyLightingEffect, "SkyLightingCubemap");
	_testFullscreenEntity->AddComponent<MaterialComponent>(pSkyLightingEffect, "SkyIrradianceCubemap");
	_testFullscreenEntity->AddComponent<MaterialComponent>(pHDREffect, "HDRTechnique");
	_testFullscreenEntity->AddComponent<MaterialComponent>(pPBREffect, "CTIntegrationTechnique");
	_testFullscreenEntity->AddComponent<MaterialComponent>(pHBAO, "HBAOTechnique");
	_testFullscreenEntity->AddComponent<MaterialComponent>(pHBAO, "HBAOBlurTechnique");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "SkyLighting");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "SkyLightingCubemap");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "SkyIrradianceCubemap");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "CTIntegrationTechnique");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "HDRTechnique");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "HBAOTechnique");
	QGfx::addEntityToTechniqueMap(_testFullscreenEntity.get(), "HBAOBlurTechnique");



	createObjectMaterials(pSkyLightingEffect);
	createObjectMaterials(pPBREffect);
	createObjectMaterials(pHDREffect);
	createObjectMaterials(pGBufferEffect);
	createObjectMaterials(pHBAO);

	createForwardRenderQueueNew();
	
	// Generate skybox cubemaps
	vec3<float> skyboxCenter(8000.0f, 1.0f, 8000.0f);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(1.0f, 0.0f, 0.0f), 512, 512, mCubemapCameras[0]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(-1.0f, 0.0f, 0.0f), 512, 512, mCubemapCameras[1]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, 0.0f, -1.0f), 512, 512, mCubemapCameras[2]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, 0.0f, 1.0f), 512, 512, mCubemapCameras[3]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, -1.0f, 0.0f), 512, 512, mCubemapCameras[4]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, 1.0f, 0.0f), 512, 512, mCubemapCameras[5]);

	// GUI must be initialized after all effets and object associations are made
	mEffectGui = new CEffectGui;
	mEffectGui->initialize(winWidth, winHeight);

	mEffectGui->addGeneralSlider("g_WalkSpeed", 1.0f, 100.0f, 1.0f);
	mEffectGui->addGeneralSlider("g_SprintSpeed", 100.0f, 5000.0f, 100.0f);
	mEffectGui->addGeneralSlider("solar_Latitude", -90.0f, 90.0f, 32.7767f);
	mEffectGui->addGeneralSlider("solar_Longitude", -180.0f, 180.0f, -96.7970f);
	mEffectGui->addGeneralSlider("solar_Day", 0.0f, 365.0f);
	mEffectGui->addGeneralSlider("solar_Hour", 0.0f, 23.0f, 12.0f);
	mEffectGui->addGeneralSlider("solar_Minute", 0.0f, 59.0f);
	mEffectGui->addGeneralSlider("solar_UTCOffset", -12.0f, 12.0f, -5);
	mEffectGui->addGeneralRadio("solar_UseSystemTime");


}

void SandboxGamestate::_loadCamera(float aFov, uint32_t aWinWidth, uint32_t aWinHeight)
{
	mCamera = new Camera();

	vec3<float> up(0.0f, 1.0f, 0.0f);
	vec3<float> pos(0.0f, 40.0f, -40.0f);
	vec3<float> lookAt;
	vec3<float> lookDir(0.0f, -1.0f, 1.0f);

	lookAt = pos + lookDir;

	mCamera->setPerspective(aFov, (float)aWinWidth / (float)aWinHeight, 0.05f, 80000.0f);
	mCamera->setCamera(pos, lookAt, up);
}


void SandboxGamestate::spawnPlayer(vec3<float> aPosition)
{

}

void SandboxGamestate::tick(const UpdateContext& aUpdateContext)
{

}

void SandboxGamestate::update(const UpdateContext& aUpdateContext)
{
	mTimer.start();

	float dt = static_cast<float>(aUpdateContext.elapsed);
	vec2<int32_t> mouse = InputManager::instance().getMousePositionFromCenter();


	// Update Camera pose //
	if(InputManager::instance().isCursorCentering())
	{
		auto pINI = mEngineContext->application->getApplicationINI();
		float sensitivity = pINI->getFloat("input", "sensitivity");

		mCamera->update(mouse.x, mouse.y, sensitivity);
	}

	vec2<int32_t> guiMouse = InputManager::instance().getMousePosition();
	mEffectGui->setMousePos(guiMouse);
	
	float wasd_speed;
	if(mIsSprinting)
		wasd_speed = mEffectGui->getGeneralSliderValue("g_SprintSpeed");
	else
		wasd_speed = mEffectGui->getGeneralSliderValue("g_WalkSpeed");

	if ((mCharacterDirection & Forward) == Forward)
		mCamera->moveForward(wasd_speed * dt);

	if ((mCharacterDirection & Backward) == Backward)
		mCamera->moveBack(wasd_speed * dt);

	if ((mCharacterDirection & Left) == Left)
		mCamera->moveLeft(wasd_speed * dt);

	if ((mCharacterDirection & Right) == Right)
		mCamera->moveRight(wasd_speed * dt);

	std::vector<std::string> changedFx = mEffectLoader->rescanEffects();

	double updateTime = mTimer.getElapsedMilliSec();
	mTimer.stop();

	mEffectGui->updateTimerStat("Update", updateTime);
	mSolar.Update();

	if(updateTime > maxUpdateDt)
		maxUpdateDt = updateTime;

	EntityFactory::instance().OnUpdate();

	mEffectGui->updateTimerStat("Max Update", maxUpdateDt);
}

void SandboxGamestate::render(const UpdateContext& aUpdateContext)
{
	mTimer.start();

	QGfx::beginRenderFrame();

	// Solar 
	float lat = mEffectGui->getGeneralSliderValue("solar_Latitude");
	float lon = mEffectGui->getGeneralSliderValue("solar_Longitude");
	float day = mEffectGui->getGeneralSliderValue("solar_Day");
	float hour = mEffectGui->getGeneralSliderValue("solar_Hour");
	float minute = mEffectGui->getGeneralSliderValue("solar_Minute");
	float utc = mEffectGui->getGeneralSliderValue("solar_UTCOffset");
	bool useSystemTime = mEffectGui->getGeneralRadioValue("solar_UseSystemTime");

	if(!useSystemTime)
	{
		mSolar.SetDay(day);
		mSolar.SetHour(hour);
		mSolar.SetMinute(minute);
	}
	mSolar.SetLatitude(lat);
	mSolar.SetLongitude(lon);
	mSolar.SetUTCOffset(utc);
	mSolar.UseSystemTime(useSystemTime);

	mEffectGui->updateTimerStat("Lat: ", lat);
	mEffectGui->updateTimerStat("Lon: ", lon);
	mEffectGui->updateTimerStat("Day: ", mSolar.GetDay());
	mEffectGui->updateTimerStat("Hour: ", mSolar.GetHour());
	mEffectGui->updateTimerStat("Minute: ", mSolar.GetMinute());
	mEffectGui->updateTimerStat("UTC: ", utc);
	mEffectGui->updateTimerStat("Use System Time: ", useSystemTime);

	vec3<float> testLightVec = mSolar.GetLightVector();
	testLightVec = testLightVec * -1.0f;


//	GLint vp[4];
	vec3<float> vLightVec, vLightColor, vSurfaceColor;
	float lightIntensity, lightAmbientIntensity;
	float rayleighBrightness, rayleighCollectionPower, rayleighStrength, rayleighDistribution;
	float mieDistribution, mieBrightness, mieCollectionPower, mieStrength;
	float scatterStrength, surfaceHeight;
	float middleGrey, brightnessThreshold, brightnessOffset, bloomScale, whitePoint, exposure;
	float gamma, noiseRepeat, adaptationFactor, finalGrey;
	float ao_radius, ao_bias, ao_exponent, ao_blurRadius, ao_blurSharpness;
	bindDefaultSurface();
	_clearBuffer(0, vec4<float>(0.0f, 0.0f, 0.0f, 1.0f));

	// Update Camera
	mCamera->render();
	mat4<float> viewMatrix = QGfx::getMatrix(VIEW);
	mat4<float> projectionMatrix = QGfx::getMatrix(PROJECTION);
	vec3<float> camPos = mCamera->getPosition();
	vec4<float> camPos4(camPos.x, camPos.y, camPos.z, 1.0);
	vec3<float> viewVec = mCamera->getDirection();
	vec4<float> viewVec4(viewVec.x, viewVec.y, viewVec.z, 0.0);
	float farClipDist = mCamera->getFarClip();
	float nearClipDist = mCamera->getNearClip();
	vec2<float> farClipDist2(nearClipDist, farClipDist);

	// Get frustum extents //
	float aspect = mCamera->getAspectRatio();
	float halfFOV = mCamera->getFov() / 2.0f;
	float farClip = mCamera->getFarClip();
	float halfHeight = farClip * tanf(halfFOV);
	float halfWidth = halfHeight * aspect;

	vec4<float> ul = vec4<float>(-halfWidth, halfHeight, -farClip, 0.0f);
	vec4<float> ur = vec4<float>(halfWidth, halfHeight, -farClip, 0.0f);
	vec4<float> ll = vec4<float>(-halfWidth, -halfHeight, -farClip, 0.0f);
	vec4<float> lr = vec4<float>(halfWidth, -halfHeight, -farClip, 0.0f);

	// Update terrain
	vec3<float> terrainPos = mTestTerrain->getTerrainPosition();
	vec2<float> terrainUVPatchWidth = mTestTerrain->getTerrainUVPatchWidth();
	vec2<float> terrainAspect = mTestTerrain->getTerrainAspect();
	vec2<float> terrainTexelSize = mTestTerrain->getTexelSize();
	float terrainMaxElevation = mTestTerrain->getMaxElevation();
	vec2<float> terrainSpan = mTestTerrain->getTerrainSpan();
	vec2<float> terrainSnowElevations = mTestTerrain->getTerrainSnowElevations();
	vec2<float> terrainMins = mTestTerrain->getTerrainMins();

	// Get GLOBAL GUI values 
	mEffectGui->getData(nullptr, "g_dirLight_LightVec", &vLightVec.x);
	mEffectGui->getData(nullptr, "g_dirLight_LightColor", &vLightColor.x);
	mEffectGui->getData(nullptr, "g_dirLight_LightIntensity", &lightIntensity);
	mEffectGui->getData(nullptr, "g_dirLight_LightAmbientIntensity", &lightAmbientIntensity);
	mEffectGui->getData(nullptr, "g_sky_rayleighBrightness", &rayleighBrightness);
	mEffectGui->getData(nullptr, "g_sky_rayleighCollectionPower", &rayleighCollectionPower);
	mEffectGui->getData(nullptr, "g_sky_rayleighStrength", &rayleighStrength);
	mEffectGui->getData(nullptr, "g_sky_mieDistribution", &mieDistribution);

	// TODO: WTF
	mEffectGui->getData(nullptr, "rayleighDistribution", &rayleighDistribution);
	mEffectGui->getData(nullptr, "g_sky_mieBrightness", &mieBrightness);
	mEffectGui->getData(nullptr, "g_sky_mieCollectionPower", &mieCollectionPower);
	mEffectGui->getData(nullptr, "g_sky_mieStrength", &mieStrength);
	mEffectGui->getData(nullptr, "g_sky_scatterStrength", &scatterStrength);
	mEffectGui->getData(nullptr, "g_sky_surfaceHeight", &surfaceHeight);
	mEffectGui->getData(nullptr, "g_terrain_noiseRepeat", &noiseRepeat);
	mEffectGui->getData(nullptr, "g_hdr_middleGrey", &middleGrey);
	mEffectGui->getData(nullptr, "g_hdr_brightnessThreshold", &brightnessThreshold);
	mEffectGui->getData(nullptr, "g_hdr_brightnessOffset", &brightnessOffset);
	mEffectGui->getData(nullptr, "g_hdr_bloomScale", &bloomScale);
	mEffectGui->getData(nullptr, "g_hdr_whitePoint", &whitePoint);
	mEffectGui->getData(nullptr, "g_hdr_exposure", &exposure);
	mEffectGui->getData(nullptr, "g_hdr_adaptationFactor", &adaptationFactor);
	mEffectGui->getData(nullptr, "g_hdr_finalGrey", &finalGrey);
	mEffectGui->getData(nullptr, "g_terrain_maxElevation", &terrainMaxElevation);
	mEffectGui->getData(nullptr, "g_terrain_snowElevations", &terrainSnowElevations);
	mEffectGui->getData(nullptr, "g_gamma", &gamma);
	mEffectGui->getData(nullptr, "g_ao_bias", &ao_bias);
	mEffectGui->getData(nullptr, "g_ao_exponent", &ao_exponent);
	mEffectGui->getData(nullptr, "g_ao_radius", &ao_radius);
	mEffectGui->getData(nullptr, "g_ao_blurRadius", &ao_blurRadius);
	mEffectGui->getData(nullptr, "g_ao_blurSharpness", &ao_blurSharpness);

	// Execute GBuffer pass on all geometry bound
	Technique* currentTechnique = nullptr;
	Pass* currentPass = nullptr;
	Shader* currentShader = nullptr;
	const Effect* currentEffect = nullptr;
	Material* currentUBO = nullptr;

	// GBuffer pass
	auto renderQueue = QGfx::getRenderQueue();
	size_t nRenderPasses = renderQueue.size();

	auto materialMap = QGfx::getObjectMaterialMap();
	mat4<float> worldMatrix, modelMatrix;

	double renderSetupTime = mTimer.getElapsedMilliSec();
	double renderTotal = renderSetupTime;
	mTimer.stop();
	mEffectGui->updateTimerStat("Render setup", renderSetupTime);

	// TEST /////////////////////////////////////
	auto RenderEntities = EntityFactory::instance().GetComponentsByType<RenderComponent>();
	auto MaterialEntities = EntityFactory::instance().GetComponentsByType<MaterialComponent>();
	std::map<std::string, std::vector<Component*>> techniqueComponentMap;
	for(auto component : MaterialEntities)
	{
		std::string techName = ((MaterialComponent*)component)->GetTechniqueName();
		techniqueComponentMap[techName].push_back(component);
	}

	while(renderQueue.size() > 0)
	{
		std::string currentTechniqueName = renderQueue.front();
	
		renderQueue.pop();
		std::vector<Entity*> entities = QGfx::getEntitiesByTechnique(currentTechniqueName);
		size_t nRenderObjects = entities.size();
		if(nRenderObjects <= 0)
			continue;

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		for(uint32_t renderObject = 0; renderObject < nRenderObjects; renderObject++)
		{
			Entity* thisRenderObject = entities[renderObject];
			MaterialComponent* thisMaterial = nullptr; 

			// loop over materials or do a find
			auto materialComponents = thisRenderObject->getComponents<MaterialComponent>();
			for(auto mat : materialComponents)
			{
				if(mat->GetTechniqueName().compare(currentTechniqueName) == 0)
				{
					thisMaterial = mat;
					break;
				}
			}
			if(!thisMaterial)
			{
				int jonathan = 0;
				continue;
			}

			std::string fxName = thisMaterial->GetEffectName();
			currentTechnique = thisMaterial->GetTechnique();
			std::string techName = currentTechniqueName;
			if(thisMaterial->GetTechniqueName().compare(currentTechniqueName) != 0)
				continue;

			uint32_t nPasses = thisMaterial->GetNumPasses();
			Pass* currentPass = nullptr;
			for(uint32_t pass = 0; pass < nPasses; pass++)
			{
				mTimer.start();

				thisMaterial->Bind(pass);
				currentPass = currentTechnique->getPass(pass);
				if(currentPass)
				{
					currentShader = currentPass->getShader();
					OutputBufferInfo* obi = currentPass->getOutputBuffer();

					int jonathan = 0;
				}

				std::string passName = thisMaterial->GetPassNameByIndex(pass);

				// Set and bind any UBOs
				vec3<float> white(1.0f, 1.0f, 1.0f);
				if (currentTechnique->hasUBO("CameraUniforms"))
				{
					currentUBO = currentTechnique->getMaterial("CameraUniforms");
					currentUBO->setUniform("g_cam_ViewMatrix", &viewMatrix);
					currentUBO->setUniform("g_cam_ProjectionMatrix", &projectionMatrix);
					currentUBO->setUniform("g_cam_ViewportDimensions", &mViewportDimensions);
					currentUBO->setUniform("g_cam_CamPosWorldSpace", &camPos4);
					currentUBO->setUniform("g_cam_ViewVecWorldSpace", &viewVec4);
					currentUBO->setUniform("g_cam_ClipDistances", &farClipDist2);

					currentUBO->setUniform("g_cam_frustumUL", &ul);
					currentUBO->setUniform("g_cam_frustumLL", &ll);
					currentUBO->setUniform("g_cam_frustumLR", &lr);
					currentUBO->setUniform("g_cam_frustumUR", &ur);

					currentUBO->bind(currentShader);
				}

				if(currentTechnique->hasUBO("AOUniforms"))
				{
					float projScale = mViewportDimensions.y / (tanf(halfFOV) * 2.0f);
					float radToScreen = ao_radius * 0.5f * projScale;

					currentUBO = currentTechnique->getMaterial("AOUniforms");
					currentUBO->setUniform("g_ao_radius", &ao_radius);
					currentUBO->setUniform("g_ao_bias", &ao_bias);
					currentUBO->setUniform("g_ao_exponent", &ao_exponent);
					currentUBO->setUniform("g_ao_radToScreen", &radToScreen);
					currentUBO->setUniform("g_ao_blurSharpness", &ao_blurSharpness);
					currentUBO->setUniform("g_ao_blurRadius", &ao_blurRadius);

					currentUBO->bind(currentShader);
				}

				if (currentTechnique->hasUBO("Constants"))
				{
					currentUBO = currentTechnique->getMaterial("Constants");
					currentUBO->setUniform("g_gamma", &gamma);
					currentUBO->setUniform("g_screenWidth", &mViewportDimensions.x);
					currentUBO->setUniform("g_screenHeight", &mViewportDimensions.y);
					currentUBO->bind(currentShader);
				}

				if (currentTechnique->hasUBO("TerrainUniforms"))
				{
					currentUBO = currentTechnique->getMaterial("TerrainUniforms");
					currentUBO->setUniform("g_terrain_worldPos", &terrainPos);
					currentUBO->setUniform("g_terrain_uvPatchWidth", &terrainUVPatchWidth);
					currentUBO->setUniform("g_terrain_aspect", &terrainAspect);
					currentUBO->setUniform("g_terrain_texelSize", &terrainTexelSize);
					currentUBO->setUniform("g_terrain_maxElevation", &terrainMaxElevation);
					currentUBO->setUniform("g_terrain_span", &terrainSpan);
					currentUBO->setUniform("g_terrain_snowElevations", &terrainSnowElevations);
					currentUBO->setUniform("g_terrain_noiseRepeat", &noiseRepeat);
					currentUBO->bind(currentShader);
				}

				if (currentTechnique->hasUBO("LightUniforms"))
				{
					currentUBO = currentTechnique->getMaterial("LightUniforms");
		//			currentUBO->setUniform("g_dirLight_LightVec", &vLightVec.x);
					currentUBO->setUniform("g_dirLight_LightVec", &testLightVec.x);
					currentUBO->setUniform("g_dirLight_LightColor", &vLightColor.x);
					currentUBO->setUniform("g_dirLight_LightIntensity", &lightIntensity);
					currentUBO->setUniform("g_dirLight_LightAmbientIntensity", &lightAmbientIntensity);
					currentUBO->bind(currentShader);
				}

				if (currentTechnique->hasUBO("SkyUniforms"))
				{
					currentUBO = currentTechnique->getMaterial("SkyUniforms");
					currentUBO->setUniform("g_sky_rayleighBrightness", &rayleighBrightness);
					currentUBO->setUniform("g_sky_rayleighCollectionPower", &rayleighCollectionPower);
					currentUBO->setUniform("g_sky_rayleighStrength", &rayleighStrength);
					currentUBO->setUniform("g_sky_mieDistribution", &mieDistribution);
					currentUBO->setUniform("g_sky_mieBrightness", &mieBrightness);
					currentUBO->setUniform("g_sky_mieCollectionPower", &mieCollectionPower);
					currentUBO->setUniform("g_sky_mieStrength", &mieStrength);
					currentUBO->setUniform("g_sky_scatterStrength", &scatterStrength);
					currentUBO->setUniform("g_sky_surfaceHeight", &surfaceHeight);
					currentUBO->bind(currentShader);
				}

				if (currentTechnique->hasUBO("CubemapCameraUniforms"))
				{
					vec2<float> shit(64.0f, 64.0f);
					currentUBO = currentTechnique->getMaterial("CubemapCameraUniforms");
					currentUBO->setUniform("g_cubemap_ProjectionMatrixRight", &mCubemapCameras[0].projMat);
					currentUBO->setUniform("g_cubemap_ProjectionMatrixLeft", &mCubemapCameras[1].projMat);
					currentUBO->setUniform("g_cubemap_ProjectionMatrixFront", &mCubemapCameras[2].projMat);
					currentUBO->setUniform("g_cubemap_ProjectionMatrixBack", &mCubemapCameras[3].projMat);
					currentUBO->setUniform("g_cubemap_ProjectionMatrixUp", &mCubemapCameras[4].projMat);
					currentUBO->setUniform("g_cubemap_ProjectionMatrixDown", &mCubemapCameras[5].projMat);

					currentUBO->setUniform("g_cubemap_ViewMatrixRight", &mCubemapCameras[0].viewMat);
					currentUBO->setUniform("g_cubemap_ViewMatrixLeft", &mCubemapCameras[1].viewMat);
					currentUBO->setUniform("g_cubemap_ViewMatrixFront", &mCubemapCameras[2].viewMat);
					currentUBO->setUniform("g_cubemap_ViewMatrixBack", &mCubemapCameras[3].viewMat);
					currentUBO->setUniform("g_cubemap_ViewMatrixUp", &mCubemapCameras[4].viewMat);
					currentUBO->setUniform("g_cubemap_ViewMatrixDown", &mCubemapCameras[5].viewMat);

					if(techName == "SkyIrradianceCubemap")
						currentUBO->setUniform("g_cubemap_ViewportDimensions", &shit);
					else
						currentUBO->setUniform("g_cubemap_ViewportDimensions", &mCubemapCameras[0].viewportDims);
					currentUBO->bind(currentShader);
				}

				if (currentTechnique->hasUBO("HDRUniforms"))
				{
					currentUBO = currentTechnique->getMaterial("HDRUniforms");

					currentUBO->setUniform("g_hdr_middleGrey", &middleGrey);
					currentUBO->setUniform("g_hdr_brightnessThreshold", &brightnessThreshold);
					currentUBO->setUniform("g_hdr_brightnessOffset", &brightnessOffset);
					currentUBO->setUniform("g_hdr_bloomScale", &bloomScale);
					currentUBO->setUniform("g_hdr_whitePoint", &whitePoint);
					currentUBO->setUniform("g_hdr_exposure", &exposure);
					currentUBO->setUniform("g_hdr_adaptationFactor", &adaptationFactor);
					currentUBO->setUniform("g_hdr_finalGrey", &finalGrey);
					currentUBO->bind(currentShader);
				}

				// TOTAL HACK! MOVE THIS!!!
				if(techName == "BlinnPhongDeferred")
				{
					GLuint mGBuffer0 = QGfx::getRenderTarget("GBufferFBO0");
					GLuint mGBuffer1 = QGfx::getRenderTarget("GBufferFBO1");
					GLuint mGBuffer2 = QGfx::getRenderTarget("GBufferFBO2");
					GLuint mGBuffer3 = QGfx::getRenderTarget("GBufferFBO3");
					currentShader->setTexture("GBufferFBO0", mGBuffer0);
					currentShader->setTexture("GBufferFBO1", mGBuffer1);
					currentShader->setTexture("GBufferFBO2", mGBuffer2);
					currentShader->setTexture("GBufferFBO3", mGBuffer3);
				}

				if(techName == "CTIntegrationTechnique")
				{
					vec2<float> dims(64.0f, 64.0f);
					currentShader->setUniform2f("textureDims", dims);
				}

				if(techName == "GBufferTerrain" || techName == "SlimTerrainGBuffer" || 
				techName == "ForwardTerrainLightingTechnique")

				{
					currentShader->setTextureBuffer("patchInstanceBuffer", mTestTerrain->getInstanceTextureHandle());
					currentShader->setTexture("heightmap", mTestTerrain->getHeightmapTexId());
				}
			
				if(techName == "HDRTechnique")
				{
					vec2<float> avSamples[16];
					vec2<float> sampOffsets[16];
					vec4<float> sampWeights[16];
					float fSampleOffsets[16];

					if(passName == "downScale4Pass")
					{
						uint32_t hdrWidth = (uint32_t)1440;
						uint32_t hdrHeight = (uint32_t)900;
						get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
						currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
					}

					if(passName == "sampleLuminancePass")
					{
						uint32_t hdrWidth = (uint32_t)360;
						uint32_t hdrHeight = (uint32_t)225;
						get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
						currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
					}

					if(passName == "resampleLuminancePass1")
					{
						uint32_t hdrWidth = (uint32_t)64;
						uint32_t hdrHeight = (uint32_t)64;
						get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
						currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
					}

					if(passName == "resampleLuminancePass2")
					{
						uint32_t hdrWidth = (uint32_t)16;
						uint32_t hdrHeight = (uint32_t)16;
						get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
						currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
					}

					if(passName == "finalLuminancePass")
					{
						uint32_t hdrWidth = (uint32_t)4;
						uint32_t hdrHeight = (uint32_t)4;
						get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
						currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
					}

					if(passName == "gaussPass")
					{
						uint32_t brightSurfWidth = 720;
						uint32_t brightSurfHeight = 450;
						get5x5GaussianOffsets(brightSurfWidth, brightSurfHeight, sampOffsets, sampWeights, 1.0F);
						currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
						currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
					}

					if(passName == "downScale2Pass")
					{
						uint32_t brightSurfWidth = 180;
						uint32_t brightSurfHeight = 112;
						get2x2SampleOffsets(brightSurfWidth, brightSurfHeight, sampOffsets);
						currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
					}

					if(passName == "gaussPass2")
					{
						uint32_t bloomWidth = 180;
						uint32_t bloomHeight = 112;
						get5x5GaussianOffsets(bloomWidth, bloomHeight, sampOffsets, sampWeights, 1.0F);
						currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
						currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
					}

					if(passName == "bloomPass")
					{
						uint32_t tempBloomWidth2 = 180;
						getBloomOffsets(tempBloomWidth2, fSampleOffsets, sampWeights, 3.0F, 2.0F);

						for (int i = 0; i < 16; ++i)
						{
							sampOffsets[i].x = fSampleOffsets[i];
							sampOffsets[i].y = 0.0F;
						}

						currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
						currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
					}

					if(passName == "bloomPass2")
					{
						uint32_t tempBloomHeight1 = 112;
						getBloomOffsets(tempBloomHeight1, fSampleOffsets, sampWeights, 3.0F, 2.0F);
						for (int i = 0; i < 16; ++i)
						{
							sampOffsets[i].x = 0.0F;
							sampOffsets[i].y = fSampleOffsets[i];
						}

						currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
						currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
					}
				}

				auto test = currentPass->getSamplerLoadPaths();
				size_t nTextures = test.size();
				if (nTextures > 0)
				{
					if(techName == "ForwardPBRTechnique")
						int jonathan = 0;

					for (auto texName : test)
					{
						std::string texPath = texName.second.second;
						std::string name = texName.first;
						if (texName.second.first == EffectVariableType::SAMPLER2D)
						{
							auto texAsset = AssetManager::instance().get<TextureAsset>(name);

							// Is hard texture asset 
							if(texAsset)
							{
								if(texAsset->getTextureID() > 0)
									currentShader->setTexture(name, texAsset->getTextureID());
							}
							
							// Is Render buffer
							else
							{
								if(texPath != "")
								{
									GLuint id = QGfx::getRenderTarget(texPath);
									if(id > 0)
										currentShader->setTexture(name, id);
								}
							}
						}

						else if (texName.second.first == EffectVariableType::SAMPLERCUBE)
						{
							auto texAsset = AssetManager::instance().get<CubemapAsset>(name);
							currentShader->setCubemapTexture(name, texAsset->getTexture());
						}
					}
				}

				// loop over all renderable objects and render
				for(size_t i = 0; i < nRenderObjects; i++)
				{
					// Validate has proper components
					Entity* curRenderObj = entities[i];
					if(!curRenderObj)
						continue;

					float objID = 0.0f;

					//TODO: Make sure world matrix is updated
					worldMatrix = curRenderObj->transform->mMatrix;

					RenderComponent* renderComponent = curRenderObj->getComponent<RenderComponent>();
					auto meshes = renderComponent->GetMeshes();
					size_t nMeshes = meshes.size();

					if(nMeshes <= 0)
						break;

					
//					bool isGuiSelected = ro->isGuiSelected() && mEffectGui->isVisible();
					bool isGuiSelected = mEffectGui->isVisible();
					float guiSelected = 0.0f;
					if(isGuiSelected)
						guiSelected = 1.0f;

					
					for(size_t j = 0; j < nMeshes; j++)
					{
						std::shared_ptr<MeshNode> thisMesh = meshes[j];
						MeshNodeMaterial pbrMaterials = thisMesh->meshMaterial;
						
						std::string objMatName = techName + std::string("+") + passName;
						ObjectMaterial* curObjMaterial = QGfx::getObjectMaterial(objMatName.c_str());
						
						modelMatrix = thisMesh->transform;
						modelMatrix.transpose();
						modelMatrix = worldMatrix * modelMatrix;
						
						curObjMaterial->setUniformMat4("ModelMatrix", modelMatrix);
						curObjMaterial->setUniform1f("isGuiSelected", guiSelected);
						curObjMaterial->setUniform3f("surfaceColor", vSurfaceColor);
						curObjMaterial->setUniform1f("RenderObjectID", objID);
						curObjMaterial->bind();
						
						
						if(techName == "ForwardPBRTechnique" || techName == "GBufferPBR")
						{
							SetPBRMaterials(curObjMaterial, techName.c_str(), "p0", pbrMaterials);
						}
	
						uint32_t primType = thisMesh->mesh->getIBO()->getPrimType();

						thisMesh->mesh->bind();
						uint32_t nInstances = thisMesh->mesh->getNumMeshInstances();
						if(nInstances <= 1)
							QGfx::GL::gl_DrawElements(primType, thisMesh->mesh->getIndexCount(), GL_UNSIGNED_INT, 0);
						else
							QGfx::GL::gl_DrawElementsInstanced(primType, thisMesh->mesh->getIndexCount(), 
															GL_UNSIGNED_INT, 0, nInstances);
						thisMesh->mesh->unbind();

						curObjMaterial->unbind();
					
					}
				}
			}
			/////////////////////////////////////////////////////////////////////////////////////////////////////////

			currentPass->unbind();

			double passTime = mTimer.getElapsedMilliSec();
			renderTotal += passTime;
			mTimer.stop();

//			std::string str(techName);
//			str.append(" + ");
//			str.append(passName);
//			mEffectGui->updateTimerStat(str, passTime);
		}
	}
	
	bindDefaultSurface();

	// If debug surface is selected, paint over
	std::string selectedFB = mEffectGui->getSelectedFB();
	if(selectedFB.size() > 0)
	{
		GLuint fb = QGfx::getRenderTarget(selectedFB);

		auto fx = AssetManager::instance().get<EffectAsset>("Fullscreen");
		auto effect = fx->getEffect();

		Technique* tech = effect->getTechnique("FullscreenNoBlendTechnique");
		Pass* pass = tech->getPass("p0");
		Shader* shader = pass->getShader();

		pass->bind();
		shader->setTexture("tex0", fb);
		Render::renderFullscreenTexturedQuad();

		pass->unbind();
	}

	mTimer.start();

	// render IMGUI //
	int mouseX;
	int mouseY;
	mEngineContext->application->getMousePosition(mouseX, mouseY);
	mEffectGui->setMousePos({ mouseX, mouseY });
	mEffectGui->begin();
	if(mEffectGui->isVisible())
	{
		mEffectGui->update(aUpdateContext.elapsed);	
		mEffectGui->updateSelected(aUpdateContext.elapsed, static_cast<uint32_t>(selectedObj));
	}

	EffectGuiStats stats;
	stats.camPos = mCamera->getPosition();
	stats.dt = aUpdateContext.elapsed;
	stats.selectedObjectID = selectedObj;
	if(mEffectGui->areStatsVisible())
		mEffectGui->updateStats(stats);

	mEffectGui->end();
	mEffectGui->render();

	double guiTime = mTimer.getElapsedMilliSec();
	renderTotal += guiTime;
	mTimer.stop();
	mEffectGui->updateTimerStat("Gui Time", guiTime);
	mEffectGui->updateTimerStat("Render total", renderTotal);

	frameCount++;
	avgTime += renderTotal;

	if (frameCount > 100)
	{
		lastAvg = avgTime / (double)frameCount;
		frameCount = 0;
		avgTime = 0.0;
	}
	mEffectGui->updateTimerStat("Average FPS (100)", lastAvg);

	QGfx::resetBufferClearFlags();
//	mClearedBuffers.clear();

	QGfx::endRenderFrame();
}

void SandboxGamestate::onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods)
{
	if (aKey == VKEY_TAB && aAction == EKeyAction::PRESS)
	{
		bool isCursorCentered = InputManager::instance().isCursorCentering();

		InputManager::instance().setCenterCursor(!isCursorCentered);
		isCursorCentered = !isCursorCentered;

		if (isCursorCentered)
		{
			mEngineContext->application->getApplicationWindow()->hideCursor();
		}
			
		else
		{
			mEngineContext->application->getApplicationWindow()->showCursor();
		}

		mEffectGui->toggleVisibility();

		return;
	}

	if (aKey == VKEY_ESCAPE && aAction == EKeyAction::PRESS)
	{
		mEngineContext->engine->quit();
		return;
	}

	if (aKey == VKEY_SHIFT && aAction == EKeyAction::PRESS)
		mIsSprinting |= 1;

	if(aKey == VKEY_SHIFT && aAction == EKeyAction::RELEASE)
		mIsSprinting ^= 1;
		
	if ((aKey == 'W' || aKey == 'w') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Forward;

	if ((aKey == 'W' || aKey == 'w') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Forward;

	if ((aKey == 'S' || aKey == 's') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Backward;

	if ((aKey == 'S' || aKey == 's') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Backward;

	if ((aKey == 'A' || aKey == 'a') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Left;

	if ((aKey == 'A' || aKey == 'a') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Left;

	if ((aKey == 'D' || aKey == 'd') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Right;

	if ((aKey == 'D' || aKey == 'd') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Right;

	if ((aKey == 'Z' || aKey == 'z') && aAction == EKeyAction::PRESS)
	{
		mEffectGui->toggleStatsVisibility();
	}

//	if((aKey == 'F' || aKey == 'f') && aAction == EKeyAction::PRESS)
//	{
//		mEffectGui->setRTDebugVisibility(true);
//	}

	mEffectGui->keyEvent(aKey, static_cast<int>(aAction));
}

void SandboxGamestate::onCharEvent(const int aCodepoint)
{

}

void SandboxGamestate::onGainedFocus()
{

}

void SandboxGamestate::onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction)
{
	mEffectGui->mouseEvent(static_cast<int>(aButton), static_cast<int>(aAction));

	if (aAction == EMouseButtonAction::PRESS)
	{
		// Get GBuffer FBO and pass to GUI for object picking if GUI is visible.
		FboId fbo = QGfx::getRenderBuffer("GBufferFBO");
		vec2<int32_t> mousePos = InputManager::instance().getMousePosition();

		if (mEffectGui->isVisible() && !mEffectGui->isMouseHovering(mousePos))
		{
			selectedObj = mEffectGui->getSelectedObject(fbo, mousePos);
			if(selectedObj != lastSelectedObj)
			{
				std::shared_ptr<RenderObject> ro = QGfx::getRenderObject(static_cast<uint32_t>(selectedObj));
				if(ro)
					ro->setGuiSelected(true);

				ro = QGfx::getRenderObject(static_cast<uint32_t>(lastSelectedObj));
				if(ro)
					ro->setGuiSelected(false);

				lastSelectedObj = selectedObj;
			}
		}
	}
}

void SandboxGamestate::onMouseMove(const double aX, const double aY)
{

}

void SandboxGamestate::onMouseWheelScroll(const double aDeltaX, const double aDeltaY)
{

}
