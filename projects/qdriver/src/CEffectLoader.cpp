#include "CEffectLoader.h"

#if defined(_WIN32) || defined(_WIN64)
	#include <Windows.h>
	#include <filesystem>
#endif

#if !defined(_WIN32) || !defined(_WIN64)
	#include <sys/types.h>
	#include <sys/stat.h>
#endif

#include "io/FileSystem.h"
#include "assets/AssetManager.h"
#include "assets/EffectAsset.h"
#include "assets/CubemapAsset.h"
#include "assets/TextureAsset.h"
#include "core/Timer.h"
#include "core/QLog.h"
#include "StringUtils.h"
#include "render/QGfx.h"


CEffectLoader::CEffectLoader(const char* aScanDir)
{
	std::string effectDir = aScanDir;
	std::string scanDir = effectDir;
	#if defined(_WIN32) || defined(_WIN64)
		scanDir += "/*";
	#endif
    std::vector<std::string> fxFiles = FileSystem::getFiles(scanDir);   //    + "/*"
	size_t nFiles = fxFiles.size();
	for(size_t i = 0; i < nFiles; i++)
	{
		size_t nameStart;
		#if defined(_WIN32) || defined(_WIN64)
			nameStart = fxFiles[i].find_last_of('\\') + 1;
		#else
			nameStart = fxFiles[i].find_last_of('/') + 1;
		#endif
		size_t nameEnd = fxFiles[i].find_last_of('.');
		size_t nChars = nameEnd - nameStart;
		std::string fxName = fxFiles[i].substr(nameStart, nChars);
		
		if (StringUtils::endsWith(fxFiles[i], "glslh"))
			continue;

		std::shared_ptr<EffectAsset> fxAsset = nullptr;
		std::string loadPath;
		std::string loadDir;
		#if defined(_WIN32) || defined(_WIN64)
			loadDir = effectDir + "/";
			loadPath = effectDir + "/" + fxFiles[i];
		#else
			loadDir = "";
			loadPath = fxFiles[i];
		#endif
		fxAsset = AssetManager::instance().load<EffectAsset>(fxName, ASSET_LOAD_MED_PRIO, loadPath);

		if(!fxAsset)
			continue;

		std::shared_ptr<Effect> fx = fxAsset->getEffect();

		if(!fx)
			continue;

		for(auto buffer : fx->getOutputBuffers())
		{
			// Load render buffers //
			OutputBufferInfo* obi = buffer;
			if (!obi)
				continue;

			char bufName[128];
			strcpy(bufName, obi->name.c_str());
			size_t len = strlen(bufName);
			for (size_t i = 0; i < len; i++)
				bufName[i] = tolower(bufName[i]);

			// Load cubemaps separately via asset manager
			if (obi->colorBuffers != 6)
				QGfx::createRenderBuffer(obi);
			else
			{
				AssetManager::instance().load<CubemapAsset>(obi->name, ASSET_LOAD_MED_PRIO, obi->width, obi->height, obi->colorFormat);
			}
		}

		std::vector<Technique*> techs = fx->getTechniques();
		size_t nTechs = techs.size();
		for (size_t j = 0; j < nTechs; j++)
		{
			Pass** passes = techs[j]->getPasses();
			size_t nPasses = techs[j]->getNumPasses();
			std::string thisTechName = techs[j]->getName();
			for (size_t c = 0; c < nPasses; c++)
			{
				std::string thisPassName = passes[c]->getName();	

				// Load textures //
				auto samplerPaths = passes[c]->getSamplerLoadPaths();   // <uniName, <type, path>>
				for (auto sampler : samplerPaths)
				{
					size_t loadPathLen = sampler.second.second.length();
					if (sampler.second.first == EffectVariableType::SAMPLER2D)
					{
						// ONLY load samplers into asset manager that are physical files on disk
						// Otherwise soft samplers (like local buffers) are attempted to be loaded by asset manager
						if(sampler.second.second.length() > 0 && sampler.second.second.find(".") != std::string::npos)
							AssetManager::instance().load<TextureAsset>(sampler.first, ASSET_LOAD_MED_PRIO, sampler.second.second);
					}
				}
			}
		}

		#if defined(_WIN32) || defined(_WIN64)
			HANDLE fHandle = NULL;
			//fHandle = CreateFileA(fxFiles[i].c_str(),
			//					  GENERIC_READ,
			//					  0,
			//					  NULL,
			//					  OPEN_EXISTING,
			//					  FILE_ATTRIBUTE_NORMAL,
			//					  NULL);
			
			fHandle = CreateFileA(loadPath.c_str(),
								  GENERIC_READ,
								  0, 
								  NULL, 
								  OPEN_EXISTING,
								  FILE_ATTRIBUTE_NORMAL,
								  NULL);

			FILETIME creationTime;
			FILETIME lastAccessTime;
			FILETIME lastWriteTime;
			SYSTEMTIME sysTime;
			GetFileTime(fHandle, &creationTime, &lastAccessTime, &lastWriteTime);
			FileTimeToSystemTime(&lastWriteTime, &sysTime);
			CloseHandle(fHandle);

			EffectFileTime fxTime;
			fxTime.day = sysTime.wDay;
			fxTime.hour = sysTime.wHour;
			fxTime.minute = sysTime.wMinute;
			fxTime.month = sysTime.wMonth;
			fxTime.second = sysTime.wSecond;
			fxTime.year = sysTime.wYear;
			mEffectFileTimeMap[fxFiles[i]] = fxTime;
		#else
			struct stat res;
			stat(fxFiles[i].c_str(), &res);
			int32_t lastTime = res.st_atim.tv_sec;
			EffectFileTime fxTime;
			fxTime.time = lastTime;
			mEffectFileTimeMap[fxFiles[i]] = fxTime;
		#endif
	}
}

CEffectLoader::~CEffectLoader()
{

}


std::vector<std::string> CEffectLoader::rescanEffects()
{
	std::vector<std::string> changedEffects;
	std::vector<std::string> fxFiles = FileSystem::getFiles("Media/effects/*");
	size_t nFiles = fxFiles.size();

	for (size_t i = 0; i < nFiles; i++)
	{
		#if defined (_WIN32) || defined(_WIN64)
			DWORD err = 0;
			SYSTEMTIME sysTime;
			FILETIME creationTime;
			FILETIME lastAccessTime;
			FILETIME lastWriteTime;

		// We must sit and spin to wait until the text editor has released 
		// the file for writing
		do
		{
			HANDLE fHandle = NULL;
			std::string fileName = "Media/effects/" + fxFiles[i];
			fHandle = CreateFileA(fileName.c_str(),
								  GENERIC_READ,
								  0,
								  NULL,
								  OPEN_EXISTING,
								  FILE_ATTRIBUTE_NORMAL,
								  NULL);


			err = GetLastError();

			GetFileTime(fHandle, &creationTime, &lastAccessTime, &lastWriteTime);
			FileTimeToSystemTime(&lastWriteTime, &sysTime);
			CloseHandle(fHandle);
		}while(err != 0);
	


			auto fx = mEffectFileTimeMap.find(fxFiles[i]);
			if(fx != mEffectFileTimeMap.end())
			{
				if(fx->second.day != sysTime.wDay || fx->second.hour != sysTime.wHour ||
				   fx->second.minute != sysTime.wMinute || fx->second.month != sysTime.wMonth ||
				   fx->second.second != sysTime.wSecond || fx->second.year != sysTime.wYear)
				{
					size_t nameStart = fxFiles[i].find_last_of('\\') + 1;
					size_t nameEnd = fxFiles[i].find_last_of('.');
					size_t nChars = nameEnd - nameStart;
					std::string fxName = fxFiles[i].substr(nameStart, nChars);

					std::shared_ptr<EffectAsset> fxAsset = nullptr;
					std::string loadPath = "Media/effects/" + fxFiles[i];
//					fxAsset = AssetManager::instance().load<EffectAsset>(fxName, ASSET_LOAD_MED_PRIO, loadPath);
					AssetManager::instance().reload<EffectAsset>(fxName);

					EffectFileTime fxTime;
					fxTime.day = sysTime.wDay;
					fxTime.hour = sysTime.wHour;
					fxTime.minute = sysTime.wMinute;
					fxTime.month = sysTime.wMonth;
					fxTime.second = sysTime.wSecond;
					fxTime.year = sysTime.wYear;
					mEffectFileTimeMap[fxFiles[i]] = fxTime;

					changedEffects.push_back(fxName);
				}
			}
		#else
			// We must sit and spin to wait until the text editor has released 
			// the file for writing
			int fd;
			int32_t lastTime = 0;
			struct stat res;
			stat(fxFiles[i].c_str(), &res);
			lastTime = res.st_atim.tv_sec;

			auto fx = mEffectFileTimeMap.find(fxFiles[i]);
			if(fx != mEffectFileTimeMap.end())
			{
				if(fx->second.time != lastTime)
				{
					size_t nameStart;
					#if defined(_WIN32) || defined(_WIN64)
					 	nameStart = fxFiles[i].find_last_of('\\') + 1;
					#else
						nameStart = fxFiles[i].find_last_of('/') + 1;
					#endif

					size_t nameEnd = fxFiles[i].find_last_of('.');
					size_t nChars = nameEnd - nameStart;
					std::string fxName = fxFiles[i].substr(nameStart, nChars);

					std::shared_ptr<EffectAsset> fxAsset = nullptr;
					fxAsset = AssetManager::instance().load<EffectAsset>(fxName, ASSET_LOAD_MED_PRIO, fxFiles[i]);

					EffectFileTime fxTime;
					fxTime.time = lastTime;
					mEffectFileTimeMap[fxFiles[i]] = fxTime;

					changedEffects.push_back(fxName);
				}
			}
		#endif
	}	

	return changedEffects;
}