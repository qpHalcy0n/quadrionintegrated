#define _CRTDBG_MAP_ALLOC
#include <cstdlib>  

#if defined(_WIN32) || defined(_WIN64)
#include <crtdbg.h> 
#include <windows.h>
#include <string>
#endif

#include "GameEngine.h"
#include "SandboxGamestate.h"



////////////////////////////////////////////////////////////////////////////////////
// WinMain 
#if defined(_WIN32) || defined(_WIN64)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	GameEngine gameEngine = GameEngine("Media/config.ini");

	std::string mode("");
	auto pINI = gameEngine.getApplicationINI();

	if (pINI)
		mode = pINI->getString("game", "mode", "");

	if (mode.find("sandbox") != std::string::npos)
	{
		auto newState = gameEngine.createGameState<SandboxGamestate>();
		gameEngine.pushGameState(newState);
	}

	gameEngine.gameLoop();

	return 0;
}
#else

int main()
{

	GameEngine gameEngine = GameEngine("Media/config.ini");

	std::string mode("");
	auto pINI = gameEngine.getApplicationINI();

	if (pINI)
		mode = pINI->getString("game", "mode", "");

	if (mode.find("sandbox") != std::string::npos)
	{
		auto newState = gameEngine.createGameState<SandboxGamestate>();
		gameEngine.pushGameState(newState);
	}

	gameEngine.gameLoop();

	return 0;
}

#endif
