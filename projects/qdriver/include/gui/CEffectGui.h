#ifndef __CEFFECTGUI_H_
#define __CEFFECTGUI_H_


#include <vector>
#include <map>
#include <string>
#include "QMath.h"
#include "imgui.h"
#include "gui/CGuiMemory.h"
#include "render/QGfx.h"


struct GeneralGuiEntry
{
	std::string name;
	void*		data;
	uint32_t	nBytes;
	float		minVal;
	float		maxVal;
};

struct EffectGuiStats
{
	double	dt;
	vec3<float> camPos;
	float selectedObjectID;
};


class CEffectGui
{
	public:
	
		CEffectGui();
		~CEffectGui();
	
		void update(const double aDt);
		void updateStats(const EffectGuiStats& aStats);
		void updateSelected(const double aDt, uint32_t aRenderObjId);
		void render();
		void initialize(const int aFbWidth, const int aFbHeight);

		void begin();
		void end();

		// General entries
		bool addGeneralGuiEntry(const GeneralGuiEntry& aEntry);
		void* getGeneralGuiEntry(std::string aName);

		bool addGeneralSlider(const std::string& aName, float min, float max, float aDefault = 0.0f);
		float getGeneralSliderValue(const std::string& aName);
		bool addGeneralRadio(const std::string& aName);
		bool getGeneralRadioValue(const std::string& aName);

		// Stats entries
		void updateTimerStat(const std::string& aName, double aDt);

	
		void mouseEvent(const int aButton, const int aAction);
		void keyEvent(const int aKey, const int aAction);
		void setMousePos(const vec2<int32_t> aMousePos);
	
		void getData(const char* aMaterialName, const char* aUniformName, void* aData);
	
		void toggleVisibility();
		void setVisibility(const bool aVisibility);
		inline bool isVisible() const
		{
			return mIsVisible;
		}

		void toggleStatsVisibility();
		void setStatsVisibility(const bool aVisibility);
		inline bool areStatsVisible() const
		{
			return mAreStatsVisible;
		}

//		void setRTDebugVisibility(const bool aVisibility);

		bool isMouseHovering(const vec2<int32_t>& aMousePos);

		float getSelectedObject(FboId gbuffer, vec2<int32_t> mousePos);

		std::string getSelectedFB();

	private:

		bool	mIsGeneralEntriesVisible;
		bool	mIsEffectDebugVisible;
		bool	mIsRTWindowVisible;

		int32_t mFbWidth;
		int32_t mFbHeight;
	
		bool    mIsVisible;
		bool    mIsInitialized;
		bool    checkInit();
		bool    mIsTestWindowVisible;
		bool	mAreStatsVisible;
//		bool	mIsRTDebugVisible;
	
		std::map<std::string, bool> mGeneralRadios;

		std::map<std::string, void*> mGeneralGuiMemory;
		std::map<std::string, std::pair<float, float>> mGeneralMinMax;
		std::map<std::string, GeneralGuiEntry> mGeneralGuiEntries;
		std::vector<std::string> mEffectNames;
//		std::map<std::string, double> mTimerStats;
		std::vector<std::pair<std::string, double>> mTimerStats;

		CGuiMemory* mEffectMemory;
	
		std::string mSelectedEffect;
		std::string mSelectedMaterial;
		std::string mSelectedFB;
	
		char mLastMaterialName[64];

		vec2<float> mWindowPos;
		vec2<float> mWindowSize;
		vec2<float> mSelectedWindowPos;
		vec2<float> mSelectedWindowSize;
};



#endif

