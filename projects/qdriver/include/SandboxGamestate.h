#ifndef __SANDBOXGAMESTATE_H_
#define __SANDBOXGAMESTATE_H_


#include <memory>
#include <list>
#include "EngineExport.h"
#include "GameState.h"
#include "entities/EntityFactory.h"
#include "render/Camera.h"
#include "assets/EffectAsset.h"
#include "render/VertexArray.h"
#include "assets/MeshAsset.h"
#include "assets/ProceduralMeshAsset.h"
#include "gui/CEffectGui.h"
#include "render/Material.h"
#include "CEffectLoader.h"
#include "render/QGfx.h"
#include "render/CubemapCamera.h"
#include "core/Timer.h"
#include "Solar.h"



class SandboxGamestate : public GameState
{
	public:
		SandboxGamestate();
		~SandboxGamestate();
	
		virtual void onInitialize(const char** aArgs, size_t aNumArgs) override;
		virtual void onGainedFocus() override;
		virtual void tick(const UpdateContext& aUpdateContext) override;
		virtual void update(const UpdateContext& aUpdateContext) override;
		virtual void render(const UpdateContext& aUpdateContext) override;
	
		virtual void onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods) override;
		virtual void onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction) override;
		virtual void onMouseMove(const double aX, const double aY) override;
		virtual void onMouseWheelScroll(const double aDeltaX, const double aDeltaY) override;
		virtual void onCharEvent(const int aCodepoint) override;
	
		void initCamera(float aFov, uint32_t aWinWidth, uint32_t aWinHeight);
		void spawnPlayer(vec3<float> aPosition);
	
	private:

		bool		_isBufferCleared(uint32_t aId);
		void		_clearBuffer(uint32_t aId, vec4<float> aClearColor);
		void		_loadEffects();
		void		_loadGeometry();
		void		_loadCamera(float aFov, uint32_t aWinWidth, uint32_t aWinHeight);

//		std::shared_ptr<Effect>		mColoredEffect;
		std::shared_ptr<MeshAsset>					mTestMesh;
		std::shared_ptr<ProceduralMeshAsset>		mTestTerrain;

		VertexArray*				mSampleVAO;

		Camera*						mCamera;
		uint8_t						mCharacterDirection;
		uint8_t						mIsSprinting;

		CEffectGui*					mEffectGui;
		CEffectLoader*				mEffectLoader;

//		EntityFactory*				mEntityFactory;

		ObjectMaterial* mObjectMaterial1;
		ObjectMaterial* mObjectMaterial2;
		ObjectMaterial* mObjectMaterial3;

		std::list<ObjectMaterial*> mMaterialList;
		std::vector<uint32_t> mClearedBuffers;

		GLuint mGBuffer0, mGBuffer1, mGBuffer2, mGBuffer3;
		DepthStencilId mGBufferDS;
		FboId mGBuffer;

		vec2<float> mViewportDimensions;

		CubemapCamera mCubemapCameras[6];

		Timer	mTimer;
		Solar	mSolar;

		std::shared_ptr<Entity> _testEntity;
		std::shared_ptr<Entity> _testFullscreenEntity;
};




#endif
