#ifndef __CEFFECTLOADER_H_
#define __CEFFECTLOADER_H_

#include <string>
#include <vector>
#include <map>

#if defined(_WIN32) || defined(_WIN64)
struct EffectFileTime
{
    uint16_t    month;
    uint16_t    year;
    uint16_t    day;
    uint16_t    minute;
    uint16_t    second;
    uint16_t    hour;
};
#else
struct EffectFileTime
{
    int32_t time;
};
#endif

class CEffectLoader
{
    public:

        CEffectLoader(const char* aScanDir);
        ~CEffectLoader();

//        std::vector<CEffectGuiWindow*>& getEffectWindows();
//        CEffectGuiWindow* getEffectWindow(const char* aEffectName);

//        const inline size_t getNumEffectWindows()
//        {
//            return mEffectWindows.size();
//        }

        std::vector<std::string> rescanEffects();

//        void* getDataStruct(fxName, uniName)

    private:

        std::map<std::string, EffectFileTime> mEffectFileTimeMap;
//        std::vector<CEffectGuiWindow*> mEffectWindows;
        std::map<std::string, std::map<std::string, void*>> mUniformMap;
        
};

#endif